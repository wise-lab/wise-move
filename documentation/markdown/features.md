<a href=README.md><img src="documentation/markdown/home_button.png" height="30" /></a><br><br>

Features
========

**WiseMove** supports options-based reinforcement learning, which allows the motion planning problem to be decomposed into high- and low-level planning. An option is an elementary manoeuvre, using a low-level policy that defines what it does. A high-level policy chooses which option to apply in a given situation.


### Verification

Both high- and low-level policies are trained using runtime verification, with respect to user-defined linear temporal logic (LTL) constraints. These can apply to all options or can be option-specific.

### Monte Carlo Tree Search

Constraining the learning process with LTL does not guarantee that the resulting policies will always respect the constraints and be safe. **WiseMove** thus executes the policies using Monte Carlo tree search (MCTS), which explores ahead in time to see if there exists a safer option than the one suggested by the learned policy.

### Learning Backends

**WiseMove** supports alternative learning backends, which are simple to plug in and use. The <a href="http://github.com/keras-rl/keras-rl">KerasRL</a> reinforcement learning framework was used to learn and test policies for the simple intersection environment featured in <a href="http://arxiv.org/abs/1902.04118">this paper</a>. The <a href="http://github.com/hill-a/stable-baselines">stable-baselines</a> library has also been incorporated.
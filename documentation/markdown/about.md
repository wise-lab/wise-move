<a href=README.md><img src="documentation/markdown/home_button.png" height="30" /></a><br><br>

About
=====

The [**WiseMove**](https://git.uwaterloo.ca/wise-lab/wise-move) effort is led by researchers at [WISE Lab](https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/), University of Waterloo, under the supervision of [Prof. K. Czarnecki](https://gsd.uwaterloo.ca/kczarnec).

### Primary Contributors

- Jaeyoung Lee
- Aravind Balakrishnan
- Ashish Gaurav
- Sean Sedwards

### Contact

For queries that have not already been answered in the issue tracker, feel free to either open an issue or contact us at <a href="mailto:wise.move@yahoo.com">wise.move@yahoo.com</a>.
<a href=README.md><img src="documentation/markdown/home_button.png" height="30" /></a><br><br>

Installation
============

The following has been tested on a Ubuntu 18.04 machine.

* Download the wise-move repo.
* You can either use Docker or use an Anaconda environment for running WiseMove.
* For docker:
	* First, install and setup docker:
		* Use instructions in the following link to install docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/
		* Follow post-install instructions to use docker as non-root user from: https://docs.docker.com/install/linux/linux-postinstall/
		* Use the instructions in the following link to install nvidia-docker: https://github.com/NVIDIA/nvidia-docker
	* Navigate to the root of the wise-move project.
	* `bash ./scripts/setup_docker_image.sh` to create the docker image.
	* `bash ./scripts/start_docker_container.sh` to start the container.
	* `bash ./scripts/enter_docker_container.sh` to enter the container. You can run wisemove code from this terminal.
	* When you're done, type `exit` to exit the container and use `bash ./scripts/stop_docker_container.sh` to stop the running container. 
	* Note: visualization (--visualize) does not work from docker container at this point.
* For using Anaconda:
	* You can either install Anaconda yourself or let the below script install it for you.
	* To install yourself, see instructions at https://www.anaconda.com/distribution. Make sure that the `conda` command is available from the terminal. Use `conda init bash` if it is not and restart the terminal.
	* Use `bash ./scripts/install_in_conda.sh` to create a conda environment with all the necessary dependancies for Wisemove.
		* It first checks if `conda` command is available. If not, it installs Miniconda (a light version of Anaconda). Note: It runs `conda init bash` and will add `conda` command in .bashrc.
		* It creates a conda environment called `wisemove` with all necessary packages.
		* You may have to restart the terminal after running the script to enable `conda` command.
	* Enter the conda environment using `conda activate wisemove` and navigate to the root of the wise-move project to run any code.

### Requirements

* Python 3.6
* Sphinx
* Please check `requirements.txt` for the complete python3 package list.


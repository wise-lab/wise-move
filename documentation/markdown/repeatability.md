<a href=README.md><img src="documentation/markdown/home_button.png" height="30" /></a><br><br>

Repeatability Guide
===================

It is recommended that you read the package documentation before running the code.

* Download the package and open `./documentation/sphinx/.build/index.html` to view the documentation.
* If the file does not exist, use command `./scripts/generate_doc.sh build` to generate documentation first (requires `Sphinx` pip package)

## 1. cross_intersection

Given below are the minimum steps required to reproduce the options policies trained in WiseMove and reported in 

Balakrishnan, A., Lee, J., Gaurav, A., Czarnecki, K. and Sedwards, S.,  
"Transfer Reinforcement Learning for Autonomous Driving: From WiseMove to WiseSim,"  
submitted to the Special Issue of ACM Transactions on Modeling and Computer Simulation (TOMACS), 2020.

* Open terminal and navigate to the root of the project directory (which we denote 'wisemove').
* Use `scripts/run_transfer_experiments.py` to reproduce the results for each reported high-level policy. It has two modes: train and test, set using the arguments --train and --test respectively; for a visualization, add the option --visualize.
* Run `python scripts/run_transfer_experiments.py --help` to view the arguments and get detailed info on how to use them.
* To test pre-trained policies:
    * `python  scripts/run_transfer_experiments.py --test --${POLICY_NAME}`
    * Replace ${POLICY_NAME} with any of: random, rule, r_rule, distill, wm, lag, kf, lagkf, lag_dr, kf_dr, vanish, xy_dr, percept, best
* To train new policies:
    * `python  scripts/run_transfer_experiments.py --train --${POLICY_NAME}`
    * Replace ${POLICY_NAME} with any of: wm, lag, kf, lagkf, lag_dr, kf_dr,  vanish, xy_dr, percept, best
    * Note: The training is stochastic so you may need mutliple trials to see reported results.
    * To test this newly trained policy use: `python  scripts/run_transfer_experiments.py --test --saved_policy_in_root --${POLICY_NAME}`. This searches for the new policy in the base directory rather than the pre-trained folder.
* To visualize the process, add --visualize. For example, 
    * `python  scripts/run_transfer_experiments.py --test --${POLICY_NAME} --visualize`
* Note: This reproduces only the results in WiseMove. Results using the high-fidelity simulator (e.g., those with the distill and d_rule policies) need Unreal-based WiseSim, which is not provided in this repo.

## 2. simple_intersection

Given below are the minimum steps required to generate the low-level and high-level policies described in [the preprint](http://arxiv.org/abs/1902.04118) and 

Lee, J., Balakrishnan, A., Gaurav, A., Czarnecki, K. and Sedwards, S.,  
"[WiseMove: a framework to investigate safe deep reinforcement learning for autonomous driving](https://doi.org/10.1007/978-3-030-30281-8_20),"  
In: Parker D., Wolf V. (eds) Quantitative Evaluation of Systems. QEST 2019. Lecture Notes in Computer Science, vol. 11785. Springer, Cham, Sep., 2019.

(the results can be different from the tables therein; for the reproducibility of those numbers, go to the branch qest-2019 and follow the instructions).

A detailed user guide is available in the documentation.

* Open terminal and navigate to the root of the project directory (which we denote 'wisemove').
* Change the directory to "wisemove/worlds/intersection/simple/"
* Low-level policies:
    * Use `python low_level_main.py --help` to see all available commands.
    * You can choose to test the provided pre-trained options:
        * To visually inspect all pre-trained options: `python low_level_main.py --test`
        * To evaluate all pre-trained options: `python low_level_main.py --evaluate`
        * To visually inspect a specific pre-trained policy: e.g., `python low_level_main.py --option=wait_then_go --test`.
        * To evaluate a specific pre-trained policy: e.g., `python low_level_main.py --option=wait_to_go --evaluate`.
        * Available options are: wait_then_go, change_lane, stop_to_decelerate, track_speed, follow
    * Or, you can train and test all the options, noting that this may take some time. Newly trained policies are saved to the root folder by default.
        * To train all low-level policies from scratch (~40 minutes): `python low_level_main.py --train`.
        * To visually inspect all the new low-level policies: `python low_level_main.py --test --saved_policy_in_root`.
        * To evaluate all the new low-level policies: `python low_level_main.py --evaluate --saved_policy_in_root`.        
        * Make sure the training is fully complete before running the above test/evaluation.
    * It is faster to verify the training of a few options using the commands below (**Recommended**):
        * To train a single low-level policy, e.g., *change_lane* (~6 minutes): `python low_level_main.py --option=change_lane --train`. This is saved to the root folder.
        * To evaluate the new *change_lane*: `python low_level_policy_main.py --option=change_lane --evaluate --saved_policy_in_root`
        * Available options are: wait_then_go, change_lane, stop_to_decelerate, track_speed, follow  
        &nbsp;


* High-level policy:
    * Use `python high_level_main.py --help` to see all available commands.
    * You can use the provided pre-trained high-level policy:
        * To visually inspect this policy: `python high_level_main.py --test`
        * To evaluate this policy (~5 minutes): `python high_level_main.py --evaluate`
    * Or, you can train the high-level policy from scratch. Note that this takes some time:
        * To train using pre-trained low-level policies for 0.2 million steps (~50 minutes): `python high_level_main.py --train`
        * To visually inspect this new policy: `python high_level_main.py --test --saved_policy_in_root`
        * To evaluate this new policy (~5 minutes): `python high_level_main.py --evaluate --saved_policy_in_root`.
    * Since above training takes a long time, you can instead verify using a lower number of steps: 
        * To train for 0.1 million steps (~25 minutes): `python high_level_main.py --train --nb_steps=100000`
        * Note that this has a much lower success rate of ~75%.

The time taken to execute the above scripts may vary depending on your configuration. The reported results were obtained using a system of the following specs:

Intel(R) Core(TM) i7-7700 CPU @ 3.60GHz  
16GB memory  
Nvidia GeForce GTX 1080 Ti  
Ubuntu 16.04  

<a href=README.md><img src="documentation/markdown/home_button.png" height="30" /></a><br><br>

Contribution Guide
==================

* To request features or report bugs, use the [issue tracker](https://git.uwaterloo.ca/wise-lab/wise-move/issues).
* If you want to solve an issue by yourself, [fork our repository](https://docs.gitlab.com/ee/workflow/forking_workflow.html), and send a merge request. We will then review it and merge it as necessary.
* Please ensure that all your merge requests follow our coding standards, described below.
* Make sure your merge requests don't have an unnecessary number of commits. If possible, consider squashing those commits before submitting a merge request.

### Coding Standards

* We follow PEP8 style guidelines for coding and PEP257 for documentation.
* It is not necessary to keep these in mind while coding, but before submitting a merge request, you will need to run the style formatting scripts.
* To format the code, use `yapf`: `yapf -i YOUR_MODIFIED_FILE.py`
* To format the docstrings, use `docformatter`: `docformatter --in-place YOUR_MODIFIED_FILE.py`
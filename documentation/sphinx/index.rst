.. saferl documentation master file, created by
   sphinx-quickstart on Tue Sep  4 16:50:18 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

########
Contents
########

User Guide
===========
.. toctree::
   :maxdepth: 2

   usage/quickstart

Code documentation
==================

* :ref:`genindex`
* :ref:`modindex`

Search
==================
* :ref:`search`

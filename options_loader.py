import json
import os  # for the use of os.path.isfile
from backends import RLController, DDPGLearner, MCTSController, ManualPolicy
import importlib
from gym.spaces import Discrete


class OptionsLoader:
    """Loads options and config for experiments.
    """
    #: visualization flag
    visualize_low_level_steps = False

    def __init__(self, json_path, *env_args, **env_kwargs):
        """Constructor for the options graph.

        Args:
            json_path: path to the json file which stores the configuration
                for this options graph
            env_class: the gym compliant environment class
            env_args: arguments to pass when initializing the env
            env_kwargs: named arguments to pass when initializing the env
        """

        self.global_config = json.load(open(json_path))
        env_config_file = self.global_config["config"]
        self.config = json.load(open(env_config_file)) #TODO: make this relative to env folder

        #: Gym compliant environment instance. This instance also has a
        #  back-reference to this options instance.
        env_module = importlib.import_module(self.global_config["world"])
        env_class = getattr(env_module, self.global_config["env_class"])

        self.env = env_class(*env_args, **env_kwargs)
        #: nodes of the graph
        self.nodes = self.config["nodes"]
        #: edges of the graph
        self.edges = self.config["edges"]
        #: adjacency list from the graph
        self.adj = {key: [] for key, value in self.nodes.items()}
        for i, j in self.edges.items():
            self.adj[i].append(j)

        #: Hold the different maneuver objects
        #  Each maneuver object also has a back-reference to this options
        #  instance. TODO: allow for passing arguments to maneuver
        #  objects at init, for example, noise
        # TODO: Change name maneuver as it is SimpleIntersectionEnv specific.
        # TODO: the env may not have options. handle that by checking if it exists in the env config
        options_module = importlib.import_module(self.global_config["world"])
        # use the following code for simple_intersection...
        # options_module = importlib.import_module(self.global_config["world"])
        self.maneuvers = {
            key: getattr(options_module,value)(self.env)
            for key, value in self.nodes.items()
        }
        self.maneuvers_alias = list(self.maneuvers.keys())  # so that maneuver can be referenced using an int

        #: starting node
        #  TODO: reimplement if needed for multi-agent training?
        self.start_node_alias = self.config["start_node"]

        #: high level policy over low level policies
        #  possible classes : rl
        if self.config["method"] == "rl":
            self.controller = RLController(self.env, self.maneuvers,
                                           self.start_node_alias)
        elif self.config["method"] == "manual":
            self.controller = ManualPolicy(self.env, self.maneuvers, self.adj,
                                           self.start_node_alias)
        elif self.config["method"] == "mcts":
            self.controller = MCTSController(self.env, self.maneuvers,
                                             self.start_node_alias)
        else:
            raise Exception(self.__class__.__name__ + \
                                 "Controller to be used not specified")

    def step(self, option):
        """Complete an episode using specified option. This assumes that the
        manager's env is at a place where the option's initiation condition is
        met.

        Args:
            option: index of high level option to be executed
        """

        #check if the incoming option is an integer or an option alias(string)
        try:
            option = int(option)
            option_alias = self.maneuvers_alias[option]
        except ValueError:
            option_alias = option

        self.controller.set_current_node(option_alias)

        # execute whole maneuver
        return self.controller.step_current_node(
            visualize_low_level_steps=self.visualize_low_level_steps)

    def reset(self):
        """Reset the environment. This function may be needed to reset the
        environment for eg. after an MCTS rollout and update. Also reset the
        controller to root node.

        Returns whatever the environment's reset returns.
        """

        self.controller.set_current_node(self.start_node_alias)
        return self.env.reset()

    @property
    def action_space(self):
        return Discrete(len(self.maneuvers))

    @property
    def observation_space(self):
        return self.env.observation_space

    def set_controller_policy(self, policy):
        """Sets the trained controller policy as a function which takes in
        feature vector and returns an option index(int). By default,
        trained_policy is None.

        Args:
            policy: a function which takes in feature vector and returns an option index(int)
        """
        self.controller.set_trained_policy(policy)

    def set_controller_args(self, **kwargs):
        """Sets custom arguments depending on the chosen controller.

        Args:
            **kwargs: dictionary of args and values
        """
        self.controller.set_controller_args(**kwargs)

    def execute_controller_policy(self):
        """Performs low level steps and transitions to other nodes using
        controller transition policy.

        Returns state after one step, step reward,
        episode_termination_flag, info
        """
        if self.controller.can_transition():
            self.controller.do_transition()

        return self.controller.low_level_step_current_node()

    def set_current_node(self, node_alias):
        """Sets the current node for controller. Used for training/testing a
        particular node.

        Args:
            node_alias: alias of the node as per config file
        """
        self.controller.set_current_node(node_alias)

    @property
    def current_node(self):
        return self.controller.current_node

    # TODO: specify values using config file, else use these defaults
    # TODO: error handling
    def load_trained_low_level_policies(self):
        for key, maneuver in self.maneuvers.items():
            # TODO: Ensure that for manual policies, nothing is loaded
            trained_policy_path = "backends/trained_policies/simple_intersection/" + key + "/"
            critic_file_exists = os.path.isfile(trained_policy_path + key + "_weights_critic.h5f")
            actor_file_exists = os.path.isfile(trained_policy_path + key + "_weights_actor.h5f")

            if actor_file_exists and critic_file_exists:
                agent = DDPGLearner(
                    input_shape=(maneuver.get_reduced_feature_len(), ),
                    nb_actions=2,
                    gamma=0.9985,
                    nb_steps_warmup_critic=200,
                    nb_steps_warmup_actor=200,
                    lr=1e-3)
                agent.load_model(trained_policy_path + key + "_weights.h5f")
                maneuver.set_policy(agent.predict)

            elif not critic_file_exists and actor_file_exists:
                print("\n Warning: unable to load the low-level policy of \"" + key +
                      "\". the file of critic weights have to be located in the same " +
                      "directory of the actor weights file; the manual policy will be used instead.\n")

            else:
                print("\n Warning: the trained low-level policy of \"" + key +
                      "\" does not exists; the manual policy will be used.\n")

    def get_number_of_nodes(self):
        return len(self.maneuvers)

    def render(self, mode='human'):
        return self.env.render(mode=mode)


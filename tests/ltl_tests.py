
from termcolor import colored

from verifier import LTLProperty, LTLProperties, AtomicPropositions
from verifier.parser import Parser, Errors
from verifier.scanner import Scanner
from utilities import Bits


class TestCase:
    def __init__(self, x, keys, mode, trace, ground_truth):
        self.ltl = LTLProperty(x, keys, mode)
        self.trace = trace
        self.ground_truth = ground_truth


def test(case):
    print('\n\nTrace: ' + str(case.trace))

    test_passed = True

    def print_result(test_method):
        result = (case.ground_truth == case.ltl.mc_status)
        print(f'\nTest method: {test_method.__name__:11} Ground-truth: {case.ground_truth:1}')
        print(f'Result: {case.ltl.mc_status:1},\t\t\t\t Matching with ground truth: {result}')
        return result

    case.ltl.check(case.trace)
    test_passed &= print_result(case.ltl.check)

    case.ltl.reset(case.trace[0])
    for state in case.trace[1:]:
        case.ltl.check_incremental(state)

    case.ltl.reset(case.trace[0])
    for state in case.trace[1:]:
        case.ltl(state)
    test_passed &= print_result(case.ltl.__call__)

    ltls = LTLProperties(case.ltl.aps_keys)
    ltls.add(alias='unknown', x=case.ltl.x, mode=case.ltl.mode)

    ltls.reset(case.trace[0])
    for state in case.trace[1:]:
        ltls(state)
    test_passed &= print_result(ltls.__call__)

    return test_passed


print(f'Parser status:\tTrue ({Parser.TRUE}),\tUndecided({Parser.UNDECIDED}),\tFalse({Parser.FALSE})')

aps_keys = ('in_stop_region', 'has_entered_stop_region', 'before_but_close_to_stop_region', 'stopped_now',
            'has_stopped_in_stop_region', 'in_intersection', 'over_speed_limit', 'on_route', 'highest_priority',
            'intersection_is_clear', 'veh_ahead', 'lane', 'parallel_to_lane', 'before_intersection', 'target_lane',
            'veh_ahead_slow', 'veh_ahead_too_close')

aps = AtomicPropositions(aps_keys)
hashmap = aps.hash_map


def int_expression(*args):
    result = 0
    for key in args:
        result |= int(pow(2, hashmap[key]))
    return result


x1 = 'G (has_entered_stop_region => (in_stop_region U has_stopped_in_stop_region) )'
x2 = 'G ( not stopped_now U (in_stop_region or veh_ahead))'
x3 = 'G ( (not in_stop_region) U veh_ahead)'
x4 = 'not stopped_now U veh_ahead_slow'
x5 = 'G( not in_intersection U highest_priority)'
x6 = '(not in_intersection) U highest_priority'
mode1, mode2 = 'violation', 'satisfaction'

trace1 = [0, 0, 0, 0, 0, 0, 0, int_expression('has_entered_stop_region', 'in_stop_region')]
trace2 = [0, 0, 0, 0, 0, 0, 0, int_expression('has_entered_stop_region')]

trace3 = [int_expression('in_stop_region'), 0, 0, 0, 0, 0, 0]

trace4 = [int_expression('stopped_now', 'in_stop_region'), 0, 0, 0, 0, 0, 0]

trace5 = [int_expression('veh_ahead'), 0, 0, 0, 0, 0, 0]

trace6 = [int_expression('in_stop_region', 'veh_ahead')] * 10

trace7 = [int_expression('in_stop_region', 'veh_ahead'), 0, 0, 0, 0, 0, 0, 0, 0, 0]

trace8 = [0, 0, 0, int_expression('veh_ahead_slow'), int_expression('stopped_now'), 0, 0, 0]

trace9 = [int_expression('in_intersection', 'highest_priority')]

trace10 = [0, 0, 0, int_expression('veh_ahead_slow'), int_expression('stopped_now'), 0, 0, 0]

trace11 = [int_expression('veh_ahead')] * 10

trace12 = [0] * 10

trace13 = [0] * 5 + [int_expression('in_stop_region')] + [int_expression('in_stop_region', 'veh_ahead')] * 4

trace14 = [int_expression('in_intersection', 'highest_priority')]

cases = [TestCase(x1, aps_keys, mode1, trace1, Parser.UNDECIDED),
         TestCase(x1, aps_keys, mode1, trace2, Parser.FALSE),
         TestCase(x1, aps_keys, mode1, trace3, Parser.UNDECIDED),
         TestCase(x3, aps_keys, mode1, trace5, Parser.UNDECIDED),
         TestCase(x3, aps_keys, mode1, trace6, Parser.UNDECIDED),
         TestCase(x3, aps_keys, mode1, trace7, Parser.UNDECIDED),
         TestCase(x4, aps_keys, mode1, trace8, Parser.TRUE),
         TestCase(x5, aps_keys, mode1, trace9, Parser.UNDECIDED),
         TestCase(x4, aps_keys, mode1, trace10, Parser.TRUE),
         TestCase(x3, aps_keys, mode1, trace11, Parser.UNDECIDED),
         TestCase(x3, aps_keys, mode1, trace12, Parser.UNDECIDED),
         TestCase(x3, aps_keys, mode1, trace13, Parser.FALSE),
         TestCase(x6, aps_keys, mode1, trace14, Parser.TRUE)]

final_result = colored('\nALL UNIT TESTS PASSED...', 'green')

for c in cases:
    if not test(c):
        print(colored('A UNIT TEST NOT PASSED...', 'red'))
        final_result = colored('\nSOME UNIT TEST(S) NOT PASSED...', 'red')
    else:
        print(colored('PASSED...', 'green'))


def construct_parser_and_bits(propositions, scan_str):
    assert(type(propositions) == list)
    p = Parser()
    #: initialise the Errors class to give meaningful messages when calling parser.SetProperty
    Errors.Init(scan_str, "", False, p.getParsingPos, p.errorMessages)

    p.APdict = {key: i for i, key in enumerate(propositions)}
    p.SetProperty(Scanner(scan_str))
    return p, Bits()


def test_parser(aps_keys, prop, ap_seq, ground_truth, reset):
    results = tuple()
    ground_truth_matched = tuple()

    p, ap_bit = construct_parser_and_bits(aps_keys, prop)

    for i, aps in enumerate(ap_seq):
        for j, ap in enumerate(aps):
            ap_bit[j] = ap
        if reset[i]:
            p.ResetProperty()
        result = p.CheckIncremental(int(ap_bit))
        results += result,
        ground_truth_matched += result == ground_truth[i],

    ground_truth_matched = all(ground_truth_matched)

    print(f'Test method: Parser.CheckIncremental')
    print(f'Ground-truth: {ground_truth}, \tResult: {results},')
    print(f'Matching with ground truth: {ground_truth_matched}')

    if not ground_truth_matched:
        print(colored('A UNIT TEST NOT PASSED...\n', 'red'))
    else:
        print(colored('PASSED...\n', 'green'))

    return ground_truth_matched


parser_result = True
print(colored('\n[Test Parser]\n', 'green'))
print(colored('Test single_factor', 'green'))
test_parser(['A'], 'A', ((1,), (0,)), (Parser.TRUE, Parser.FALSE), reset=(True, True))

print(colored('Test conjunction', 'green'))
parser_result &= test_parser(['A', 'B'], 'A and B', ((0, 0), (0, 1), (1, 0), (1, 1)),
                             (Parser.FALSE, Parser.FALSE,  Parser.FALSE, Parser.TRUE), reset=(True, True, True, True))

print(colored('Test conjunction', 'green'))
parser_result &= test_parser(['A', 'B'], 'A and B', ((0, 0), (0, 1), (1, 0), (1, 1)),
                             (Parser.FALSE, Parser.FALSE,  Parser.FALSE, Parser.TRUE), reset=(True, True, True, True))

print(colored('Test disjunction', 'green'))
parser_result &= test_parser(['A', 'B'], 'A or B', ((0, 0), (0, 1), (1, 0), (1, 1)),
                             (Parser.FALSE, Parser.TRUE, Parser.TRUE, Parser.TRUE), reset=(True, True, True, True))

print(colored('Test until', 'green'))
parser_result &= test_parser(['A', 'B'], 'A U B', ((1, 0), (1, 0), (1, 0), (0, 1)),
                    (Parser.UNDECIDED, Parser.UNDECIDED, Parser.UNDECIDED, Parser.TRUE), reset=(True, True, True, True))

if not parser_result:
    final_result = colored('\nSOME UNIT TEST(S) NOT PASSED...', 'red')

print(final_result)

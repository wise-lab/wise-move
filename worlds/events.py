from verifier import LTLProperty, LTLProperties


class Event(object):
    """ Event structure which defines an event (mostly a termination event) by describing its condition and
    the reward(s) that will be given when the event happens.

    """
    def __init__(self, condition=lambda: False, reward=lambda: None, step=lambda: None, reset=lambda: None, enable=True):
        """ sets the condition for the event to be activated (when True) and the reward(s) given when the event happens.

        Args:
            condition: a function instance which returns True if the condition met, otherwise False.
                        The function must have no arguments (than self if it's an instance method).
            reward: a function returning the reward(s) when the "condition" is violated (i.e., when the event happens).
                        Here, reward(s) means a None or a int/float scalar value, or a tuple/list thereof.
                        The function must have no arguments (than self if it's an instance method).
            enable: a boolean, True by default. When False, it indicates that the event is not enabled/activated.
        """

        assert all(callable(arg) for arg in [condition, reward, step, reset])
        assert isinstance(enable, bool)

        self.condition = condition
        self.reward = reward
        self.step = step
        self.reset = reset
        self.enable = enable


class TimeOut(Event):
    """ An Event class driven by an instance of the Time class.
    """
    def __init__(self, time, reward=lambda: None, enable=True):
        """
        Args:
            time: an instance of the class Time
            reward: a function returning the reward(s) when the "condition" is violated (i.e., when the event happens);
                        Here, reward(s) means a None or a int/float scalar value, or a tuple/list thereof;
                        The function must have no arguments (than self if it's an instance method);
            enable: a boolean, True by default. When False, it indicates that the event is not enabled/activated.
        """
        super().__init__(lambda: time.timeout_happened, reward, time.step, time.reset, enable)


class LTLEvent(Event):
    """An Event class driven by an instance of the LTLProperty class.
    """
    def __init__(self, ltl_property, state, reward=lambda: None, enable=True):
        """
        Args:
            ltl_property: an instance of the class LTLProperty;
            state: a function that takes no argument and returns the state to be checked by verifier;
            reward: a function returning the reward(s) when the "condition" is violated (i.e., when the event happens);
                        Here, reward(s) means a None or a int/float scalar value, or a tuple/list thereof;
                        The function must have no arguments (than self if it's an instance method);
            enable: a boolean, True by default. When False, it indicates that the event is not enabled/activated.
        """
        assert isinstance(ltl_property, LTLProperty)
        assert callable(state)

        self.ltl_property = ltl_property

        self.state = state
        super().__init__(condition=lambda: self.ltl_property.done, reward=reward, enable=enable,
                         step=lambda: self.ltl_property(self.state()),
                         reset=lambda: self.ltl_property.reset(self.state()))


def ltl_events(ltl_properties, state, rewards=lambda: None, enables=True):
    """ returns a dictionary of the form alias:LTL_Event which can be used in EpisodeEnv or its subclass to define
    at once several termination events driven by a TLProperty class.

    Args:
        ltl_properties: an instance of LTLProperties (for a meaningful use, it must contains more than one LTL property;
        state: the same as that in LTLEvent.__init__
        rewards: the same as that in LTLEvent.__init__ or a tuple/array thereof
        enable: the same as that in LTLEvent.__init__ or a tuple/array thereof

    Returns: a dictionary of the form alias:LTL_Event

    """

    if callable(rewards):
        rewards = [rewards] * len(ltl_properties)

    if isinstance(enables, bool):
        enables = [enables] * len(ltl_properties)

    assert all(isinstance(arg, (tuple, list)) for arg in [rewards, enables])
    assert len(enables) == len(rewards) == len(ltl_properties)

    events = dict()

    for i, alias in enumerate(ltl_properties.aliases):
        events.update({alias: LTLEvent(ltl_properties[alias], state, rewards[i], enables[i])})

    return events


def enable(events, aliases, condition=True):
    """ Enables the events corresponding to the aliases, if the given condition is True
        (in other words, if the given condition is False, then disables those events -- see disable method below).

    Args:
        events: a dictionary of the form {alias:Event};
        aliases: a string representing an alias of the event to be enabled or a tuple or list thereof.
        condition: the given condition (True or False)
    """

    assert isinstance(events, dict)
    assert isinstance(condition, bool)

    if not isinstance(aliases, (tuple, list)):
        aliases = [aliases]

    assert all(alias in events.keys() for alias in aliases)

    for alias in aliases:
        events[alias].enable = condition


def disable(events, aliases):
    """ Disables the events corresponding to the aliases.

        Args:
            events: a dictionary of the form {alias:Event};
            aliases: a string representing an alias of the event to be disabled or a tuple or list thereof.
        """
    enable(events, aliases, condition=False)


def set_rewards(events, aliases, rewards):
    """ Set all the rewards of the events corresponding to the aliases.
        This is particularly useful when one wants to set the rewards of many events to the same value "at once".

        Args:
            events: a dictionary of the form {alias:Event};
            aliases: a tuple or list of the aliases of the events for which the rewards are to be set.
            rewards: a function returning the reward with no arguments, or a tuple (or list) of such functions;
                     when given as a tuple (or list), the order has to be the same as that of aliases.
    """

    assert isinstance(events, dict)

    if not isinstance(aliases, (tuple, list)):
        aliases = [aliases]

    assert all(alias in events.keys() for alias in aliases)

    if not isinstance(rewards, (tuple, list)):
        rewards = (rewards,) * len(aliases)

    for i, alias in enumerate(aliases):
        events[alias].reward = rewards[i]


from .constants import LEFT, RIGHT


class RoadElement(object):
    """ The base class of road elements defined below, i.e., StopRegion, Lane, Route.
    The meaning of each attribute can be different over the types of road elements.
    In each road element, 'forward' means "left/top -> right/bottom" and 'backward' means "left/top <- right/bottom"

    """
    def __init__(self, element_type=None, width=None, boundary=None):
        """ initializes the road element.

        Args:
            element_type: a string indicating the type of the element (not the class name);

            width: a scalar indicating the width of the road element;

            boundary: the boundary of the element (None or a list only with two elements).
        """
        self.type = element_type
        self.width = width
        self.__boundary = boundary

    def get_boundary(self):
        return self.__boundary

    def set_boundary(self, boundary):
        """ The setter of the boundary attribute. When setting the boundary,
        the center (__center) and the width are updated (and for the width also checked) as well.

        Args:
            boundary: None or a list only with two elements (depending on the type of road element).

        """
        if boundary is None:
            self.__boundary = None
            self.__centre = None
        else:
            assert len(boundary) == 2
            boundary.sort()
            self.__boundary = boundary
            self.__centre = (boundary[0] + boundary[1]) / 2

            if self.width is None:
                self.width = boundary[1] - boundary[0]
            else:
                assert self.width == boundary[1] - boundary[0]

    #: boundary attribute. One can consider it as None or a list having two elements.
    boundary = property(get_boundary, set_boundary, None)

    @property
    def centre(self):
        """ Returns the centre of the boundary.
        Note: the attribute __centre is set in the setter of boundary above.

        """
        return self.__centre

    @staticmethod
    def raise_unknown_type_exception():
        raise NotImplementedError("the exception method "
                                  "'_raise_unknown_route_type_exception'"
                                  "has to be implemented in the subclass"
                                  "before calling it.")

    def __eq__(self, other):
        """ Two RoadElement's are equal if the attributes in this base class are equal.

        """
        return self.boundary == other.boundary and \
               self.width == other.width and \
               self.type == other.type and \
               self.centre == other.centre


class StopRegion(RoadElement):
    """ Implements the stop region that implements a stop sign, in an intersection. The attributes 'boundary' and \
    'ahead_boundary' are initialized by calling assign_stop_region method in Lane class below, \
    not anywhere in the body of this class.

    """
    def __init__(self, lane_dir, width=None, ahead_dist=0):
        """ initializes the stop region.

        Args:
            lane_dir: a string 'forward' or 'backward'.
            width: the length of the stop region (given as a scalar) in the longitudinal direction.
            ahead_dist: a scalar indicating a look-ahead dist before the stop region, usually meaning the max. distance
                        before the stop_region at which a feature or the (autonomous) driver can recognize the
                        stop region.
        """
        assert ahead_dist >= 0
        assert lane_dir == 'forward' or lane_dir == 'backward'

        self.ahead_dist = ahead_dist
        self.ahead_boundary = None
        super().__init__(lane_dir, width, None)

    def set_boundary(self, boundary):
        """ Overrides the base class set_boundary in order to set ahead_boundary which indicates the longitudinal
            boundary corresponding to the ahead_dist. This method is called by assign_stop_region method in Lane class.

        Args:
            boundary: the stop region longitudinal boundary.

        """
        super().set_boundary(boundary)

        if boundary is None:
            self.ahead_boundary = None
        else:
            if self.type == 'forward':
                self.ahead_boundary = [self.boundary[0] - self.ahead_dist,
                                       self.boundary[0]]

            elif self.type == 'backward':
                self.ahead_boundary = [self.boundary[1],
                                       self.boundary[1] + self.ahead_dist]

            else:
                Lane.raise_unknown_type_exception()

    # Reassign the property due to the override of set_boundary above.
    boundary = property(RoadElement.get_boundary, set_boundary, None)

    def __eq__(self, other):
        return super().__eq__(other) and \
               self.ahead_boundary == other.ahead_boundary and \
               self.ahead_dist == other.ahead_dist


class Lane(RoadElement):
    """ Defines the lanes of a route.

    """
    def __init__(self, lane_dir, width):
        """ Initializes the lane. The stop region is not assigned by default.

        Args:
            lane_dir: a string 'forward' or 'backward'.
            width: the width of the lane, i.e., the size in the lateral direction.
        """

        assert lane_dir == 'forward' or lane_dir == 'backward'

        # the stop region in a lane is initially not assigned but can be done by calling assign_stop_region below.
        self.stop_region = None

        super().__init__(lane_dir, width)

    def assign_stop_region(self, route, width, offset=0, ahead_dist=0):
        """ Assigns a stop region on the lane. It must be given a route (Route class below) that intersects with the
            route containing the lane to which the stop region is assigned.

        Args:
            route: a Route class that intersects with the route containing the lane
            width: the width param of the stop_region
            offset: the offset from the intersection.
                    If zero, the stop region ends exactly at the start pos of the intersection.
            ahead_dist: the ahead_dist param of the stop_region

        """

        assert isinstance(route.boundary, list) and \
               len(route.boundary) == 2

        self.stop_region = StopRegion(self.type, width, ahead_dist)

        if self.type == 'forward':
            self.stop_region.boundary = [route.boundary[0] - offset - width,
                                         route.boundary[0] - offset]

        elif self.type == 'backward':
            self.stop_region.boundary = [route.boundary[1] + offset,
                                         route.boundary[1] + offset + width]

        else:
            self.raise_unknown_type_exception()

    @staticmethod
    def raise_unknown_type_exception():
        raise ValueError("Road type has to be either 'forward' or 'backward'.")


class Route(RoadElement):
    """A class that contains the lane(s) information in a straight route
    The current implementation is tested when the number of lanes is "2".

    Properties:
        * type: 'h' for horizontal or 'v' for vertical lane
        * n_lanes: the number of lanes (has to be multiples of 2)
        * start_pos: the position that the route starts
        * end_pos: the position that the route ends
        * min_pos: the minimum value of the position on the route
        * max_pos: the maximum value of the position on the route
        * middle_pos: the center value of the position on the route
        * length: the length of the route
        * width: the total width of the route (summation of each lane's width)
        * centre: the centreline of the route
        * centreline: the centreline of the route. If the two lanes are give
                      with the equal width, then this is equal to centre.
    """

    def __init__(self, route_type, start_pos, end_pos):
        """ Initialize the Route.
        The coordination system is the same as that in computer graphics,
        but the centre of intersection is always at (0, 0).

        Args:
            route_type: either 'h' or 'v'
            start_pos: the starting position.
            end_pos: the end position.
        """

        if (route_type != 'h') and (route_type != 'v'):
            self.raise_unknown_type_exception()

        assert start_pos < end_pos

        self.start_pos = start_pos
        self.end_pos = end_pos
        self.middle_pos = (start_pos + end_pos) / 2
        self.length = end_pos - start_pos

        self.lanes = None
        super().__init__(route_type)

    @property
    def lanes(self):
        return self.__lanes

    @lanes.setter
    def lanes(self, lanes):
        """ Set the lanes in the Route.

        Args:
            lanes: a list with two components, each of which is Lane class.

        """
        if lanes is None:
            self.__lanes = None
            self.boundary = None
            self.n_lanes = 0
            self.centreline = 0

        else:
            assert len(lanes) == 2
            self.__lanes = lanes

            self.n_lanes = 2
            self.centreline = 0

            if self.type == 'h':
                self.boundary = [-lanes[LEFT].width, lanes[RIGHT].width]
                lanes[LEFT].boundary = [-lanes[LEFT].width, 0]
                lanes[RIGHT].boundary = [0, lanes[RIGHT].width]

            elif self.type == 'v':
                self.boundary = [-lanes[RIGHT].width, lanes[LEFT].width]
                lanes[RIGHT].boundary = [-lanes[RIGHT].width, 0]
                lanes[LEFT].boundary = [0, lanes[LEFT].width]

            else:
                self.raise_unknown_type_exception()

    @property
    def stop_region(self):
        """ Returns the stop_region when the left and right lanes have the same dir and so do the stop regions.
        Note: this cannot be used when the route is bidirectional (e.g., left: backward, right: forward). In this case,
        one can individually access them by self.lanes[LEFT].stop_region and self.lanes[RIGHT].stop_region.

        Returns: the stop region if assigned, otherwise None.

        """

        assert self.lanes is not None

        stop_region = self.lanes[LEFT].stop_region
        stop_region_comp = self.lanes[RIGHT].stop_region

        if stop_region is None and stop_region_comp is None:
            return None

        # The two stop regions must be equal, i.e., have the same dir and ahead_boundary.
        assert self.lanes[LEFT].type == self.lanes[RIGHT].type  # The lanes must have the same dir.
        assert stop_region == stop_region_comp

        return stop_region

    @staticmethod
    def raise_unknown_type_exception():
        raise ValueError("Route type has to be either 'h' or 'v'.")

    @property
    def pos_range(self):
        """ Returns min-max values of the possible position as a list.

        """
        return self.start_pos, self.end_pos

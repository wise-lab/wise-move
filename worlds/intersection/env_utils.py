from numpy import sqrt
from .constants import constants as consts
import numpy as np


def calc_time_to_cover_dist(dist, u, max_acc=consts.MAX_ACCELERATION, max_vel=consts.rd.speed_limit):
    if dist < 0:
        return -1

    #time taken to reach max vel
    t_1 = max(max_vel - u,0) / max_acc

    # dist covered in this time
    s_1 = u*t_1 + (1/2) * max_acc*(t_1**2)

    if s_1 <= dist:
        # time for rest of the distance at max velocity
        t_2 = (dist-s_1)/max_vel

        return t_1 + t_2
    #
    else:
        # calculate the discriminant
        d = (u ** 2) - (4 * max_acc/2 * - dist)

        if d >= 0:
            # find two solutions
            sol1 = (-u - sqrt(d)) / (2 * max_acc/2)
            sol2 = (-u + sqrt(d)) / (2 * max_acc/2)

            return max(sol1, sol2)
        else:
            return -1

def calc_dist_covered_in_given_time(initial_vel, given_time, max_acc=consts.MAX_ACCELERATION, max_vel=consts.rd.speed_limit):
    #time taken to reach max vel
    t_1 = max(max_vel - initial_vel,0) / max_acc

    if given_time <= t_1:
        # dist covered in this time
        return initial_vel*given_time + (1/2) * max_acc*(given_time**2)
    else:
        # dist covered during acceleration
        s_1 = initial_vel * t_1 + (1 / 2) * max_acc * (t_1 ** 2)

        # rest of dist covered at max speed
        s_2 = max_vel * (given_time - t_1)

    return s_1 + s_2

def calculate_v_max(dist, max_acc=consts.MAX_ACCELERATION):
    """Calculate the maximum velocity you can reach at the given position
    ahead.

    Args:
        dist: the distance you travel from the current position.

    Returns:
        the maximum reachable velocity.
    """
    return sqrt(2.0 * max_acc * max(dist, 0))


def calculate_s(v):
    """Calculate the distance traveling when the vehicle is maximally de-
    accelerating for a complete stop.

    Args:
        v: the current speed of the vehicle.

    Returns:
        the distance traveling when the vehicle is under the maximum
        de-acceleration to stop completely.
    """
    return pow(v, 2) / abs(consts.MIN_ACCELERATION) / 2.0


# TODO: this is simple_intersection scenario-specific.
def check_init_veh(veh, s, v_max_multiplier):
    """ Check a given initialized vehicle (veh) and modify its
        speed (v) if the vehicle is physically not able to stop at the stop
        region. This is internally called by the above method
        generate_scenario.

        Args:
            veh: the vehicle reference
            s: the distance traveling when the vehicle is under the maximum
               de-acceleration to stop completely.

        Returns:
            the same s to the input param s, but when the speed is modified.
    """
    veh.init_local_discrete_var()
    dist_before_stop_region = None

    if veh.which_route is 'h':
        lane = consts.rd.h.lanes[veh.APs['lane']]
        pos_long = veh.x

    else:  # when veh.which_route is 'v':
        lane = consts.rd.v.lanes[veh.APs['lane']]
        pos_long = veh.y

    if lane.stop_region is None or lane.type != veh.which_dir:
        # When there is no stop region or the vehicle is on the opposite lane, do nothing.
        return s

    bdy = lane.stop_region.boundary

    if veh.APs['in_stop_region']:
        dist_before_stop_region = bdy[1] - pos_long

    elif (not veh.APs['has_entered_stop_region']) and (bdy[0] - pos_long <= 2.0 * s):
        dist_before_stop_region = bdy[0] - pos_long

    # We suppose that the vehicle has stopped in the stop region. With this assumption,
    # the following code removes the non-reachable velocity at the current position in the intersection.
    elif veh.APs['in_intersection']:
        v_max = calculate_v_max(pos_long - bdy[0])
        veh.v = np.random.uniform(0, v_max)
        s = calculate_s(veh.v)

    if dist_before_stop_region is not None:

        v_max = calculate_v_max(dist_before_stop_region)

        if v_max < veh.v:
            veh.v = v_max_multiplier * np.random.uniform(0, v_max)
            s = calculate_s(veh.v)

    veh.init_local_discrete_var()

    return s


def regulate_veh_speed(veh, dist, v_max_multiplier):
    """sets and regulate the speed of the vehicle in a way that it never
        requires to travel the given distance for complete stop.

    Args:
        veh: the vehicle reference.
        dist: the distance over which the vehicle has to not
              drive for complete stop.
        v_max_multiplier: the same as that in SimpleIntersectoinEnv.generate_scenario.

    Returns:
        the distance traveling when the vehicle is under
        the maximum de-acceleration to stop completely.
    """

    v_max = calculate_v_max(dist)
    veh.v = v_max_multiplier * np.random.uniform(0, v_max)

    s = calculate_s(veh.v)
    s = check_init_veh(veh, s, v_max_multiplier)

    return s


def normalize_tuple(vec, ranges, scale_factor=1, clipping=True):
    """Normalizes each element in a tuple according to ranges defined in ranges.

       Normalizes between 0 and 1 and scales by scale_factor.

       Args:
           vec: the tuple to be normalized
           ranges: a tuple of (min, max)-ranges of the values of vec
           scale_factor: scaling factor to multiply the normalized values by
           clipping: if True, the normalized values beyond the range are all clipped to [0, scale_factor]

       Returns:
           The normalized tuple
    """
    assert len(vec) == len(ranges)

    normalized_vec = tuple()
    for index, value in enumerate(vec):
        min_val, max_val = ranges[index]
        val = (value - min_val) / (max_value - min_val)
        val = np.clip(val * scale_factor, 0, scale_factor) if clipping else val * scale_factor
        normalized_vec += (val, )

    return normalized_vec


# Methods
# used in pyglet_utils and related to SimpleIntersectionEnv.


def get_APs(env, index, *args):
    """Retrieve atomic proposition data from the env.

    Args:
        env: SimpleIntersectionEnv object
        index: vehicle index
        args: atomic proposition names (variable arguments)

    Returns:
        multiline string with the required data
    """
    properties = []
    for arg in args:
        properties.append('%s: %s' % (arg, env.vehs[index].APs[arg]))

    return "\n" + "\n".join(properties) + "\n"


# TODO: env and index info are not used for now. Remove or implement it.
def get_veh_attributes(env, index, *args):
    """Retrieve vehicle attributes from env.

    Args:
        env: SimpleIntersectionEnv object
        index: vehicle index
        args: atomic proposition names (variable arguments)

    Returns:
        multiline string with the required data
    """
    attributes = []
    for arg in args:
        attributes.append("%s: %s" % (arg, round(eval("env.vehs[index].%s" % arg), 2)))

    return "\n" + "\n".join(attributes) + "\n"

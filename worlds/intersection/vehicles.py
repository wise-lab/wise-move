import numpy as np

from verifier import AtomicPropositions
from vehicle_models import KinematicBicycle
from utilities import BoundingBox as bb_utils
from utilities import normalize_radian

from .constants import constants as consts


class VehicleState(KinematicBicycle):
    """A class that defines the vehicle's (cont.) state variables and provides
    a mean to calculate the next state.

    * which_route: the route the vehicle follows. It has to be
                   either horizontal ('h') or vertical ('v');
    * which_dir: the direction of the vehicle within the route. It has to
                 be either 'forward' or 'backward'
    * x, y: the x and y position of the vehicle;
    * theta: the heading angle of the vehicle;
    * v: the velocity along the heading angle;
    * steer: the steering angle;
    * (a, steer_rate): the acceleration and the rate of change of the
        steering angle. If u = (u[0], u[1]) in step is in the reasonable
        range, this is equal to the previous input u at the previous step.
    """

    def __init__(self, which_route, lane_i, pos_in_lane, v, steer, mode='fast'):
        """The constructor which sets the properties of the class.

        Args:
            which_route: the route the vehicle follows. It has to be
                either horizontal ('h') or vertical ('v');
            lane_i: the lane index (either LEFT or RIGHT);
            pos_in_lane: the 1D position in the lane;
            (v, steer): the velocity and the steering angle of the
                vehicle, respectively.
            mode: either 'fast' (use 1D Euler method when steer = 0) or
                'normal'.
        """

        assert which_route in ('h', 'v')
        assert lane_i in (0, 1)

        self.mode = mode
        self.which_route = which_route

        if which_route is 'h':
            assert (consts.rd.h.start_pos <= pos_in_lane <= consts.rd.h.end_pos)
            lane = consts.rd.h.lanes[lane_i]
            x = pos_in_lane
            y = lane.centre
            theta = 0.0

        else:  # the case of "which_route is 'v'"
            assert (consts.rd.v.start_pos <= pos_in_lane <= consts.rd.v.end_pos)
            lane = consts.rd.v.lanes[lane_i]
            y = pos_in_lane
            x = lane.centre
            theta = - np.sign(consts.rd.v.start_pos) * np.pi / 2.0

        if lane.type is 'backward':
            theta += np.pi
            theta = normalize_radian(theta)

        self.theta_ref = theta
        self.which_dir = lane.type

        super().__init__(x, y, theta, v, steer,
                         consts.VEHICLE_SIZE, consts.VEHICLE_WHEEL_BASE,
                         consts.MAX_STEERING_ANGLE, consts.MAX_STEERING_ANGLE_RATE,
                         a_min=consts.MIN_ACCELERATION, a_max=consts.MAX_ACCELERATION)

    def step(self, u, dt=consts.DT):
        """Calculate the next state and update the other properties for the
        given input u.

        Args:
            u: a tuple or an array whose first element corresponds to the
                acceleration and the second element corresponds to the rate
                of change of the steering angle.
        """

        if self.mode is 'fast':
            assert u[1] == 0 and self.steer == 0 and self.theta == self.theta_ref

            self.a = np.clip(u[0], self.a_min, self.a_max)
            self.steer_rate = 0

            if self.which_route == 'h':
                self.x += self.v * dt if self.which_dir == 'forward' else -self.v * dt

            else:  # the case of "which_route is 'v'"
                self.y += self.v * dt if self.which_dir == 'forward' else -self.v * dt

            self.v = np.clip(self.v + self.a * dt, 0, self.v_max)
        else:
            super().step(u, dt)


class Vehicle(VehicleState):
    """A vehicle class that inherits VehicleState (so, contains the
    (continuous) state info.), and also have discrete state variables. (i.e.,
    atomic propositions (APs)).

    Properties are the same as of VehicleState +
        * APs: the class AtomicPropositions that stores all the APs
            of the vehicle as True or False.
        * waited_count: the number of time steps the vehicle stays
            in the stop region; it is -1 when the car is not in the stop region;
    """

    def __init__(self, which_route, lane_i=None, v_upper_lim=consts.rd.speed_limit):
        """The constructor which sets the properties of the class, with vehicle
        randomly posed with random speed.

        Args:
            which_route: the direction of the vehicle. It has to be either
                horizontal ('h') or vertical ('v');
            lane_i: the lane number (0 is the right lane, 1 means the left
                lane); if it's equal to None, it chooses 0 or 1
                uniformly randomly.
            v_upper_lim: the upper bound of the random speed of the vehicle
        """
        self.APs = AtomicPropositions(consts.APS_KEYS)
        self.waited_count = -1
        self.random_reset(which_route, lane_i, v_upper_lim)

    def reset(self, which_route, lane_i, pos_in_lane, v, steer):
        """Reset the properties of the class including vehicle's state
        variables; this reset method updates all the atomic propositions and
        the discrete state (i.e., waited_count)."""

        super().__init__(which_route, lane_i, pos_in_lane, v, steer)

        # Initialization of the discrete states (has_entered_stop_region,
        # has_stopped_in_stop_region, and waited_count).

        self.init_local_discrete_var()

        APs = self.APs
        # The following APs are updated globally, not by any method
        # within Vehicle class. Initially, highest_priority and
        # intersection_is_clear are set to False so that the vehicle
        # has to wait before intersection when both APs are not updated.
        # the others are also initially set to False.
        APs['highest_priority'] = False
        APs['intersection_is_clear'] = False
        APs['veh_ahead'] = False
        APs['target_lane'] = False
        APs['veh_ahead_slow'] = False
        APs['veh_ahead_too_close'] = False
        APs['veh_ahead_has_passed_stop_region'] = False

    def random_reset(self, which_route, lane_i=None, v_upper_lim=consts.rd.speed_limit):
        """Randomly reset the properties of the class including vehicle's state
        variables; the speed is [and the lane number can be] chosen randomly.

        The parameters of the method are exactly same as those in the
        constructor.
        """
        lane_i = np.random.randint(0, 2) if lane_i is None else lane_i

        route = consts.rd.h if which_route is 'h' else consts.rd.v
        pos_in_lane = (route.end_pos - route.start_pos) * np.random.rand() + route.start_pos

        # v is randomly chosen within the range [0, v_upper_lim]
        v = v_upper_lim * np.random.rand()

        self.reset(which_route, lane_i, pos_in_lane, v, 0)

    @property
    def _lane(self):
        return self.y > 0 if self.which_route == 'h' else self.x < 0

    def init_local_discrete_var(self):

        if self.which_route == 'h':
            pos_long = self.x
            lane = consts.rd.h.lanes[self._lane]

        else:  # the case of "which_route is 'v'"
            pos_long = self.y
            lane = consts.rd.v.lanes[self._lane]

        APs = self.APs

        if (lane.stop_region is None) or (self.which_dir != lane.type):
            APs['has_entered_stop_region'] = False
            APs['has_stopped_in_stop_region'] = False

        else:
            bdy = lane.stop_region.boundary
            if self.which_dir == 'forward':
                APs['has_entered_stop_region'] = True if bdy[0] <= pos_long else False
                APs['has_stopped_in_stop_region'] = True if bdy[1] < pos_long else False

            else:  # when "which_dir is 'backward'"
                APs['has_entered_stop_region'] = True if pos_long <= bdy[1] else False
                APs['has_stopped_in_stop_region'] = True if pos_long < bdy[0] else False

        #: the number of time steps the vehicle stays in the
        #  stop region; it is -1 when the car is not in the stop region;
        self.waited_count = -1

        self.update_local_APs()

        # Update waited_count after reset;
        # so waited_count is initially either -1 or 0.
        self.update_waited_count()

    def update_local_APs(self):
        """Update local atomic propositions (related to the vehicle, road
        geometry, and road environment); based upon the continuous and the
        previous discrete states, all the atomic propositions within the class
        are updated, except the global ones ('highest_priority',
        'intersection_is_clear', and 'veh_ahead')."""

        APs = self.APs

        APs['stopped_now'] = True if self.v <= pow(10, -10) else False
        APs['over_speed_limit'] = True if self.v > consts.rd.speed_limit else False

        if self.which_route is 'h':
            route = consts.rd.h
            pos_long= self.x
            pos_lat = self.y
            intersection_len = consts.rd.intersection_width_w_offset

        else:  # the case of "which_route is 'v'"
            route = consts.rd.v
            pos_long = self.y
            pos_lat = self.x
            intersection_len = consts.rd.intersection_height_w_offset

        APs['lane'] = self._lane

        bdy = route.boundary
        APs['on_route'] = True if bdy[0] <= pos_lat <= bdy[1] else False

        lane = route.lanes[APs['lane']]

        if (lane.stop_region is None) or (self.which_dir != lane.type):
            APs['in_stop_region'] = False
            APs['before_but_close_to_stop_region'] = False

        else:
            bdy = lane.stop_region.boundary
            APs['in_stop_region'] = True if bdy[0] <= pos_long <= bdy[1] else False

            bdy = lane.stop_region.ahead_boundary
            APs['before_but_close_to_stop_region'] = True if bdy[0] < pos_long < bdy[1] else False

        APs['has_entered_stop_region'] |= APs['in_stop_region']
        APs['has_stopped_in_stop_region'] |= APs['stopped_now'] and APs['in_stop_region']

        APs['parallel_to_lane'] = True if abs(self.theta - self.theta_ref) <= 0.1 else False

        if consts.rd.intersection_exists:
            if (abs(self.x) <= consts.rd.intersection_width / 2.0 and
                abs(self.y) < consts.rd.intersection_height_w_offset / 2.0) or \
               (abs(self.y) <= consts.rd.intersection_height / 2.0 and
                abs(self.x) < consts.rd.intersection_width_w_offset / 2.0):
                APs['in_intersection'] = True
            else:
                APs['in_intersection'] = False

            if lane.type == 'forward':
                APs['before_intersection'] = True if pos_long < - intersection_len / 2.0 else False
            else:
                APs['before_intersection'] = True if pos_long > intersection_len / 2.0 else False

        else:
            APs['in_intersection'] = False
            APs['before_intersection'] = False

    def update_waited_count(self):
        """
        Increase waited_count if the vehicle is in the stop region on
        the same route; otherwise set waited_count = -1.
        """
        if self.APs['stopped_now'] and self.APs['in_stop_region']:
            self.waited_count += 1
        else:
            self.waited_count = -1

    def step(self, u, dt=consts.DT):
        """Calculate the next state and update the other properties for the
        given input u. NOTE: the global atomic propositions ('highest_priority'
        and 'intersection_is_clear') are not updated.

        Args:
            u: a tuple or an array whose first element corresponds to the
                acceleration and the second element corresponds to the rate
                of change of the steering angle.
        """

        super().step(u, dt)
        self.update_local_APs()
        self.update_waited_count()

    def is_within_road_boundaries(self, route):
        """Checks if this vehicle is within road boundaries given a route.

        Args:
            route: A Route class defined in road.py

        Returns:
            True if vehicle is within road boundaries, False otherwise
        """

        bb = self.bounding_box
        bdy = route.boundary
        # get right and left road boundaries depending on whether 'h' or 'v'
        if route.type is 'h':
            boundary1_line = np.array([[route.start_pos, bdy[0]], [route.end_pos, bdy[0]]])
            boundary2_line = np.array([[route.start_pos, bdy[1]], [route.end_pos, bdy[1]]])

        else:  # the case of "which_route is 'v'"
            boundary1_line = np.array([[bdy[0], route.start_pos], [bdy[0], route.end_pos]])
            boundary2_line = np.array([[bdy[1], route.start_pos], [bdy[1], route.end_pos]])

        # Find side of bounding box w.r.t boundary1
        side_wrt_boundary1 = bb_utils.localize_bounding_box_wrt_line(boundary1_line, bb)

        # Find side of bounding box w.r.t boundary2
        side_wrt_boundary2 = bb_utils.localize_bounding_box_wrt_line(boundary2_line, bb)

        # if bb is on same side w.r.t both boundaries, it means it is
        # outside the boundaries
        offroad = False if side_wrt_boundary1 * side_wrt_boundary2 >= 0 else True

        return offroad

    def is_within_lane_boundaries(self):
        """Checks if this vehicle is within boundaries of the larget lane.

        Returns:
            True if vehicle is within road boundaries, False otherwise
        """

        bb = self.bounding_box

        # get the lane boundaries depending on whether 'h' or 'v'
        if self.which_route is 'h':
            route = consts.rd.h
            bdy = route.lanes[self.APs['target_lane']].boundary
            boundary1_line = np.array([[route.start_pos, bdy[0]], [route.end_pos, bdy[0]]])
            boundary2_line = np.array([[route.start_pos, bdy[1]], [route.end_pos, bdy[1]]])

        else:
            route = consts.rd.v
            bdy = route.lanes[self.APs['target_lane']].boundary
            boundary1_line = np.array([[bdy[0], route.start_pos], [bdy[0], route.end_pos]])
            boundary2_line = np.array([[bdy[1], route.start_pos], [bdy[1], route.end_pos]])

        # Find side of bounding box w.r.t boundary1
        side_wrt_boundary1 = bb_utils.localize_bounding_box_wrt_line(boundary1_line, bb)

        # Find side of bounding box w.r.t boundary2
        side_wrt_boundary2 = bb_utils.localize_bounding_box_wrt_line(boundary2_line, bb)

        # if bb is on same side w.r.t both boundaries, it means it is
        # outside the boundaries
        offlane = False if side_wrt_boundary1 * side_wrt_boundary2 >= 0 else True

        return offlane

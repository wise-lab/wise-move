from numpy import rad2deg
from ..constants import constants as consts
from .pyglet_utils import road2image
from .pyglet_utils import Shape, Image, Text


class VehicleNetworkCross(Shape):
    """Vehicle network for the cross scenario.

    This scenario has just one horizontal lane and one vertical lane.
    Relevant constants are used from the constants file.
    """

    def __init__(self, image_url, env):
        """Constructor for VehicleNetworkCross.

        Args:
                image_url: url for the image file, relative to the graphics folder
                env: SimpleIntersectionEnv object which should be utilized for positions and
                        velocities.
        """

        self.env = env
        self.vehs_sprites = []
        self.ego_pic = Image('ego_vehicle.png', image_type="car", tile_scale=True)
        self.veh_pic = Image('car_agent.png', image_type="car", tile_scale=True)

    def draw(self, ego_info_text):
        """
        Draw the cars.
        TODO: add the code for drawing the lines on the road, e.g.,::

        Args:
                ego_info:_text: the text to be displayed above ego

            pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ("v2f", (0, 320, 640, 320)))

        """
        for i, veh in enumerate(self.env.vehs):
            x = road2image(veh.x, 'h')
            y = road2image(veh.y, 'v')

            sprite = self.ego_pic if (i == consts.EGO_INDEX) else self.veh_pic

            sprite.sprite.x = x
            sprite.sprite.y = y
            sprite.sprite.update(rotation=rad2deg(self.env.vehs[i].theta))
            sprite.draw()

            if i is 0:
                # Put text 'ego' on the ego vehicle.
                label = Text(
                    ego_info_text,
                    color=(255, 255, 255, 255),
                    x=x,
                    y=y + 10,
                    font_size=10)
                label.draw()

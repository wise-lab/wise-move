from .pyglet_utils import Shape, Image, Rectangle
from .pyglet_utils import road2image, draw_all_shapes

from ..constants import constants as consts


class RoadNetworkCross(Shape):
    """Road network for the cross scenario.

    This scenario has just one horizontal lane and one vertical lane.
    Relevant constants are used from the constants file.
    """

    def __init__(self, image_url):
        """Constructor for RoadNetworkCross.

        Args:
                image_url: url for the image file, relative to the graphics folder
        """

        self.hlane_sprites, self.vlane_sprites = [], []

        # Pyglet sprites for drawing horizontal road
        for i in range(consts.NUM_HTILES):
            road_tile = Image(image_url, tile_scale=True)
            self.hlane_sprites.append(road_tile)
            road_tile.sprite.x = (i + 0.5) * consts.NUM_HPIXELS / consts.NUM_HTILES
            road_tile.sprite.y = road2image(0, 'v')

        # Pyglet sprites for drawing vertical road
        for j in range(consts.NUM_VTILES):
            road_tile = Image(image_url, tile_scale=True)
            self.vlane_sprites.append(road_tile)
            road_tile.sprite.x = road2image(0, 'h')
            road_tile.sprite.y = (j + 0.5) * consts.NUM_VPIXELS / consts.NUM_VTILES
            
        # Draw the lane separators. There should be 4 of them, 2 on the
        # horizontal road, 2 on the vertical road.
        originx, originy = road2image(0, 'h'), road2image(0, 'v')
        hlane_width, vlane_width = consts.LANE_WIDTH, consts.LANE_WIDTH
        self.lane_separators = [
            Rectangle(
                0,
                originx - vlane_width,
                originy - consts.LANE_SEPARATOR_HALF_WIDTH,
                originy + consts.LANE_SEPARATOR_HALF_WIDTH,
                color=(200, 200, 200, 255)),
            Rectangle(
                originx + vlane_width,
                consts.NUM_HPIXELS,
                originy - consts.LANE_SEPARATOR_HALF_WIDTH,
                originy + consts.LANE_SEPARATOR_HALF_WIDTH,
                color=(200, 200, 200, 255)),
            Rectangle(
                originx - consts.LANE_SEPARATOR_HALF_WIDTH,
                originx + consts.LANE_SEPARATOR_HALF_WIDTH,
                0,
                originy - hlane_width,
                color=(200, 200, 200, 255)),
            Rectangle(
                originx - consts.LANE_SEPARATOR_HALF_WIDTH,
                originx + consts.LANE_SEPARATOR_HALF_WIDTH,
                originy + hlane_width,
                consts.NUM_VPIXELS,
                color=(200, 200, 200, 255)),
        ]

        # Draw the stop region
        self.stop_regions = list()
        for lane in consts.rd.h.lanes:
            if lane.stop_region is None:
                continue

            stop_region_bdy = lane.stop_region.boundary

            self.stop_regions.append(Rectangle(road2image(stop_region_bdy[0], 'h'),
                                               road2image(stop_region_bdy[1], 'h'),
                                               originy - vlane_width,
                                               originy + vlane_width,
                                               color=(180, 180, 180, 255)))

        for lane in consts.rd.v.lanes:
            if lane.stop_region is None:
                continue

            stop_region_bdy = lane.stop_region.boundary

            self.stop_regions.append(Rectangle(originx - hlane_width,
                                               originx + hlane_width,
                                               road2image(stop_region_bdy[0], 'v'),
                                               road2image(stop_region_bdy[1], 'v'),
                                               color=(180, 180, 180, 255)))

    def draw(self):
        """Draw the horizontal and vertical lane."""
        draw_all_shapes(self.hlane_sprites)
        draw_all_shapes(self.vlane_sprites)
        draw_all_shapes(self.lane_separators)
        draw_all_shapes(self.stop_regions)

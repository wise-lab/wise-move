import pyglet
from pyglet import gl
import numpy as np
import math

from ..constants import constants as consts

# TODO: some of these shapes could be moved to utilities, since these
# are general classes could be used anywhere, not just in env.simple_intersection


class Shape:
    """Shape abstract class."""


class Rectangle(Shape):
    """Rectangle using OpenGL."""

    def __init__(self, xmin, xmax, ymin, ymax, color=(0, 0, 0, 255)):
        """Constructor for Rectangle.

        The rectangle has the four points (xmin, ymin), (xmin, ymax),
        (xmax, ymax), (xmax, ymin).

        Args:
            xmin: minimum x coordinate
            xmax: maximum x coordinate
            ymin: minimum y coordinate
            ymax: maximum y coordinate
            color: rectangle color
        """

        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.color = list(map(lambda x: x / 255.0, color))

    def draw(self):
        """Draw Rectangle object using OpenGL."""
        gl.glBegin(gl.GL_QUADS)
        gl.glColor4f(*self.color)
        gl.glVertex3f(self.xmin, self.ymin, 0)
        gl.glVertex3f(self.xmin, self.ymax, 0)
        gl.glVertex3f(self.xmax, self.ymax, 0)
        gl.glVertex3f(self.xmax, self.ymin, 0)
        gl.glEnd()


class Text(Shape):
    """Holds a pyglet label."""

    def __init__(self,
                 text,
                 x,
                 y,
                 font_size=12,
                 color=(0, 0, 0, 255),
                 multiline=False,
                 width=None):
        """Constructor for Text.

        Args:
                text: the text this label contains
                x: x position
                y: y position in the inverted y frame (computer graphics)
                font_size: font size for text
                color: RGBA color for text
                multiline: whether or not this text is multiline
                width: to be set whenever the text is multiline
        """

        if multiline == True and width is None:
            width = 300
        self.label = pyglet.text.Label(
            text,
            font_size=font_size,
            color=color,
            x=x,
            y=y,
            multiline=multiline,
            width=width)

    def draw(self):
        """Draw Text object."""

        self.label.draw()


class Image(Shape):
    """Holds an image."""

    def __init__(self,
                 image_url,
                 anchor=None,
                 tile_scale=False,
                 image_type=None):
        """Constructor for Image.

        Args:
                image_url: url for the image file, relative to the graphics folder
                anchor: anchor coordinates, if None then by default at the center
                tile_scale: if True, then use H_TILE_SCALE and V_TILE_SCALE defined
                        in the constants
                image_type: could be None (simple image), "bg" (background, ordered
                        group), "car" (represents a car)
        """

        self.image = pyglet.image.load('worlds/intersection/graphics/' +
                                       image_url)

        if anchor is None:
            self.image.anchor_x = self.image.width // 2
            self.image.anchor_y = self.image.height // 2
        else:
            self.image.anchor_x, self.image.anchor_y = anchor

        self.image_type = image_type
        if image_type == "bg":
            self.batch = pyglet.graphics.Batch()
            self.sprite = pyglet.sprite.Sprite(
                self.image,
                batch=self.batch,
                group=pyglet.graphics.OrderedGroup(0))
        else:
            self.sprite = pyglet.sprite.Sprite(self.image)

        if tile_scale == True:
            if self.image_type == "car":
                self.sprite.update(
                    scale_x=consts.H_CAR_SCALE(self.image.width),
                    scale_y=consts.V_CAR_SCALE(self.image.height))
            else:
                self.sprite.update(
                    scale_x=consts.H_TILE_SCALE(self.image.width),
                    scale_y=consts.V_TILE_SCALE(self.image.height))

    def draw(self):
        """Draw Image object."""

        if self.sprite is not None:
            if self.image_type == "bg":
                self.batch.draw()
            else:
                self.sprite.draw()


# Methods (get_veh_attributes, config_Pyglet,
# road2image, draw_all_shapes) related to Pyglet for graphical output.

def config_Pyglet(pyglet, win_x, win_y):
    """Configure pyglet and return the window that can be used by other pyglet
    objects.

    Args:
        win_x: x width of the window
        win_y: y width of the window

    Returns:
        Pyglet window object
    """

    pyglet.options['debug_gl'] = False

    # Use the no arguments version, if you don't have a GPU. Uncomment the
    # line below then.
    # config = pyglet.gl.Config()

    # Otherwise use the following line.
    config = pyglet.gl.Config(sample_buffers=1, samples=4)

    window = pyglet.window.Window(width=win_x, height=win_y, config=config)
    return window


def road2image(pos, which_route):
    """Transform a 1D point to the 1D point in the graphics coordinate for the
    same direction ('h' or 'v').

    Args:
        pos: 1D position in x or y-axis of the road coordinate system
        which_dir: the direction of the coordinate. It has to be either
            horizontal ('h') or vertical ('v').

    Returns:
        the corresponding 1D position in the graphics coordinate.
    """
    if which_route is 'h':
        pos_out = (pos - consts.rd.h.start_pos) * consts.H_SPACE_SCALE
    elif which_route is 'v':
        pos_out = (pos - consts.rd.v.start_pos) * consts.V_SPACE_SCALE
        pos_out = consts.NUM_VPIXELS - pos_out
    else:
        raise AssertionError

    return pos_out


def draw_all_shapes(shapes):
    """Go through all the shapes and call their draw function.

    Args:
        shapes: list of shapes
    """

    for shape in shapes:
        shape.draw()

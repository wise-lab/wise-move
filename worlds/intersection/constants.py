import importlib

constants = None

LEFT = 0
RIGHT = 1 - LEFT


# TODO: find better way so that we don't have to use constants.constants
def set_constants(path_to_import):
    global constants
    constants = importlib.import_module(path_to_import)

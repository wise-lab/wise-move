from .. import Route, Lane, StopRegion

#: whether the intersection exists or not
intersection_exists = True

#: the intersection is posed at (0, 0) in the coordinate.
#  The variable "intersection_pos" indicates this position of
#  the intersection, but not used at all in the current implementation.
intersection_pos = [0.0, 0.0]

#: the horizontal offset between the edges of the intersection and
#  the end points of the stop regions.
#  TODO: Fix the inter-referencing problem between constants.py and road_geokinemetry.py and substitute VEHICLE_SIZE_HALF[0] to 2.5/2.0
intersection_hoffset = 2.5 / 2.0

#: the vertical offset between the edges of the intersection and
#  the end points of the stop regions.
#  TODO: Fix the inter-referencing problem between constants.py and road_geokinemetry.py and substitute VEHICLE_SIZE_HALF[0] to 2.5/2.0
intersection_voffset = 2.5 / 2.0

#: the speed limit of the road environment.
#  This is the only one regarding the road kinemetry.
#  11.176 m/s = 40 km/h
speed_limit = 11.176

#: the lanes on the horizontal route
hlane_width = 3.0
h = Route('h', -75.0, 40.0)
h.lanes = [Lane('forward', hlane_width), Lane('forward', hlane_width)]

#: the lanes on the vertical route
vlane_width = 3.0
v = Route('v', -75.0, 15.0)
v.lanes = [Lane('forward', vlane_width), Lane('forward', vlane_width)]

h_stop_region_width = 5.0
for lane in h.lanes:
    lane.assign_stop_region(v, h_stop_region_width, intersection_hoffset, 35.0)

v_stop_region_width = 5.0
for lane in v.lanes:
    lane.assign_stop_region(h, v_stop_region_width, intersection_voffset, 35.0)

intersection_width = v.width
intersection_height = h.width

intersection_width_w_offset = intersection_width + 2 * intersection_hoffset
intersection_height_w_offset = intersection_height + 2 * intersection_voffset

intersection_width_wo_offset = intersection_width
intersection_height_wo_offset = intersection_height


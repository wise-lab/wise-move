from numpy import inf, sign, random, exp, array

from verifier import LTLProperty, LTLProperties
from worlds.events import Event, LTLEvent, ltl_events, enable, disable, set_rewards

from . import SimpleIntersectionEnv
from .maneuver import Maneuver

from .constants import rd, pi, MAX_ACCELERATION, APS_KEYS
from .features import extract_ego_features, extract_other_veh_features


class TrackSpeed(Maneuver):

    # The cost_weights for training TrackSpeed maneuver.
    cost_weights = 2.5e-3 * array((12.0, 10.0, 2.5, 2.5, 10.0, 100.0, 1.0, 0.0, 0.1, 0.1))

    def __init__(self, env):
        super().__init__(env)

        do_not_stop = LTLProperty('F ( stopped_now )', APS_KEYS, mode='satisfaction')
        do_not_stop = LTLEvent(do_not_stop, state=self.env.unwrapped.ego_ap2int, reward=lambda: -100)
        self.termination_events.update({'LTL: do not stop': do_not_stop})

        self.mode = 'execute'

    def _set_env_params(self, mode):
        super()._set_env_params(mode)
        env_te = self.env.unwrapped.termination_events
        disable(env_te, ('LTL: priority rule', 'LTL: stop region rule', 'LTL: intersection rule'))

        self.env.unwrapped.cost_extra_action_weights_flag = True

        if mode is 'train':
            enable(env_te, 'ego off lane')
            disable(env_te, 'goal achieved')
        else:  # the case "mode = 'test'":
            enable(env_te, 'goal achieved')
            disable(env_te, 'ego off lane')

    def _reset_params(self, mode):
        self.v_ref = rd.speed_limit
        self.target_lane = self.ego.APs['lane']
        if mode in {'train', 'test'}:
            self.time.set_timeout(20)

        enable(self.termination_events, 'LTL: do not stop', condition=(mode == 'train' and self.additional_LTLs_on))

    def generate_training_scenario(self):
        self._generate_scenario(
            ego_pos_range=(rd.intersection_width_w_offset / 2, rd.h.end_pos),
            ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
            v_max_multiplier=0.75,
            ego_heading_towards_lane_centre=True)

    def generate_testing_scenario(self):
        self._generate_scenario(
            ego_pos_range=(rd.h.start_pos, 0),
            ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
            ego_heading_towards_lane_centre=True)

    @staticmethod
    def _features_dim_reduction(features_tuple):
        return extract_ego_features(features_tuple,
                                    'v', 'v_ref', 'e_y', 'steer', 'v tan(steer)/L', 'theta', 'lane', 'a', 'steer_rate')

    @property
    def initiation_condition(self):
        """ TODO: Establish a condition for TrackSpeed which is always true (it's weird that initiation_condition is False under the given conditions above).
            a virtual property from Maneuver.
            As TrackSpeed is a default maneuver, it has to be activated and
            available at any time, state, and condition (refer to
            initiation_condition of Maneuver for the usual case).

            Returns: True.
        """
        return True


class DecelerateToStop(Maneuver):

    # The cost_weights for training DecelerateToStop maneuver.
    cost_weights = 2.5e-4 * array((12.0, 2.5, 2.5, 2.5, 10.0, 15.0, 1.0, 0.0, 0.1, 1.0))

    def __init__(self, env):
        super().__init__(env)

        ltls = LTLProperties(APS_KEYS)
        ltls.add('LTL: do not stop before stop region', 'G ( not stopped_now U in_stop_region )',
                 mode='violation')
        ltls.add('LTL: precondition',
                 'G ( before_intersection )',
                 mode='violation')
        ltls.add('LTL: has stopped in stop region', 'F ( has_stopped_in_stop_region )',
                 mode='satisfaction')

        events = ltl_events(ltls, state=env.unwrapped.ego_ap2int,
                            rewards=(lambda: -100, lambda: None, lambda: None))

        self.termination_events.update(events)
        self.mode = 'execute'

    def reward_in_goal_achieved(self):
        ego = self.ego
        return 100 * exp(- ego.theta**2 - (ego.y - rd.h.lanes[self.target_lane].centre)**2 - 0.25 * ego.steer**2)

    def _set_env_params(self, mode):
        super()._set_env_params(mode)
        env = self.env.unwrapped
        env.cost_extra_action_weights_flag = True
        enable(env.termination_events, 'ego off lane', condition=(mode == 'train'))

    def _reset_params(self, mode):
        self._set_v_ref()
        self.target_lane = self.ego.APs['lane']

        if mode in {'train', 'test'}:
            self.time.set_timeout(40)

        te = self.termination_events

        enable(te, ('LTL: precondition', 'LTL: has stopped in stop region'))
        set_rewards(te, ('LTL: precondition', 'LTL: has stopped in stop region'),
                    (lambda: -100, self.reward_in_goal_achieved))

        # Additional LTLs and conditions for speeding-up training #
        disable(te, 'LTL: do not stop before stop region')
        #########################

        if mode is 'train' and self.additional_LTLs_on:
                enable(te, 'LTL: do not stop before stop region')

        elif mode is 'execute':
            set_rewards(te, ('LTL: precondition', 'LTL: has stopped in stop region'), lambda: None)

    def _update_params(self, mode):
        self._set_v_ref()

    def _set_v_ref(self):
        x = self.ego.x
        stop_region = rd.h.stop_region
        pos_near_stop_region = stop_region.ahead_boundary[0]

        if pos_near_stop_region < x:
            v_ref = (rd.speed_limit / (stop_region.centre - pos_near_stop_region)) * (stop_region.centre - x)
            v_ref = max(v_ref, 0.0)
        else:
            v_ref = rd.speed_limit

        return v_ref

    def generate_training_scenario(self):
        self._generate_scenario(ego_pos_range=(rd.h.stop_region.ahead_boundary[0], -rd.intersection_width_w_offset / 2),
                                ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
                                v_max_multiplier=0.75,
                                ego_heading_towards_lane_centre=True)

    @staticmethod
    def _features_dim_reduction(features_tuple):
        return extract_ego_features(features_tuple,
                                    'pos_near_stop_region', 'v', 'v_ref', 'e_y', 'steer', 'v tan(steer)/L', 'theta',
                                    'lane', 'a', 'steer_rate', 'pos_stop_region', 'not_in_stop_region')


class WaitThenGo(Maneuver):

    # The cost_weights for training WaitThenGo maneuver.
    cost_weights = 2.5e-3 * array((10.0, 10.0, 10.0, 10.0, 10.0, 0.0, 0.05, 0.0, 0.1, 1.0))

    def __init__(self, env):
        super().__init__(env)

        ltls = LTLProperties(APS_KEYS)
        ltls.add('LTL: goal', 'F (in_intersection and highest_priority and intersection_is_clear)', mode='satisfaction')
        ltls.add('LTL: wait rule', 'G ((in_stop_region and stopped_now) '
                                   'U (highest_priority and intersection_is_clear))', mode='violation')

        self.termination_events.update(ltl_events(ltls, state=env.unwrapped.ego_ap2int, rewards=lambda: None))

        self.mode = 'execute'

    def _set_env_params(self, mode):
        super()._set_env_params(mode)
        env = self.env.unwrapped
        env.cost_extra_action_weights_flag = False  # Initially, the ego stopped in the stop region.
        enable(env.termination_events, 'ego off lane', condition=(mode == 'train'))

    def _reset_params(self, mode):
        self._update_params(mode)
        self.target_lane = self.ego.APs['lane']

        te = self.termination_events

        if mode in {'train', 'test'}:
            self.time.set_timeout(20)
            set_rewards(te, ('LTL: goal', 'LTL: wait rule'), (lambda: 100, lambda: -100))
        else:  # the case "mode is 'execute'":
            set_rewards(te, ('LTL: goal', 'LTL: wait rule'), lambda: None)

    def _update_params(self, mode):
        ego = self.ego
        env = self.env.unwrapped
        if ego.APs['highest_priority'] and ego.APs['intersection_is_clear']:
            self.v_ref = rd.speed_limit
            env.cost_extra_action_weights_flag = (mode == 'train')
        else:
            self.v_ref = 0
            env.cost_extra_action_weights_flag = False

    def generate_training_scenario(self):
        ego = self.ego
        n_others = random.randint(0, 4)
        self._generate_scenario(n_others_range=(n_others,)*2,
                                ego_pos_range=rd.h.stop_region.boundary,
                                n_others_stopped_in_stop_region=n_others,
                                ego_v_upper_lim=0,
                                ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
                                ego_heading_towards_lane_centre=True)

        max_waited_count = 0
        min_waited_count = 130
        for veh in self.env.vehs[1:]:
            max_waited_count = max(max_waited_count, veh.waited_count)
            min_waited_count = min(min_waited_count, veh.waited_count)

        min_max_tuple = min_waited_count, max_waited_count
        min_waited_count, max_waited_count = min(min_max_tuple), max(min_max_tuple)

        if random.rand() <= 0.5:
            ego.waited_count = random.randint(0, min_waited_count+1)
        else:
            ego.waited_count = random.randint(min_waited_count, max_waited_count + 21)

        self.env.init_APs(False)

    @staticmethod
    def _features_dim_reduction(features_tuple):
        return extract_ego_features(features_tuple,
                                    'v', 'v_ref', 'e_y', 'steer', 'v tan(steer)/L', 'theta', 'lane', 'a', 'steer_rate',
                                    'pos_stop_region', 'intersection_is_clear', 'highest_priority')


class ChangeLane(Maneuver):

    min_y_distance = rd.hlane_width / 4

    # high_level_extra_reward = -50

    # The cost_weights for training ChangeLane maneuver.
    cost_weights = 2.5e-3 * array((25.0, 5.0, 2.5, 1.0, 10.0, 10.0, 10.0, 0.0, 1.0, 1.0))

    def __init__(self, env):
        super().__init__(env)

        ltls = LTLProperties(APS_KEYS)
        ltls.add('LTL: on route under speed limit', 'G ( on_route and not over_speed_limit )', mode='violation')
        ltls.add('LTL: do not stop', 'F ( stopped_now )', mode='satisfaction')
        ltls.add('LTL: not in stop or intersection regions', 'G ( not in_intersection and not in_stop_region )',
                 mode='violation')

        events = ltl_events(ltls, state=env.unwrapped.ego_ap2int, rewards=(lambda: -100, lambda: -100, lambda: None))
        events.update({'goal achieved': Event(self.goal_achieved, lambda: None, enable=True)})

        self.termination_events.update(events)

        self.mode = 'execute'

    def goal_achieved(self):
        ego = self.ego
        APs = ego.APs
        on_target_lane = APs['lane'] == self.target_lane
        achieved_y_displacement = abs(ego.y - rd.h.lanes[self.target_lane].centre) <= self.min_y_distance

        return on_target_lane and APs['on_route'] and achieved_y_displacement and APs['parallel_to_lane']

    def _set_env_params(self, mode):
        super()._set_env_params(mode)
        env = self.env.unwrapped
        env_te = env.termination_events

        env.cost_extra_action_weights_flag = True
        disable(env_te, ('goal achieved', 'LTL: intersection rule', 'LTL: priority rule', 'LTL: stop region rule'))

    def _reset_params(self, mode):
        self.v_ref = rd.speed_limit
        self.target_lane = not self.ego.APs['lane']

        te = self.termination_events

        disable(te, ('LTL: do not stop', 'LTL: on route under speed limit', 'LTL: not in stop or intersection regions'))

        if mode != 'execute':
            self.time.set_timeout(15)
            te['goal achieved'].reward = lambda: 100
            if mode == 'train' and self.additional_LTLs_on:
                enable(te, ('LTL: do not stop', 'LTL: on route under speed limit'))
        else:
            enable(te, 'LTL: not in stop or intersection regions')

    def generate_training_scenario(self):
        self._generate_scenario(ego_pos_range=(rd.intersection_width_w_offset / 2, rd.h.end_pos),
                                ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
                                v_max_multiplier=0.75)

    def generate_testing_scenario(self):
        self._generate_scenario(ego_pos_range=(rd.intersection_width_w_offset / 2, rd.h.end_pos),
                                ego_perturb_lim=(rd.hlane_width / 10, pi / 10))

    @staticmethod
    def _features_dim_reduction(features_tuple):
        return extract_ego_features(features_tuple,
                                    'v', 'v_ref', 'e_y', 'steer', 'v tan(steer)/L', 'theta', 'lane', 'a', 'steer_rate')


class Follow(Maneuver):

    _target_veh_i = None

    # The cost_weights for training Follow maneuver.
    cost_weights = 5.0e-4 * array((3.0, 10.0, 3.0, 3.0, 2.0, 20.0, 1.0, 20.0, 1.0, 1.0))

    def __init__(self, env):
        super().__init__(env)

        ltls = LTLProperties(APS_KEYS)
        ltls.add('LTL: precondition', 'G( veh_ahead U ((in_stop_region or before_but_close_to_stop_region) '
                                      'and veh_ahead_has_passed_stop_region))', mode='violation')
        ltls.add('LTL: veh not too close', 'F ( veh_ahead_too_close )',
                 mode='satisfaction'),
        ltls.add('LTL: do not stop', 'G( not stopped_now U (veh_ahead_slow or in_stop_region) )',
                 mode='violation')
        ltls.add('LTL: stop at the stop region', 'F ( has_stopped_in_stop_region and in_stop_region and stopped_now )',
                 mode='satisfaction'),

        events = ltl_events(ltls, state=env.unwrapped.ego_ap2int, rewards=lambda: None)

        self.termination_events.update(events)
        self.mode = 'execute'

    def reward_in_stop_at_the_stop_region(self):
        ego = self.ego
        return 100 * exp(- 2 * ego.theta**2 - (ego.y - rd.h.lanes[self.target_lane].centre)**2 - ego.steer**2)

    def reward_in_env_goal_achieved(self):
        ego = self.ego
        veh_ahead_i, veh_ahead_dist = self.env.get_v2v_distance()
        if veh_ahead_i is not None and veh_ahead_dist <= (self.env.veh_ahead_min + self.env.veh_ahead_max) / 2:
            dv = ego.v - self.env.vehs[veh_ahead_i].v
            ittc = max(dv, 0) / veh_ahead_dist
        else:
            ittc = 0.0

        return 100 * exp(- 2 * ittc**2 - 2 * ego.theta**2
                         - (ego.y - rd.h.lanes[self.target_lane].centre)**2 - ego.steer**2)

    def _set_env_params(self, mode):
        super()._set_env_params(mode)
        env = self.env.unwrapped
        env_te = env.termination_events
        env.cost_extra_action_weights_flag = True

        enable(env_te, 'ego off lane', condition=(mode == 'train'))
        enable(env_te, 'goal achieved')
        env_te['goal achieved'].reward = self.reward_in_env_goal_achieved

    def _reset_params(self, mode):
        self._set_v_ref()
        self.target_lane = self.ego.APs['lane']

        te = self.termination_events
        enable(te, ('LTL: precondition', 'LTL: stop at the stop region'))
        disable(te, ('LTL: veh not too close', 'LTL: do not stop'))

        if mode != 'execute':
            self.time.set_timeout(20)
            set_rewards(te, ('LTL: precondition', 'LTL: stop at the stop region'),
                        (lambda: -100, self.reward_in_stop_at_the_stop_region))

            if mode == 'train' and self.additional_LTLs_on:
                enable(te, ('LTL: veh not too close', 'LTL: do not stop'))
                set_rewards(te, ('LTL: veh not too close', 'LTL: do not stop'), lambda: -100)

    def generate_training_scenario(self):
        self._generate_scenario(n_others_range=(1, 6), v_max_multiplier=0.75,
                                ego_perturb_lim=(rd.hlane_width / 10, pi / 10),
                                veh_ahead_scenario=True,
                                ego_heading_towards_lane_centre=True)

    def _update_params(self, mode):
        self._set_v_ref()

    def _set_v_ref(self):
        self._target_veh_i, _ = self.env.unwrapped.get_v2v_distance()

        ego = self.ego
        stop_region = rd.h.stop_region
        pos_near_stop_region = stop_region.ahead_boundary[0]

        if ego.APs['before_intersection'] and pos_near_stop_region < ego.x:
            self.v_ref = (rd.speed_limit / (stop_region.centre - pos_near_stop_region)) * (stop_region.centre - ego.x)
            self.v_ref = max(self.v_ref, 0.0)
        else:
            self.v_ref = rd.speed_limit

        if self._target_veh_i is not None:
            veh_ahead = self.env.unwrapped.vehs[self._target_veh_i]
            self.v_ref = min(self.v_ref, veh_ahead.v)

    def _features_dim_reduction(self, features_tuple):
        ego_features = extract_ego_features(features_tuple,
                                            'pos_near_stop_region', 'v', 'v_ref', 'e_y', 'steer', 'v tan(steer)/L',
                                            'theta', 'lane', 'a', 'steer_rate', 'pos_stop_region', 'not_in_stop_region')

        if self._target_veh_i is not None:
            # TODO: include waited feature and train Follow in ./backends again.
            veh_ahead_f = list(extract_other_veh_features(features_tuple, self._target_veh_i,
                                                          'rel_x', 'rel_y', 'v', 'a'))

            veh_ahead_f[1] = 40 * veh_ahead_f[1] / (2 * rd.h.lanes[self.target_lane].width) + 0.5
            veh_ahead_f[2] = veh_ahead_f[2] / rd.speed_limit
            veh_ahead_f[3] = veh_ahead_f[3] / (2 * MAX_ACCELERATION) + 0.5

            return ego_features + tuple(veh_ahead_f)

        return ego_features + (1.0, 0.5, 1.0, 0.5)

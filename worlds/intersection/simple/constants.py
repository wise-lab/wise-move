#: pi included in constants.py and used to define MAX_STEERING_ANGLE
from numpy import pi

from . import road_geokinemetry as rd

#: Index of the ego vehicle in the vehicles list
EGO_INDEX = 0

#: Number of pixels in the horizontal direction
NUM_HPIXELS = 640

#: Number of pixels in the vertical direction
NUM_VPIXELS = round(NUM_HPIXELS * rd.v.length / rd.h.length)

#: Horizontal space scale
H_SPACE_SCALE = NUM_HPIXELS / rd.h.length

#: Vertical space scale
V_SPACE_SCALE = NUM_VPIXELS / rd.v.length

#: Number of horizontal tiles
NUM_HTILES = round(rd.h.length / rd.h.width)

#: Number of vertical tiles
NUM_VTILES = round(rd.v.length / rd.v.width)

#: Lambda function that takes in tile width and returns scale_x for road tile
H_TILE_SCALE = lambda w: (NUM_HPIXELS / NUM_HTILES) / w

#: Lambda function that takes in tile height and returns scale_y for road tile
V_TILE_SCALE = lambda h: (NUM_VPIXELS / NUM_VTILES) / h

#: (x,y)-size of the car as a bounding box when\
#  horizontally aligned
VEHICLE_SIZE = [2.5, 1.7]

#: half of the (x,y)-size of the car as a bounding box when\
#  horizontally aligned
VEHICLE_SIZE_HALF = [VEHICLE_SIZE[0] / 2.0, VEHICLE_SIZE[1] / 2.0]

#: vehicle wheel base ( = L in the paper)
VEHICLE_WHEEL_BASE = 1.7

#: maximum acceleration
MAX_ACCELERATION = 2.0

#: maximum acceleration
MIN_ACCELERATION = -2.0

#: maximum steering angle - 33 deg. (extreme case: 53 deg.)
MAX_STEERING_ANGLE = 33.0 * pi / 108.0

#: maximum rate of change of steering angle
MAX_STEERING_ANGLE_RATE = 1.0

#: the time difference btw. the current and the next time steps.
DT = 0.1

#: time scale of the animation; 1 sec in the road environment
#  corresponds to "time_scale" sec in the animation.
TIME_SCALE = 1

#: Lambda function that takes in car width and returns scale_x for car
H_CAR_SCALE = lambda w: VEHICLE_SIZE_HALF[0] * 2 * H_SPACE_SCALE / w

#: Lambda function that takes in car height and returns scale_y for car
V_CAR_SCALE = lambda h: VEHICLE_SIZE_HALF[1] * 2 * V_SPACE_SCALE / h

#: Lane separator width
LANE_SEPARATOR_HALF_WIDTH = 1

#: Road tile image dimension
ROAD_TILE_IMAGE_DIM = 300

#: Lane width
LANE_WIDTH = ROAD_TILE_IMAGE_DIM * V_TILE_SCALE(ROAD_TILE_IMAGE_DIM) / 2

#: MAX NUMBER OF OTHER VEHICLES
MAX_NUM_OTHER_VEHICLES = 5

#: APS_KEYS contains all of the atomic propositions on the road and the vehicle.
#
#  * in_stop_region: True if the veh. is in stop region
#  * has_entered_stop_region: True if the veh. has entered or passed the stop region
#  * before_but_close_to_stop_region: True if the veh. is before but close to the stop region
#  * stopped_now: True if the veh. is now stopped
#  * has_stopped_in_stop_region: True if the veh. has ever stopped in the stop region before or now
#  * in_intersection: True if the veh. is in the intersection
#  * over_speed_limit: True if the veh. is over the speed limit
#  * on_route: True if the veh. is on the same route to the initial
#  * highest_priority: True if the veh. has the highest priority in the intersection
#  * intersection_is_clear: True if there is no (other) veh. in the intersection
#  * veh_ahead: True if there is a veh. ahead close to it
#  * lane: True if veh. is on the right lane (or side), False if on the left lane (or side)
#  * target_lane: True if veh.'s target lane is on the right lane, False if on the left;
# this is equal to lane except the ego vehicle.
#  * veh_ahead_slow: True if "veh_ahead and (the veh ahead is slow, i.e., its speed <= 20% of the speed limit)"

APS_KEYS = ('in_stop_region', 'has_entered_stop_region', 'before_but_close_to_stop_region', 'stopped_now',
            'has_stopped_in_stop_region', 'in_intersection', 'over_speed_limit', 'on_route', 'highest_priority',
            'intersection_is_clear', 'veh_ahead', 'lane', 'parallel_to_lane', 'before_intersection', 'target_lane',
            'veh_ahead_slow', 'veh_ahead_too_close', 'veh_ahead_has_passed_stop_region')

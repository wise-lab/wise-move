import numpy as np
import argparse
import time, datetime
import sys, os

from backends import DQNLearner
from worlds.intersection.simple.options_env import OptionsEnv
from worlds.intersection.simple.constants import DT


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("logfile.log", "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        self.log.flush()

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass


sys.stdout = Logger()


def mcts_evaluation(depth,
                    nb_traversals,
                    nb_episodes,
                    nb_trials,
                    visualize=False,
                    debug=False,
                    pretrained=True,
                    highlevel_policy_file="highlevel_weights.h5f"):
    """Do RL of the low-level policy of the given maneuver and test it.

    Args:
     depth: depth of each tree search
     nb_traversals: number of MCTS traversals per episodes
     nb_episodes: number of episodes per trial
     nb_trials: number of trials
     visualize: visualization / rendering
     debug: whether or not to show debug information
    """

    # initialize the numpy random number generator
    np.random.seed()

    # load config and maneuvers
    # TODO: set 'ego_pos_range' to exactly or around rd.hlanes.start_pos and run mcts_evaluation to see whether it gives 100% success or not.
    options = OptionsEnv("mcts_config.json", randomize_special_scenarios=True) #TODO: move this argument to config
    options.load_trained_low_level_policies()

    # load high level policy for UCT prediction
    agent = DQNLearner(
        input_shape=(50, ),
        nb_actions=options.get_number_of_nodes(),
        low_level_policies=options.maneuvers)

    if pretrained:
        highlevel_policy_file = "backends/trained_policies/simple_intersection/highlevel/" + highlevel_policy_file
    agent.load_model(highlevel_policy_file)

    # set predictor
    options.set_controller_args(
        predictor=agent.get_softq_value_using_option_alias,
        max_depth=depth,
        nb_traversals=nb_traversals,
        debug=debug)

    # Evaluate
    print("\nConducting {} trials of {} episodes each".format(nb_trials, nb_episodes))
    timeout = 40
    overall_reward_list = []
    overall_success_percent_list = []
    overall_termination_reason_list = {}
    overall_termination_reason_episodic_list = {}
    for num_tr in range(nb_trials):
        num_successes = 0
        reward_list = []
        trial_termination_reason_counter = {}
        trial_termination_reason_episodic_counter = {}
        for num_ep in range(nb_episodes):
            options.reset()
            episode_reward = 0
            start_time = time.time()
            t = 0
            while True:
                options.controller.do_transition()

                features, R, terminal, info = options.controller.step_current_node(visualize_low_level_steps=visualize)
                episode_reward += R
                t += DT
                # print('Intermediate Reward: %f (ego x = %f)' %
                #       (R, options.env.vehs[0].x))
                # print('')

                if terminal or t > timeout:
                    if 'episode_termination_reasons' not in info:
                        info['episode_termination_reasons'] = ''
                        episodes_termination_reasons = set()
                    else:
                        episodes_termination_reasons = set(info['episode_termination_reasons'].split(','))

                    if t > timeout:
                        episodes_termination_reasons |= 'timeout'

                    for termination_reason in episodes_termination_reasons:
                        if termination_reason in trial_termination_reason_counter:
                            trial_termination_reason_counter[termination_reason] += 1
                        else:
                            trial_termination_reason_counter[termination_reason] = 1

                    termination_reason = ','.join(episodes_termination_reasons)
                    if termination_reason in trial_termination_reason_episodic_counter.keys():
                        trial_termination_reason_episodic_counter[termination_reason] += 1
                    else:
                        trial_termination_reason_episodic_counter[termination_reason] = 1

                    break

            end_time = time.time()
            total_time = int(end_time-start_time)
            if options.env.goal_achieved:
                num_successes += 1
            print('Episode {}: Reward = {:.2f} ({})'.format(num_ep, episode_reward,
                                                            datetime.timedelta(seconds=total_time)))
            reward_list += [episode_reward]

        for reason, count in trial_termination_reason_counter.items():
            if reason in overall_termination_reason_list:
                overall_termination_reason_list[reason].append(count)
            else:
                overall_termination_reason_list[reason] = [count]

        for reason, count in trial_termination_reason_episodic_counter.items():
            if reason in overall_termination_reason_list:
                overall_termination_reason_episodic_list[reason].append(count)
            else:
                overall_termination_reason_episodic_list[reason] = [count]

        print("\nTrial {}: Reward = (Avg: {:.2f}, Std: {:.2f}), Successes: {}/{}".\
            format(num_tr, np.mean(reward_list), np.std(reward_list), num_successes, nb_episodes))
        print("Trial {} Termination Reason(s):".format(num_tr))
        for reason, count in trial_termination_reason_counter.items():
            print("{}: {}".format(reason, count))
        print("Trial {} Termination Reason(s) with Episodic Rearrangement:".format(num_tr))
        for reason, count in trial_termination_reason_episodic_counter.items():
            print("{}: {}".format(reason, count))
        print("\n")

        overall_reward_list += reward_list
        overall_success_percent_list += [num_successes * 100.0 / nb_episodes]

    print("===========================")
    print('Overall: Reward = (Avg: {:.2f}, Std: {:.2f}), Success = (Avg: {:.2f}, Std: {:.2f})\n'.\
        format(np.mean(overall_reward_list), np.std(overall_reward_list),
            np.mean(overall_success_percent_list), np.std(overall_success_percent_list)))

    print("Termination reason(s):")
    for reason, count_list in overall_termination_reason_list.items():
        count_list = np.array(count_list)
        while count_list.size != nb_trials:
            count_list = np.append(count_list, 0)
        print("{}: Avg: {:.2f}, Std: {:.2f}".format(reason, np.mean(count_list), np.std(count_list)))

    print("Termination reason(s) with Episodic Rearrangements:")
    for reason, count_list in overall_termination_reason_episodic_list.items():
        count_list = np.array(count_list)
        while count_list.size != nb_trials:
            count_list = np.append(count_list, 0)
        print("{}: Avg: {:.2f}, Std: {:.2f}".format(reason, np.mean(count_list), np.std(count_list)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--evaluate",
        help="Evaluate over n trials, no visualization by default.",
        action="store_true")
    parser.add_argument(
        "--test",
        help="Tests MCTS for 100 episodes by default.",
        action="store_true")
    parser.add_argument(
        "--nb_episodes_for_test",
        help="Number of episodes to test/evaluate. Default is 100",
        default=10,
        type=int)
    parser.add_argument(
        "--visualize",
        help=
        "Visualize the training.",
        action="store_true")
    parser.add_argument(
        "--depth",
        help="Max depth of tree per episode. Default is 5",
        default=5,
        type=int)
    parser.add_argument(
        "--nb_traversals",
        help="Number of traversals to perform per episode. Default is 50",
        default=50,
        type=int)
    parser.add_argument(
        "--nb_episodes",
        help="Number of episodes per trial to evaluate. Default is 100",
        default=100,
        type=int)
    parser.add_argument(
        "--nb_trials",
        help="Number of trials to evaluate. Default is 10",
        default=10,
        type=int)
    parser.add_argument(
        "--debug",
        help="Show debug output. Default is false",
        action="store_true")
    parser.add_argument(
        "--highlevel_policy_in_root",
        help=
        "Use saved high-level policy in root of project rather than backends/trained_policies/highlevel/",
        action="store_true")

    args = parser.parse_args()

    if args.evaluate:
        mcts_evaluation(
            depth=args.depth,
            nb_traversals=args.nb_traversals,
            nb_episodes=args.nb_episodes,
            nb_trials=args.nb_trials,
            visualize=args.visualize,
            debug=args.debug,
            pretrained=not args.highlevel_policy_in_root)
    elif args.test:
        mcts_evaluation(
            depth=args.depth,
            nb_traversals=args.nb_traversals,
            nb_episodes=args.nb_episodes,
            nb_trials=1,
            visualize=True,
            debug=args.debug,
            pretrained=not args.highlevel_policy_in_root)


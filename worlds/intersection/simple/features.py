from numpy import tan, clip, pi
from .constants import rd, MAX_ACCELERATION, VEHICLE_WHEEL_BASE, MAX_NUM_OTHER_VEHICLES, \
    MAX_STEERING_ANGLE, MAX_STEERING_ANGLE_RATE, DT

#: con_ego_feature_dict contains the indexing information regarding elements
#  of the ego vehicle's continuous feature vector.
#
#  * e_stop: the error between the x-pos and the end of the stop region
#       (this is clipped by some threshold).
#  * v: velocity
#  * v_ref: reference velocity
#  * e_y: lateral error btw the ego and the target lane centreline
#  * steer: steering angle
#  * v tan(steer)/L: the term v tan(steer) / L -- see VehicleState in vehicles.py
#  * theta: heading angle
#  * lane: current lane (0: left, 1: right -- just the same to LEFT and RIGHT constants in consts)
#  * e_y,lane: lateral error btw the ego and the current lane centreline
#  * acc: acceleration at the previous time
#  * steer_rate: the time derivative at steering angle at the previous time
#  * pos_stop_region: position in the stop region
con_ego_feature_dict = {
    'pos_near_stop_region': 0,
    'v': 1,
    'v_ref': 2,
    'e_y': 3,
    'steer': 4,
    'v tan(steer)/L': 5,
    'theta': 6,
    'lane': 7,
    'e_y,lane': 8,
    'a': 9,
    'steer_rate': 10,
    'pos_stop_region': 11
}

#: dis_ego_feature_dict contains all indexing information regarding
#  each element of the ego vehicle's discrete feature vector.
#
# * not_in_stop_region: True if the ego is in stop region;
# * has_entered_stop_region: True if the ego has entered the stop region;
# * has_stop_in_stop_region: True if the ego has stopped in the stop region;
# * in_intersection: True if the ego is in the intersection
# * over_speed_limit: True if the speed of the ego is over the speed limit;
# * on_route: True if the ego is on the horizontal route;
# * intersection_is_clear: True when the intersection is clear;
# * highest_priority: True if the ego has the highest priority in the
#       intersection.
dis_ego_feature_dict = {
    'not_in_stop_region': 0,
    'has_entered_stop_region': 1,
    'has_stopped_in_stop_region': 2,
    'in_intersection': 3,
    'over_speed_limit': 4,
    'on_route': 5,
    'intersection_is_clear': 6,
    'highest_priority': 7
}

other_veh_feature_dict = {
    'rel_x': 0,
    'rel_y': 1,
    'v': 2,
    'a': 3,
    'waited': 4
}

ego_feature_dict = dict()
for key in con_ego_feature_dict.keys():
    ego_feature_dict[key] = con_ego_feature_dict[key]
for key in dis_ego_feature_dict.keys():
    ego_feature_dict[key] = dis_ego_feature_dict[key] + len(con_ego_feature_dict)

ego_feature_len = len(ego_feature_dict)
other_veh_feature_len = len(other_veh_feature_dict)


def extract_ego_features(features_tuple, *args):
    return tuple(features_tuple[ego_feature_dict[key]] for key in args)


def extract_other_veh_features(features_tuple, veh_index, *args):
    return tuple(features_tuple[ego_feature_len +
                                (veh_index - 1) * other_veh_feature_len +
                                other_veh_feature_dict[key]] for key in args)


class OtherVehFeatures(object):
    def __init__(self, rel_x, rel_y, v, a, waited_count):
        self.rel_x = rel_x / 40
        self.rel_y = rel_y / 40
        self.v = v / rd.speed_limit
        self.a = a / (2 * MAX_ACCELERATION) + 0.5
        self.waited = waited_count / 100


class Features(object):
    def __init__(self, env):
        """For the current state, get the feature vector of the ego vehicle.

        Args:
            env: the target SimpleIntersectionEnv from which the features are
                 extracted.

        Returns:
            a tuple of the complete features
        """

        ego = env.ego
        v_ref = env.v_ref
        route = rd.h
        target_lane = env.target_lane

        stop_region_end_pos = route.stop_region.boundary[1]
        stop_region_width = route.stop_region.width

        assert (target_lane == True) or (target_lane == False)

        # Normalized continuous feature vector of the ego-vehicle.
        self.con_ego = (
            clip(stop_region_end_pos - ego.x, 0, 50) / 50,
            ego.v / rd.speed_limit,
            v_ref / rd.speed_limit,
            (ego.y - route.lanes[target_lane].centre) / route.lanes[target_lane].width + 0.5,
            ego.steer / (2 * MAX_STEERING_ANGLE) + 0.5,
            ego.v * tan(ego.steer) / (2 * rd.speed_limit * MAX_STEERING_ANGLE) + 0.5,
            ego.theta * 3 / pi,
            ego.APs['lane'],
            (ego.y - route.lanes[ego.APs['lane']].centre) / route.lanes[ego.APs['lane']].width + 0.5,
            ego.a / (2 * MAX_ACCELERATION) + 0.5,
            ego.steer_rate / (2 * MAX_STEERING_ANGLE) + 0.5,
            clip(stop_region_end_pos - ego.x, 0, stop_region_width) / stop_region_width)

        # Discrete feature vector of the ego-vehicle.
        self.dis_ego = (
            not ego.APs['in_stop_region'],
            ego.APs['has_entered_stop_region'],
            ego.APs['has_stopped_in_stop_region'],
            ego.APs['in_intersection'],
            ego.APs['over_speed_limit'],
            ego.APs['on_route'],
            ego.APs['intersection_is_clear'],
            ego.APs['highest_priority'],
        )

        self.other_vehs = ()

        # Features of the other vehicles (relative distance (x,y),
        # velocity, acceleration, waited_j).
        for veh in env.vehs[1:]:
            self.other_vehs += (OtherVehFeatures(veh.x - ego.x, veh.y - ego.y, veh.v, veh.a, veh.waited_count),)

    def reset(self, env):
        self.__init__(env)

    def __getitem__(self, index):
        if index in con_ego_feature_dict:
            return self.con_ego[con_ego_feature_dict[index]]
        elif index in dis_ego_feature_dict:
            return self.dis_ego[dis_ego_feature_dict[index]]
        else:
            return self.other_vehs[index]

    def get_features_tuple(self):
        """continuous + discrete features."""
        feature = self.con_ego + self.dis_ego

        for other_veh in self.other_vehs:
            feature += (other_veh.rel_x, other_veh.rel_y, other_veh.v, other_veh.a, other_veh.waited)

        # Add buffer features to make a fixed length feature vector
        for i in range(MAX_NUM_OTHER_VEHICLES - len(self.other_vehs)):
            feature += (1.0, 0.5, 1.0, 0.5, -0.01)

        return feature

import sys, os, argparse
import numpy as np

if 'simple' in os.getcwd():
    os.chdir('../../..')
else:
    pass  # assuming that the current working directory is the wisemove root directory.

# Set the base wise-move directory to the Python working directory.
sys.path.append(os.getcwd())

from backends.kerasrl.learners import DQNLearner
from worlds.intersection.simple import OptionsEnv
from worlds.intersection.simple import road_geokinemetry as rd
from worlds.intersection.simple.constants import DT


def train(nb_steps, load_weights=False, nb_max_episode_steps=30 / DT, test_after_train=True, visualize=False,
          nb_episodes_for_test=20, tensorboard=False, save_path="highlevel_weights.h5f"):
    """Do RL of the high-level policy and test it.

    Args:
     nb_steps: the number of steps to perform RL
     load_weights: True if the pre-learned NN weights are loaded (for initializations of NNs)
     test_after_train: True to enable testing
     nb_episodes_for_test: the number of episodes for testing
    """
    # initialize the numpy random number generator
    np.random.seed()

    if not (isinstance(load_weights, bool) and isinstance(test_after_train, bool)):
        raise ValueError("Type error: the variable has to be boolean.")

    if not isinstance(nb_steps, int) or nb_steps <= 0:
        raise ValueError("nb_steps has to be a positive number.")

    global options

    agent = DQNLearner(input_shape=(45, ),
                       nb_actions=options.get_number_of_nodes(),
                       target_model_update=1e-3,
                       delta_clip=100,
                       low_level_policies=options.maneuvers,
                       gamma=0.9985)

    if load_weights:
        agent.load_model(save_path)

    if visualize:
        options.visualize_low_level_steps = True

    agent.train(options,
                nb_steps=nb_steps,
                nb_max_episode_steps=nb_max_episode_steps,
                tensorboard=tensorboard)
    agent.save_model(save_path)

    if test_after_train:
        test(nb_episodes_for_test=nb_episodes_for_test)

    return agent


def test(nb_episodes_for_test, trained_agent_file="highlevel_weights.h5f", pretrained=False, visualize=True):
    global options

    agent = DQNLearner(input_shape=(45, ),
                       nb_actions=options.get_number_of_nodes(),
                       low_level_policies=options.maneuvers)

    if pretrained:
        trained_agent_file = "backends/trained_policies/simple_intersection/highlevel/" + trained_agent_file

    agent.load_model(trained_agent_file)
    options.set_controller_policy(agent.predict)

    agent.test_model(
        options, nb_episodes=nb_episodes_for_test, visualize=visualize)


def evaluate(nb_episodes_for_test, nb_trials, trained_agent_file, pretrained, visualize=False):
    global options

    agent = DQNLearner(input_shape=(45, ),
                       nb_actions=options.get_number_of_nodes(),
                       low_level_policies=options.maneuvers)

    if pretrained:
        trained_agent_file = "backends/trained_policies/simple_intersection/highlevel/" + trained_agent_file

    agent.load_model(trained_agent_file)
    options.set_controller_policy(agent.predict)

    def append_termination_reasons_list(current_reasons_list, reasons_lists):
        for reason, count in current_reasons_list.items():
            if reason in reasons_lists:
                reasons_lists[reason].append(count)
            else:
                reasons_lists[reason] = [count]

    def print_termination_reasons(reasons_lists, off_reasons=('goal_achieved')):
        for reason, count_list in reasons_lists.items():
            count_list = np.array(count_list)
            while count_list.size != nb_trials:
                count_list = np.append(count_list, 0)

            if reason not in off_reasons:
                print(f'\t{reason}: {np.mean(count_list):.2f}, (std {np.std(count_list):.2f})')

    success_list = []
    termination_reasons_lists = {}
    atomic_termination_reasons_lists = {}
    print(f'\nConducting {nb_trials} trials of {nb_episodes_for_test} episodes each')
    for trial in range(nb_trials):
        current_success, current_termination_reasons, current_atomic_termination_reasons \
            = agent.test_model(options, nb_episodes=nb_episodes_for_test, visualize=visualize)
        print(f'\nTrial {trial + 1}: success: {current_success}\n')
        success_list.append(current_success)

        append_termination_reasons_list(current_termination_reasons, termination_reasons_lists)
        append_termination_reasons_list(current_atomic_termination_reasons, atomic_termination_reasons_lists)

    success_list = np.array(success_list)
    print(f'\nSuccess: {np.mean(success_list):.2f} (std: {np.std(success_list):.2f})')

    if termination_reasons_lists != atomic_termination_reasons_lists:
        print("Termination reason(s) (episodic):")
        print_termination_reasons(termination_reasons_lists)
        print("Termination reason(s) (atomic):")
    else:
        print("Termination reason(s):")

    print_termination_reasons(atomic_termination_reasons_lists)


def find_good_high_level_policy(nb_steps=25000,
                                load_weights=False,
                                nb_episodes_for_test=100,
                                visualize=False,
                                tensorboard=False,
                                save_path="./highlevel_weights.h5f"):
    max_num_successes = 0
    current_success = 0
    while current_success < 0.95 * nb_episodes_for_test:
        agent = train(
            nb_steps=nb_steps,
            load_weights=load_weights,
            visualize=visualize,
            tensorboard=tensorboard,
            test_after_train=False)
        options.set_controller_policy(agent.predict)

        current_success, termination_reason_counter = agent.test_model(
            options, nb_episodes=nb_episodes_for_test, visualize=visualize)

        if current_success > max_num_successes:
            os.rename(save_path,
                      "highlevel_weights_{}.h5f".format(current_success))
            max_num_successes = current_success


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--train",
        help=
        "Train a high level policy with default settings. Always saved in root folder. Always tests after training",
        action="store_true")
    parser.add_argument(
        "--test",
        help=
        "Test a saved high level policy. Uses backends/trained_policies/simple_intersection/"
        "highlevel/highlevel_weights.h5f by default",
        action="store_true")
    parser.add_argument(
        "--evaluate",
        help="Evaluate a saved high level policy over n trials. "
        "Uses backends/trained_policies/simple_intersection/highlevel/highlevel_weights.h5f by default",
        action="store_true")
    parser.add_argument(
        "--saved_policy_in_root",
        help=
        "Use saved policies in root of project rather than backends/trained_policies/simple_intersection/highlevel/",
        action="store_true")
    parser.add_argument(
        "--load_weights",
        help="Load a saved policy from root folder first before training",
        action="store_true")
    parser.add_argument(
        "--tensorboard",
        help="Use tensorboard while training",
        action="store_true")
    parser.add_argument(
        "--visualize",
        help=
        "Visualize the training. Testing is always visualized. Evaluation is not visualized by default",
        action="store_true")
    parser.add_argument(
        "--nb_steps",
        help="Number of steps to train for. Default is 200000",
        default=200000,
        type=int)
    parser.add_argument(
        "--nb_episodes_for_test",
        help="Number of episodes to test/evaluate. Default is 100",
        default=100,
        type=int)
    parser.add_argument(
        "--nb_trials",
        help="Number of trials to evaluate. Default is 10",
        default=10,
        type=int)
    parser.add_argument(
        "--save_file",
        help=
        "filename to save/load the trained policy. Location is as specified by --saved_policy_in_root",
        default="highlevel_weights.h5f")

    args = parser.parse_args()
    options = None

    if args.train:
        # load options graph
        options = OptionsEnv("config.json", ego_pos_range=(rd.h.start_pos, rd.h.end_pos), randomize_special_scenarios=True)
        options.load_trained_low_level_policies()

        train(args.nb_steps, load_weights=args.load_weights, save_path=args.save_file,
              tensorboard=args.tensorboard, visualize=args.visualize)

    else:
        # load options graph
        options = OptionsEnv("config.json", ego_pos_range=(rd.h.start_pos, rd.h.stop_region.ahead_boundary[0]),
                             randomize_special_scenarios=True)
        options.load_trained_low_level_policies()

    if args.test:
        test(args.nb_episodes_for_test, trained_agent_file=args.save_file, pretrained=not args.saved_policy_in_root)

    if args.evaluate:
        evaluate(args.nb_episodes_for_test, args.nb_trials,
                 trained_agent_file=args.save_file, pretrained=not args.saved_policy_in_root, visualize=args.visualize)

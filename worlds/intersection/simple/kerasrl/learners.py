import numpy as np

from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Flatten, Input, Concatenate, Lambda

from backends.kerasrl.learners import DDPGLearner
from backends.kerasrl.random import ZeroMeanMultiOUProcess

from ..constants import MIN_ACCELERATION, MAX_ACCELERATION, MAX_STEERING_ANGLE_RATE, DT


class DDPGLowLevelPolicyLearner(DDPGLearner):

    def __init__(self, input_shape, **kwargs):
        """The constructor which sets the properties of the class.

        Args:
            input_shape: Shape of observation space, e.g (10,);
            **kwargs: other optional key-value arguments with defaults defined in property_defaults
        """
        # the input dimension is "2" (acceleration, steer_rate)
        super().__init__(input_shape, nb_actions=2, **kwargs)

    @staticmethod
    def __transform_actor_output(x, acc_range, max_steer_rate):
        a = [(acc_range[1] - acc_range[0]) / 2, max_steer_rate]
        b = [np.average([acc_range[0], acc_range[1]]), 0]

        return a * x + b

    @property
    def actor_model(self):
        """ Creates the actor model (Keras Model of actor which takes observation as input and outputs actions).
            This needs to be implemented in the subclass.

        Returns: Keras Model object of actor
        """

        actor = Sequential()
        actor.add(Flatten(input_shape=(1, ) + self.input_shape))
        actor.add(Dense(64, activation='relu', use_bias=False))
        actor.add(Dense(64, activation='relu', use_bias=False))
        actor.add(Dense(self.nb_actions, use_bias=True))
        actor.add(Activation('tanh'))
        actor.add(Lambda(DDPGLowLevelPolicyLearner.__transform_actor_output,
                         arguments={'acc_range': (MIN_ACCELERATION, MAX_ACCELERATION),
                                    'max_steer_rate': MAX_STEERING_ANGLE_RATE}))

        print(actor.summary())

        return actor

    @property
    def critic_model(self):
        """Creates the critic model.

        Returns: a tuple (critic, critic_action_input), where
            critic: Keras Model of critic that takes concatenation of observation and action and outputs
                    a single value
            critic_action_input: Keras Input which was used in creating action input of the critic model.
        """

        action_input = Input(shape=(self.nb_actions, ), name='action_input')
        observation_input = Input(
            shape=(1, ) + self.input_shape, name='observation_input')
        flattened_observation = Flatten()(observation_input)
        x = Concatenate()([action_input, flattened_observation])
        x = Dense(64, activation='relu', use_bias=False)(x)
        x = Dense(64, activation='relu', use_bias=False)(x)
        x = Dense(64, activation='relu', use_bias=False)(x)
        x = Dense(1, use_bias=True)(x)
        critic = Model(inputs=[action_input, observation_input], outputs=x)

        print(critic.summary())

        return critic, action_input

    @property
    def random_process(self):
        """Creates OrnsteinUhlenbeckProcess object as random_process to be used for exploration.

        Returns:    KerasRL OrnsteinUhlenbeckProcess object
        """
        ou_process = ZeroMeanMultiOUProcess(dt=DT)
        ou_process.append(kappa=10, sigma=0.7, sigma_min=0.3, n_steps_annealing=50000)
        ou_process.append(kappa=10, sigma=0.3, sigma_min=0.1, n_steps_annealing=50000)

        return ou_process

from backends.kerasrl.callbacks import Callback


class ManeuverEvaluateCallback(Callback):

    def __init__(self, maneuver):
        self.low_reward_count = 0
        self.mid_reward_count = 0
        self.high_reward_count = 0
        self.maneuver = maneuver

    def on_episode_end(self, episode, logs={}):
        """Called at end of each episode"""
        if logs['episode_reward'] < -50:
            self.low_reward_count += 1
        elif logs['episode_reward'] > 50:
            self.high_reward_count += 1
        else:
            self.mid_reward_count += 1

        super().on_episode_end(episode, logs)

    def on_train_end(self, logs=None):
        print("\nThe total # of episodes: " + str(self.low_reward_count +
                                          self.mid_reward_count +
                                          self.high_reward_count))
        print(" # of episodes with reward < -50: " + str(self.low_reward_count) +
              "\n # of episodes with -50 <= reward <= 50: " + str(self.mid_reward_count) +
              "\n # of episodes with reward > 50: " + str(self.high_reward_count))

        success_count = self.high_reward_count

        print("\n # of success episode: " + str(success_count) + '\n')

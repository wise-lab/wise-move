import sys, os, argparse
import numpy as np

if 'simple' in os.getcwd():
    os.chdir('../../..')
else:
    pass  # assuming that the current working directory is the wisemove root directory.

# Set the base wise-move directory to the Python working directory.
sys.path.append(os.getcwd())

from backends.kerasrl.callbacks import EpisodicDataCollector
from worlds.intersection.simple import OptionsEnv
from worlds.intersection.simple.kerasrl.callbacks import ManeuverEvaluateCallback
from worlds.intersection.simple.kerasrl.learners import DDPGLowLevelPolicyLearner


def train(options, maneuver, nb_steps, RL_method='DDPG', load_weights=False, test_after_train=True,
          visualize=False, nb_episodes_for_test=10, tensorboard=False, without_ltl=False,
          model_savepoints=False,gamma=0.9985):
    """ Do RL of the low-level policy of the given maneuver and test it.

    Args:
     options: an OptionsEnv instance
     maneuver: the name of the maneuver defined in config.json (e.g., 'default').
     nb_steps: the number of steps to perform RL.
     RL_method: either DDPG or PPO2.
     load_weights: True if the pre-learned NN weights are loaded (for initializations of NNs).
     test_after_train: True to enable testing.
     visualize: True to see the graphical outputs during training.
     nb_episodes_for_test: the number of episodes for testing.
    """

    # initialize the numpy random number generator
    np.random.seed()

    if not (isinstance(load_weights, bool) and isinstance(test_after_train, bool)):
        raise ValueError("Type error: the variable has to be boolean.")

    if not isinstance(maneuver, str):
        raise ValueError("maneuver param has to be a string.")

    if not isinstance(nb_steps, int) or nb_steps <= 0:
        raise ValueError("nb_steps has to be a positive number.")

    if RL_method not in ['DDPG', 'PPO2']:
        raise ValueError("Unsupported RL method.")

    options.set_current_node(maneuver)

    options.current_node.additional_LTLs_on = not without_ltl

    # One can create and add here another leaner in order to use different RL methods.
    agent = DDPGLowLevelPolicyLearner(input_shape=(options.current_node.get_reduced_feature_len(), ),
                                      nb_steps_warmup_critic=200, nb_steps_warmup_actor=200, gamma=gamma)

    if load_weights:
        agent.load_model(maneuver + "_weights.h5f")

    options.current_node.mode = 'train'
    agent.train(options.current_node, nb_steps=nb_steps,
                visualize=visualize, verbose=1, log_interval=nb_steps / 4, tensorboard=tensorboard,
                model_savepoints=model_savepoints, savepoints_label=maneuver, savepoints_interval=10000)

    # Save the NN weights for reloading them in the future.
    agent.save_model(maneuver + "_weights.h5f")

    if test_after_train:
        # TODO: the graphical window is not closed before completing the test.
        # Make sure that the mode change from 'train' to 'test' doesn't
        # introduce any erroneous initialization, otherwise,
        # TODO: Reload the corresponding Maneuver at this point.
        options.current_node.mode = 'test'
        agent.test_model(options.current_node, nb_episodes=nb_episodes_for_test)


def test(options, maneuver, nb_episodes_for_test=20, pretrained=False, visualize=True,
         model_savepoints=False, savepoints_number=0, mode='test', agent=None, test_data=None, gamma=0.9985):

    # initialize the numpy random number generator
    np.random.seed()

    options.set_current_node(maneuver)
    options.current_node.reset()

    if agent is None:
        agent = DDPGLowLevelPolicyLearner(input_shape=(options.current_node.get_reduced_feature_len(), ),
                                          nb_steps_warmup_critic=200, nb_steps_warmup_actor=200, gamma=gamma)

    assert not (pretrained and model_savepoints)

    path = maneuver + "_weights{step}.h5f"

    if pretrained:
        path = "backends/trained_policies/simple_intersection/" + maneuver + "/" + path.format(step='')

    elif model_savepoints:
        path = "savepoints/" + path.format(step=f'_{savepoints_number}k')
        print(f'Testing {path}...')

    else:
        path = path.format(step='')

    agent.load_model(path)

    assert mode in {'train', 'test'}
    options.current_node.mode = mode

    if test_data is None:
        test_data = EpisodicDataCollector(env=options.current_node, nb_episodes=nb_episodes_for_test)

    agent.test_model(options.current_node, nb_episodes=nb_episodes_for_test,
                     visualize=visualize, callbacks=[ManeuverEvaluateCallback(maneuver), test_data])

    return {'index': savepoints_number,
            'reward': (test_data.step_rewards, test_data.termination_rewards, test_data.episode_rewards),
            'nb_steps': test_data.nb_steps}


def evaluate(options, maneuver, nb_episodes_for_eval=100, pretrained=False, visualize=False, gamma=0.9985):

    return test(options, maneuver, nb_episodes_for_eval, pretrained, visualize=visualize, gamma=gamma)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--train",
        help="Train a low level policy with default settings. Always saved "
             "in root folder. Always tests after training",
        action="store_true")
    parser.add_argument(
        "--option",
        help="the option to train. "
             "Eg. stop, keeplane, wait, changelane, follow. "
             "If not defined, trains all of the five options")
    parser.add_argument(
        "--test",
        help="Test a saved low level policy. Uses saved policy in "
             "backends/trained_policies/simple_intersection/OPTION_NAME/ by default",
        action="store_true")

    parser.add_argument(
        "--without_additional_ltl_properties",
        help="Train a low level policy without additional LTL constraints.",
        action="store_true")

    parser.add_argument(
        "--evaluate",
        help="Evaluate a saved low level policy over 100 episodes. "
             "Uses backends/trained_policies/simple_intersection/highlevel/highlevel_weights.h5f "
             "by default",
        action="store_true")

    parser.add_argument(
        "--saved_policy_in_root",
        help="Use saved policies in root of project rather than "
             "backends/trained_policies/simple_intersection/OPTION_NAME",
        action="store_true")
    parser.add_argument(
        "--load_weights",
        help="Load a saved policy first before training",
        action="store_true")
    parser.add_argument(
        "--tensorboard",
        help="Use tensorboard while training",
        action="store_true")
    parser.add_argument(
        "--visualize",
        help="Visualize the training. Testing is always visualized.",
        action="store_true")
    parser.add_argument(
        "--nb_steps",
        help="Number of steps to train for. Default is 100000",
        default=100000,
        type=int)
    parser.add_argument(
        "--nb_episodes_for_test",
        help="Number of episodes to test. Default is 10",
        default=10,
        type=int)
    parser.add_argument(
        "--savepoints",
        help="save the model every 10,000 steps of training (available only with --train option).",
        action="store_true")
    parser.add_argument(
        "--gamma",
        help="set the discount factor gamma in reinforcement learning.",
        default=0.9985,
        type=float)
    args = parser.parse_args()

    options = OptionsEnv("config.json")

    target_maneuvers = args.option.split(',') if args.option else list(options.maneuvers.keys())

    if not (args.train or args.test or args.evaluate):
        raise ValueError('Give an option of --train, --test, or --evaluate.')

    mode_str = 'Training' if args.train else \
               'Testing' if args.test else \
               'Evaluating'

    if len(target_maneuvers) > 1:
        print(mode_str + ' {} maneuver(s)...'.format(target_maneuvers) + '\n')

    for maneuver in target_maneuvers:
        print(mode_str + ' {} maneuver...'.format(maneuver) + '\n')
        if args.train:
            # The experiments of the low-level training can be repeated roughly
            # by executing the code (with np_steps=200000 for better result(s))
            train(options, maneuver, args.nb_steps, load_weights=args.load_weights,
                  nb_episodes_for_test=args.nb_episodes_for_test, visualize=args.visualize,
                  tensorboard=args.tensorboard, without_ltl=args.without_additional_ltl_properties,
                  model_savepoints=args.savepoints, gamma=args.gamma)

        elif args.test:
            # load options
            test(options, maneuver, args.nb_episodes_for_test, pretrained=not args.saved_policy_in_root, visualize=True,
                 gamma=args.gamma)

        elif args.evaluate:
            evaluate(options, maneuver, pretrained=not args.saved_policy_in_root, visualize=args.visualize,
                     gamma=args.gamma)

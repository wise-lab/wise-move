import numpy as np

from utilities import Time

from .constants import rd, DT, MAX_ACCELERATION, MAX_STEERING_ANGLE_RATE, MAX_STEERING_ANGLE
from worlds import EpisodicEnv, events


class Maneuver(EpisodicEnv):
    """The abstract class from which each maneuver is defined and inherited.

    In all of the maneuvers, we assume that the ego-vehicle is supposed
    to drive on the 'h'orizontal route.
    """
    __policy = None

    additional_LTLs_on = True

    #: the additional reward given to the high-level learner for choosing
    # the maneuver (each maneuver is an action in the high-level);
    # plays no role in low-level policy training and testing.
    high_level_extra_reward = 0

    #: the cost_weights for training and testing the low-level policies.
    # If None (by default), the subclass imports the cost_weights from the environment.
    # If one wants to specify the cost_weights for training and testing, then one must do in the subclass.
    cost_weights = None

    def __init__(self, env):
        """Constructor for the maneuver.

        Args:
            env: the SimpleIntersectionEnv reference
        """

        #: the associated environment
        # TODO: make a flag parameter for the deep-copy option of env.
        self.env = env

        super().__init__(time=Time(DT, 1.0))

        #: the target lane for each maneuver
        # must be set to True (right) or False (left) in the subclass
        # _init_maneuver_params module
        self.target_lane = None

        #: the reference velocity for each maneuver
        # must be set to a positive value in the subclass
        # _init_maneuver_params module
        self.v_ref = None

    @property
    def ego(self):
        return self.env.unwrapped.ego

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        """
        sets the mode (train, test or execute) and initialize the parameters.
        (Tip: set the mode only at the beginning or the end of the
        training, testing, or executing).

        Args:
            mode: the desired mode ('train', 'test, or 'execute')
        """
        assert mode in {'train', 'test', 'execute'}

        if hasattr(self, '__mode'):
            if self.__mode in {'train', 'test'} and mode == 'execute':
                raise ValueError("Mode transition is not allowed from train/test to execute.")

        self.__mode = mode

        # when mode is 'train' or 'test', initialize the env parameters such as
        # reward_in_goal and terminate_in_reward, if necessary in the subclass
        if mode in {'train', 'test'}:
            self.time.set_timeout(40)  # the default timeout for 'train' and 'test' mode.
            self.show_info = True
            self._set_env_params(mode)

        elif mode in {'execute'}:
            self.show_info = False

        # initialize the maneuver parameters including target_lane, v_ref,
        # cost_weights, timeout, if necessary in the subclass.
        self._reset_params(mode)

    def step(self, u_ego):
        """The main function, increases timeout count and calculates the reward
        based on model- and collision-checking.

        Returns: a tuple (reduced_features_tuple, R, terminal, info), where
            reduced_features_tuple: the features tuple after _features_dim_reduction;
            R: the reward for the transition;
            terminal: True if the next state is the terminal state, False if not;
            info: log info.
        """

        env = self.env.unwrapped

        # Substitute the maneuver-specific variables to those in self.env
        if self.v_ref is not None:
            env.v_ref = self.v_ref
        if self.target_lane is not None:
            env.target_lane = self.target_lane

        features, reward, terminal, env_info = env.step(u_ego, terminal_reward=False)

        if terminal:
            self.terminal_reward.update(env.terminal_reward())

        self._update_params(self.__mode)
        terminal |= self.step_termination_events()

        reward += self.terminal_reward()

        info = self.get_info()
        info.setdefault('timeout', False)
        info.setdefault('episode_termination_reasons', '')

        env_keys = env_info.keys()

        if 'timeout' in env_keys:
            info['timeout'] = info['timeout'] | env_info['timeout']

        if 'episode_termination_reasons' in env_keys:
            if len(env_info['episode_termination_reasons']) > 0:
                if len(info['episode_termination_reasons']) > 0:
                    info['episode_termination_reasons'] = ','.join((info['episode_termination_reasons'],
                                                                    env_info['episode_termination_reasons']))
                else:
                    info['episode_termination_reasons'] = env_info['episode_termination_reasons']

        return self._features_dim_reduction(features), reward, terminal, info

    def get_reduced_feature_len(self):
        """get the length of the feature tuple after applying
        _features_dim_reduction.

        Returns:
            the length of the reduced feature tuple.
        """
        return len(self.get_reduced_features_tuple())

    def get_reduced_features_tuple(self):
        return self._features_dim_reduction(self.env.get_features_tuple())

    @property
    def observation_space(self):
        return self.env.observation_space

    @property
    def action_space(self):
        return self.env.action_space

    def reset(self):
        """when mode = 'train' or 'test':
            a Gym compliant reset function -- reset the environment and maneuver
            as specified below with generate_training/testing_scenario
            and then return the initial features tuple.

        when mode = 'execute', simply resets this maneuver for re-use.

        Returns:
            when mode = 'train' or 'test',
                features tuple after _features_dim_reduction
            when mode = 'execute, nothing.
        """

        assert self.__mode in {'train', 'test', 'execute'}

        if self.__mode is 'train':
            self.generate_training_scenario()
        elif self.__mode is 'test':
            self.generate_testing_scenario()

        # _reset_env_params method is not called under the assumption that
        # the user properly chooses the mode depending on the current purpose.
        self._reset_params(self.__mode)

        super().reset()

        if self.__mode is 'execute':
            # in the execute mode, this reset method is general-purposed,
            # not gym-compliant-purposed.
            return

        if not self.initiation_condition:
            import warnings
            warnings.warn(f"the maneuver {self.__class__.__name__} is ready but the initiation condition doesn't hold.")

        return self._features_dim_reduction(self.env.get_features_tuple())

    # TODO: change/remove mode parameter if we can; it is confused with self._mode.
    def render(self, mode='human'):
        self.env.render(mode)  # Do nothing but calling self.env.render()

    def termination_condition(self):
        # Note: this method "termination_condition" is used only in initiation_condition, not in step method.
        return super().termination_condition() or self.env.termination_condition()

    # TODO: Determine whether this method depends on the external features_tuple or for simplicity, define and use a features_tuple within the class.
    def policy(self, reduced_features_tuple):
        """the low level policy as a map from a feature vector to an action (a,
        steer_rate).

        By default, it'll call low_level_manual_policy below if it's
        implemented in the subclass.
        """
        if self.__policy is None:
            return self.manual_policy(reduced_features_tuple)
        else:
            return self.__policy(reduced_features_tuple)

    def set_policy(self, policy):
        """Sets the __policy as a function which takes in feature vector
        and returns an action (a, steer_rate).

        By default, __policy is None
        """
        assert callable(policy)

        self.__policy = policy

    # A series of virtual functions that might need to be rewritten
    # (if necessary) in each subclass for each specific maneuver.

    def manual_policy(self, reduced_features_tuple):
        """the manually-defined low level policy as a map from a feature vector
        to an action (a, steer_rate).

        _low_level_policy will call this manual policy unless modified
        in the subclass. Implement this in the subclass whenever
        necessary.
        """
        raise NotImplementedError(self.__class__.__name__ + "._low_Level_manual_policy is not implemented.")

    @staticmethod
    def _features_dim_reduction(features_tuple):
        """Reduce the dimension of the features in step and reset.

        Param: features_tuple: a tuple obtained by e.g.,
                self.env.get_features_tuple()
        Return: the reduced features tuple
                (by default, it returns features_tuple itself).
        """
        return features_tuple

    def generate_training_scenario(self):
        raise NotImplementedError(self.__class__.__name__ + ".generate_training_scenario is not implemented.")

    # Override this method in the subclass if some customization is needed.
    def generate_testing_scenario(self):
        self.generate_training_scenario()

    def _generate_scenario(self, **kwargs):
        """generates the scenario for training and testing of low-level policy.
        This method will be used in generate_training_scenario and
        generate_test_scenario in the subclasses.

        Param:
            timeout: the timeout for the scenario (which is infinity by default)
            **kwargs: the arguments of generate_scenario of the environment.
        """

        kwargs.setdefault('n_others_range', (0, 0))
        kwargs.setdefault('ego_pos_range', (rd.h.start_pos, rd.h.end_pos))

        while not self.env.generate_scenario(**kwargs):
            pass

    @property
    def initiation_condition(self):
        """this method specifies the initiation condition (or in a technical
        term, initiation set) of the maneuver.

        Returns True if the condition is satisfied, and False otherwise.
        """

        return not self.termination_condition()

    def _set_env_params(self, mode):
        """Set the env parameters. This is called in @mode.setter method only when mode is changed to 'train' or 'test',
        so no relevant to 'execute' mode.
        """
        # Set the cost_weights of the environment if the cost_weights in the subclass is not None.
        env = self.env.unwrapped
        if self.cost_weights is not None:
            assert len(env.cost_weights) == len(self.cost_weights)
            env.cost_weights = self.cost_weights

    def _reset_params(self, mode):
        """Reset the maneuver parameters.
        This is called in __init__, @mode.setter, and reset methods.
        """
        return  # do nothing unless specified in the subclass

    def _update_params(self, mode):
        """Update the maneuver parameters
        in every calling the gym-compliant 'step' method above.
        """
        return  # do nothing unless specified in the subclass

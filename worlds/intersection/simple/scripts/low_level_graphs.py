from collections import defaultdict
import os, argparse

if 'simple/scripts' in os.getcwd():
    os.chdir('../../../..')
else:
    pass  # assuming that the current working directory is the wisemove root directory.

import numpy as np
import csv
import matplotlib.pyplot as plt

from worlds.intersection.simple import OptionsEnv


def boxplot_points(data):
    q2 = np.median(data)
    mean = np.average(data)
    std = np.std(data)
    m = np.min(data)
    M = np.max(data)
    q1 = np.median([d for d in data if d <= q2])
    q3 = np.median([d for d in data if d >= q2])
    iqr = q3 - q1
    q_min = q1 - 1.5 * iqr
    q_max = q3 + 1.5 * iqr
    return {'q_min': q_min, 'q_1': q1, 'med': q2, 'q_3': q3, 'q_max': q_max,
            'min': m, 'mean': mean, 'std': std, 'max': M}


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--option",
        help="the option to train. "
             "Eg. stop, keeplane, wait, changelane, follow. "
             "If not defined, trains all of the five options")
    args = parser.parse_args()

    maneuvers = args.option.split(',') if args.option else OptionsEnv("config.json").maneuvers.keys()

    for maneuver in maneuvers:
        plt.rc('axes', facecolor='#E6E6E6', edgecolor='none', axisbelow=True)
        plt.rc('legend', facecolor='#FFFFFF')
        plt.rc('patch', facecolor='none', edgecolor='#E6E6E6')

        with open(f'savepoints/{maneuver}_nb_steps_data.csv') as f:
            reader = csv.reader(f, delimiter=',')
            # get header from first row
            headers = next(reader)
            print(f'Headers: {headers}.')
            # get all the rows as a list
            nb_steps_data = list(reader)
            # transform data into numpy array
            nb_steps_data = np.array(nb_steps_data).astype(float)
            nb_steps_step = nb_steps_data[:, 0]
            nb_steps_data = nb_steps_data[:, 1:]

            boxplt = defaultdict(list)

            for d in nb_steps_data:
                boxplt_pts = boxplot_points(d)
                for key, value in boxplt_pts.items():
                    boxplt[key].append(value)

            plt.fill_between(nb_steps_step, boxplt['q_1'], boxplt['q_3'], color='#e41a1c', alpha=0.1, linewidth=0.0)
            plt.plot(nb_steps_step, boxplt['med'], color='#e41a1c', alpha=1.0, linewidth=2.0)
            plt.xlabel('episode')
            plt.ylabel('steps')
            plt.title(f'The Number of Steps per Episode: {maneuver}')
            plt.savefig(f"{maneuver}_steps")
            plt.close()

        reward_types = ('step', 'termination')
        colors = {'step': '#4daf4a', 'termination': '#52be80'}
        gamma = 0.9985
        gammas_data = gamma ** nb_steps_data

        for r_type in reward_types:
            with open(f'savepoints/{maneuver}_{r_type}_reward_data.csv', 'r') as f:
                reader = csv.reader(f, delimiter=',')
                # get header from first row
                headers = next(reader)
                print(f'Headers: {headers}.')
                # get all the rows as a list
                data = list(reader)
                # transform data into numpy array
                data = np.array(data).astype(float)

                graphs = dict()
                graphs[headers[0]] = data[:, 0]
                graphs[headers[1]] = data[:, 1:]

                if r_type == 'step':
                    graphs[headers[1]] /= nb_steps_data
                else:
                    graphs[headers[1]] *= gammas_data

                steps = graphs[headers[0]]
                boxplt = defaultdict(list)

                for d in graphs[headers[1]]:
                    boxplt_pts = boxplot_points(d)
                    for key, value in boxplt_pts.items():
                        boxplt[key].append(value)

                plt.fill_between(steps, boxplt['q_1'], boxplt['q_3'], color=colors[r_type], alpha=0.1, linewidth=0.0)
                plt.plot(steps, boxplt['med'], color=colors[r_type], alpha=1.0, linewidth=2.0)
                plt.xlabel('episode')
                plt.ylabel(f"{'step reward (accumulated)' if r_type == 'step' else 'terminal reward'}")
                plt.title(f"{'Step Reward (accumulated)' if r_type == 'step' else 'Terminal Reward'}: {maneuver}")
                plt.savefig(f"{maneuver}_{'cumulative_step_reward' if r_type == 'step' else 'terminal_reward'}")
                plt.close()

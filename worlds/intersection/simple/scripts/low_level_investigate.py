import os

if 'simple/scripts' in os.getcwd():
    os.chdir('../../../..')
else:
    pass  # assuming that the current working directory is the wisemove root directory.

import numpy as np

from backends.kerasrl.callbacks import EpisodicDataCollector
from worlds.intersection.simple import OptionsEnv
from worlds.intersection.simple.kerasrl.callbacks import ManeuverEvaluateCallback
from worlds.intersection.simple.kerasrl.learners import DDPGLowLevelPolicyLearner


if __name__ == "__main__":

    options = OptionsEnv("config.json")

    # maneuvers = options.maneuvers_alias
    # Uncomment the line below and specify the maneuvers you want to analyze below:
    maneuver = 'keeplane'

    num = 870
    nb_episodes = 100

    options.set_current_node(maneuver)
    options.current_node.reset()
    options.current_node.mode = 'train'

    agent = DDPGLowLevelPolicyLearner(input_shape=(options.current_node.get_reduced_feature_len(),),
                                      nb_steps_warmup_critic=200, nb_steps_warmup_actor=200, gamma=0.9985)

    test_data = EpisodicDataCollector(options.current_node, 100)

    np.random.seed()

    path = maneuver + "_weights{step}.h5f"
    path = "savepoints/" + path.format(step=f'_{num}k')
    print(f'Testing {path}...')

    agent.load_model(path)
    agent.test_model(options.current_node, nb_episodes=nb_episodes,
                     visualize=False, callbacks=[ManeuverEvaluateCallback(maneuver), test_data])

    data = {'index': num,
            'reward': (test_data.step_rewards, test_data.termination_rewards, test_data.episode_rewards),
            'nb_steps': test_data.nb_steps}

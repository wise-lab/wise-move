import os, argparse

if 'simple/scripts' in os.getcwd():
    os.chdir('../../../..')
else:
    pass  # assuming that the current working directory is the wisemove root directory.

import numpy as np

from backends.kerasrl.callbacks import EpisodicDataCollector
from worlds.intersection.simple import OptionsEnv
from worlds.intersection.simple.kerasrl.callbacks import ManeuverEvaluateCallback
from worlds.intersection.simple.kerasrl.learners import DDPGLowLevelPolicyLearner


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--option",
        help="the option to train. "
             "Eg. stop, keeplane, wait, changelane, follow. "
             "If not defined, trains all of the five options")
    args = parser.parse_args()

    options = OptionsEnv("config.json")

    maneuvers = args.option.split(',') if args.option else list(options.maneuvers.keys())

    data_num = 100
    step_increase = 10
    nb_episodes = 100

    training_steps = list()
    step_rewards = np.zeros((data_num + 1, nb_episodes + 1))
    episodic_rewards = np.zeros((data_num + 1, nb_episodes + 1))
    termination_rewards = np.zeros((data_num + 1, nb_episodes + 1))
    nb_steps = np.zeros((data_num + 1, nb_episodes + 1))

    for m in maneuvers:
        options.set_current_node(m)
        options.current_node.reset()
        options.current_node.mode = 'train'

        agent = DDPGLowLevelPolicyLearner(input_shape=(options.current_node.get_reduced_feature_len(),),
                                          nb_steps_warmup_critic=200, nb_steps_warmup_actor=200, gamma=0.9985)
        test_data = EpisodicDataCollector(options.current_node, 100)

        for num in range(0, data_num + 1):
            # initialize the numpy random number generator
            np.random.seed()

            path = m + "_weights{step}.h5f"
            path = "savepoints/" + path.format(step=f'_{num * step_increase}k')
            print(f'Testing {path}...')

            agent.load_model(path)
            agent.test_model(options.current_node, nb_episodes=nb_episodes,
                             visualize=False, callbacks=[ManeuverEvaluateCallback(m), test_data])

            data = {'index': num,
                    'reward': (test_data.step_rewards, test_data.termination_rewards, test_data.episode_rewards),
                    'nb_steps': test_data.nb_steps}

            step_rewards[num, :] = np.concatenate(([data['index']], data['reward'][0]))
            termination_rewards[num, :] = np.concatenate(([data['index']], data['reward'][1]))
            episodic_rewards[num, :] = np.concatenate(([data['index']], data['reward'][2]))
            nb_steps[num, :] = np.concatenate(([data['index']], data['nb_steps']))

        step_reward_data = np.round(np.array(step_rewards), 4)
        np.savetxt(f'savepoints/{m}_step_reward_data.csv', np.asarray(step_reward_data), delimiter=',',
                   header='train-step,step-rewards')

        termination_reward_data = np.round(np.array(termination_rewards), 4)
        np.savetxt(f'savepoints/{m}_termination_reward_data.csv', np.asarray(termination_reward_data), delimiter=',',
                   header='train-step,termination-rewards')

        episodic_reward_data = np.round(np.array(episodic_rewards), 4)
        np.savetxt(f'savepoints/{m}_episodic_reward_data.csv', np.asarray(episodic_reward_data), delimiter=',',
                   header='train-step,episodic-rewards')

        nb_steps_data = np.round(np.array(nb_steps), 4)
        np.savetxt(f'savepoints/{m}_nb_steps_data.csv', np.asarray(nb_steps_data), delimiter=',',
                   header='train-step,steps of episodes')

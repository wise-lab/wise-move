import sys, os, argparse

# Set the base wise-move directory to the Python working directory.
os.chdir(os.path.dirname(os.path.realpath(__file__)) + '/../../..' )
sys.path.append(os.getcwd())

from worlds.intersection.cross import OptionsEnv
from worlds.intersection.cross.kerasrl.learners import DQNLearner as KerasDQNLearner
from worlds.intersection.cross.runner import Runner

def create_options_env(**kwargs):
    return OptionsEnv("config.json", **kwargs)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--train",
        help=
        "Train a high level policy with default settings. Always saved in root folder. Always tests after training",
        action="store_true")
    parser.add_argument(
        "--test",
        help=
        "Test a saved high level policy. Uses a pre-trained policy by default",
        action="store_true")
    parser.add_argument(
        "--evaluate",
        help="Evaluate a saved high level policy over n trials. "
        "Uses a pre-trained policy by default",
        action="store_true")

    parser.add_argument(
        "--saved_policy_in_root",
        help=
        "Use saved policies in root of project rather than a pre-trained policy",
        action="store_true")
    parser.add_argument(
        "--load_weights",
        help="Load a saved policy from root folder first before training",
        action="store_true")
    parser.add_argument(
        "--tensorboard",
        help="Use tensorboard while training",
        action="store_true")
    parser.add_argument(
        "--visualize",
        help=
        "Visualize the training. Testing is always visualized. Evaluation is not visualized by default",
        action="store_true")
    parser.add_argument(
        "--nb_steps",
        help="Number of steps to train for. Default is 200000",
        default=150000,
        type=int)
    parser.add_argument(
        "--nb_episodes_for_test",
        help="Number of episodes to test/evaluate. Default is 100",
        default=1000,
        type=int)
    parser.add_argument(
        "--nb_trials",
        help="Number of trials to evaluate. Default is 10",
        default=1,
        type=int)
    parser.add_argument(
        "--save_file",
        help=
        "filename to save/load the trained policy. Location is as specified by --saved_policy_in_root",
        default="highlevel_weights.h5f")
    parser.add_argument(
        "--test_seed",
        help=
        "the starting seed to use for scenario generation during testing",
        default=2,
        type=int)
    parser.add_argument(
        "--test_type",
        help="0: noisy env, 1: randomized env",
        default=0,
        type=int)
    parser.add_argument(
        "--save_feature_space",
        help=
        "Save feature space to file",
        action="store_true")
    parser.add_argument(
        "--perception",
        help=
        "Model all perception errors",
        action="store_true")
    parser.add_argument(
        "--lag",
        help=
        "Model fixed perception lag",
        action="store_true")
    parser.add_argument(
        "--rand_lag",
        help=
        "Model randomized perception lag",
        action="store_true")
    parser.add_argument(
        "--kf",
        help=
        "Model fixed velocity estimation error",
        action="store_true")
    parser.add_argument(
        "--rand_kf",
        help=
        "Model randomized velocity estimation error",
        action="store_true")
    parser.add_argument(
        "--vanish",
        help=
        "Model vanishing vehicles",
        action="store_true")
    parser.add_argument(
        "--rand_xy",
        help=
        "Model noise for x-y features",
        action="store_true")        
    parser.add_argument(
        "--use_robust",
        help=
        "Use robust rule based policy",
        action="store_true")
    parser.add_argument(
        "--rule_based",
        help=
        "Use simple TTC rule-based policy",
        action="store_true")
    parser.add_argument(
        "--random_policy",
        help=
        "Use a random action selection policy",
        action="store_true")
    parser.add_argument(
        "--use_distilled",
        help=
        "Use the distilled rule-based policy",
        action="store_true")                    
    parser.add_argument(
        "--perturb_feature",
        help="Perturb feature space for analysis. Give column index of the feature.",
        default=-1,
        type=int)

    args = parser.parse_args()

    SAVE_FEATURE_SPACE = args.save_feature_space

    IMPERFECT_PERCEPTION = args.perception
    SYSTEM_LAG=args.lag
    RAND_SYSTEM_LAG=args.rand_lag
    KF_ERROR=args.kf
    RAND_KF_ERROR = args.rand_kf
    VANISH = args.vanish
    XY_NOISE = args.rand_xy

    SPACE_ABSTRACT_LEVEL = 0
    VEHS_ABSTRACT_LEVEL = 0
    TTC_ABSTRACT_LEVEL = 0
    ALL_ABSTRACT_LEVEL = 0

    PERTURB_FEATURE_INDEX = args.perturb_feature

    USE_ROBUST_CROSS_POLICY=args.use_robust
    USE_DISTILLED_CROSS_POLICY=args.use_distilled

    env_default_kwargs = {
        'seed': 1000,
        'space_abstract_level': SPACE_ABSTRACT_LEVEL,
        'vehs_abstract_level': VEHS_ABSTRACT_LEVEL,
        'ttc_abstract_level': TTC_ABSTRACT_LEVEL,
        'imperfect_perception': IMPERFECT_PERCEPTION,
        'system_lag': SYSTEM_LAG,
        'rand_system_lag': RAND_SYSTEM_LAG,
        'kf_error': KF_ERROR,
        'rand_kf_error': RAND_KF_ERROR,
        'vanish_veh': VANISH,
        'xy_noise': XY_NOISE,
        'all_abstract_level': ALL_ABSTRACT_LEVEL
    }

    test_env_kwargs = {
        'seed': args.test_seed-1,
        'save_feature_space': SAVE_FEATURE_SPACE,
        'perturb_feature_index': PERTURB_FEATURE_INDEX,
        'use_robust_rule_based': USE_ROBUST_CROSS_POLICY,
        'use_distilled_rule_based': USE_DISTILLED_CROSS_POLICY,
        'space_abstract_level': SPACE_ABSTRACT_LEVEL,
        'vehs_abstract_level': VEHS_ABSTRACT_LEVEL,
        'ttc_abstract_level': TTC_ABSTRACT_LEVEL,
        'imperfect_perception': IMPERFECT_PERCEPTION,
        'system_lag': SYSTEM_LAG,
        'rand_system_lag': RAND_SYSTEM_LAG,
        'kf_error': KF_ERROR,
        'rand_kf_error': RAND_KF_ERROR,
        'vanish_veh': VANISH,
        'xy_noise': XY_NOISE,
        'all_abstract_level': ALL_ABSTRACT_LEVEL
    }

    options = create_options_env(**env_default_kwargs)

    agent_class = KerasDQNLearner

    agent_default_kwargs = {
        'tensorboard': args.tensorboard
    }

    runner = Runner(agent_class=agent_class, options_class=OptionsEnv, transfer_test_type=args.test_type)

    if args.train:
        runner.train_high_level_policy(
            nb_steps=args.nb_steps,
            save_path=args.save_file,
            visualize=args.visualize,
            test_env_kwargs=test_env_kwargs,
            train_env_kwargs=env_default_kwargs,
            agent_default_kwargs=agent_default_kwargs)

    if args.test:
        runner.test_high_level_policy(
            visualize=args.visualize,
            nb_episodes_for_test=args.nb_episodes_for_test,
            pretrained=not args.saved_policy_in_root,
            trained_agent_file=args.save_file,
            test_env_kwargs=test_env_kwargs,
            save_feature_space=SAVE_FEATURE_SPACE,
            use_rule_based=args.rule_based,
            use_random_policy=args.random_policy)

    if args.evaluate:
        runner.evaluate_high_level_policy(
            visualize=args.visualize,
            nb_episodes_for_test=args.nb_episodes_for_test,
            pretrained=not args.saved_policy_in_root,
            trained_agent_file=args.save_file,
            nb_trials=args.nb_trials,
            test_env_kwargs=test_env_kwargs,
            verbose=1,
            use_rule_based=args.rule_based,
            use_random_policy=args.random_policy
        )

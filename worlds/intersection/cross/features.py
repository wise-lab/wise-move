from .constants import *
import numpy as np
from functools import partial

from collections import deque, namedtuple, defaultdict
from gym.spaces import Box

from ..env_utils import calc_time_to_cover_dist

import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

class ModifiedFeatures():
    # Named tuple to store baselink features of a vehicle
    Vehicle = namedtuple("Vehicle", "id x y v acc theta")

    def __init__(self, env, space_abstract_level=0, vehs_abstract_level=0,
                 ttc_abstract_level=0):
        """
        Args:
            env: the target SimpleIntersectionEnv from which the features are
                 extracted.
            space_abstract_level: level of abstraction for position features, x and y
            vehs_abstract_level: level of abstraction for velocity feature
            ttc_abstract_level: level of abstraction for ttc feature
        """
        # RandomState for seeding feature generation
        self.np_random = np.random

        self.env = env

        # Global variables from constants
        self.num_veh_features = NUM_FEATURES_PER_VEHICLE
        self.num_vehicles_in_feature = NUM_VEHICLES_IN_FEATURE
        self.detection_range = DETECTION_RANGE_Y

        # Feature abstraction variables
        self.abstraction_ratios = [0,1,2,4,6,8,10,12,14,16,18,20]
        self.set_space_abstraction_level(space_abstract_level)
        self.set_veh_abstraction_level(vehs_abstract_level)
        self.set_ttc_abstraction_level(ttc_abstract_level)

        # Perception error model variables
        self.kf_error = False
        self.use_rand_kf_error = True
        self.lag = False
        self.use_rand_lag = True
        self.vanishing_vehicles = False
        self.xy_noise = False
        self.np_random_kf = None
        self.np_random_lag = None
        self.np_random_perturb = None
        self.np_random_vanish = None
        self.np_random_xy = None
        self.dummy_feature = np.zeros((NUM_VEHICLES_IN_FEATURE, NUM_FEATURES_PER_VEHICLE))

        # Reset perception error model
        self.reset_kf_error_vars()
        self.reset_lag_vars()
        self.reset_vanish_vars()
        self.reset_state()

        # Variables for saving feature space
        self.feature_space = np.zeros((0,NUM_VEHICLES_IN_FEATURE,NUM_FEATURES_PER_VEHICLE))
        self.ego_features = np.zeros((0,4))
        self.dyn_obj1_features = np.zeros((0, 3))
        self.scenario_features = {}
        self.save_feature_space_flag = False

        # Variables for feature perturbation
        self.perturb_feature_index = -1
        self.vel_dist = None
        self.ttc_dist = None

    def update_features(self):
        """ Generate features for current env state

        Returns:
            feature numpy array
        """
        feature = self.get_2d_features()

        return feature

    def get_baselink(self, point):
        """ Convert a position from wisemove env to baselink frame of reference

        Args:
            point: (x,y) tuple or array for point


        Returns:
            Baselink poisiton as a python list
        """
        return [- (self.env.ego.x - VEHICLE_SIZE_HALF[0]*3/4 - point[0]), self.env.ego.y - point[1]]

    def reset_kf_error_vars(self):
        """ Reset variables used for velocity error model
        """
        #Reset vars for kf delay model
        if self.use_rand_kf_error and self.np_random_kf is not None:
            self.kf_delay = int(self.np_random_kf.normal(11,1)) # number of steps to delay velocity estimation
            self.kf_base = self.np_random_kf.normal(11, 1) # base value at which estimation starts
        else:
            self.kf_delay = 11
            self.kf_base = 11
        self.kf_id_hash = {} # keep track of vehicles for which velocity has stabilized
        self.kf_delay_hash = defaultdict(int) # keep track of stabilization time for each vehicle

        # vel noise smoothing
        self.vel_hash = defaultdict(partial(deque, maxlen=5)) # past velocities used to obtain a smoother curve

    def reset_lag_vars(self):
        """ Reset variables used perception lag
        """
        if self.use_rand_lag and self.np_random_lag is not None:
            lag_queue_size = int(self.np_random_lag.choice([2,3,4],p=[0.16,0.68,0.16])) # queue size
        else:
            lag_queue_size = 3
        self.lag_queue = deque(maxlen=lag_queue_size) # queue for lagging observations
        while (len(self.lag_queue) < lag_queue_size):  # append dummy features for start of episode
            self.lag_queue.append(self.dummy_feature)

    def reset_vanish_vars(self):
        """ Reset variables used vanishing vehicles
        """
        self.missed_detection_probability = 0.005 # vanishing probability per step
        self.max_missed_detection_window = 10 # max time for which vehicles disappear
        self.missed_detection_hash = dict() # to track which vehicles have disappeared
        self.missed_detection_length_hash = dict() # to track the time of vanishing

    def reset_state(self):
        """ Reset other variables
        """
        self.last_given_id = MAX_NUM_VEHICLES - 1 # reset id generation value
        self.veh_id_hash = dict() # track vehicle IDs
        for n in range(MAX_NUM_VEHICLES):
            self.veh_id_hash[n] = n
        self.veh_teleport_hash = defaultdict(int) # track vehicle teleportation


    def use_imperfect_perception(self, np_random_state=None, np_random_state_kf=None, np_random_state_lag=None,
                                 np_random_state_vanish=None, np_random_state_xy=None):
        """ Turns on impoerfect perception.

        Args:
            np_random_state: RandomState used for scenario generation
            np_random_state_kf: RandomState for velocity error. Must be different from  np_random_state
            np_random_state_lag: RandomState for perception lag error. Must be different from  np_random_state
        """
        if np_random_state is None:
            raise ValueError("Np random state should be provided for imperfect perception. Should NOT be same as generate scenario")
        self.use_kf_error(np_random_state_kf)
        self.use_lag(np_random_state_lag)
        self.use_vanishing_vehicles(np_random_state_vanish)
        self.use_xy_error(np_random_state_xy)

    def use_kf_error(self, np_random_state_kf=None):
        """ Turns on velocity error model.

        Args:
            np_random_state_kf: RandomState for velocity error. Must be different from  np_random_state
        """
        self.kf_error = True

        if np_random_state_kf is None:
            self.use_rand_kf_error = False
        else:
            self.np_random_kf = np_random_state_kf
            self.use_rand_kf_error = True

        self.reset_kf_error_vars()

    def use_lag(self, np_random_state_lag=None):
        """ Turns on perception lag.

        Args:
            np_random_state_lag: RandomState for lag error. Must be different from  np_random_state
        """
        self.lag = True

        if np_random_state_lag is None:
            self.use_rand_lag = False
        else:
            self.np_random_lag = np_random_state_lag
            self.use_rand_lag = True

        self.reset_lag_vars()

    def use_vanishing_vehicles(self, np_random_state_vanish=None):
        """ Turns on vanishing vehicles error.

        Args:
            np_random_state_vanish: RandomState for vanish error. Must be different from  np_random_state
        """
        if np_random_state_vanish is None:
            raise ValueError("Need to pass np random state different from scenario generation")
        else:
            self.vanishing_vehicles = True
            self.np_random_vanish = np_random_state_vanish

        self.reset_vanish_vars()

    def use_xy_error(self, np_random_state_xy=None):
        """ Turns on position noise error.

        Args:
            np_random_state_xy: RandomState for xy feature noise. Must be different from  np_random_state
        """
        if np_random_state_xy is None:
            raise ValueError("Need to pass np random state different from scenario generation")
        else:
            self.xy_noise = True
            self.np_random_xy = np_random_state_xy

    def model_detection_error(self, x, y, theta):
        """ Applies noise to position and orientation.

        Args:
            x: x feature value
            y: y feature value
            theta: orientation feature in degrees

        Returns:
            noisy [x, y, orientation]
        """
        x = x + self.np_random_xy.normal(0, 0.025)
        y = y + self.np_random_xy.normal(0, 0.75)

        theta = theta + self.np_random_xy.normal(0, 0.5)
        theta = (theta + 180) % 360 - 180

        return x, y, theta

    def model_kf_delay(self, idx, vel):
        """ Applies noise to position and orientation.

        Args:
            idx: index of the vehicle
            vel: velocity of the vehicle in km/hr

        Returns:
            vel: velocity with perception error
        """
        # apply kalmann filter error when vehicle is first observed
        # if new idx, means it is newly observed
        if idx not in self.kf_id_hash:
            self.kf_delay_hash[idx] += 1 # count stabilization time
            multiplier = self.kf_delay_hash[idx] # multiplier for stabilization
            if multiplier == 1:
                vel = 3.6  # initially it is mostly near this value in stack
            elif multiplier <= self.kf_delay:
                vel = self.kf_base + (multiplier/self.kf_delay) * (vel - self.kf_base) #scale linearly after that
            else:
                self.kf_id_hash[idx] = 1 # vehicle velocity has stabilized

        if self.use_rand_kf_error:
            # under-estimate velocity by noisy 10%
            vel_temp = max(0,vel - abs(self.np_random_kf.normal(0, 0.1*vel)))
        else:
            # fixed 10% underestimation
            vel_temp = max(0, vel - 0.1*vel)

        # smoothen velocity curve because random under-estimation makes it noise
        self.vel_hash[idx].append(vel_temp)
        vel = np.mean(self.vel_hash[idx])

        return vel

    def get_non_abstract_features(self, feature):
        """ Get back actual feature values if it is abstracted.

        Args:
            feature: feature array

        Returns:
            corrected feature array
        """
        if self.space_abstract_level > 0:
            feature[:,0] = feature[:,0] * self.space_abstract_level
            feature[:, 1] = feature[:, 1] * self.space_abstract_level
        if self.vel_abstract_level > 0:
            feature[:, 3] = feature[:, 3] * self.vel_abstract_level

        return feature

    def set_space_abstraction_level(self, space_level=0):
        """ Set the space abstraction level

        Args:
            space_level: abstraction level
        """
        if space_level < 0 or space_level >= len(self.abstraction_ratios):
            raise ValueError("Abstraction level must be between 0 and {}".format(len(self.abstraction_ratios)))
        self.space_abstract_level = self.abstraction_ratios[space_level]

    def set_veh_abstraction_level(self, vehs_level=0):
        """ Set the velocity abstraction level

        Args:
            space_level: abstraction level
        """
        if vehs_level < 0 or vehs_level >= len(self.abstraction_ratios):
            raise ValueError("Abstraction level must be between 0 and {}".format(len(self.abstraction_ratios)))
        self.vel_abstract_level = self.abstraction_ratios[vehs_level]
        self.acc_abstract_level = self.abstraction_ratios[vehs_level]
        self.orientation_abstract_level = self.abstraction_ratios[vehs_level]

    def set_ttc_abstraction_level(self, ttc_level=0):
        """ Set the ttc abstraction level

        Args:
            space_level: abstraction level
        """
        if ttc_level < 0 or ttc_level >= len(self.abstraction_ratios):
            raise ValueError("Abstraction level must be between 0 and {}".format(len(self.abstraction_ratios)))
        self.ttc_abstract_level = self.abstraction_ratios[ttc_level] / 5

    def set_individual_veh_abstraction_levels(self, vel_abstract_level=0, acc_abstract_level=0,
                                              orientation_abstract_level=0):
        """ Set the vehicle abstraction levels individually
        """
        if vel_abstract_level < 0 or vel_abstract_level >= len(self.abstraction_ratios) or \
                acc_abstract_level < 0 or acc_abstract_level >= len(self.abstraction_ratios) or \
                orientation_abstract_level < 0 or orientation_abstract_level >= len(self.abstraction_ratios):
            raise ValueError("Abstraction level must be between 0 and {}".format(len(self.abstraction_ratios)))
        self.vel_abstract_level = self.abstraction_ratios[vel_abstract_level]
        self.acc_abstract_level = self.abstraction_ratios[acc_abstract_level]
        self.orientation_abstract_level = self.abstraction_ratios[orientation_abstract_level]

    def importance_metric(self, veh):
        """ Importance of a vehicle.

        Args:
            veh: vehicle object
        """
        return abs(veh.y) #importance is distance to ego in y-axis

    def filter_veh(self, veh):
        """ Returns true if vehicle is beyond the set detection range

        Args:
            veh: vehicle object

        Returns:
            true if vehicle is beyond the set detection range, false otherwise
        """
        if (abs(veh.y) < self.detection_range):
            return True
        else:
            return False

    def get_veh_features_list(self, veh):
        """ Gets required vehicle features as a list in the correct units

        Args:
            veh: vehicle object
                its features should have the following:
                x-axis should be positive forward of ego.
                y-axis is -ve to right, +ve to left of ego
                3rd element is orientation in radians. 0 towards same direction as ego, positive clockwise
                vel is in m/s

        Returns:
            [x,y,theta,v]
        """
        return [veh.x, veh.y, np.rad2deg(veh.theta), veh.v*3.6]

    def get_other_veh_features(self, veh):
        """ Generates the features for a vehicle.

        Args:
            veh: vehicle object

        Returns:
            [x,y,theta,v,ttc] in baselink frame of reference
        """
        # get baselink
        x, y, orientation, vel = self.get_veh_features_list(veh)

        # apply velocity estimation error
        if self.kf_error:
            vel = self.model_kf_delay(veh.id, vel)

        # apply position noise
        if self.xy_noise:
            x, y, orientation = self.model_detection_error(x, y, orientation)

        # find ttc
        lane = np.sign(orientation)
        if x != 0 and y != 0 and ((y > 0 and lane == 1) or (y < 0 and lane == -1)) and vel > 0:
            dist_collision = abs(y)
            ttc = min(20, calc_time_to_cover_dist(dist_collision, vel/3.6, 2.0, vel/3.6))
        else:
            ttc = 20

        # Perturb feature space using known feature distributions
        if self.perturb_feature_index > -1 and self.perturb_feature_index < NUM_FEATURES_PER_VEHICLE:
            if self.perturb_feature_index == 0:
                x = self.np_random_perturb.choice([18.2, 12.7])
            elif self.perturb_feature_index == 1:
                y = self.np_random_perturb.uniform(-80, 80)
            elif self.perturb_feature_index == 2:
                orientation = self.np_random_perturb.choice([-90.0, 90.0])
            elif self.perturb_feature_index == 3:
                vel = self.np_random_perturb.choice(self.vel_dist)
            elif self.perturb_feature_index == 4:
                ttc = self.np_random_perturb.choice(self.ttc_dist)

        # Apply feature abstractions
        if self.space_abstract_level > 0:
            x = int(x / self.space_abstract_level)
            y = int(y / self.space_abstract_level)
        if self.vel_abstract_level > 0:
            vel = int(vel / self.vel_abstract_level)
        if self.orientation_abstract_level > 0:
            orientation = np.sign(orientation)
        else:
            orientation = np.radians(orientation)
        if self.ttc_abstract_level > 0:
            ttc = int(ttc / self.ttc_abstract_level) * self.ttc_abstract_level

        return [x, y, orientation, vel, ttc]

    def get_2d_features(self):
        """ Generates the features for all vehicles in detection range. Sort by importance metric

        Returns:
            2d nd-array of size (NUM_VEHICLES_IN_FEATURE, NUM_FEATURES_PER_VEHICLE )
        """
        other_veh_features = np.empty([0,self.num_veh_features])
        vehs_by_importance = np.empty([0,2]) # first col is index, second is importance metric
        filtered_veh_index = 0

        # generate features for each  relevant vehicle
        for index, veh in enumerate(self.vehicles):
            if (self.filter_veh(veh)):
                # to sort by value of importance
                vehs_by_importance = np.append(vehs_by_importance,
                                                [[filtered_veh_index, self.importance_metric(veh)]],
                                                axis=0)

                other_veh_features = np.append(other_veh_features,
                                                    [self.get_other_veh_features(veh)],
                                                    axis=0)
                filtered_veh_index += 1

        # Keep only the top NUM_VEHICLES_IN_FEATURE vehiles (sorted by distance)
        vehs_by_importance = vehs_by_importance[vehs_by_importance[:, 1].argsort()] # sort by importance
        other_veh_features = other_veh_features[vehs_by_importance[:,0].astype(int)][:self.num_vehicles_in_feature]

        if self.save_feature_space_flag:
            self.log_features(other_veh_features)

        # Add buffer features to make a fixed length feature vector
        other_veh_features = np.pad(other_veh_features, ((0,self.num_vehicles_in_feature - len(other_veh_features)),(0,0)), 'constant')

        return other_veh_features

    def log_features(self, feature):
        """ Log features in a file

        Args:
            feature: feature array
        """
        if self.env.ego.v < 0.01:
            for i in range(NUM_VEHICLES_IN_FEATURE - len(feature)):
                feature = np.append(feature, [[0.0]*NUM_FEATURES_PER_VEHICLE],  axis=0)
            self.feature_space = np.append(self.feature_space, [feature], axis=0)
        if (self.env.step_count >= self.env.startup_delay):
            self.ego_features = np.append(self.ego_features, [[self.env.ego.x,
                                                              self.env.ego.y,
                                                              self.env.ego.v,
                                                              self.env.ego.acc]], axis=0)
        if len(self.vehicles) >= 1:
            veh = self.vehicles[0]
            self.dyn_obj1_features = np.append(self.dyn_obj1_features, [[veh.x,
                                                                      veh.y,
                                                                      veh.v]], axis=0)
            if np.sign(veh.theta) > 0:
                self.scenario_features['dist_to_right_traffic'] = veh.x
            else:
                self.scenario_features['dist_to_left_traffic'] = veh.x

        self.scenario_features['stopline_to_road'] = rd.translate_point_stopline_origin(
            [-rd.v.width/2,-rd.h.width/2])[0]

        self.scenario_features['lane_width'] = rd.v.width

    def get_new_id(self):
        """ Generate new id for vehicle

        Returns:
            new id
        """
        self.last_given_id += 1
        return self.last_given_id

    def include_detection_check(self, idx):
        """ Simulate missed detection of vehicle by flagging a vehicle has disappeared.
        Randomly selects if it disappears.

        Args:
            idx: ID of vehicle that may disappear.

        Returns:
            True if this vehicle is perceived by perception
        """
        if idx in self.missed_detection_hash and self.missed_detection_hash[idx] != -1:
            if (self.missed_detection_hash[idx] < self.missed_detection_length_hash[idx]):
                self.missed_detection_hash[idx] += 1
                return False
            else:
                self.missed_detection_hash[idx] = -1
                self.missed_detection_length_hash[idx] = -1
                self.veh_id_hash[idx] = self.get_new_id()
                return True
        elif (self.np_random_vanish.uniform() <= self.missed_detection_probability):
            self.missed_detection_hash[idx] = 1
            self.missed_detection_length_hash[idx] = self.np_random_vanish.randint(1,self.max_missed_detection_window)
            return False
        else:
            return True

    def did_veh_teleport(self, idx, veh):
        """ Checks if a vehicle has teleported.

        Args:
            idx: ID of vehicle
            veh: vehicle object

        Returns:
            True if vehicle just teleported
        """
        if veh.teleport_count != self.veh_teleport_hash[idx]:
            self.veh_teleport_hash[idx] = veh.teleport_count
            return True
        else:
            return False

    def get_veh_id(self, idx, veh):
        """ Get vehicle ID. Generated new ID if vehicle disappeared or teleported.

        Args:
            idx: ID of vehicle
            veh: vehicle object

        Returns:
            ID of vehicle
        """
        if self.did_veh_teleport(idx, veh):
            self.veh_id_hash[idx] = self.get_new_id()

        return self.veh_id_hash[idx]

    def convert_veh_list_to_baselink(self, veh_list):
        """ Convert features of each vehicle to baselink frame and save in a class variable.

        Args:
            veh_list: list of vehicles.

        Returns:
            list of vehicles in baselink
        """
        veh_list_baselink = []
        for idx, veh in enumerate(veh_list):

            # check if the vehicle is detected in the first place.
            # simulate missed detections
            check = True
            if self.vanishing_vehicles:
                check = self.include_detection_check(idx)

            if check:
                x,y = self.get_baselink([veh.x, veh.y])

                # theta is signed and should be within bounds
                orientation = np.rad2deg(veh.theta) - np.rad2deg(self.env.ego.theta)
                orientation = (orientation + 180) % 360 - 180
                orientation = np.radians(orientation)

                # Make new list with namedtuple Vehicle
                veh_list_baselink.append(
                    self.Vehicle(
                        self.get_veh_id(idx, veh),
                        x,
                        y,
                        veh.v,
                        veh.a,
                        orientation
                    )
                )
        return veh_list_baselink

    def get_features_tuple(self):
        """ Gernerate feature array

        Returns:
            feature array
        """
        self.vehicles = self.convert_veh_list_to_baselink(self.env.vehs[1:])
        feature = self.update_features()

        # Simulate perception lag
        if self.lag and self.lag_queue.maxlen != 0:
            feature_temp = self.lag_queue.popleft()
            self.lag_queue.append(feature)
            return feature_temp

        return feature

    def get_observation_space(self):
        """ Observation space for gym-compliance.

        Returns:
            Gym.spaces.Box
        """
        return Box( low=-np.finfo(np.float32).max,
                    high=np.finfo(np.float32).max,
                    shape=(NUM_VEHICLES_IN_FEATURE,self.num_veh_features))

    def save_feature_space(self, file_suffix='wisemove'):
        """ Save features to file.
        """
        np.save(SAVE_FOLDER + 'feature-space-' + file_suffix, self.feature_space)
        np.save(SAVE_FOLDER + 'ego-features-' + file_suffix, self.ego_features)
        np.save(SAVE_FOLDER + 'dynobj1-features-' + file_suffix, self.dyn_obj1_features)
        for key, val in self.scenario_features.items():
            print ("{}: {}".format(key, val))

    def set_perturb_feature_index(self, index, np_random):
        """ Perturb a feature for analysis.

        Args:
            index: column index of the feature to be perturbed
            np_random: RandomState for perturbing
        """
        self.perturb_feature_index = index
        if index == 3:
            self.vel_dist = np.load('./worlds/intersection/cross/data/vel_dist.npy')
        if index == 4:
            self.ttc_dist = np.load('./worlds/intersection/cross/data/ttc_dist.npy')
        self.np_random_perturb = np_random
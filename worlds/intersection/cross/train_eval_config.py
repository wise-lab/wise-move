# Experiments list

class Experiments:
    def __init__(self):
        self.wm = {
            'hyperparameter_name': 'wm',
            'hyperparameter_description': "WM road ego align",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
            }
        }

        self.wm_lag = {
            'hyperparameter_name': 'wm-lag',
            'hyperparameter_description': "WM road ego align with avg lag",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'system_lag': True
            }
        }

        self.wm_kf = {
            'hyperparameter_name': 'wm-kf',
            'hyperparameter_description': "WM road ego align with avg kf",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True
            }
        }

        self.wm_lag_kf = {
            'hyperparameter_name': 'wm-lag-kf',
            'hyperparameter_description': "WM road ego align with avg lag,kf",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True
            }
        }

        self.wm_road = {
            'hyperparameter_name': 'wm-road',
            'hyperparameter_description': "Road noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True,
                'noisy_intersection': True
            }
        }

        self.wm_ego = {
            'hyperparameter_name': 'wm-ego',
            'hyperparameter_description': "ego noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True,
                'ego_noise': True
            }
        }

        self.wm_veh = {
            'hyperparameter_name': 'wm-veh',
            'hyperparameter_description': "veh noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True
            }
        }


        self.wm_noise = {
            'hyperparameter_name': 'wm-noise',
            'hyperparameter_description': "WM avg lag,kf with ego,road noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True,
                'noisy_intersection': True,
                'ego_noise': True
            }
        }

        self.wm_lag_dr = {
            'hyperparameter_name': 'wm-lag-dr',
            'hyperparameter_description': "WM road ego align avg kf with lag dr",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'rand_system_lag': True
            }
        }

        self.wm_no_lagdr = {
            'hyperparameter_name': 'wm-no-lagdr',
            'hyperparameter_description': "WM road ego align with Imperfect Perception but no lagdr",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'system_lag': True,
                'xy_noise': True,
                'vanish_veh': True
            }
        }

        self.wm_kf_dr = {
            'hyperparameter_name': 'wm-kf-dr',
            'hyperparameter_description': "WM road ego align avg lag with kf dr",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'system_lag': True
            }
        }

        self.wm_no_kfdr = {
            'hyperparameter_name': 'wm-no-kfdr',
            'hyperparameter_description': "WM road ego align with Imperfect Perception but no kfdr",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'rand_system_lag': True,
                'xy_noise': True,
                'vanish_veh': True
            }
        }

        self.wm_lag_kf_dr = {
            'hyperparameter_name': 'wm-lag-kf-dr',
            'hyperparameter_description': "WM road ego align with lag and kf dr",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True
            }
        }

        self.wm_vanish = {
            'hyperparameter_name': 'wm-vanish',
            'hyperparameter_description': "WM avg lag,kf with vanishing vehicles",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True,
                'vanish_veh': True
            }
        }

        self.wm_no_vanish = {
            'hyperparameter_name': 'wm-no-vanish',
            'hyperparameter_description': "WM road ego align with Imperfect Perception but no vanish",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True,
                'xy_noise': True
            }
        }

        self.wm_xy = {
            'hyperparameter_name': 'wm-xy',
            'hyperparameter_description': "WM avg lag,kf with xy perception errors",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'kf_error': True,
                'system_lag': True,
                'xy_noise': True
            }
        }

        self.wm_no_xy = {
            'hyperparameter_name': 'wm-no-xy',
            'hyperparameter_description': "WM road ego align with Imperfect Perception but no xy",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True,
                'vanish_veh': True
            }
        }

        self.wm_percept_road = {
            'hyperparameter_name': 'wm-percept-road',
            'hyperparameter_description': "WM ego align with Imperfect Perception and road noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'imperfect_perception': True,
                'noisy_intersection': True
            }
        }

        self.wm_percept_ego = {
            'hyperparameter_name': 'wm-percept-ego',
            'hyperparameter_description': "WM road align with Imperfect Perception and ego noise",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'imperfect_perception': True,
                'ego_noise': True
            }
        }

        self.wm_percept = {
            'hyperparameter_name': 'wm-percept',
            'hyperparameter_description': "WM road ego align with Imperfect Perception",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'imperfect_perception': True
            }
        }

        self.wm_dr = {
            'hyperparameter_name': 'wm-dr',
            'hyperparameter_description': "WM-Noise plus imperfect perception",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'imperfect_perception': True,
                'noisy_intersection': True,
                'ego_noise': True
            }
        }

        self.best = {
            'hyperparameter_name': 'wm-best',
            'hyperparameter_description': "best",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True,
                'xy_noise': True
            }
        }

        self.best1 = {
            'hyperparameter_name': 'wm-best1',
            'hyperparameter_description': "best1",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True,
                'xy_noise': True,
                'all_abstract_level': 1
            }
        }

        self.best2 = {
            'hyperparameter_name': 'wm-best2',
            'hyperparameter_description': "best2",
            'hyperparameter_val_list': [0],
            'train_env_base': {
                'seed': 1000,
                'rand_kf_error': True,
                'rand_system_lag': True,
                'xy_noise': True,
                'all_abstract_level': 2
            }
        }

        self.space_abstract = self.get_space_abstract(train_env_base=self.wm_lag_kf['train_env_base'])
        self.veh_abstract = self.get_veh_abstract(train_env_base=self.wm_lag_kf['train_env_base'])
        self.ttc_abstract = self.get_ttc_abstract(train_env_base=self.wm_lag_kf['train_env_base'])
        self.all_abstract = self.get_all_abstract(train_env_base=self.wm_lag_kf['train_env_base'])
        self.space_abstract_dr = self.get_space_abstract(train_env_base=self.wm_dr['train_env_base'])
        self.veh_abstract_dr = self.get_veh_abstract(train_env_base=self.wm_dr['train_env_base'])

        # 0 1 2 3 4 5 6
        self.experiments = [
            self.wm, #0
            self.wm_lag, #1
            self.wm_kf, #2
            self.wm_lag_kf, #3
            self.wm_noise, #4
            self.wm_lag_dr, #5
            self.wm_kf_dr, #6
            self.wm_lag_kf_dr, #7
            self.wm_percept, #8
            self.wm_dr, #9
            self.best  #10
        ]
        self.experiments += self.space_abstract     # 11 12 13 14 15 16 17 18 19 20 21
        self.experiments += self.veh_abstract       # 22 23 24 25 26 27 28 29 30 31 32
        self.experiments += self.space_abstract_dr  # 33 34 35 36 37 38 39 40 41 42 43
        self.experiments += self.veh_abstract_dr    # 44 45 46 47 48 49 50 51 52 53 54

        self.experiments += [
            self.wm_road, # 55
            self.wm_ego, # 56
            self.wm_veh, # 57
            self.wm_percept_road, # 58
            self.wm_percept_ego # 59
        ]

        self.experiments += self.ttc_abstract # 60 61 62 63 64 65 66 67 68 69 70

        self.experiments += self.all_abstract # 71 72 73 74 75 76 77 78 79 80 81

        self.experiments += [
            self.wm_vanish, # 82
            self.wm_xy #83
        ]

        self.experiments += [
            self.wm_no_kfdr, #84
            self.wm_no_lagdr, #85
            self.wm_no_vanish, #86
            self.wm_no_xy  #87
        ]

        self.experiments += [
            self.best1, #88
            self.best2 #89

        ]
        for idx, experiment in enumerate(self.experiments):
            eval_env_base = self.get_test_env_details(dict(), experiment['train_env_base'])
            experiment['eval_env_base'] = eval_env_base
            validation_env_base = self.get_test_env_details(experiment['train_env_base'], experiment['train_env_base'])
            experiment['validation_env_base'] = validation_env_base

        self.options_default_kwargs = {
        }

        self.agent_default_kwargs = {
        }

    def get_test_env_details(self, env_template, train_env):
        env_base = env_template.copy()
        if 'vehs_abstract_level' in train_env:
            env_base['vehs_abstract_level'] = train_env['vehs_abstract_level']
        if 'space_abstract_level' in train_env:
            env_base['space_abstract_level'] = train_env['space_abstract_level']
        if 'ttc_abstract_level' in train_env:
            env_base['ttc_abstract_level'] = train_env['ttc_abstract_level']
        if 'all_abstract_level' in train_env:
            env_base['all_abstract_level'] = train_env['all_abstract_level']
        if 'dynamic_space_abstract' in train_env:
            env_base['dynamic_space_abstract'] = train_env['dynamic_space_abstract']
        env_base['seed'] = 1
        env_base['no_zero_veh_scenario'] = True
        return env_base

    def get_space_abstract(self, train_env_base=None):
        space_abstract = {}
        space_abstract['hyperparameter_name'] = 'space_abstract_level'
        space_abstract['hyperparameter_description'] = "Space abstraction test"
        if train_env_base is None:
            space_abstract['train_env_base'] = {
                'seed': 1000
            }
        else:
            space_abstract['train_env_base'] = train_env_base

        space_abstract_list = []
        for i in range(1,12):
            curr = space_abstract.copy()
            curr['hyperparameter_val_list'] = [i]
            space_abstract_list.append(curr)

        return space_abstract_list

    def get_veh_abstract(self, train_env_base=None):
        veh_abstract = {}
        veh_abstract['hyperparameter_name'] = 'vehs_abstract_level'
        veh_abstract['hyperparameter_description'] = "Vehicle abstraction test"
        if train_env_base is None:
            veh_abstract['train_env_base'] = {
                'seed': 1000
            }
        else:
            veh_abstract['train_env_base'] = train_env_base

        veh_abstract_list = []
        for i in range(1,12):
            curr = veh_abstract.copy()
            curr['hyperparameter_val_list'] = [i]
            veh_abstract_list.append(curr)

        return veh_abstract_list

    def get_ttc_abstract(self, train_env_base=None):
        ttc_abstract = {}
        ttc_abstract['hyperparameter_name'] = 'ttc_abstract_level'
        ttc_abstract['hyperparameter_description'] = "TTC abstraction test"
        if train_env_base is None:
            ttc_abstract['train_env_base'] = {
                'seed': 1000
            }
        else:
            ttc_abstract['train_env_base'] = train_env_base

        ttc_abstract_list = []
        for i in range(1,12):
            curr = ttc_abstract.copy()
            curr['hyperparameter_val_list'] = [i]
            ttc_abstract_list.append(curr)

        return ttc_abstract_list

    def get_all_abstract(self, train_env_base=None):
        all_abstract = {}
        all_abstract['hyperparameter_name'] = 'all_abstract_level'
        all_abstract['hyperparameter_description'] = "All abstraction test"
        if train_env_base is None:
            all_abstract['train_env_base'] = {
                'seed': 1000
            }
        else:
            all_abstract['train_env_base'] = train_env_base

        all_abstract_list = []
        for i in range(1,12):
            curr = all_abstract.copy()
            curr['hyperparameter_val_list'] = [i]
            all_abstract_list.append(curr)

        return all_abstract_list
import pyglet
import gym

from utilities import BoundingBox as bb_utils
from utilities import Time
from worlds import EpisodicEnv
from worlds.events import Event

# import the env-related methods from env.utils
from ..env_utils import *

# import the classes and a method (config_Pyglet) for graphical output
from ..graphics.pyglet_utils import Text, Image, config_Pyglet

from . import Vehicle
from .features import ModifiedFeatures
from . import RoadNetworkCross, VehicleNetworkCross

from .constants import *
from ..constants import LEFT

from .. import seeding


class CrossIntersectionEnv(EpisodicEnv):
    """ A Road environment corresponding to an intersection scenario,
    where each route has two lanes with the same direction (left -> right, top -> down).

    This scenario has just one horizontal route and another vertical route.
    Relevant constants are used from the constants file.
    """
    # max number of steps for each episode
    max_steps_per_episode = 37 / DT

    # pyglet window object
    window = None

    # action space foe this environment
    __action_space = gym.spaces.Box(low=np.array([MIN_ACCELERATION, -MAX_STEERING_ANGLE_RATE]),
                                    high=np.array([MAX_ACCELERATION, MAX_STEERING_ANGLE_RATE]))

    @property
    def action_space(self): return self.__action_space

    # the observation_space is available only after generate_scenario has been called.
    @property
    def observation_space(self):
        return self.features_class.get_observation_space()

    @property
    def ego(self): return self.vehs[EGO_INDEX] if len(self.vehs) > 0 else None

    def __init__(self, **kwargs):
        """SimpleIntersectionEnv constructor.

        Args:
            options: high level options instance
            n_other_vehs: number of other vehicles
        """

        #: info for displaying on top of ego
        self.ego_info_text = ""

        #: the number of the other vehicles, initialized in generate_scenario
        self.n_other_vehs = None

        #: the list of the vehicles, initialized in generate_scenario
        self.vehs = list()

        #: the vehicle_nework for render, initialized in generate_scenario
        self.vehicle_network = None

        #: the target_lane (0: left, 1: right) of the ego to follow
        # (it will be set to the ego's lane by default)
        self.target_lane = LEFT

        # Save the scenario option to reuse it in reset
        self.scenario_option = kwargs

        # counts the number of steps
        self.step_count = 0

        # keeps track of total rewards during each episode
        self.episode_reward = 0.0

        # current seed value
        self.current_seed = None

        # sets initial seed; None means it uses system time
        self.seed(self.current_seed)

        # store the feature generation class
        self.features_class = ModifiedFeatures(self)

        # Variables for generating episode JSON
        self.current_scenario_json = {}
        self.scenarios_list_json = []
        self.veh_index_map = []
        self.next_veh_index_json = 0
        self.startup_delay = 7.0 / DT  # steps
        self.trigger_delay = 0.4  # seconds

        # stores the feature array for current state
        self.current_feature_tuple = None

        # flag if velocity feature error model is enabled
        self.unreliable_velocity = False

        # flag if perception lag error model is enabled
        self.lag_flag = False

        # flag to check whether it is the first time generate scenario is called
        self.first_flag = True

        super().__init__(time=Time(DT, np.inf))

        events = \
            {
                'goal achieved': Event(lambda: self.ego.x >= rd.h.end_pos,
                                       lambda: 12),

                'ego collision': Event(lambda: self.check_ego_collision(), lambda: -12),

                'timed out': Event(lambda: self.episode_timed_out(), lambda: 0)
            }

        self.termination_events.update(events)

        self.generate_scenario(**kwargs)

    def seed(self, seed=None):
        """ Seed the scenario generation and other environment modifications.

        Args:
            seed: can be any value. one value will always generate a specific scenario. update this before
            generating a new scenario so that a different one is generated.
        """

        self.current_seed = seed
        # this seed is used for all scenario generation
        # and is passed to Vehicles class
        self.np_random, seed = seeding.np_random(seed)

        # Rest of these seeds are used for modifications of the environment
        # anything outside of scenario generation should use new seeds
        self.np_random_road, seed = seeding.np_random(seed)
        self.np_random_ego, seed = seeding.np_random(seed)
        self.np_random_veh, seed = seeding.np_random(seed)
        self.np_random_perception, seed = seeding.np_random(seed)
        self.np_random_lag, seed = seeding.np_random(seed)
        self.np_random_kf, seed = seeding.np_random(seed)
        self.np_vanish, seed = seeding.np_random(seed)
        self.np_xy_noise, seed = seeding.np_random(seed)
        return [seed]

    def get_current_seed(self):
        """ Get current seed value.
        """
        return self.current_seed

    def generate_scenario(self, **kwargs):
        """Randomly generate a road scenario with.

            "the N-number of vehicles + an ego vehicle"

        (n_others_range[0] <= N <= n_others_range[1]),
        all of whose speeds are also set randomly (or deterministically
        depending on the options) between 0 and the speed limit of the road env.
        The position and speed of the vehicles are set (some of them randomly)
        but not in a way that causes inevitable collision or violation.
        The ego vehicle is always posed on a horizontal lane and there is
        initially no or only one vehicle posed in the intersection.

        TODO: More debugging
        TODO: Add more comments for each block within the function for comprehensive understanding.
        TODO: restructure the code by making private method for each part for a certain option.

        Args:
            n_others_range: a tuple (n1, n2), which gives the range of the
                vehicles on the road, excluding the ego vehicle,
                by n1 <= N <= n2.
            ego_pos_range: a tuple or list (x1, x2); the ego's x-position
                will be sampled uniformly in [x1, x2].
            ego_lane_i: the index of the lane on which ego will be initialized
            others_v_pos_range: the range of the other vehicles that are on the
                vertical route and initially not stopped in the stop region.
            v_max_multiplier: a constant between 0 and 1, used in resetting the
                speeds of the vehicles if necessary to make sure that at least
                an input to the ego exists to avoid collisions or stop-sign
                violations. The less v_max_multiplier, more conservative
                scenario is generated (but when v_max_multipler = 1, it could
                generate a scenario where a collision is inevitable). 0.9 is
                a value set by the developer through trial-and-error and
                exhaustive testing.

            space_abstract_level: sets the x and y feature abstract level. Levels: 1 to 11
            vehs_abstract_level: sets the velocity feature abstract level. Levels: 1 to 11
            ttc_abstract_level: sets the ttc feature abstract level. Levels: 1 to 11
            all_abstract_level: sets single abstract level for all features. Levels: 1 to 11

            imperfect_perception: Model all perception errors
            system_lag: Model fixed perception lag only
            rand_system_lag: Model randomized perception lag only
            kf_error: Model fixed velocity estimation error only
            rand_kf_error: Model randomized velocity estimation error only
            vanish_veh: Model vanishing vehicle error
            xy_noise: Model x,y feature noise

            seed: set seed for this scenario
            save_feature_space: save the feature space to a file
            perturb_feature_index: the feature column index to perturb using known distributions

            use_robust_rule_based: use robust rule-based policy instead
            use_distilled_rule_based: use distilled rule-based policy instead
        Returns:
            False if the initial state is Terminal, True if not.
        """

        # -------------------------------------------------------------------------------------
        # Get scenario generation parameters from kwargs
        # TODO: move this out or make it more general

        n_others_range = kwargs.setdefault('n_others_range', (MIN_NUM_VEHICLES, MAX_NUM_VEHICLES))
        ego_pos_range = \
            kwargs.setdefault('ego_pos_range', (rd.h.stop_region.boundary[0], rd.h.stop_region.boundary[0]))
        ego_lane_i = \
            kwargs.setdefault('ego_lane_i', LEFT)
        others_v_pos_range = \
            kwargs.setdefault('others_v_pos_range', rd.v.pos_range)
        v_max_multiplier = \
            kwargs.setdefault('v_max_multiplier', 0.9)

        space_abstract_level = kwargs.setdefault('space_abstract_level', 0)
        vehs_abstract_level = kwargs.setdefault('vehs_abstract_level', 0)
        ttc_abstract_level = kwargs.setdefault('ttc_abstract_level', 0)
        all_abstract_level = kwargs.setdefault('all_abstract_level', 0)

        imperfect_perception = kwargs.setdefault('imperfect_perception', False)
        system_lag = kwargs.setdefault('system_lag', False)
        rand_system_lag = kwargs.setdefault('rand_system_lag', False)
        kf_error = kwargs.setdefault('kf_error', False)
        rand_kf_error = kwargs.setdefault('rand_kf_error', False)
        vanish_veh = kwargs.setdefault('vanish_veh', False)
        xy_noise = kwargs.setdefault('xy_noise', False)

        seed = kwargs.setdefault('seed', None)
        self.save_feature_space_flag = kwargs.setdefault('save_feature_space', False)
        perturb_feature_index = kwargs.setdefault('perturb_feature_index', -1)

        self.use_robust_rule_based = kwargs.setdefault('use_robust_rule_based', False)
        self.use_distilled_rule_based = kwargs.setdefault('use_distilled_rule_based', False)
        #-------------------------------------------------------------------------------------------

        if n_others_range[0] < 0:
            raise ValueError(
                "the number of other vehicles has to be non-negative.")

        if n_others_range[1] < n_others_range[0]:
            raise ValueError(
                "the range of the number of other vehicles is wrong.")

        n_others = self.np_random.randint(n_others_range[0], n_others_range[1] + 1)

        if not (0 <= v_max_multiplier <= 1):
            raise ValueError("v_max_multiplier has to be within the range [0, 1].")

        if ego_lane_i not in {0, 1} and ego_lane_i is not None:
            raise ValueError("ego_lane_i has to be 0 or 1.")

        # get seed from kwargs if its the first generate scenario
        if self.first_flag:
            if seed is not None:
                self.current_seed = seed
            self.first_flag = False
        # -------------------------------------------------------------------------------------
        # Use kwargs to modify scenario generation
        # TODO: move this out to a function
        self.n_other_vehs = n_others

        self.seed(self.current_seed)
        self.features_class.save_feature_space_flag = self.save_feature_space_flag
        if perturb_feature_index > -1:
            self.features_class.set_perturb_feature_index(perturb_feature_index, self.np_random_veh)
        if all_abstract_level > 0:
            space_abstract_level = all_abstract_level
            vehs_abstract_level = all_abstract_level
            ttc_abstract_level = all_abstract_level
        if space_abstract_level > 0:
            self.features_class.set_space_abstraction_level(space_abstract_level)
        if vehs_abstract_level > 0:
            self.features_class.set_veh_abstraction_level(vehs_abstract_level)
        if ttc_abstract_level > 0:
            self.features_class.set_ttc_abstraction_level(ttc_abstract_level)
        if imperfect_perception:
            self.features_class.use_imperfect_perception(self.np_random_perception, self.np_random_kf,
                                                         self.np_random_lag, self.np_vanish, self.np_xy_noise)
            self.unreliable_velocity = True
            self.lag_flag = True
        else:
            if rand_system_lag:
                self.features_class.use_lag(self.np_random_lag)
                self.lag_flag = True
            elif system_lag:
                self.features_class.use_lag()
                self.lag_flag = True
            else:
                self.lag_flag = False
            if rand_kf_error:
                self.features_class.use_kf_error(self.np_random_kf)
                self.unreliable_velocity = True
            elif kf_error:
                self.features_class.use_kf_error()
                self.unreliable_velocity = True
            else:
                self.unreliable_velocity = False
            if vanish_veh:
                self.features_class.use_vanishing_vehicles(self.np_vanish)

            if xy_noise:
                self.features_class.use_xy_error(self.np_xy_noise)
        #--------------------------------------------------------------------------

        vehs = self.vehs

        # Ego vehicle
        #TODO: move this out to a function?
        #-------------------------------------------------------------------------
        ego_speed_limit = 8.05
        ego_max_acceleration = 1.05
        vehs.clear()  # Remove the existing vehicles if any.
        vehs.append(Vehicle('h', lane_i=ego_lane_i, v_upper_lim=ego_speed_limit, np_random=self.np_random,
                            max_acc=ego_max_acceleration, v_start=0))

        ego = self.ego
        ego_lane_i = ego.APs['lane']
        ego_lane_centre = rd.h.lanes[ego_lane_i].centre

        ego_pos_min = min(ego_pos_range)
        ego_pos_max = max(ego_pos_range)

        others_v_pos_min = min(others_v_pos_range)
        others_v_pos_max = max(others_v_pos_range)

        ego.x = self.np_random.uniform(ego_pos_min, ego_pos_max)
        ego.y = ego_lane_centre
        self.ego.theta = 0

        ego.init_local_discrete_var()

        self.init_APs(False)

        # s is the distance that the vehicle (in this case, the ego)
        # has to inevitably drive for complete stop.
        s = calculate_s(ego.v)
        s = check_init_veh(ego, s, v_max_multiplier)
        s_seq = [s]

        # -------------------------------------------------------------------------
        # Other vehicles
        #TODO: move this out to a function?
        curr_i = 1
        for i in range(curr_i, n_others + 1):
            success_flag = False
            while not success_flag:
                vehs.append(Vehicle('v', self.np_random.randint(0, 2), rd.speed_limit, np_random=self.np_random))
                vehs[i].y = self.np_random.uniform(others_v_pos_min, others_v_pos_max)

                vehs[i].init_local_discrete_var()
                self.init_APs(False)

                s = calculate_s(vehs[i].v)
                s_seq.append(s)
                self.init_APs(False)

                for j in range(0, i + 1):
                    k, v2v_dist = self.get_v2v_distance(j)
                    if k is not None:
                        if v2v_dist < 0 or \
                           (v2v_dist + s_seq[k] < s_seq[j] + self.vehs[j].V2V_ref):
                            vehs.pop()
                            s_seq.pop()
                            self.init_APs(False)
                            break
                    if j == i:
                        success_flag = True
                        break

        # Finally checks the constraints that every vehicle has to satisfy,
        # and if not satisfied, modify the speeds.
        success_flag = False
        while not success_flag:
            success_flag = True
            for i, veh in enumerate(vehs):
                k, v2v_dist = self.get_v2v_distance(i)
                if k is None:
                    continue
                elif v2v_dist + s_seq[k] < s_seq[i] + veh.V2V_ref:
                    s_seq[i] = regulate_veh_speed(veh, v2v_dist + s_seq[k] - veh.V2V_ref, v_max_multiplier)
                    success_flag = False

        self.init_APs(False)

        # Suffle the vehs in self.vehs, except the ego (i = 0).
        other_vehs_indices = self.np_random.permutation(n_others) + 1

        ego.mode = 'normal'
        other_vehs = []
        for i in other_vehs_indices:
            vehs[i].mode = 'fast'
            other_vehs.append(vehs[i])

        vehs[1:] = other_vehs

        self.__cost = None

        # Reset the variables related to verifier, collision-checking and termination.
        return not super().reset()

    def init_APs(self, update_local_APs=True):
        """Initialize the global (and local if update_local_APs is true) atomic
        propositions (APs) of each vehicle using the current state. This method
        is basically called by "generate_scenario" method. Compared with
        update_APs, the way of setting 'highest_priority' is different.

        Args:
            update_local_APs: True if the local APs are also necessary to
                be initialized; False if not, and only the global APs are
                required to be initialzied.
        """

        if update_local_APs is True:
            for veh in self.vehs:
                veh.update_local_APs()

        self.update_APs(False, False)

    def update_APs(self, update_local_APs=True, update_highest_priority=True):
        """Update the global (and local if update_local_APs is true) atomic
        propositions (APs) of each vehicle using the current state. This method
        is basically called by "step" method below after it updates the
        vehicle's continuous state. By a global AP, it means that the state
        information of the other cars has to be used to update the AP (whereas
        a local AP means that the information of the other cars are not
        necessary). For comparison and more details, see also "updateLocalAPs"
        method in vehicles module.

        Args:
            update_local_APs: True if the local APs are also necessary to
                be update; False if not, and only the global APs are required
                to be updated.
        """

        if update_local_APs is True:
            for veh in self.vehs:
                veh.update_local_APs()

    def get_v2v_distance(self, veh_index=0):
        """Calculates the distance between the target and the nearest adjacent
        car ahead in the same lane. The distance is given as a bumper-to-bumper
        one, so being it not positive means that the two cars collide.

        Args:
            veh_index: the index of the vehicle whose v2v distance is
                returned, ranging from 0 to len(self.vehs)
                 (with 0 for the index of the ego).

        Returns:
            a tuple (i, v2vdist), where v2vdist is the v2v distance
            from the indexed vehicle to the closest vehicle, with its index i,
            on the same route; i is None and v2vdist == np.inf
            when there is no vehicle ahead.
        """
        if veh_index not in range(0, len(self.vehs)):
            raise ValueError("veh_index in SimpleIntersectionEnv.get_v2v_distance out of index")

        tar_veh = self.vehs[veh_index]

        v2v_dist = np.inf
        index = None

        for i, veh in enumerate(self.vehs):

            if i == veh_index:
                continue

            elif (veh.which_route == tar_veh.which_route) and (veh.APs['lane'] == tar_veh.APs['lane']):

                sign = 1 if tar_veh.which_dir == 'forward' else -1

                if tar_veh.which_route is 'h':
                    dist = sign * (veh.x - tar_veh.x)
                    if 0.0 <= dist < v2v_dist:
                        v2v_dist = dist
                        index = i

                else: # the case of "tar_veh.which_route is 'v'":
                    dist = sign * (veh.y - tar_veh.y)
                    if 0.0 <= dist < v2v_dist:
                        v2v_dist = dist
                        index = i

        return index, v2v_dist - VEHICLE_SIZE[0]

    def aggressive_driving_policy(self, veh_index):
        """Implementation of the aggressive driving policy. Calculate the
        (veh_index)-th vehicle's low-level action (a, dot_phi) using aggressive
        driving policy. If using aggressive driving policy for all vehicles,
        there is [has to be] no collision.

        Args:
            veh_index: the index of the vehicle to calculate the action
                (a, dot_phi) via aggressive driving policy.

        Returns:
            current low-level action (a, dot_phi) generated by the
            aggressive driving policy for the current state.
        """
        assert 0 <= veh_index <= len(self.vehs) - 1

        tar_veh = self.vehs[veh_index]

        # Note that:
        # 1) calculate_s calculates the dist. required for the vehicle to
        # completely stop under maximum deceleration; calculation is based
        # on the current speed of the vehicle.
        # 2) the order of IF-THEN rules below is important -- if the
        # order has changed, then it would not behave in the same way.

        i, v2v_dist = self.get_v2v_distance(veh_index)
        s = calculate_s(tar_veh.v)

        stop_region, tar_dist_long = \
            (rd.h.stop_region, tar_veh.x) if tar_veh.which_route is 'h' else (rd.v.stop_region, tar_veh.y)

        # If the clear-ahead-dist (=v2v_dist + s_i) is less than
        # "the v2v reference dist" + "the dist. traveling under the
        # maximum brake", then apply emergency stop.
        if i is not None and (v2v_dist + calculate_s(self.vehs[i].v) < s + tar_veh.V2V_ref):
            a = MIN_ACCELERATION  # emergency stop

        # If the vehicle-ahead-dist. is less than v2v_ref + some margin
        # (margin includes the size of the vehicles), and if the vehicle
        # is not in the stop region and has enough dist. ahead to stop in
        # the stop region, then apply THE VEHICLE-AHEAD FOLLOWING
        # CONTROLLER, whose objective is to maintain the v2v distance to
        # v2v_ref. Note that this can be activated even if the vehicle's
        # speed is zero (i.e., s = 0).
        elif i is not None and (v2v_dist <= tar_veh.V2V_ref + 6):
            a = 31.6228 * (v2v_dist - tar_veh.V2V_ref) - 7.9527 * (tar_veh.v - self.vehs[i].v) + self.vehs[i].a

        else:

            a = -10.0 * (tar_veh.v - tar_veh.max_vel)

        return np.clip(a, MIN_ACCELERATION, tar_veh.max_acc), 0

    def episode_timed_out(self):
        if self.step_count >= self.max_steps_per_episode:
            return True
        else:
            return False

    def check_ego_collision(self):
        """Checks if ego collides with any other vehicle in the environment.
        Uses bounding boxes to check if a seperating line exists between given
        vehicle and each of the other vehicles.

        Returns:     True if ego collides with another vehicle, False
        otherwise
        """

        current_veh_bb = self.ego.bounding_box
        for i, veh in enumerate(self.vehs):
            if i is EGO_INDEX:
                continue
            other_veh_bb = veh.bounding_box
            if bb_utils.does_bounding_box_intersect(current_veh_bb,
                                                    other_veh_bb):
                return True

        return False

    def check_other_veh_collisions(self):
        """Checks if other vehicles other than ego collide in the environment.
        Uses v2v distance for vehicles in same lane, otherwise uses bounding
        boxes Checks all vehicles other than ego.

        Returns: True if a vehicle collides with another vehicle,
                 False otherwise
        """

        for index in range(len(self.vehs)):
            if index is EGO_INDEX:
                continue
            current_veh = self.vehs[index]
            for j in range(index + 1, len(self.vehs)):
                veh = self.vehs[j]
                # check collision using v2v for other vehicles only in same lane
                # and travelling in same direction
                if veh.which_route == current_veh.which_route and \
                        veh.APs['lane'] == current_veh.APs['lane']:
                    if current_veh.which_route is 'h':
                        v2v_dist = abs(veh.x - current_veh.x) - VEHICLE_SIZE[0]
                    else:
                        v2v_dist = abs(veh.y - current_veh.y) - VEHICLE_SIZE[0]

                    if v2v_dist < 0:
                        # TODO: Remove this print or log this somehow
                        #print("\nVehicle[", index, "] collided with vehicle[", j,"]")
                        return True

        return False

    def get_features_tuple(self):
        """Get the features array previously generated and saved to class variable

        Returns features tuple
        """

        return self.current_feature_tuple

    def generate_features_tuple(self):
        """Calculate the features array and save it in a class variable

        Returns features tuple
        """
        self.current_feature_tuple = self.features_class.get_features_tuple()

    def step(self, action, action_clipping=True, terminal_reward=True):
        """Gym compliant step function.

        Calculate the next continuous and discrete states of every vehicle
        using the given action u_ego for ego-vehicle and aggressive driving
        policy for the other vehicles. The variables in self.vehs will be
        automatically updated. Then, call the options _step and get the reward
        and terminal values.

        Args:
            action: the low level action (a, dot_phi) of the ego vehicle.
            action_clipping: if True, the action is always projected to the action space;
                             otherwise, raise an assertion error when action doesn't belong to self.action_space
            terminal_reward: if False, the terminal reward is not added to the reward output,
                             but the user deals with it externally by calling self.terminal_reward().

        Returns:
            new_state: the new state of the environment
            R: reward obtained
            terminal: whether or not the current policy finished, that is, its termination_condition() is satisfied;
            info: information variables
        """
        self.step_count = self.step_count + 1
        reward = 0

        # input clipping function.
        def clip_input(u):
            action_space = self.action_space
            return u if action_space.contains(u) else np.clip(u, action_space.low, action_space.high)

        if action_clipping:
            action = clip_input(action)
        else:
            assert self.action_space.contains(action)

        for i, veh in enumerate(self.vehs):
            veh.step(action if i == EGO_INDEX else self.aggressive_driving_policy(i))

            # Teleportation logic when vehicles  reach edge of screen
            # TODO: Move this to another function?
            if i != EGO_INDEX:
                #  if vehicle goes out of screen, teleport them to start of random lane
                teleport_interval = VEHICLE_SIZE[0]
                if (veh.APs['lane'] and (veh.y > rd.v.end_pos + 5)) or \
                        (not veh.APs['lane'] and (veh.y < rd.v.start_pos - 5)):
                    veh.teleport_count += 1
                    new_lane = self.np_random.randint(0, 2)

                    if new_lane:
                        lane_begin = rd.v.start_pos
                        teleport_offset_sign = -1
                    else:
                        lane_begin = rd.v.end_pos
                        teleport_offset_sign = 1

                    # teleport to beyond bounds by incrementally checking if collision happens
                    collision = True
                    multiplier = self.np_random.randint(1, MAX_NUM_VEHICLES + 1)
                    dist_to_stop = calculate_s(veh.v)
                    while (collision):
                        pos_in_lane = lane_begin + teleport_offset_sign * multiplier * teleport_interval
                        collision = False
                        for n, vehicle in enumerate(self.vehs):
                            if n != 0 and n != i:
                                v2v_dist = abs(vehicle.y - pos_in_lane) - VEHICLE_SIZE[0]
                                if v2v_dist < dist_to_stop:
                                    collision = True
                                    break
                        multiplier = multiplier + 1

                    veh.reset('v', new_lane, pos_in_lane, veh.max_vel, 0)

        self.update_APs(False, True)

        terminal = self.step_termination_events()
        if terminal_reward and terminal:
            reward += self.terminal_reward()

        self.episode_reward += reward
        self.generate_features_tuple()

        return self.get_features_tuple(), reward, terminal, self.get_info()

    def reset(self):
        """Gym compliant reset function.

        Reinitialize this environment with whatever parameters it was initially
        called with.

        Returns:
            new_state: the environment state after reset
        """
        self.features_class.reset_state()
        self.step_count = 0
        if self.current_seed is not None:
            self.current_seed += 1
        self.episode_reward = 0.0

        while not self.generate_scenario(**self.scenario_option):
            pass

        self.generate_features_tuple()
        return self.get_features_tuple()

    def draw(self, info=None):
        """Draw the background, road network and vehicle network. Also show
        information as passed on by info.

        Args:
            info: the information string to be shown on the window
        """

        self.background.draw()

        # draw the road network
        self.road_network.draw()

        # draw the vehicle network
        self.vehicle_network.draw(self.ego_info_text)

        if info is not None:
            label = Text(info, x=10, y=NUM_VPIXELS - 20, multiline=True)
            label.draw()

    def render(self, mode='human'):
        """Gym compliant render function."""

        if mode == 'human':
            if self.window is None:
                # load the background texture
                self.background = Image('background.png', anchor=(0, 0), image_type="bg")

                # load the road network
                self.road_network = RoadNetworkCross('road_texture.png')
                self.vehicle_network = VehicleNetworkCross('car_agent.png', self)
                self.window = config_Pyglet(pyglet, NUM_HPIXELS, NUM_VPIXELS)

            self.window.clear()
            self.window.switch_to()
            self.window.dispatch_events()

            # Text information about ego vehicle's states
            info = "Ego Attributes:" + \
                get_veh_attributes(self, EGO_INDEX, 'x', 'v', 'a') + \
                "Seed: " + str(self.current_seed) + \
                "\nStep: " + str(self.step_count)

            self.draw(info)

            self.window.flip()

        #TODO: make this similar to gym standards
        elif mode == 'rgb_array':
            return self.get_features_tuple()

    def close_window(self):
        if self.window is not None:
            self.window.close()

    def set_ego_info_text(self, info):
        self.ego_info_text = info

    # Rule-based policies
    # TODO: move these to another place
    # -------------------------------------------------------------------------------------------------
    def can_ego_cross(self):
        feature = self.get_features_tuple()
        dist_to_collide_down_lane = (-rd.v.width / 2 - self.ego.x)
        dist_to_collide_up_lane = (rd.v.width / 2 - self.ego.x)
        buffer_time = 1.5

        dist_collision = []
        vehs_collision = []

        for row in feature:
            vel = row[3] / 3.6
            x, y = row[:2]
            lane = np.sign(row[2])
            if x != 0 and y != 0:
                if ((y > 0 and lane == 1) or (y < 0 and lane == -1)) and vel > 0:  # down lane
                    dist_collision.append(abs(y))
                    vehs_collision.append([x, y, lane, vel])

        time_to_cross = []
        for i, veh in enumerate(vehs_collision):
            time_to_cross.append(calc_time_to_cover_dist(dist_collision[i], veh[3], 2.0, veh[3]))

        if len(time_to_cross) == 0:
            return True

        time_to_cross_down_lane = calc_time_to_cover_dist(dist_to_collide_down_lane, self.ego.v, self.ego.max_acc,
                                                          self.ego.max_vel)
        time_to_cross_intersection = calc_time_to_cover_dist(dist_to_collide_up_lane, self.ego.v, self.ego.max_acc,
                                                             self.ego.max_vel)

        for i, t in enumerate(time_to_cross):
            if (vehs_collision[i][2] == 1 and abs(time_to_cross_down_lane - t) < buffer_time) or \
                    (vehs_collision[i][2] == -1 and abs(time_to_cross_intersection - t) < buffer_time):
                return False

        return True

    def pos_theta_tree(self, y, vel, ttc):
        if ttc <= 3.4389535188674927:
            if vel <= 29.016040802001953:
                if ttc <= 3.249743700027466:
                    if ttc <= 3.1655906438827515:
                        return True
                    else:  # if ttc > 3.1655906438827515
                        if vel <= 27.802101135253906:
                            return False
                        else:  # if vel > 27.802101135253906
                            return True
                else:  # if ttc > 3.249743700027466
                    return False
            else:  # if vel > 29.016040802001953
                if vel <= 30.87932300567627:
                    if y <= 28.845558166503906:
                        return True
                    else:  # if y > 28.845558166503906
                        return False
                else:  # if vel > 30.87932300567627
                    return True
        else:  # if ttc > 3.4389535188674927
            if y <= 73.42737579345703:
                return False
            else:  # if y > 73.42737579345703
                return True

    def neg_theta_tree(self, y, vel, ttc):
        if ttc <= 4.5806496143341064:
            if y <= 52.42517280578613:
                if y <= 48.451921463012695:
                    return True
                else:  # if y > 48.451921463012695
                    if ttc <= 4.422658681869507:
                        return True
                    else:  # if ttc > 4.422658681869507
                        if y <= 50.024253845214844:
                            if y <= 49.02048873901367:
                                return False
                            else:  # if y > 49.02048873901367
                                return True
                        else:  # if y > 50.024253845214844
                            return False
            else:  # if y > 52.42517280578613
                return False
        else:  # if ttc > 4.5806496143341064
            return False

    def can_ego_cross_distilled(self):
        feature = self.get_features_tuple()
        decision = True

        for row in feature:
            vel = row[3] / 3.6
            x, y = row[:2]
            lane = np.sign(row[2])
            if x != 0 and y != 0:
                if (y > 0 and lane == 1) or (y < 0 and lane == -1):  # down lane
                    time_to_cross = calc_time_to_cover_dist(abs(y), vel, 2.0, vel)
                    if lane:
                        decision = self.pos_theta_tree(abs(y), row[3], time_to_cross)
                        if not decision:
                            break
                    else:
                        decision = self.neg_theta_tree(abs(y), row[3], time_to_cross)
                        if not decision:
                            break

        return decision

    def can_ego_cross_robust(self):
        decision = True
        feature = self.get_features_tuple()
        feature = self.features_class.get_non_abstract_features(feature)
        dist_to_collide_down_lane = (-rd.v.width / 2 - self.ego.x)
        dist_to_collide_up_lane = (rd.v.width / 2 - self.ego.x)
        buffer_time = 1.5
        unreliable_vel = 6.94
        avg_lag_secs = 0.3

        dist_collision = []
        vehs_collision = []

        for row in feature:
            vel = row[3]/3.6
            x,y = row[:2]
            lane = np.sign(row[2])
            if x != 0 and y != 0:
                if self.unreliable_velocity and vel < unreliable_vel:  # < 30km/hr
                    decision = False
                    vehs_collision = []
                    dist_collision = []
                    break
                if self.unreliable_velocity:
                    vel = vel * 1.1  # 10% increase in vel to offset underestimation
                if self.lag_flag:
                    extrapolation_dist = vel * avg_lag_secs
                    y = y - lane*extrapolation_dist # extrapolate vehicle to future
                if (y > 0 and lane==1) or (y < 0 and lane == -1): # down lane
                    dist_collision.append(abs(y))
                    vehs_collision.append([x,y,lane,vel])

        time_to_cross = []
        for i, veh in enumerate(vehs_collision):
            time_to_cross.append(calc_time_to_cover_dist(dist_collision[i], veh[3], 2.0, veh[3]))

        if len(time_to_cross) > 0:
            time_to_cross_down_lane = calc_time_to_cover_dist(dist_to_collide_down_lane, 0.0, self.ego.max_acc, self.ego.max_vel)
            time_to_cross_intersection = calc_time_to_cover_dist(dist_to_collide_up_lane, 0.0, self.ego.max_acc, self.ego.max_vel)

            for i, t in enumerate(time_to_cross):
                if (vehs_collision[i][2] == 1 and abs(time_to_cross_down_lane - t) < buffer_time) or \
                        (vehs_collision[i][2] == -1 and abs(time_to_cross_intersection - t) < buffer_time):
                    decision = False
                    break

        return decision

    def get_time_to_collision_with_ego(self, veh):
        dist_to_collide_down_lane = (-rd.v.width/2 - self.ego.x)
        dist_to_collide_up_lane = (rd.v.width/2 - self.ego.x)
        line_of_collision = rd.h.width/2

        dist_collision = None

        if 0 < (line_of_collision - veh.y) and veh.APs['lane']:  # down lane
            dist_collision = line_of_collision - veh.y
        elif 0 < (veh.y - line_of_collision) and not veh.APs['lane']:  # up lane
            dist_collision = veh.y - line_of_collision

        if dist_collision is None:
            return -1
        else:
            time_to_cross = calc_time_to_cover_dist(dist_collision, veh.v, veh.max_acc, veh.max_vel)

            if veh.APs['lane']:
                return abs(time_to_cross)
            else:
                return abs(time_to_cross)

    def select_optimal_option(self):
        if self.use_robust_rule_based:
            return 0 if self.can_ego_cross_robust() else 1
        elif self.use_distilled_rule_based:
            return 0 if self.can_ego_cross_distilled() else 1
        else:
            return 0 if self.can_ego_cross() else 1

    # -------------------------------------------------------------------------------------------------
    # Functions for creating JSON for import into unreal
    # TODO: move these somewhere else.
    #_-------------------------------------------------------------------------------------------------
    def save_json(self, file_suffix='wisemove'):
        import json
        with open(SAVE_FOLDER + file_suffix + '-scenario-list.json', 'w') as f:
            json.dump(self.scenarios_list_json, f)

    def add_current_scenario_json(self):
        self.scenarios_list_json.append(self.current_scenario_json)

    def veh_has_teleported(self, idx, veh):
        if self.last_teleport_count[idx] != veh.teleport_count:
            self.last_teleport_count[idx] = veh.teleport_count
            return True
        else:
            return False

    def update_state_for_json(self):
        for idx, veh in enumerate(self.vehs[1:]):
            # check if just got teleported, if so add it as new vehicle
            if self.veh_has_teleported(idx,veh):
                self.veh_index_map[idx] = self.next_veh_index_json
                self.next_veh_index_json +=1
                curr_veh = dict()
                curr_veh['start'] = rd.translate_point_stopline_origin([veh.x, veh.y])
                curr_veh['start_speed'] = veh.v * 3.6
                curr_veh['path'] = []
                delay = self.step_count * DT - self.trigger_delay
                if delay > 0:
                    curr_veh['time_trigger'] = delay
                self.current_scenario_json['vehicles'].append(curr_veh)
            else:
                path_node = {
                    'point': rd.translate_point_stopline_origin([veh.x,veh.y]),
                    'speed': veh.v * 3.6
                }
                self.current_scenario_json['vehicles'][self.veh_index_map[idx]]['path'].append(path_node)

    def save_initial_state_for_json(self):
        self.current_scenario_json = {}
        self.veh_index_map = []
        self.last_teleport_count = []
        self.next_veh_index_json = 0
        self.current_scenario_json['ego_pos'] = rd.translate_point_stopline_origin([self.ego.x,self.ego.y])
        self.current_scenario_json['vehicles'] = []
        self.current_scenario_json['seed'] = self.current_seed
        self.current_scenario_json['num_vehicles'] = len(self.vehs) - 1
        for idx, veh in enumerate(self.vehs[1:]):
            self.veh_index_map.append(idx)
            self.last_teleport_count.append(0)
            self.next_veh_index_json = idx+1
            curr_veh = dict()
            curr_veh['start'] = rd.translate_point_stopline_origin([veh.x,veh.y])
            curr_veh['start_speed'] = veh.v * 3.6
            curr_veh['path'] = []
            self.current_scenario_json['vehicles'].append(curr_veh)

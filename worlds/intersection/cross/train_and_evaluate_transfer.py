import numpy as np
import sys, os
from collections import defaultdict
import csv as csv
from shutil import copytree, copy
import errno
import argparse
import json

# Set the base wise-move directory to the Python working directory.
os.chdir('../../..')
sys.path.append(os.getcwd())

from worlds.intersection.cross import OptionsEnv
from worlds.intersection.cross.kerasrl.learners import DQNLearner as KerasDQNLearner
from worlds.intersection.cross.constants import rd

from worlds.intersection.cross.train_eval_config import Experiments
from worlds.intersection.cross.runner import Runner
from worlds.intersection.cross.runner import LOG_PATH_BASE,NUM_POLICIES_TO_SAVE

def get_datetime():
    from datetime import datetime
    now = datetime.now()
    return now.strftime("%d/%m/%Y, %H:%M:%S")

def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise:
    variance = np.average((values-average)**2, weights=weights)
    return (average, np.sqrt(variance))

def copy_folder(src, dest):
    try:
        copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

def find_files( path_to_dir, suffix=".h5f" ):
    filenames = os.listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def evaluate_transfer_and_get_results(path_to_dir, eval_env_base=None, create_summary=True):
    if eval_env_base is None:
        with open(os.path.join(path_to_dir, "eval_env_base.json"), "r") as read_file:
            eval_env_base = json.load(read_file)
    abs_path_to_dir = os.path.abspath(path_to_dir)
    hyperparameter_name = os.path.basename(abs_path_to_dir)
    hyperparameter_val_list = get_immediate_subdirectories(abs_path_to_dir)
    param_comparison_list = defaultdict(list)
    eval_name_list = None
    for hyperparameter_value in hyperparameter_val_list:
        hyperparameter_value = int(hyperparameter_value)
        log_path = path_to_dir + str(hyperparameter_value) + '/'
        model_names = find_files(log_path)
        runner.logger.set_log_path(log_path, overwrite=False)
        eval_env = eval_env_base.copy()
        eval_env[hyperparameter_name] = hyperparameter_value

        with open(log_path + '{}-results.csv'.format(hyperparameter_name), 'w+', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["Run date-time", current_datetime])
            writer.writerow([])

        runner.logger.log("Evaluating policies for {}:{}".format(hyperparameter_name,hyperparameter_value))
        result_summary = defaultdict(list)
        termination_summary = defaultdict(list)
        metrics_summary = defaultdict(list)
        for model_filename in model_names:
            model_filename = log_path + model_filename
            results, eval_name_list = runner.evaluate_transfer(model_filename, runner.transfer_tests, eval_env, rd)
            for eval_name, result in results.items():
                result_summary[eval_name].append(result[0])
                termination_summary[eval_name].append(result[1])
                metrics_summary[eval_name].append(result[2])

        success_avg_list = []
        term_reason_list = defaultdict(list)
        metrics_list = defaultdict(list)
        with open(log_path + '{}-results.csv'.format(hyperparameter_name), 'a+', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([hyperparameter_name + ": " + str(hyperparameter_value)])
            writer.writerow(['Metric', 'Test-Env'] + model_names + ['avg', 'std'])

            for eval_name in eval_name_list:
                result = np.array(result_summary[eval_name])
                mean_std_list = []
                for row in result:
                    mean_std_list.append(str(row[0]) + " +- " + str(row[1]))
                # TODO: fix for multiple trials
                writer.writerow(["Success", eval_name] + mean_std_list + [np.mean(result[:,0]), np.std(result[:,0])])
                success_avg_list.append(np.mean(result[:,0]))
            writer.writerow([])

            writer.writerow(["Termination reasons"])
            eval_index = 0
            for eval_name in eval_name_list:
                termination_result = termination_summary[eval_name]
                termination_count = defaultdict(list)
                termination_std_count = defaultdict(list)
                for trial in termination_result:
                    for key, value in trial.items():
                        termination_count[key].append(value[0])
                        termination_std_count[key].append(value[1])
                for term_type, term_count in termination_count.items():
                    while len(term_count) != len(model_names):
                        term_count.append(0)
                        termination_std_count[term_type].append(0)
                    mean_std_list = []
                    for n in range(len(term_count)):
                        mean_std_list.append(str(term_count[n]) + " +- " + str(termination_std_count[term_type][n]))
                    pooled_avg = np.mean(term_count)
                    # TODO: fix for multiple trials
                    pooled_std = np.std(term_count)
                    writer.writerow([term_type, eval_name] + mean_std_list + [pooled_avg, pooled_std])

                    if term_type not in term_reason_list:
                        dummy_index = 0
                        while(dummy_index < eval_index):
                            term_reason_list[term_type].append(0.0)
                            dummy_index += 1
                    term_reason_list[term_type].append(pooled_avg)

                # some term_reasons may not exist in some trials. so filling those in
                for term_type, term_count in term_reason_list.items():
                    while (len(term_count) < eval_index+1):
                        term_reason_list[term_type].append(0.0)
                eval_index +=1
            writer.writerow([])

            writer.writerow(["Metrics"])
            for eval_name in eval_name_list:
                metric_result = metrics_summary[eval_name]
                metrics_mean = defaultdict(list)
                metrics_std = defaultdict(list)
                metrics_num_samples = defaultdict(list)
                metrics_median = defaultdict(list)

                for trial in metric_result:
                    for key, value in trial.items():
                        metrics_mean[key].append(value[0])
                        metrics_std[key].append(value[1])
                        metrics_num_samples[key].append(value[2])
                        metrics_median[key].append(value[3])

                for metric_type, metric_mean in metrics_mean.items():
                    mean_std_list = []
                    for n in range(len(metric_mean)):
                        mean_std_list.append(str(metric_mean[n]) + "+-" + str(metrics_std[metric_type][n]) + ", " + str(metrics_median[metric_type][n]))
                    pooled_avg, pooled_std = weighted_avg_and_std(np.array(metric_mean), np.array(metrics_num_samples[metric_type]))
                    pooled_median = np.mean(metrics_median[metric_type])
                    pooled_median_std = np.std(metrics_median[metric_type])
                    writer.writerow([metric_type, eval_name] + mean_std_list + [pooled_avg, pooled_std, pooled_median, pooled_median_std])
                    metrics_list[metric_type].append(pooled_median)
            writer.writerow([])

        param_comparison_list['success'].append(success_avg_list)
        for key, val in term_reason_list.items():
            param_comparison_list[key].append(val)
        for key, val in metrics_list.items():
            param_comparison_list[key].append(val)

    if create_summary:
        with open(path_to_dir + '/{}-results-summary.csv'.format(hyperparameter_name), 'w+', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([])
            writer.writerow([])
            writer.writerow(['Metric name', 'Parameter'] + eval_name_list)
            for metric_name, metrics_list in param_comparison_list.items():
                for idx, param in enumerate(hyperparameter_val_list):
                    metric_list = metrics_list[idx] if idx < len(metrics_list) else [0]
                    writer.writerow([metric_name, param] + metric_list)
                writer.writerow([])

def run_trials(hyperparameter_name, hyperparameter_val_list, eval_env_base, validation_env_base, train_env_base,
               logpath=LOG_PATH_BASE, eval_transfer=False):
    for param in hyperparameter_val_list:
        log_path = logpath + hyperparameter_name + '/' + str(param) + '/'
        if not os.path.exists(log_path):
            os.makedirs(log_path, exist_ok=True)
        with open(logpath + hyperparameter_name + "/eval_env_base.json", "w+") as write_file:
            json.dump(eval_env_base, write_file)
        runner.logger.set_log_path(log_path)
        train_env = train_env_base.copy()
        train_env[hyperparameter_name] = param
        train_env['log_path'] = log_path
        validation_env = validation_env_base.copy()
        validation_env[hyperparameter_name] = param
        eval_env = eval_env_base.copy()
        eval_env[hyperparameter_name] = param
        runner.logger.log("\n###############################################################")
        runner.logger.log("Finding good policy for {}:{} - {}".format(hyperparameter_name,param, get_datetime()))
        runner.logger.log("###############################################################")
        runner.find_good_high_level_policy(log_path=log_path, train_env=train_env, validation_env=validation_env)
        runner.logger.log("Saving best model...")
        for n in range(NUM_POLICIES_TO_SAVE):
            model_filename = log_path + "{}_{}_{}.h5f".format(hyperparameter_name, param, n+1)
            curr_file_name = (log_path+'best_policy_{}.h5f').format(n+1)
            if os.path.exists(curr_file_name):
                os.rename(curr_file_name, model_filename)

    if eval_transfer:
        evaluate_transfer_and_get_results(logpath + hyperparameter_name + '/', eval_env_base, create_summary=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--logdir",
        help="Directory for saving results and logs. Should have / at the end",
        default="./experiments/",
        type=str)
    parser.add_argument(
        "--experiments",
        nargs='+',
        help="List of experiment indices to run",
        default=-1,
        type=int)
    parser.add_argument(
        "--test_type",
        help="0: noisy env, 1: randomized env",
        default=0,
        type=int)
    parser.add_argument(
        "--eval_dir",
        help="eval directory name inside --logdir",
        default="",
        type=str)
    parser.add_argument(
        "--eval_transfer",
        help="Flag for evaluation right after training",
        action="store_true")

    args = parser.parse_args()

    agent_class = KerasDQNLearner
    runner = Runner(agent_class=agent_class,options_class=OptionsEnv, transfer_test_type=args.test_type)

    current_datetime = get_datetime()

    if args.eval_dir != "":
        evaluate_transfer_and_get_results(args.logdir + args.eval_dir + '/')
    else:
        config = Experiments()
        if args.experiments == -1:
            experiments = config.experiments
        else:
            experiments = [config.experiments[i] for i in args.experiments]
        options_default_kwargs = config.options_default_kwargs
        agent_default_kwargs = config.agent_default_kwargs
        for experiment in experiments:
            eval_env_base = experiment['eval_env_base']
            validation_env_base = experiment['validation_env_base']
            train_env_base = experiment['train_env_base']

            hyperparameter_name = experiment['hyperparameter_name']
            hyperparameter_val_list = experiment['hyperparameter_val_list']
            run_trials(hyperparameter_name, hyperparameter_val_list,
                       train_env_base=train_env_base, validation_env_base=validation_env_base,eval_env_base=eval_env_base,
                       logpath=args.logdir, eval_transfer=args.eval_transfer)
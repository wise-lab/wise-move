from numpy import array

from .maneuver import Maneuver

from .constants import MIN_ACCELERATION

# TODO: separate out into different files?? is it really needed?


class TrackSpeed(Maneuver):

    LTL_dict = {}

    def __init__(self, env):
        super().__init__(env)
        self.mode = 'execute'

    def _reset_params(self, mode):
        self.target_lane = self.ego.APs['lane']

    def manual_policy(self, reduced_features_tuple):
        a = -10.0 * (self.env.ego.v - self.env.ego.max_vel)
        return array([a, 0])

    @property
    def initiation_condition(self):
        """a virtual function (property) from ManeuverBase.
            As KeepLane is a default maneuver, it has to be activated and
            available at any time, state, and condition (refer to
            initiation_condition of ManeuverBase for the usual case).

            Returns: True.
        """
        return True


class Yield(Maneuver):

    LTL_dict = {}

    def __init__(self, env):
        super().__init__(env)
        self.mode = 'execute'

    def _reset_params(self, mode):
        self._update_params(mode)
        self.target_lane = self.ego.APs['lane']

    def manual_policy(self, reduced_features_tuple):
        return array([MIN_ACCELERATION, 0])

    @property
    def initiation_condition(self):
        """a virtual function (property) from ManeuverBase.
            As KeepLane is a default maneuver, it has to be activated and
            available at any time, state, and condition (refer to
            initiation_condition of ManeuverBase for the usual case).

            Returns: True.
        """
        return True

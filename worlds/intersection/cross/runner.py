from collections import defaultdict
import numpy as np
from datetime import datetime
import signal, sys

# default paraemters for training
EPISODES_FOR_TEST = 1000
TRIALS_FOR_TEST = 1
TRIALS_FOR_TRAIN = 75
NUM_STEPS_TRAIN = 150000
NUM_POLICIES_TO_SAVE = 10
QUIT_PATIENCE = 1
LOG_PATH_BASE = "./experiments/"
RESULT_RANGE_THRESHOLD = int(0.01*EPISODES_FOR_TEST)
EXP_ANNEAL_STEPS = 20000
BASELINE_SUCCESS=75

# paraemters for quick training test
# EPISODES_FOR_TEST = 10
# TRIALS_FOR_TEST = 2
# TRIALS_FOR_TRAIN = 2
# NUM_STEPS_TRAIN = 5000
# NUM_POLICIES_TO_SAVE = 2
# QUIT_PATIENCE = 1
# LOG_PATH_BASE = "./experiments/"
# RESULT_RANGE_THRESHOLD = 0.001
# EXP_ANNEAL_STEPS = 500
# BASELINE_SUCCESS=1

""" Logger class that prints to screen, and output to file
"""
class Logger():
    def __init__(self, log_path=None):
        self.set_log_path(log_path)

    def set_log_path(self, log_path, overwrite=True):
        """ sets the log path to use
        """
        if log_path is not None:
            self.log_path = log_path + "log.txt"
            if overwrite:
                with open(self.log_path, 'w+') as f:
                    f.write(datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + "\n")
        else:
            self.log_path = None

    def log(self, out):
        """ Print to screen and save to file.
        """
        print (out)
        if self.log_path is not None:
            with open(self.log_path, 'a+') as f:
                print(out, file=f)

""" General class containing training and testing scripts
"""
class Runner():
    def __init__(self, agent_class, options_class, log_path=None, transfer_test_type=0):
        """
        Args:
            agent_class: class to use for initializing agent
            options_class: class to use for initializing environment
            log_path: log path to save output
            transfer_test_type: type of transfer test to run
        """
        self.agent_class = agent_class
        self.options_class = options_class
        self.logger = Logger(log_path)

        tests = []

        if transfer_test_type is 0:
            # ttc tests
            tests.append([])
            tests.append(['system_lag'])
            tests.append(['kf_error'])
            tests.append(['system_lag', 'kf_error'])
        elif transfer_test_type is 1:
            # dr tests
            tests.append(['system_lag', 'kf_error'])
            tests.append(['system_lag', 'kf_error', 'noisy_intersection', 'ego_noise'])
            tests.append(['imperfect_perception'])
            tests.append(['noisy_intersection', 'ego_noise', 'imperfect_perception'])
        elif transfer_test_type is 2:
            # many tests
            tests.append([])
            tests.append(['system_lag'])
            tests.append(['kf_error'])
            tests.append(['system_lag', 'kf_error'])
            tests.append(['rand_system_lag', 'kf_error'])
            tests.append(['system_lag', 'rand_kf_error'])
            tests.append(['system_lag', 'kf_error', 'vanish_veh'])
            tests.append(['system_lag', 'kf_error', 'xy_noise'])
            tests.append(['imperfect_perception'])
            tests.append(['system_lag', 'kf_error', 'ego_noise'])
            tests.append(['system_lag', 'kf_error', 'noisy_intersection'])
            tests.append(['system_lag', 'kf_error', 'noisy_intersection', 'ego_noise'])
            tests.append(['noisy_intersection', 'ego_noise', 'imperfect_perception'])
        elif transfer_test_type is 3:
            # abstract test
            tests.append(['system_lag', 'kf_error'])
            tests.append(['imperfect_perception'])
            tests.append(['noisy_intersection', 'ego_noise', 'imperfect_perception'])
        else:
            tests.append(['randomize_intersection'])
            tests.append(['randomize_ego'])
            tests.append(['randomize_vehicles'])
            tests.append(['imperfect_perception'])
            tests.append(['randomize_intersection', 'randomize_ego', 'randomize_vehicles'])
            tests.append(['randomize_intersection', 'randomize_ego', 'randomize_vehicles', 'imperfect_perception'])
            tests.append(['noisy_intersection', 'ego_noise', 'imperfect_perception'])

        self.transfer_tests = tests

    @staticmethod
    def create_options_env(options_class, **kwargs):
        """ Initialize environment using environment class
        Args:
            options_class: class to use for initializing environment
        """
        return options_class("config.json", **kwargs)

    @staticmethod
    def train_agent(    options = None,
                        agent=None,
                        nb_steps=50000,
                        save_path=None,
                        checkpoint=False,
                        checkpoint_interval=10000,
                        log_in_file=False,
                        verbose=1):

        if agent is None or options is None:
            raise ValueError("agent/options has to be passed")

        agent.train(options, nb_steps=nb_steps, model_checkpoints=checkpoint, log_in_file=log_in_file, checkpoint_interval=checkpoint_interval,
                    verbose=verbose)

        if save_path is not None:
            agent.save_model(save_path)

        return agent

    @staticmethod
    def evaluate_agent(     options = None,
                            agent=None,
                            nb_episodes_for_test=EPISODES_FOR_TEST,
                            nb_trials=TRIALS_FOR_TEST,
                            visualize=False,
                            verbose=2,
                            use_rule_based=False,
                            use_random_policy=False):

        if agent is None or options is None:
            raise ValueError("agent/options has to be passed")

        success_list = []
        termination_reason_list = defaultdict(list)
        metrics_list = defaultdict(list)
        for trial in range(nb_trials):
            current_success, current_termination_reason, _, metrics = agent.test_model(
                options, nb_episodes=nb_episodes_for_test, visualize=visualize,verbose=verbose, use_rule_based=use_rule_based, 
                use_random_policy=use_random_policy)
            success_list.append(current_success)

            # collate termination reasons
            for reason, count in current_termination_reason.items():
                termination_reason_list[reason].append(count)
            # collate metrics
            for metric, value_list in metrics.items():
                metrics_list[metric] += value_list

        success_list = np.array(success_list)
        result = "Success: Avg: {}, Std: {}\n".format(
            np.mean(success_list), np.std(success_list))
        print (result)

        for reason, count_list in termination_reason_list.items():
            while len(count_list) != nb_trials:
                count_list.append(0)
            termination_reason_list[reason] = count_list
        return success_list, termination_reason_list, metrics_list

    def signal_handler(self, sig, frame):
        self.agent.save_model(file_name=self.save_path_backup)
        sys.exit(0)

    def train_high_level_policy(self,
                                nb_steps=150000,
                                test=True,
                                visualize=False,
                                save_path="highlevel_weights.h5f",
                                checkpoint=False,
                                checkpoint_interval=10000,
                                log_in_file=False,
                                test_env_kwargs=None,
                                train_env_kwargs=None,
                                agent_default_kwargs=None):

        signal.signal(signal.SIGINT, self.signal_handler)
        self.save_path_backup = save_path
        options = self.create_options_env(self.options_class, **train_env_kwargs)

        if visualize:
            options.visualize_low_level_steps = True

        self.agent = self.agent_class(options, **agent_default_kwargs)

        self.agent = self.train_agent(options, self.agent, nb_steps=nb_steps, save_path=save_path, checkpoint=checkpoint,
                            checkpoint_interval=checkpoint_interval, log_in_file=log_in_file)

        if test:
            options = self.create_options_env(self.options_class, **test_env_kwargs)
            options.visualize_low_level_steps = False
            self.evaluate_agent(options, self.agent, nb_episodes_for_test=100, nb_trials=10, verbose=0)
        
        self.agent.save_model(file_name=save_path)

        return self.agent

    def test_high_level_policy(self,
                               nb_episodes_for_test=100,
                               trained_agent_file="highlevel_weights.h5f",
                               pretrained=False,
                               visualize=True,
                               test_env_kwargs=None,
                               save_feature_space=False,
                               use_rule_based=False,
                               use_random_policy=False):
        options = self.create_options_env(self.options_class, **test_env_kwargs)

        if pretrained:
            trained_agent_file = "backends/trained_policies/cross_intersection/" + trained_agent_file

        agent = self.agent_class(options)

        if not use_rule_based:
            agent.load_model(file_name=trained_agent_file)

        agent.test_model(
            options, nb_episodes=nb_episodes_for_test, visualize=visualize, save_feature_space=save_feature_space,
            save_suffix='wisemove', use_rule_based=use_rule_based, use_random_policy=use_random_policy)

    def evaluate_high_level_policy(self,
                                   nb_episodes_for_test=EPISODES_FOR_TEST,
                                   nb_trials=TRIALS_FOR_TEST,
                                   trained_agent_file="highlevel_weights.h5f",
                                   pretrained=False,
                                   visualize=False,
                                   verbose=2,
                                   test_env_kwargs=None,
                                   use_rule_based=False,
                                   use_random_policy=False):
        if test_env_kwargs is None:
            raise ValueError("Pass test_env_kwargs argument.")
        options = self.create_options_env(self.options_class, **test_env_kwargs)

        if pretrained:
            trained_agent_file = "backends/trained_policies/cross_intersection/" + trained_agent_file

        agent = self.agent_class(options)

        if not use_rule_based:
            agent.load_model(file_name=trained_agent_file)

        success_list, termination_reason_list, metrics =  self.evaluate_agent(options, agent, nb_episodes_for_test=nb_episodes_for_test,
                              nb_trials=nb_trials, visualize=visualize, verbose=verbose, use_rule_based=use_rule_based,
                              use_random_policy=use_random_policy)

        # display results if verbose
        if verbose >=1:
            mean_success = np.mean(success_list)
            std_success = np.std(success_list)
            metrics_results = {}
            for metric, value_list in metrics.items():
                metrics_results[metric] = [np.mean(value_list), np.std(value_list), np.median(value_list)]

            self.logger.log("Success: {} ({})".format(mean_success, std_success))
            print("Termination reason(s):")
            for reason, count_list in termination_reason_list.items():
                count_list = np.array(count_list)
                while count_list.size != nb_trials:
                    count_list = np.append(count_list, 0)
                print("{}: Avg: {}, Std: {}".format(reason, np.mean(count_list),
                                                    np.std(count_list)))
            self.logger.log("Metrics: {}".format(metrics_results))

        return [success_list, termination_reason_list, metrics]

    def find_good_high_level_policy(self,
                                    nb_trials=TRIALS_FOR_TRAIN, nb_episodes_for_test=EPISODES_FOR_TEST,
                                    nb_trials_test=TRIALS_FOR_TEST, nb_steps_train=NUM_STEPS_TRAIN, num_policies_to_save=NUM_POLICIES_TO_SAVE,
                                    log_path=LOG_PATH_BASE, train_env=None, validation_env=None,
                                    log_metrics=False):

        if train_env is None or validation_env is None:
            raise ValueError("Pass environment to train and test on.")

        agent_default_kwargs = {'log_path': log_path}

        give_up_count = 0
        top_n = np.zeros(num_policies_to_save)
        trial = 0
        give_up_flag = False
        nb_steps = nb_steps_train
        while trial <= nb_trials:
            trial += 1
            start_time = datetime.now()
            self.logger.log("\nTrial: {}, steps: {} -  {}".format(trial, nb_steps, start_time.strftime("%m/%d/%Y, %H:%M:%S")))
            self.logger.log("-------------------------------------------------")

            options = self.create_options_env(self.options_class, **train_env)
            agent_default_kwargs['exp_anneal_steps'] = EXP_ANNEAL_STEPS
            agent = self.agent_class(options, **agent_default_kwargs)
            agent = self.train_agent(options, agent, nb_steps=nb_steps, verbose=0)

            # check if training exited early, if so, dont count it as a trial
            if hasattr(agent.agent_model, 'exited_early'):
                trial -= 1
            else:
                # test on validation set
                self.logger.log("Evaluating on validation testset...")
                options_test = self.create_options_env(self.options_class, **validation_env)
                options_test.visualize_low_level_steps = False
                success_list, termination_reason_list, metrics = self.evaluate_agent(options_test, agent, nb_episodes_for_test=nb_episodes_for_test,
                                              nb_trials=nb_trials_test, verbose=0)
                mean_success = np.mean(success_list)
                metrics_results = {}
                for metric, value_list in metrics.items():
                    metrics_results[metric] = [np.mean(value_list), np.std(value_list), len(value_list)]

                self.logger.log("Mean success: {}".format(mean_success))

                if log_metrics:
                    self.logger.log("Metrics: {}".format(metrics_results))

                min_index = np.argmin(top_n)
                if mean_success >= top_n[min_index]:
                    top_n[min_index] = mean_success
                    agent.save_model(file_name=log_path+"best_policy_{}.h5f".format(min_index+1))
                if np.min(top_n) > BASELINE_SUCCESS:
                    if np.max(top_n) - np.min(top_n) > RESULT_RANGE_THRESHOLD:
                        self.logger.log("Resetting giveup count")
                        give_up_count = 0
                    else:
                        give_up_count += 1
                        self.logger.log("Give up count: {}".format(give_up_count))
                        if give_up_count >= QUIT_PATIENCE:
                            self.logger.log("Giving up. Top policies: {}".format(top_n))
                            give_up_flag = True
                            break

                self.logger.log("Top policies: {}".format(top_n))
                self.logger.log("Std: {}".format(np.std(top_n)))
                self.logger.log("Time taken: {}".format(datetime.now() - start_time))

        if not give_up_flag:
            top_policy = np.max(top_n)
            bad_policy_indices = np.where(top_n < (top_policy - RESULT_RANGE_THRESHOLD))
            if len(bad_policy_indices) > 0:
                bad_policy_indices = bad_policy_indices[0]
            import os
            for index in bad_policy_indices:
                os.remove(log_path+"best_policy_{}.h5f".format(index+1))
                top_n = np.delete(top_n, bad_policy_indices)
        self.logger.log("Top policies: {}".format(top_n))

    def evaluate_transfer(self, filename, tests, eval_env=None, rd_object=None):
        if eval_env is None or rd_object is None:
            raise ValueError("Pass eval env base kwargs")
        results = {}
        key_order = []
        for test in tests:
            rd_object.use_default_features()
            test_env_kwargs = eval_env.copy()
            evaluation_name = "eval"
            for param in test:
                test_env_kwargs[param] = True
                evaluation_name += "-" + param
            key_order.append(evaluation_name)
            success_list, termination_reason_list, metrics = self.evaluate_high_level_policy(
                visualize=False,
                nb_episodes_for_test=EPISODES_FOR_TEST,
                pretrained=False,
                trained_agent_file=filename,
                nb_trials=TRIALS_FOR_TEST,
                verbose=0,
                test_env_kwargs=test_env_kwargs)
            result = [np.mean(success_list), np.std(success_list)]
            termination_results = {}
            for reason, count_list in termination_reason_list.items():
                count_list = np.array(count_list)
                termination_results[reason] = [np.mean(count_list), np.std(count_list)]
            metrics_results = {}
            for metric, value_list in metrics.items():
                metrics_results[metric] = [np.mean(value_list), np.std(value_list), len(value_list), np.median(value_list)]

            results[evaluation_name] = [result, termination_results, metrics_results]

        self.logger.log(results)
        return results, key_order
import json
from backends import RLController
import importlib
from gym.spaces import Discrete


class OptionsEnv:
    """Loads options and config for experiments.
    """
    #: visualization flag
    visualize_low_level_steps = False

    def __init__(self, json_path, *env_args, **env_kwargs):
        """Constructor for the options graph.

        Args:
            json_path: path to the json file which stores the configuration
                for this options graph
            env_class: the gym compliant environment class
            env_args: arguments to pass when initializing the env
            env_kwargs: named arguments to pass when initializing the env
        """

        self.global_config = json.load(open(json_path))
        env_config_file = self.global_config["config"]
        self.config = json.load(open(env_config_file)) #TODO: make this relative to env folder

        #: Gym compliant environment instance. This instance also has a
        #  back-reference to this options instance.
        env_module = importlib.import_module(self.global_config["world"])
        env_class = getattr(env_module, self.global_config["env_class"])

        self.env = env_class(*env_args, **env_kwargs)
        #: options
        self.options = self.config["options"]

        options_module = importlib.import_module(self.global_config["world"])
        self.maneuvers = {
            key: getattr(options_module,value)(self.env)
            for key, value in self.options.items()
        }
        self.maneuvers_alias = list(self.maneuvers.keys())  # so that maneuver can be referenced using an int

        #: high level policy over low level policies
        #  possible classes : rl
        if self.config["method"] == "rl":
            self.controller = RLController(self.env, self.maneuvers, None)
        else:
            raise Exception(self.__class__.__name__ + " Controller to be used not specified")

        # Adds a penalty each high-level step so that agent does not wait too long
        self.options_penalty = - 0.04


    def step(self, option):
        """Complete an episode using specified option. This assumes that the
        manager's env is at a place where the option's initiation condition is
        met.

        Args:
            option: index of high level option to be executed
        """

        #check if the incoming option is an integer or an option alias(string)
        try:
            option = int(option)
            option_alias = self.maneuvers_alias[option]
        except ValueError:
            option_alias = option

        self.controller.set_current_node(option_alias)

        if option_alias == 'trackspeed':
            commit = True
        else:
            commit = False

        # Rollout to end of episode if agent chooses keeplane, else do high-level step
        # to-go-or-not-go scenario
        if commit:
            observation, reward, terminal, info = self.controller.execute_and_get_terminal_reward(
                visualize_low_level_steps=self.visualize_low_level_steps)
        else:
            observation, reward, terminal, info = self.controller.step_current_node(
                visualize_low_level_steps=self.visualize_low_level_steps)

        # Add penalty for time taken with options
        reward += self.options_penalty

        # save feature space for JSON generation (to import into Unreal)
        if self.env.save_feature_space_flag:
            self.env.update_state_for_json()
            if terminal:
                self.env.add_current_scenario_json()

        return observation, reward, terminal, info

    def reset(self):
        self.controller.set_current_node(None)
        ret_val = self.env.reset()

        # save feature space for JSON generation (to import into Unreal)
        if self.env.save_feature_space_flag:
            self.env.save_initial_state_for_json()

        # Don't do anything for first few steps because unreal has a startup delay
        while (self.env.step_count < self.env.startup_delay):
            ret_val = self.step(1)[0]
        return ret_val

    @property
    def action_space(self):
        return Discrete(len(self.maneuvers))

    @property
    def observation_space(self):
        return self.env.observation_space

    def set_current_node(self, node_alias):
        """Sets the current node for controller. Used for training/testing a
        particular node.

        Args:
            node_alias: alias of the node as per config file
        """
        self.controller.set_current_node(node_alias)

    @property
    def current_node(self):
        return self.controller.current_node

    def get_number_of_nodes(self):
        return len(self.maneuvers)

    def render(self, mode='human'):
        self.visualize_low_level_steps = True
        return self.env.render(mode=mode)

    def seed(self, seed=None):
        return self.env.seed(seed)

    def get_current_seed(self):
        return self.env.get_current_seed()


import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import math

plt.style.use('seaborn-whitegrid')

# Set the base wise-move directory to the Python working directory.
os.chdir('../../..')
sys.path.append(os.getcwd())

TIMESTEP = 0.1
SAVE_FOLDER = '/media/a4balakr/DATADisk/projects/wise-move-dev/experiments/ego_data/'

PLOT_EGO = True
PLOT_DYN_OBJS = False
PLOT_TRANSFER_DATA = False
PLOT_STACK = True
PLOT_DEFAULT = True

STACK_COLOUR = '#DC143C'
WISEMOVE_COLOUR = '#1E90FF'
TRANSFER_COLOUR = '#8A2BE2'

EGO_TIMESTEPS=105

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def rotate_around_point(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in degrees.
    """
    ox, oy = origin
    px, py = point

    angle = math.radians(angle)

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

if PLOT_EGO:
    # EGO features
    figure, (dist_ax, vel_ax) = plt.subplots(1, 2, constrained_layout=True)

    dist_ax.set_ylabel('distance (m)')
    dist_ax.set_xlabel('timestep')

    vel_ax.set_ylabel('velocity (m/s)')
    vel_ax.set_xlabel('timestep')

    # acc_ax.set_ylabel('acceleration (m/s²)')
    # acc_ax.set_xlabel('timestep')

    # position_ax.set_ylabel('y')
    # position_ax.set_xlabel('x')

    # PLOT WISEMOVE
    if PLOT_DEFAULT:
        ego_file_wisemove = SAVE_FOLDER + 'ego-features-wisemove.npy'
        ego_features_wisemove = np.load(ego_file_wisemove)

        # calc distance covered
        points_wisemove = ego_features_wisemove[:,:2]
        distances_wisemove = [np.linalg.norm(x - points_wisemove[i-1]) for i,x in enumerate(points_wisemove)][1:]
        cum_distances_wisemove = np.cumsum(distances_wisemove)

        # calc dist from velocity
        # vel = ego_features_wisemove[:, 2]
        # dist = np.array(vel) * TIMESTEP
        # cum_distances = np.cumsum(dist)

        # # plot position
        # position_default = ego_features_wisemove[:,0]
        # position_default = position_default - position_default[0]

        dist_ax.plot(cum_distances_wisemove[:EGO_TIMESTEPS-1], color=WISEMOVE_COLOUR, linewidth=1)
        wm, = vel_ax.plot(ego_features_wisemove[:EGO_TIMESTEPS, 2], color=WISEMOVE_COLOUR, linewidth=1, label='wm')
        #acc_ax.plot(ego_features_wisemove[:, 3], color=WISEMOVE_COLOUR, label='acceleration', linewidth=1)
        #position_ax.plot(position_default, color=WISEMOVE_COLOUR, label='position', linewidth=1)

    if PLOT_STACK:
        ego_file_stack = SAVE_FOLDER + 'ego-features-stack.npy'
        ego_features_stack_all = np.load(ego_file_stack)

        ego_features_stack_trials = np.array([  ego_features_stack_all[:EGO_TIMESTEPS, :3],
                                                ego_features_stack_all[165:165 + EGO_TIMESTEPS, :3],
                                                ego_features_stack_all[368:368 + EGO_TIMESTEPS, :3],
                                                ego_features_stack_all[687:687 + EGO_TIMESTEPS, :3],
                                                ego_features_stack_all[530:530 + EGO_TIMESTEPS, :3]])

        cum_distances_stack_trials = []
        vel_stack_trials = []
        for ego_features_stack in ego_features_stack_trials:
            # calc dist from velocity
            points_stack = ego_features_stack[:,:2]
            distances_stack = [np.linalg.norm(x - points_stack[i-1]) for i,x in enumerate(points_stack)][1:]
            cum_distances_stack = np.cumsum(distances_stack)
            cum_distances_stack_trials.append(cum_distances_stack)

            vel = ego_features_stack[:, 2]
            vel_stack_trials.append(vel)
            # dist = np.array(vel) * TIMESTEP
            # cum_distances = np.cumsum(dist)

            # calc acc from velocity
            vel_diff = [x - vel[i - 1] for i, x in enumerate(vel)][1:]
            acc = np.array(vel_diff) / TIMESTEP
            #acc_stack = moving_average(acc, 5)

            # plot position
            # position_stack = ego_features_stack[:,0]
            # position_stack = position_stack - position_stack[0]

        cum_distances_stack_trials= np.array(cum_distances_stack_trials)
        vel_stack_trials = np.array(vel_stack_trials)

        dist_ax.plot(np.mean(cum_distances_stack_trials,axis=0), color=STACK_COLOUR, linewidth=1)
        dist_ax.fill_between(range(EGO_TIMESTEPS-1), np.mean(cum_distances_stack_trials,axis=0) - np.std(cum_distances_stack_trials,axis=0),
                         np.mean(cum_distances_stack_trials,axis=0) + np.std(cum_distances_stack_trials,axis=0),
                         color=STACK_COLOUR, alpha=0.3)
        stack, = vel_ax.plot(np.mean(vel_stack_trials,axis=0), color=STACK_COLOUR, linewidth=1, label='unreal')
        vel_ax.fill_between(range(EGO_TIMESTEPS), np.mean(vel_stack_trials,axis=0) - np.std(vel_stack_trials,axis=0),
                         np.mean(vel_stack_trials,axis=0) + np.std(vel_stack_trials,axis=0),
                         color=STACK_COLOUR, alpha=0.3)

        #acc_ax.plot(acc, color=STACK_COLOUR, label='acceleration', linewidth=1)
        #position_ax.plot(position_stack, color=STACK_COLOUR, label='position', linewidth=1)

    if PLOT_TRANSFER_DATA:
        ego_file_wisemove_transfer = SAVE_FOLDER + 'ego-features-wisemove-transfer.npy'
        ego_features_wisemove_transfer_all = np.load(ego_file_wisemove_transfer)

        ego_features_transfer_trials = []
        for index, n in enumerate(ego_features_wisemove_transfer_all[:,2]):
            if n == 0.0:
                ego_features_transfer_trials.append(ego_features_wisemove_transfer_all[index:index+EGO_TIMESTEPS, :3])

        cum_distances_transfer_trials = []
        vel_transfer_trials = []
        for ego_features_wisemove_transfer in ego_features_transfer_trials:
            # calc dist from velocity
            points_wisemove_transfer = ego_features_wisemove_transfer[:, :2]
            distances_wisemove_transfer = [np.linalg.norm(x - points_wisemove_transfer[i - 1]) for i, x in enumerate(points_wisemove_transfer)][1:]
            cum_distances_wisemove_transfer = np.cumsum(distances_wisemove_transfer)
            cum_distances_transfer_trials.append(cum_distances_wisemove_transfer)

            # calc acc from velocity
            vel = ego_features_wisemove_transfer[:, 2]
            vel_transfer_trials.append(vel)

        cum_distances_transfer_trials= np.array(cum_distances_transfer_trials)
        vel_transfer_trials = np.array(vel_transfer_trials)

        dist_ax.plot(np.mean(cum_distances_transfer_trials,axis=0), color=TRANSFER_COLOUR, linewidth=1)
        dist_ax.fill_between(range(EGO_TIMESTEPS-1), np.mean(cum_distances_transfer_trials,axis=0) - np.std(cum_distances_transfer_trials,axis=0),
                         np.mean(cum_distances_transfer_trials,axis=0) + np.std(cum_distances_transfer_trials,axis=0),
                         color=TRANSFER_COLOUR, alpha=0.3)
        wm_noise, = vel_ax.plot(np.mean(vel_transfer_trials,axis=0), color=TRANSFER_COLOUR, label='wm-noise', linewidth=1)
        vel_ax.fill_between(range(EGO_TIMESTEPS), np.mean(vel_transfer_trials,axis=0) - np.std(vel_transfer_trials,axis=0),
                         np.mean(vel_transfer_trials,axis=0) + np.std(vel_transfer_trials,axis=0),
                         color=TRANSFER_COLOUR, alpha=0.3)
        #acc_ax.plot(acc_transfer, color=TRANSFER_COLOUR, label='acceleration', linewidth=1)
        #position_ax.plot(position_transfer, color=TRANSFER_COLOUR, label='position', linewidth=1)

    figure.set_size_inches(12, 4)
    plt.legend(handles=[wm, stack], frameon=True, loc=4)
    plt.savefig(SAVE_FOLDER + 'ego-features.png')

    plt.clf()

if PLOT_DYN_OBJS:
    dyn1_file_wisemove = SAVE_FOLDER + 'dynobj1-features-wisemove.npy'
    dyn1_features_wisemove = np.load(dyn1_file_wisemove)

    dyn1_file_stack = SAVE_FOLDER + 'dynobj1-features-stack.npy'
    dyn1_features_stack = np.load(dyn1_file_stack)
    #dyn1_features_stack = dyn1_features_stack[:23]

    # Dynamic object features
    figure, (dist_ax, vel_ax) = plt.subplots(1, 2, constrained_layout=True)
    figure.suptitle('Dyn Object features (wisemove vs stack)', fontsize=16)

    # distance
    points_wisemove = dyn1_features_wisemove[:,:2]
    distances_wisemove = [np.linalg.norm(x - points_wisemove[i-1]) for i,x in enumerate(points_wisemove)][1:]
    cum_distances_wisemove = np.cumsum(distances_wisemove)

    points_stack = dyn1_features_stack[:,:2]
    distances_stack = [np.linalg.norm(x - points_stack[i-1]) for i,x in enumerate(points_stack)][1:]
    cum_distances_stack = np.cumsum(distances_stack)

    dist_ax.set_ylabel('distance')
    dist_ax.set_xlabel('time')
    dist_ax.plot(cum_distances_wisemove, color=WISEMOVE_COLOUR, label='distance', linewidth=1)
    dist_ax.plot(cum_distances_stack, color=STACK_COLOUR, label='distance', linewidth=1)

    # velocity
    vel_ax.set_ylabel('velocity')
    vel_ax.set_xlabel('time')
    vel_ax.plot(dyn1_features_wisemove[:, 2], color=WISEMOVE_COLOUR, label='velocity', linewidth=1)
    vel_ax.plot(dyn1_features_stack[:, 2], color=STACK_COLOUR, label='velocity', linewidth=1)

    figure.set_size_inches(10, 5)
    plt.savefig(SAVE_FOLDER + 'dyn-features.png')
import numpy as np
import sys, os, argparse
import json
import time

# Set the base wise-move directory to the Python working directory.
os.chdir('../../..')
sys.path.append(os.getcwd())

from options_loader import OptionsLoader
from worlds.intersection.two_way.learners.baselines_learner import RecurrentDQNLearner, RecurrentPPO2Learner, DQNLearner
from worlds.intersection.two_way.learners.kerasrl_learner import DQNLearner as KerasDQNLearner

from worlds.intersection.two_way.env.constants import *


def train_agent(    options = None,
                    agent=None,
                    nb_steps=50000,
                    save_path="highlevel_weights.h5f",
                    checkpoint=False,
                    checkpoint_interval=10000,
                    log_in_file=False,
                    set_low_level_policies=False):

    if agent is None or options is None:
        raise ValueError("agent/options has to be passed")

    if set_low_level_policies:
        agent.set_low_level_policies(options.maneuvers)

    agent.train(options, nb_steps=nb_steps, model_checkpoints=checkpoint, log_in_file=log_in_file, checkpoint_interval=checkpoint_interval)

    agent.save_model(save_path)

    return agent

def create_options_env(**kwargs):
    return OptionsLoader("config.json", **kwargs)

def get_current_log_path(hyperparameter_name):
    return log_path+"{}/".format(hyperparameter_name)

def conduct_trials(log_subdir_name, test_name, agent_kwargs={}, env_kwargs={}, options_kwargs={}, test_description=""):
    success_list = []
    termination_reason_list = {}
    start = time.clock()
    for trial in range(num_trials):
        options = create_options_env(options_kwargs=options_kwargs, **env_kwargs)
        options.visualize_low_level_steps = False

        if 'lr' in agent_kwargs:
            nb_steps = int((1/agent_kwargs['lr']) * 50)
        else:
            nb_steps = nb_steps_global

        if 'exp_anneal_steps' not in agent_kwargs:
            agent_kwargs['exp_anneal_steps'] =  int(exp_anneal_steps_ratio * nb_steps)

        curr_log_path = get_current_log_path(log_subdir_name) + \
                                     "{}/{}".format(test_name, trial)

        agent = agent_class(options,
                            tensorboard=True,
                            log_path=curr_log_path,
                            **agent_kwargs)

        agent = train_agent(options, agent, nb_steps=nb_steps, set_low_level_policies=set_low_level_policies,
                            save_path=curr_log_path + '/weights_{}.h5f'.format(test_name))

        options = create_options_env(**test_env_kwargs)
        options.visualize_low_level_steps = False
        current_success, current_termination_reason, _ = agent.test_model(options, nb_episodes=nb_episodes_for_test, visualize=False)

        success_list.append(current_success)
        for reason, count in current_termination_reason.items():
            if reason in termination_reason_list:
                termination_reason_list[reason].append(count)
            else:
                termination_reason_list[reason] = [count]
        options.env.close_window()

    time_taken = time.clock() - start

    success_list = np.array(success_list)
    print("\nSuccess: Avg: {}, Std: {}".format(
        np.mean(success_list), np.std(success_list)))
    print("Termination reason(s):")
    for reason, count_list in termination_reason_list.items():
        count_list = np.array(count_list)
        while count_list.size != num_trials:
            count_list = np.append(count_list, 0)
        print("{}: Avg: {}, Std: {}".format(reason, np.mean(count_list),
                                            np.std(count_list)))

    with open(get_current_log_path(log_subdir_name) + "results.txt", "a+") as myfile:
        myfile.write("{}:\n {}\n".format(test_name, test_description))
        myfile.write("Time taken: {}\n".format(time_taken))
        myfile.write("Success: Avg: {}, Std: {}\n".format(
            np.mean(success_list), np.std(success_list)))
        myfile.write("Termination reason(s):\n")
        for reason, count_list in termination_reason_list.items():
            count_list = np.array(count_list)
            while count_list.size != num_trials:
                count_list = np.append(count_list, 0)
            myfile.write("{}: Avg: {}, Std: {}\n".format(reason, np.mean(count_list),
                                                         np.std(count_list)))
        myfile.write("------------------------\n\n")

def get_random_values_for_hyperparameters(param_range_list):
    current_kwargs = {}
    for param, param_range in param_range_list.items():
        if isinstance(param_range[0], float):
            current_kwargs[param] = np.random.uniform(param_range[0], param_range[1])
        elif isinstance(param_range[0], int):
            current_kwargs[param] = np.random.randint(param_range[0], param_range[1])
    hyperparameter_values_str = ''
    for param, value in current_kwargs.items():
        hyperparameter_values_str += "{}({}),".format(param, value)

    return current_kwargs, hyperparameter_values_str

def write_params_to_textfile(log_dir):
    os.makedirs(os.path.dirname(get_current_log_path(log_dir)), exist_ok=True)
    with open(get_current_log_path(log_dir) + "results.txt", "a+") as myfile:
        myfile.write(json.dumps(env_default_kwargs) + "\n")
        myfile.write(json.dumps(agent_default_kwargs) + "\n")
        myfile.write(json.dumps(options_default_kwargs) + "\n\n")

def perform_interval_search(hyperparameter_name, hyperparameter_val_list, env_kwargs={}, agent_kwargs={}, options_kwargs={},
                       pass_hyperparameter_in='agent'):
    write_params_to_textfile(hyperparameter_name)

    for hyperparameter_val in hyperparameter_val_list:
        if pass_hyperparameter_in == "agent":
            agent_kwargs[hyperparameter_name] = hyperparameter_val
        elif pass_hyperparameter_in == "env":
            env_kwargs[hyperparameter_name] = hyperparameter_val
        elif pass_hyperparameter_in == "options":
            options_kwargs[hyperparameter_name] = hyperparameter_val

        conduct_trials(hyperparameter_name, "{}_{}".format(hyperparameter_name, hyperparameter_val), agent_kwargs, env_kwargs, options_kwargs)

def perform_random_search(env_param_range_list={}, agent_param_range_list={}, options_param_range_list={},
                          random_log_dir="random_search"):
    if len(env_param_range_list) == 0 and len(agent_param_range_list) == 0:
        raise ValueError("Atleast one of param range list of agent, env or options should be passed")

    write_params_to_textfile(random_log_dir)

    # random parameter search
    for n in range(num_combinations_to_search):
        log_name = 'combination_{}'.format(n + 1)
        hyperparameter_values_str = ''

        current_env_kwargs = env_default_kwargs
        if len(env_param_range_list) > 0:
            hyperparameter_values_str += 'env:'
            current_kwargs_temp, hyperparameter_values_temp = get_random_values_for_hyperparameters(env_param_range_list)
            current_env_kwargs.update(current_kwargs_temp)
            hyperparameter_values_str += hyperparameter_values_temp
            hyperparameter_values_str += '\n'

        current_agent_kwargs = agent_default_kwargs
        if len(agent_param_range_list) > 0:
            hyperparameter_values_str += 'agent:'
            current_kwargs_temp, hyperparameter_values_temp = get_random_values_for_hyperparameters(agent_param_range_list)
            current_agent_kwargs.update(current_kwargs_temp)
            hyperparameter_values_str += hyperparameter_values_temp
            hyperparameter_values_str += '\n'

        current_options_kwargs = options_default_kwargs
        if len(options_param_range_list) > 0:
            hyperparameter_values_str += 'options:'
            current_kwargs_temp, hyperparameter_values_temp = get_random_values_for_hyperparameters(options_param_range_list)
            current_options_kwargs.update(current_kwargs_temp)
            hyperparameter_values_str += hyperparameter_values_temp
            hyperparameter_values_str += '\n'

        conduct_trials(random_log_dir, log_name, env_kwargs=current_env_kwargs,
                       agent_kwargs=current_agent_kwargs, options_kwargs=current_options_kwargs,test_description=hyperparameter_values_str)

if __name__ == "__main__":
    agent_class = KerasDQNLearner
    set_low_level_policies = False

    nb_steps_global = 100000
    num_trials = 3
    nb_episodes_for_test = 100
    num_combinations_to_search = 60
    exp_anneal_steps_ratio = 0.57
    log_path = "./logs/"

    env_default_kwargs = {
        'n_others_range': (MAX_NUM_VEHICLES, MAX_NUM_VEHICLES),
        'generate_collision_scenario': True,
        'collision_scenario_probability': 0.5,
        'seed': 1000
    }

    test_env_kwargs = {
        'terminate_on_ego_collision': True,
        'n_others_range': (MAX_NUM_VEHICLES, MAX_NUM_VEHICLES),
        'generate_collision_scenario': True,
        'collision_scenario_probability': 0.5,
        'seed': 1
    }

    options_default_kwargs = {
    }

    agent_default_kwargs = {
    }

    print("Running hyperparameter interval search with following settings:")
    print("nb_steps_global: {}".format(nb_steps_global))
    print("num_trials: {}".format(num_trials))
    print("nb_episodes_for_test: {}".format(nb_episodes_for_test))
    print("Train env args:")
    print(env_default_kwargs)
    print("Test env args:")
    print(test_env_kwargs)

####### INTERVAL TESTS ###############################################

    hyperparameter_name = 'exp_min'
    hyperparameter_val_list = [0.01, 0.05, 0.1, 0.2, 0.3]
    perform_interval_search(hyperparameter_name, hyperparameter_val_list, env_kwargs=env_default_kwargs, agent_kwargs=agent_default_kwargs,
                       options_kwargs=options_default_kwargs, pass_hyperparameter_in='agent')

    agent_default_kwargs = {
        'exp_policy': 'eps_greedy'
    }

    hyperparameter_name = 'exp_start'
    hyperparameter_val_list = [0.1, 0.2, 0.3, 0.4]
    perform_interval_search(hyperparameter_name, hyperparameter_val_list, env_kwargs=env_default_kwargs, agent_kwargs=agent_default_kwargs,
                       options_kwargs=options_default_kwargs, pass_hyperparameter_in='agent')

########## RANDOM SEARCH #######################################################################
    # nb_steps_global = 50000
    # num_trials = 1
    # nb_episodes_for_test = 100
    # num_combinations_to_search = 60
    # exp_anneal_steps_ratio = 0.57
    # log_path = "./logs/"
    #
    # env_default_kwargs = {
    #     'terminate_on_ego_collision': True
    # }
    #
    # agent_default_kwargs = {
    # }
    #
    # options_default_kwargs = {
    # }
    #
    # # env_param_range_list = {'reward_in_goal': [0, 200],
    # #                         'reward_per_metre_progess': [0.0, 10.0],
    # #                         'reward_in_ego_collision': [-200,0]
    # #                         }
    # # perform_random_search(env_param_range_list=env_param_range_list, random_log_dir="random_rewards")
    #
    # # env_param_range_list = {'reward_in_goal': [0, 200],
    # #                         'reward_per_metre_progess': [0.0, 10.0],
    # #                         'dense_reward_in_ego_collision': [-200,0]
    # #                         }
    # # perform_random_search(env_param_range_list=env_param_range_list, random_log_dir="random_no_collision")
    #
    # agent_param_range_list = {'lr': [1e-3, 1e-6],
    #                           'exp_min': [0.0, 0.3]}
    # perform_random_search(agent_param_range_list=agent_param_range_list, random_log_dir="random_lr_steps")
import numpy as np
import sys
import pandas as pd
from math import sqrt
import matplotlib.pyplot as plt

file1 = '/media/a4balakr/DATADisk/projects/wise-move-dev/data/feature-space-wisemove-transfer.npy'
file2 = '/media/a4balakr/DATADisk/projects/bp_nn_policy_server/bp_policy_server/logs/feature-space-stack.npy'

wm = np.load(file1)
unreal = np.load(file2)

print (wm)
print (unreal)

# kalmann filter verification
single_list = []
for timestep in unreal:
    for index, row in enumerate(timestep):
        if row[0] > 15:
            pos = timestep[index,:5]
            single_list.append(pos)
            break
single_list = np.array(single_list)

plt.plot(single_list[:79,3])
plt.show()
plt.plot(single_list[81:149,3])
plt.show()

# vel verification
pos_list = []
for timestep in unreal:
    pos = timestep[0,4]
    pos_list.append(pos)

pos_list = np.array(pos_list)
y_list = pos_list[7:63,1]

dist_diff = [x - y_list[i - 1] for i, x in enumerate(y_list)][1:]
vel = np.array(dist_diff) / 0.1 * 3.6

a = 5


# ttc verification
RIGHT_LANE_DIST = 12.5
LEFT_LANE_DIST = 18.0
EGO_MAX_ACCELERATION = 1.05
EGO_MAX_SPEED = 8.1 * 3.6 #km/hr

def calc_time_to_cover_dist(dist, u, max_acc=EGO_MAX_ACCELERATION, max_vel=EGO_MAX_SPEED):
    if dist < 0:
        return -1

    #time taken to reach max vel
    t_1 = max(max_vel - u,0) / max_acc

    # dist covered in this time
    s_1 = u*t_1 + (1.0/2) * max_acc*(t_1**2)

    if s_1 <= dist:
        # time for rest of the distance at max velocity
        t_2 = (dist-s_1)/max_vel

        return t_1 + t_2
    #
    else:
        # calculate the discriminant
        d = (u ** 2) - (4 * max_acc/2 * - dist)

        if d >= 0:
            # find two solutions
            sol1 = (-u - sqrt(d)) / (2 * max_acc/2)
            sol2 = (-u + sqrt(d)) / (2 * max_acc/2)

            return max(sol1, sol2)
        else:
            return -1

def append_ttc(features_to_append, original_features):
    dist_to_collide_down_lane = RIGHT_LANE_DIST
    dist_to_collide_up_lane = LEFT_LANE_DIST
    time_to_cross_down_lane = calc_time_to_cover_dist(dist_to_collide_down_lane, 0.0, EGO_MAX_ACCELERATION,
                                                      EGO_MAX_SPEED)
    time_to_cross_intersection = calc_time_to_cover_dist(dist_to_collide_up_lane, 0.0, EGO_MAX_ACCELERATION,
                                                         EGO_MAX_SPEED)

    dist_collision = []
    vehs_collision = []

    ttc_list = []
    for row in original_features:
        vel = row[3] / 3.6
        x, y = row[:2]
        lane = np.sign(row[2])
        if x != 0 and y != 0 and ((y > 0 and lane == 1) or (y < 0 and lane == -1)):
            dist_collision = abs(y)
            time_to_cross = calc_time_to_cover_dist(dist_collision, vel, 2.0, vel)
            if lane == 1:
                ttc = abs(time_to_cross_down_lane - time_to_cross)
            elif lane == -1:
                ttc = abs(time_to_cross_intersection - time_to_cross)
            else:
                print("vehcile lane direction is ambiguous")
                ttc = np.inf
        else:
            ttc = np.inf
        ttc_list.append(ttc)

    # add ttc to feature space
    ttc_list = np.transpose(np.array(ttc_list)[np.newaxis])
    return np.append(features_to_append, ttc_list, axis=1)

for row in unreal:
    append_ttc(row, row)
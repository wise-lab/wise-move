import numpy as np
import pandas as pd
import os, sys

# Set the base wise-move directory to the Python working directory.
os.chdir('../../../..')
sys.path.append(os.getcwd())

FEATURE_SPACE_FILE = '/home/abk/work/wise-move-dev/experiments/final/stack-results/1veh/feature-space-stack.npy'
SAVE_FOLDER = '/home/abk/work/wise-move-dev/experiments/final/stack-results/1veh/'
SPACE_ABSTRACT = 0
VEL_ABSTRACT = 0
HEADING_ABSTRACT = 0
TTC_ABSTRACT=0

feature_space = np.load(FEATURE_SPACE_FILE)

MAX_NUM_VEHS=1
MANEUVER_INDEX=5

def get_features(row):
    normal_input = row[:, :MANEUVER_INDEX]
    if SPACE_ABSTRACT > 0:
        normal_input[:, :2] *= SPACE_ABSTRACT
    if HEADING_ABSTRACT > 0:
        normal_input[:, 2] *= HEADING_ABSTRACT
    if VEL_ABSTRACT > 0:
        normal_input[:, 3] *= VEL_ABSTRACT
    if TTC_ABSTRACT > 0:
        normal_input[:, 4] *= TTC_ABSTRACT
    normal_input = normal_input[:MAX_NUM_VEHS]
    normal_input = normal_input[normal_input[:, 2] < 0]
    normal_input = normal_input[normal_input[:, 1] < 0]
    normal_input[:, 1] = np.abs(normal_input[:, 1])
    if normal_input.shape[0] == 0:
        return None
    normal_input = np.delete(normal_input, [0,2], axis=1)
    normal_input = normal_input.flatten()

    return normal_input

output_data = []
normal_input_data = []
vel = []
prev = None
# for index, row in enumerate(feature_space):
#     output = 1 if row[0,MANEUVER_INDEX] == 0 else 0
#     if output == 1 and prev is not None:
#         if prev[0,MANEUVER_INDEX] == 3:
#             normal_input = get_features(prev)
#             if normal_input is not None:
#                 normal_input_data.append(normal_input)
#                 output_prev = 1 if prev[0, MANEUVER_INDEX] == 0 else 0
#                 output_data.append(output_prev)
#
#         normal_input = get_features(row)
#         if normal_input is not None:
#             normal_input_data.append(normal_input)
#             output_data.append(output)
#     prev = row

for index, row in enumerate(feature_space):
    output = 1 if row[0,MANEUVER_INDEX] == 0 else 0
    normal_input = get_features(row)
    if normal_input is not None:
        normal_input_data.append(normal_input)
        output_data.append(output)

output_data = np.array(output_data)
normal_input_data = np.array(normal_input_data)

np.save(SAVE_FOLDER + 'output.npy', output_data)
np.save(SAVE_FOLDER + 'normal_input.npy', normal_input_data)
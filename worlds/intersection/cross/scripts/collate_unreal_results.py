import sys
import os
import pandas as pd
import numpy as np
import csv as csv

if len(sys.argv) != 2 :
    print ("Pass one argument: dir path where csv results are located")
    exit()

def find_files( path_to_dir, suffix=".csv" ):
    filenames = os.listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

csv_dir_path = sys.argv[1]

summary_csv = os.path.join(csv_dir_path, 'results-summary.csv')
if os.path.exists(summary_csv):
    os.remove(summary_csv)

csv_list = find_files(csv_dir_path)
result_list = []
for file in csv_list:
    csv_df = pd.read_csv(os.path.join(csv_dir_path,file))
    start_index = csv_df.index[csv_df['Index'] == 'success'].tolist()[0]
    result_list.append(csv_df['Status'][start_index:start_index+6].tolist())

result_list = np.array(result_list)

with open(summary_csv, 'w+', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(["", "success", "failure", "unknown", "timeout", "collision", "time taken"])
    for row in result_list:
        writer.writerow([""] + row.tolist())
    result_list = result_list.astype(float)
    writer.writerow(["Mean"] + np.mean(result_list, axis=0).tolist())
    writer.writerow(["Std"] + np.std(result_list, axis=0).tolist())











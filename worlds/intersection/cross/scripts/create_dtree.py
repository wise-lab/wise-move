import numpy as np
import os, sys
from sklearn import tree
import graphviz
import matplotlib.pyplot as plt

from sklearn.tree import _tree


def tree_to_code(tree, feature_names):
    '''
    Outputs a decision tree model as a Python function

    Parameters:
    -----------
    tree: decision tree model
        The decision tree to represent as a function
    feature_names: list
        The feature names of the dataset used for building the decision tree
    '''

    tree_ = tree.tree_
    feature_name = [
        feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]
    print ("def tree({}):".format(", ".join(feature_names)))

    def recurse(node, depth):
        indent = "  " * depth
        if tree_.feature[node] != _tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            print ("{}if {} <= {}:".format(indent, name, threshold))
            recurse(tree_.children_left[node], depth + 1)
            print("{}else:  # if {} > {}".format(indent, name, threshold))
            recurse(tree_.children_right[node], depth + 1)
        else:
            val = tree_.value[node]
            action = True if val[0][0] == 0.0 else False
            print("{}return {}".format(indent, action))

    recurse(0, 1)

# Set the base wise-move directory to the Python working directory.
os.chdir('../../../..')
sys.path.append(os.getcwd())

SAVE_FOLDER = '/home/abk/work/wise-move-dev/experiments/final/stack-results/1veh/'
FEATURE_NAMES = [
#    'x 1',
    'y',
#    'theta 1',
    'vel',
    'ttc'
]
INPUT_FILE = SAVE_FOLDER + 'normal_input.npy'
OUTPUT_FILE = SAVE_FOLDER + 'output.npy'

features = np.load(INPUT_FILE)
output = np.load(OUTPUT_FILE)
cdict = {0: 'red', 1: 'green'}

dt = tree.DecisionTreeClassifier(criterion='entropy', class_weight='balanced')
dt.fit(features, output)

def plot_decision_boundary(clf, X, Y, cmap='Paired_r'):
    h = 0.02
    x_min, x_max = X[:,0].min() - 10*h, X[:,0].max() + 10*h
    y_min, y_max = X[:,1].min() - 10*h, X[:,1].max() + 10*h
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.figure(figsize=(5,5))
    plt.contourf(xx, yy, Z, cmap=cmap, alpha=0.25)
    plt.contour(xx, yy, Z, colors='k', linewidths=0.7)
    plt.scatter(X[:,0], X[:,1], c=Y, cmap=cmap, edgecolors='k')
    plt.show()

tree.export_graphviz(dt, out_file="mytree.dot",
                     feature_names=FEATURE_NAMES,
                     class_names=['YIELD','TRACK_SPEED'])

# plt.figure(figsize=(5,5))
# for g in np.unique(output):
#     ix = np.where(output == g)
#     plt.scatter(ttc_features[ix,0], ttc_features[ix,1], c = cdict[g], label = g)
# plt.show()

#plot_decision_boundary(dt, ttc_features, output)


tree_to_code(dt,FEATURE_NAMES)


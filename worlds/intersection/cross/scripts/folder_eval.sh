cd ..
FOLDER="/home/abk/work/wise-move-dev/experiments/final/top2/"
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 0 --eval_dir "wm" &
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 2 --eval_dir "wm-lagkf" &
wait
FOLDER="/home/abk/work/wise-move-dev/experiments/final/top2/lagkf-effect/"
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-lag" &
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-kf" &
wait
FOLDER="/home/abk/work/wise-move-dev/experiments/final/top2/noise-effect/"
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-ego" &
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-road" &
wait
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-noise" &
wait
FOLDER="/home/abk/work/wise-move-dev/experiments/final/top2/percept-effect/"
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-kf-dr" &
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-lag-dr" &
wait
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-vanish" &
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-xy" &
wait
python train_and_evaluate_transfer.py --logdir $FOLDER --eval_transfer --test_type 1 --eval_dir "wm-percept" &
wait

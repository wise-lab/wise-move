import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

EXPERIMENT_DIR_NAME='one_vehicle_data'
EXPERIMENT_DIR = '/media/a4balakr/DATADisk/projects/wise-move-dev/experiments/' + EXPERIMENT_DIR_NAME + "/"
feature_file_stack =  EXPERIMENT_DIR + 'feature-space-stack.npy'
feature_file_wisemove = EXPERIMENT_DIR + 'feature-space-wisemove.npy'
feature_file_wisemove_perception = EXPERIMENT_DIR + 'feature-space-wisemove-transfer.npy'
feature_file_wisemove_kf = EXPERIMENT_DIR + 'feature-space-wisemove-kf.npy'

feature_space_stack = np.load(feature_file_stack)
feature_space_wisemove = np.load(feature_file_wisemove)
feature_space_perception = np.load(feature_file_wisemove_perception)
feature_space_kf = np.load(feature_file_wisemove_kf)

# plot paths
stack_xy = feature_space_stack[:, 0, :2]
wm_xy = feature_space_wisemove[:, 0, :2]
#wm_perception_xy = feature_space_perception[:, 0, :2]
wm , = plt.plot(wm_xy[:,0], wm_xy[:,1], 'o', color='#1E90FF', label='wm')
stack , = plt.plot(stack_xy[:,0], stack_xy[:,1], 'o', color='#DC143C', label='unreal')
#wm_perception, = plt.plot(wm_perception_xy[:,0], wm_perception_xy[:,1], 'o', color='#8A2BE2', label='WISEMove + perception errors')
plt.legend(handles=[wm, stack], frameon=True)
plt.xlabel('x')
plt.ylabel('y')
plt.savefig(EXPERIMENT_DIR + 'vehicle-paths.png')
plt.clf()

# plot theta range
stack_theta = feature_space_stack[:, 0, 2]
stack_theta = stack_theta[stack_theta > 0]
wm_theta = feature_space_wisemove[:, 0, 2]
wm_theta = wm_theta[wm_theta > 0]
wm_perception_theta = feature_space_perception[:, 0, 2]
wm_perception_theta = wm_perception_theta[wm_perception_theta > 0]
print ("{}: avg:{}, std: {}".format('wm', np.mean(wm_theta), np.std(wm_theta)))
print ("{}: avg:{}, std: {}".format('stack', np.mean(stack_theta), np.std(stack_theta)))
print ("{}: avg:{}, std: {}".format('wm-perception', np.mean(wm_perception_theta), np.std(wm_perception_theta)))

# plot vel w.r.t time
stack_vel = np.array([   feature_space_stack[90:90+64, 0, 3],
                        feature_space_stack[545:545+64, 0, 3],
                        feature_space_stack[724:724+64, 0, 3],
                        feature_space_stack[921:921+64, 0, 3],
                        feature_space_stack[1064:1064+64, 0, 3],
                        feature_space_stack[1338:1338+64, 0, 3]])
stack_vel_vel_avg = np.mean(stack_vel, axis=0)
stack_vel_vel_std = np.std(stack_vel, axis=0)
wm_vel = feature_space_wisemove[152:216, 0, 3]
wm_perception_vel = np.array([   feature_space_perception[42:42+64, 0, 3],
                        feature_space_perception[158:158+64, 0, 3],
                        feature_space_perception[300:300+64, 0, 3],
                        feature_space_perception[371:371+64, 0, 3]])
wm_perception_vel_avg = np.mean(wm_perception_vel, axis=0)
wm_perception_vel_std = np.std(wm_perception_vel, axis=0)

wm_kf_vel = np.array([   feature_space_kf[0:0+64, 0, 3],
                        feature_space_kf[265:265+64, 0, 3],
                        feature_space_kf[580:580+64, 0, 3]])
wm_kf_vel_avg = np.mean(wm_kf_vel, axis=0)
wm_kf_vel_std = np.std(wm_kf_vel, axis=0)

#wm , = plt.plot(wm_vel, color='#1E90FF', label='wm')
stack , = plt.plot(stack_vel_vel_avg, color='#DC143C', label='unreal')
plt.fill_between(range(64),stack_vel_vel_avg-stack_vel_vel_std, stack_vel_vel_avg+stack_vel_vel_std, color='#DC143C', alpha=0.3)
wm_perception , = plt.plot(wm_perception_vel_avg, color='#8A2BE2', label='wm-percept')
plt.fill_between(range(64),wm_perception_vel_avg-wm_perception_vel_std, wm_perception_vel_avg+wm_perception_vel_std, color='#8A2BE2', alpha=0.3)
wm_kf , = plt.plot(wm_kf_vel_avg, color='green', label='wm-kf')
plt.fill_between(range(64),wm_kf_vel_avg-wm_kf_vel_std, wm_kf_vel_avg+wm_kf_vel_std, color='orange', alpha=0.3)
plt.legend(handles=[wm_kf, wm_perception, stack], frameon=True)
plt.xlabel('timestep')
plt.ylabel('velocity (km/hr)')
plt.savefig(EXPERIMENT_DIR + 'vehicle-vel-wrt-time.png')

# plot xy w.r.t time
figure, (x_ax, y_ax) = plt.subplots(1, 2, constrained_layout=True)
x_ax.set_ylabel('x (m)')
x_ax.set_xlabel('timestep')

y_ax.set_ylabel('y (m)')
y_ax.set_xlabel('timestep')

stack_xy = feature_space_stack[1817:1817+40, 0, :2]
wm_xy = feature_space_wisemove[1800:1800+40, 0, :2]
wm_perception_xy = feature_space_perception[1800:1800+40, 0, :2]

wm, = x_ax.plot(range(40), wm_xy[:,0], color='#1E90FF', label='wm')
stack, = x_ax.plot(range(40), stack_xy[:,0], color='#DC143C', label='unreal')
#wm_perception, = x_ax.plot(range(40), wm_perception_xy[:,0], color='#8A2BE2', label='WM-perception-noise')
y_ax.plot(range(40), wm_xy[:,1], color='#1E90FF', label='WISEMove')
y_ax.plot(range(40), stack_xy[:,1], color='#DC143C', label='Unreal')
#y_ax.plot(range(40), wm_perception_xy[:,1], color='#8A2BE2', label='WM-perception-noise')

x_ax.set_ylim(16,20)
y_ax.legend(handles=[wm, stack], loc=4, frameon=True)
figure.set_size_inches(12, 4)
figure.savefig(EXPERIMENT_DIR + 'vehicle-y-wrt-time.png')
plt.clf()



cd ..
python high_level_policy_2way.py --evaluate --rule_based
python high_level_policy_2way.py --evaluate --rule_based --lag
python high_level_policy_2way.py --evaluate --rule_based --kf
python high_level_policy_2way.py --evaluate --rule_based --lag --kf
python high_level_policy_2way.py --evaluate --rule_based --use_robust
python high_level_policy_2way.py --evaluate --rule_based --use_robust --lag
python high_level_policy_2way.py --evaluate --rule_based --use_robust --kf
python high_level_policy_2way.py --evaluate --rule_based --use_robust --lag --kf
python high_level_policy_2way.py --evaluate --rule_based --use_robust --rand_lag
python high_level_policy_2way.py --evaluate --rule_based --use_robust --rand_kf
python high_level_policy_2way.py --evaluate --rule_based --use_robust --rand_lag --rand_kf
python high_level_policy_2way.py --evaluate --rule_based --use_robust --rand_lag --rand_kf --noisy
python high_level_policy_2way.py --evaluate --rule_based --use_robust --perception
python high_level_policy_2way.py --evaluate --rule_based --use_robust --noisy --perception
import numpy as np
import pandas as pd
import os, sys

# Set the base wise-move directory to the Python working directory.
os.chdir('../../../..')
sys.path.append(os.getcwd())

TO_COMPARE_FOLDER = './experiments/final_policies/backup/all-improvements/500-1veh'
BASELINE_CSV = './experiments/final_policies/wm-hardcoded-norobustness/500-1veh/rulebased_results.csv'
SAVE_FOLDER = './data/'

def find_csv_filenames( path_to_dir, suffix=".csv" ):
    filenames = os.listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

baseline_results_file = BASELINE_CSV
to_compare_results_dir = TO_COMPARE_FOLDER

baseline_df = pd.read_csv(baseline_results_file)
to_compare_files = find_csv_filenames(to_compare_results_dir)

baseline_results = baseline_df['Status']
baseline_fail_indices = np.array(baseline_results.index[baseline_df['Status'] == 'fail'].tolist())

if not os.path.exists(SAVE_FOLDER):
    os.mkdir(SAVE_FOLDER)
with open(SAVE_FOLDER + 'changed_to_success.txt', 'w+') as f:
    for file in to_compare_files:
        to_compare_df = pd.read_csv(os.path.join(TO_COMPARE_FOLDER, file))
        to_compare_success_indices = np.array(to_compare_df.index[to_compare_df['Status'] == 'success'].tolist())
        indices_changed_to_success = baseline_fail_indices[np.isin(baseline_fail_indices, to_compare_success_indices)]
        f.write(file + ": " + np.array2string(indices_changed_to_success) + "\n")





import numpy as np
import matplotlib.pyplot as plt

file1 = '/home/abk/work/wise-move-dev/data/feature-space-stack.npy'

wm = np.load(file1)

def get_feature_values(col_index):
    f_list = []
    for state in wm:
        cur = state[:,col_index]
        cur = cur[cur != 0]
        f_list = f_list + cur.tolist()

    return np.array(f_list)

# x = get_feature_values(0)
#y = get_feature_values(1)
# theta = get_feature_values(2)
vel = get_feature_values(3)
#ttc = get_feature_values(4)

plt.hist(vel, density=True, bins=80)
plt.ylabel('probability density')
plt.xlabel('velocity (km/hr)')
plt.show()
#plt.savefig('vel_histogram.png')

#np.save('../../../../data/vel_dist.npy', vel)
#np.save('../../../../data/ttc_dist.npy', ttc)
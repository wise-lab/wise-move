import sys
import datetime

HYPERPARAM_PY_FILE = "highlevel_hyperparameter_search.py"
num_args = len(sys.argv)

if num_args != 2:
    raise Exception("Needs a path to log file to parse.")
else:
    file = sys.argv[1]
    with open(file, 'r') as f:
        line = f.readline()
        count = 1
        interval_count = 0
        time_per_interval = 0
        time_per_interval_count = 0
        while line:
            line = line.strip()
            if "log_interval:" in line:
                interval = int(line.split()[-1])
            if "nb_steps_global:" in line:
                nb_steps = int(line.split()[-1])
            if "num_trials:" in line:
                num_trials = int(line.split()[-1])
                print("Num trials: {}".format(num_trials))
            if " steps performed)" in line:
                interval_count += 1
            if "ms/step" in line:
                word_list = line.split()
                for index, word in enumerate(word_list):
                    if "ms/step" in word:
                        current_timetaken = int(word_list[index-1].replace('s',''))
                        time_per_interval = (time_per_interval*time_per_interval_count + current_timetaken)/(time_per_interval_count+1)
                        time_per_interval_count += 1
            line = f.readline()
    f.close()

    with open(HYPERPARAM_PY_FILE, 'r') as f:
        line = f.readline()
        count = 1
        num_hyperparameters = 0
        print ("Params to test:")
        while line:
            line = line.strip()
            if not line.startswith("#"):
                if "hyperparameter_val_list =" in line:
                    param_list = line.split()[2:]
                    print (param_list)
                    num_hyperparameters += len(param_list)
            line = f.readline()
    f.close()

    total_interval_count = int(nb_steps/interval)*num_trials*num_hyperparameters
    intervals_left = total_interval_count - interval_count
    print("Intervals done: {}".format(interval_count))
    print("Intervals left: {}".format(intervals_left))
    print("Time per interval: {} seconds\n".format(time_per_interval))
    time_left = intervals_left * time_per_interval

    print ("-------------------------------------------------")

    print ("Time left: {}".format(str(datetime.timedelta(seconds=time_left))))
    print("Will end at: {}".format(str(datetime.datetime.now() + datetime.timedelta(seconds=time_left))))




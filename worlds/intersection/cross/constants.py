from .road_geokinemetry import TwoWayIntersection
rd = TwoWayIntersection()

from .fixed_constants import *

## GRAPHICS CONSTANTS
LANE_SEPARATOR_HALF_WIDTH = 1

#: Number of pixels in the horizontal direction
NUM_HPIXELS = int(rd.h.length / METRES_PER_PIXEL)

#: Number of pixels in the vertical direction
NUM_VPIXELS = int(rd.v.length / METRES_PER_PIXEL)

#: Horizontal space scale
H_SPACE_SCALE = NUM_HPIXELS / rd.h.length

#: Vertical space scale
V_SPACE_SCALE = NUM_VPIXELS / rd.v.length

#: Number of horizontal tiles
NUM_HTILES = round(rd.h.length / rd.h.width)

#: Number of horizontal tiles
NUM_VTILES = round(rd.v.length / rd.v.width)

#: Lambda function that takes in tile width and returns scale_x for road tile
H_TILE_SCALE = lambda w: (NUM_HPIXELS / NUM_HTILES) / w

#: Lambda function that takes in tile height and returns scale_y for road tile
V_TILE_SCALE = lambda h: (NUM_VPIXELS / NUM_VTILES) / h

#: Lambda function that takes in car width and returns scale_x for car
H_CAR_SCALE = lambda w: VEHICLE_SIZE[0] * H_SPACE_SCALE / w

#: Lambda function that takes in car height and returns scale_y for car
V_CAR_SCALE = lambda h: VEHICLE_SIZE[1] * V_SPACE_SCALE / h

#: Lane width
LANE_WIDTH = ROAD_TILE_IMAGE_DIM * V_TILE_SCALE(ROAD_TILE_IMAGE_DIM) / 2

#: APS_KEYS contains all of the atomic propositions on the road and the vehicle.
#
#  * in_stop_region: True if the veh. is in stop region
#  * has_entered_stop_region: True if the veh. has entered or passed the stop region
#  * before_but_close_to_stop_region: True if the veh. is before but close to the stop region
#  * stopped_now: True if the veh. is now stopped
#  * has_stopped_in_stop_region: True if the veh. has ever stopped in the stop region before or now
#  * in_intersection: True if the veh. is in the intersection
#  * over_speed_limit: True if the veh. is over the speed limit
#  * on_route: True if the veh. is on the same route to the initial
#  * highest_priority: True if the veh. has the highest priority in the intersection
#  * intersection_is_clear: True if there is no (other) veh. in the intersection
#  * veh_ahead: True if there is a veh. ahead close to it
#  * lane: True if veh. is on the right lane (or side), False if on the left lane (or side)
#  * target_lane: True if veh.'s target lane is on the right lane, False if on the left;
# this is equal to lane except the ego vehicle.
#  * veh_ahead_stopped_now: True if "veh_ahead and (the veh ahead has stopped now)"

APS_KEYS = ('in_stop_region', 'has_entered_stop_region', 'before_but_close_to_stop_region', 'stopped_now',
            'has_stopped_in_stop_region', 'in_intersection', 'over_speed_limit', 'on_route', 'highest_priority',
            'intersection_is_clear', 'veh_ahead', 'lane', 'parallel_to_lane', 'before_intersection', 'target_lane',
            'veh_ahead_stopped_now', 'veh_ahead_too_close','veh_ahead_deaccelerate')

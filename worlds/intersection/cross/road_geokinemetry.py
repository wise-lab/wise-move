from .. import Route, Lane
from .fixed_constants import *

class TwoWayIntersection:
    def __init__(self):
        #: whether the intersection exists or not
        self.intersection_exists = True

        #: the intersection is posed at (0, 0) in the coordinate.
        #  The variable "intersection_pos" indicates this position of
        #  the intersection, but not used at all in the current implementation.
        self.intersection_pos = [0.0, 0.0]

        #: the vertical offset between the edges of the intersection and
        #  the end points of the stop regions.
        self.intersection_voffset = 2.5 / 2.0

        #: the speed limit of the road environment.
        #  This is the only one regarding the road kinemetry.
        #  11.176 m/s = 40 km/h
        self.speed_limit = 11.176

        # Intersections usually have a corner radiuss
        self.corner_radius = 8.43

        #: the horizontal offset between the edges of the intersection and
        #  the end points of the stop regions.
        self.intersection_hoffset = self.corner_radius

        self.stop_line_length = 0

        self.hlane_width = 5.5
        self.vlane_width = 5.5

        self.goal_dist = 10

        self.create_roads()


    def translate_point_stopline_origin(self, point):
        """ Convert point to new coordinate system with stopline center as origin
            Used for generating JSON for import to Unreal.

        Args:
            point: xy coordinates

        Returns:
            point in new coordinate system

        """
        # x: positive forwards
        # y: -ve to right
        return [point[0]-self.h.stop_region.boundary[0], -(point[1]-self.h.width/2)]

    def create_roads(self):
        self.h = Route('h', -75.0, self.hlane_width + self.corner_radius + self.goal_dist)
        self.h.lanes = [Lane('forward', self.hlane_width), Lane('forward', self.hlane_width)]

        self.v = Route('v', -DETECTION_RANGE_Y, DETECTION_RANGE_Y)
        self.v.lanes = [Lane('backward', self.vlane_width), Lane('forward', self.vlane_width)]

        h_stop_region_width = self.stop_line_length
        for lane in self.h.lanes:
            lane.assign_stop_region(self.h, h_stop_region_width, self.intersection_hoffset, 35.0)

        self.update_features()

    def update_features(self):
        self.intersection_width = self.v.width
        self.intersection_height = self.h.width

        self.intersection_width_w_offset = self.intersection_width + 2 * self.intersection_hoffset
        self.intersection_height_w_offset = self.intersection_height + 2 * self.intersection_voffset

        self.intersection_width_wo_offset = self.intersection_width
        self.intersection_height_wo_offset = self.intersection_height

    def use_default_features(self):
        self.__init__()


from numpy import pi

#: Index of the ego vehicle in the vehicles list
EGO_INDEX = 0

#: Metres per pixel
METRES_PER_PIXEL = 0.2

#: half of the (x,y)-size of the car as a bounding box when\
#  horizontally aligned
VEHICLE_SIZE = [4, 1.75]
VEHICLE_SIZE_HALF = [VEHICLE_SIZE[0]/2, VEHICLE_SIZE[1]/2]

#: vehicle wheel base ( = L in the paper)
VEHICLE_WHEEL_BASE = 2.5

#: maximum acceleration
MAX_ACCELERATION = 4

#: maximum acceleration
MIN_ACCELERATION = -2

#: maximum steering angle - 33 deg. (extreme case: 53 deg.)
MAX_STEERING_ANGLE = 33.0 * pi / 108.0

#: maximum rate of change of steering angle
MAX_STEERING_ANGLE_RATE = 1.0

#: the time difference btw. the current and the next time steps.
DT = 0.1

#: Constants for state-constrained Runge-Kutta 4 method in vehicles.py
DT_over_2 = DT / 2
DT_2_over_3 = 2 * DT / 3
DT_over_3 = DT / 3
DT_over_6 = DT / 6

#: time scale of the animation; 1 sec in the road environment
#  corresponds to "time_scale" sec in the animation.
TIME_SCALE = 1

V2V_DISTANCE = 6.0

#: Road tile image dimension
ROAD_TILE_IMAGE_DIM = 300

#: MAX NUMBER OF VEHICLES
MAX_NUM_VEHICLES = 5

#: MIN NUMBER OF VEHICLES
MIN_NUM_VEHICLES = 2

#: NUM VEHICLES IN FEATURE
NUM_VEHICLES_IN_FEATURE = 5

#: NUM FEATURES PER VEHICLE
NUM_FEATURES_PER_VEHICLE = 5

#: DETECTION RANGE (y-axis)
DETECTION_RANGE_Y = 80

#: SAVE FOLDER FOR FEATURE SPACE
SAVE_FOLDER = './data/'

#: LOG FOLDER
LOG_FOLDER = './logs/'
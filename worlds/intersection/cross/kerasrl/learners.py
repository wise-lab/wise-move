import numpy as np
from backends.kerasrl.learners import Learner

from keras.models import Model, Sequential
from keras.layers import Dense, Flatten, Input, TimeDistributed
from keras.optimizers import Adam
from keras.callbacks import TensorBoard
from keras.initializers import glorot_uniform

# # set to allocate GPU only as necessary
from keras.backend.tensorflow_backend import set_session
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
sess = tf.Session(config=config)
set_session(sess)

from rl.callbacks import ModelIntervalCheckpoint, FileLogger

from .keras_modifications import DualSequentialMemory, DQNAgentModified, RestrictedEpsGreedyQPolicy, ValidationTestCallback, test_model, ModifiedLinearAnnealedPolicy

class DQNLearner(Learner):
    def __init__(self,
                 env,
                 model=None,
                 policy=None,
                 memory=None,
                 tensorboard=False,
                 **kwargs):
        """The constructor which sets the properties of the class.

        Args:
            env: environment class used to train;
            model: Keras Model of actor which takes observation as input and outputs actions. Uses default if not given
            policy: KerasRL Policy. Uses default ModifiedLinearAnnealedPolicy if not given
            memory: KerasRL Memory. Uses default DualSequentialMemory if not given
            tensorboard: use tensorboard or not
            **kwargs: other optional key-value arguments with defaults defined in property_defaults
        """
        super(DQNLearner, self).__init__(env.observation_space.shape, env.action_space.n, **kwargs)

        self.tensorboard = tensorboard

        property_defaults = {
            "mem_size": 100000,  # size of memory
            "mem_window_length": 1,  # window length of memory
            "target_model_update": 5e-4,  # target model update frequency
            "nb_steps_warmup": 1000,  # steps for model to warmup
            "lr": 1e-4,
            "exp_policy": 'linear_annealed',
            "exp_anneal_steps": 28000,
            "exp_start": 1.0,
            "exp_min": 0.3,
            "delta_clip": 1.,
            "gamma": 0.99,
            "log_interval": 2500,
            "early_stopping_patience": 30,
            'log_path': "./logs/"
        }

        param_log = "\nKeras DQNLearner parameters:\n"
        for (prop, default) in property_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        if model is None:
            model = self.get_default_model()
        if policy is None:
            policy = self.get_default_policy()
        if memory is None:
            memory = self.get_default_memory()

        self.agent_model = self.create_agent(model, policy, memory)

    def get_default_model(self):
        """Creates the default model.

        Returns:     Keras Model object of actor
        """

        input = Input(shape=self.input_shape)
        shared_output = TimeDistributed(Dense(32, activation='relu', kernel_initializer=glorot_uniform(seed=0)))(input)
        model_vehicles_encoded = Flatten()(shared_output)

        encode_after_concat1 = Dense(32, activation='relu', kernel_initializer=glorot_uniform(seed=0))(model_vehicles_encoded)
        encode_after_concat2 = Dense(32, activation='relu', kernel_initializer=glorot_uniform(seed=0))(encode_after_concat1)
        output = Dense(self.nb_actions, activation='linear', kernel_initializer=glorot_uniform(seed=0))(encode_after_concat2)

        model = Model(inputs=input,outputs=output)

        return model

    def get_default_policy(self):
        return ModifiedLinearAnnealedPolicy(RestrictedEpsGreedyQPolicy(), attr='eps', value_max=self.exp_start, value_min=self.exp_min, value_test=0.0,
                              nb_steps=self.exp_anneal_steps)

    def get_default_memory(self):
        """Creates the default memory model.

        Returns:     KerasRL SequentialMemory object
        """
        memory = DualSequentialMemory(
            limit=int(self.mem_size/2), window_length=self.mem_window_length)
        return memory

    def create_agent(self, model, policy, memory):
        """Creates a KerasRL DDPGAgent with given components.

        Args:
            model: Keras Model of model which takes observation as input and outputs discrete actions.'
            policy: KerasRL Policy
            memory: KerasRL Memory.

        Returns:
            KerasRL DQN object
        """
        agent = DQNAgentModified(
            model=model,
            nb_actions=self.nb_actions,
            memory=memory,
            nb_steps_warmup=self.nb_steps_warmup,
            target_model_update=self.target_model_update,
            policy=policy,
            enable_dueling_network=True,
            delta_clip=self.delta_clip,
            gamma=self.gamma)

        agent.compile(Adam(lr=self.lr), metrics=['mae'])

        return agent

    def train(self,
              env,
              nb_steps=100000,
              visualize=False,
              verbose=1,
              nb_max_episode_steps=400,
              model_checkpoints=False,
              checkpoint_interval=50000,
              log_in_file=False):
        """ Train the DQN agent.

        Args:
            env: env to train in
            nb_steps: number of steps to train for
            visualize: visualize training
            verbose: verbosty levels
            nb_max_episode_steps: max number of steps per episode
            model_checkpoints create model checkpoint after each checkpoint_interval
            checkpoint_interval: interval at which to create model checkpoitns
            log_in_file: use Keras logging to file flag
        """
        callbacks = []
        if model_checkpoints:
            callbacks += [
                ModelIntervalCheckpoint(
                    self.log_path + 'checkpoint_weights_{step}.h5f',
                    interval=checkpoint_interval)
            ]
        if self.tensorboard:
            callbacks += [TensorBoard(log_dir=self.log_path)]

        if log_in_file:
            callbacks += [FileLogger(self.log_path+"keras_log.txt", interval=100)]

        callbacks += [ValidationTestCallback(   model_save_name="checkpoint_weights.h5f",
                                                log_path=self.log_path,
                                                interval=self.log_interval,
                                                patience=self.early_stopping_patience,
                                                eps_max=self.exp_start,
                                                eps_min=self.exp_min,
                                                early_stop_start_step=int(3*self.exp_anneal_steps/4))]

        self.agent_model.fit(
            env,
            nb_steps=nb_steps,
            visualize=visualize,
            verbose=verbose,
            log_interval=self.log_interval,
            nb_max_episode_steps=nb_max_episode_steps,
            callbacks=callbacks)

    def save_model(self, file_name="test_weights.h5f", overwrite=True):
        """ Save model to file.

        Args:
            file_name: filename to save as
            overwrite: overwrite if file exists
        """
        self.agent_model.save_weights(file_name, overwrite=True)

    def test_model(self,
                   env,
                   nb_episodes=5,
                   visualize=True,
                   nb_max_episode_steps=400,
                   save_feature_space=False,
                   save_suffix='wisemove',
                   verbose=2,
                   use_rule_based=False,
                   use_random_policy=False):
        """ Test model by running multiple trials of environment.

        Args:
            env: env to test in
            nb_episodes: number of steps to test for
            visualize: visualize training
            nb_max_episode_steps: max number of steps per episode
            save_feature_space: save observation of agent at each state
            save_suffix: feature space save file suffix for identification
            verbose: verbosty levels
            use_rule_based: use rule-based policy instead
        """
        return test_model(env,self.agent_model,nb_episodes,visualize,save_feature_space=save_feature_space,
                          save_suffix=save_suffix,verbose=verbose, use_rule_based=use_rule_based,
                          use_random_policy=use_random_policy)

    def load_model(self, file_name="test_weights.h5f"):
        """ Save model from file.

        Args:
            file_name: filename to save as
            overwrite: overwrite if file exists
        """
        self.agent_model.load_weights(file_name)

    def predict(self, observation):
        """ Get action from agent using state

        Args:
            observation: observation to use for generating actions
        """
        return self.agent_model.forward(observation)



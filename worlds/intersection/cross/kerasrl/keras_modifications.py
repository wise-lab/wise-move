import numpy as np
import random
from math import floor, ceil
import os

from rl.callbacks import Callback

from rl.agents import DQNAgent
from rl.memory import SequentialMemory
from rl.policy import EpsGreedyQPolicy, MaxBoltzmannQPolicy, LinearAnnealedPolicy

from collections import defaultdict, deque

def test_model(env,
               agent,
               nb_episodes=5,
               visualize=True,
               verbose=2,
               save_feature_space=False,
               save_suffix='wisemove',
               use_rule_based=False,
               use_random_policy=False):
    """ Uses an agent model's action output to to step through the environment and collect
    results from multiple trials.

    Args:
        env: gym-compliant environment
        agent: Keras agent model
        nb_episodes: number of episodes to run
        visualize: render environment after each step
        verbose: Levels of verbosity
        save_feature_space: save observation of agent at each state
        save_suffix: feature space save file suffix for identification
        use_rule_based: use rule-based policy instead
        use_random_policy: use a random action selection policy

    Returns:
        a tuple ([success_count, termination_reason_counter, [], metrics]).
        success_count: number of successes
        termination_reason_counter: collated information about reason for termination
        []: unused variable
        metrics: dictionary of metrics
    """
    if verbose > 1:
        print("Testing for {} episodes".format(nb_episodes))
    success_count = 0
    termination_reason_counter = defaultdict(int)
    metrics = defaultdict(list)
    training_flag_backup = agent.training
    agent.training=False

    for n in range(nb_episodes):
        obs = env.reset()
        terminal = False
        step = 0
        episode_reward = 0

        while not terminal:
            if visualize:
                env.render()
            action = agent.forward(obs)

            if use_rule_based:
                action = env.env.select_optimal_option()
            elif use_random_policy:
                action = np.random.randint(2)

            # Uncomment for specific action
            # action = 1

            obs, R, terminal, info = env.step(action)
            step += 1
            episode_reward += R
            if terminal:
                if 'episode_termination_reasons' in info:
                    termination_reason = info['episode_termination_reasons']
                    termination_reason_counter[termination_reason] += 1
                # TODO: remove below env-specific code
                if info['episode_termination_reasons'] == 'goal achieved':
                    metrics['goal_time_taken'].append(step)
                    success_count += 1
                if verbose > 1:
                    print(f'Episode {n + 1}: steps:{step}, reward:{episode_reward:.2f}')

    if verbose>0:
        print("\nPolicy succeeded {}/{} times!".format(success_count, nb_episodes))

    if save_feature_space:
        env.env.features_class.save_feature_space(file_suffix=save_suffix)
        env.env.save_json(file_suffix=save_suffix)

    if verbose > 1:
        print(termination_reason_counter)

    agent.training = training_flag_backup

    return [success_count, termination_reason_counter, [], metrics]

class ValidationTestCallback(Callback):
    """ Performs a validation test every interval and uses the results to decide whether to:
    #   - early stop
    #   - adjust exploration epsilon (works only with LinearAnnealed policy)
    """
    def __init__(self, model_save_name='highlevel_weights_best.h5f', log_path='./',
                 interval=5000, verbose=1, patience=10, min_delta=0.0, eps_max=1.0,
                  eps_min=0.01, random_policy_threshold=72, good_policy_threshold=95, baseline=85, num_episodes_for_validation=100,
                 early_stop_start_step=0):
        """ Initialize variables needed for the callback

        Args:
            model_save_name: temporary file name for saving best model
            log_path: directory name for log file
            interval: interval at which to run validation
            verbose: verbosity level
            patience: number of intervals to wait for until early stopping
            min_delta: accepted delta for early stopping check
            eps_max: maximum exploration value
            eps_min: minimum exploration value
            random_policy_threshold: success threshold that a random policy can achieve (for adjusting exploration)
            good_policy_threshold: acceptable performance for success (for adjusting exploration)
            baseline: baseline success above which early stopping kicks in
            num_episodes_for_validation: number of episodes to run for validation test
            early_stop_start_step: number of steps after which early stopping is checked
        """
        super(ValidationTestCallback, self).__init__()
        self.log_path = log_path
        self.filepath = self.log_path + model_save_name
        self.interval = interval
        self.verbose = verbose
        self.baseline = baseline
        self.patience = patience
        self.min_delta = min_delta
        self.early_stop_start_step = early_stop_start_step

        self.total_steps = 0
        self.wait = 0
        self.stopped_epoch = 0
        self.reached_baseline = False
        self.best = 0
        self.best_above_baseline = self.baseline
        self.last_n = deque(maxlen=3)
        self.best_num_steps = 0

        self.eps_max = eps_max
        self.eps_min = eps_min
        self.num_episodes_for_validation = num_episodes_for_validation
        self.random_policy_threshold = random_policy_threshold
        self.good_policy_threshold = good_policy_threshold

    def on_train_begin(self, logs=None):
        """ Sets class variables before training begins
        """
        self.total_steps = 0
        self.wait = 0
        self.stopped_epoch = 0
        self.reached_baseline = False
        self.best = 0
        self.best_above_baseline = self.baseline
        self.last_n = deque(maxlen=3)
        self.best_num_steps = 0


    def restore_best_model(self):
        """ Restores the last saved model
        """
        print('Restoring best weights with success={} reached at step={})'.format(self.best, self.best_num_steps))
        self.model.load_weights(self.filepath)

    def log(self, out):
        """ Log function that prints to screen as well as writes to log file.
            Args:
            out: Text to print
        """
        print (out)
        if self.log_path is not None:
            if not os.path.exists(self.log_path):
                os.makedirs(self.log_path, exist_ok=True)
            with open(self.log_path + "training_log.txt", 'a+') as f:
                print(out, file=f)

    def check_early_stopping(self,current):
        """ Checks early stopping criteria.

            Args:
            current: Current validation success
        """
        self.log("{}:{}".format(self.total_steps, current))

        # if current success > best so far, update it as the best model and save it
        if (current - self.min_delta) > self.best:
            # treat best above baseline and best overall separately
            # Early stopping kicks in only if current success > baselines
            if (current - self.min_delta) > self.best_above_baseline:
                self.reached_baseline = True
                self.best_above_baseline = current
                self.wait = 0
            self.best = current
            self.best_num_steps = self.total_steps
            self.model.save_weights(self.filepath, overwrite=True)
        # if not, check if it is time to early stop
        else:
            # if model success has remained same for last n, means it is not learning
            # or nothing more to learn. so stop training
            if len(self.last_n) == 3 and len(set(self.last_n)) == 1:
                if current < 80:
                    self.log('Exiting since model staying at same success: {}'.format(current))
                    if self.best < 80:
                        self.model.exited_early = True
                    raise KeyboardInterrupt
            # if reached baseline and policy diverged too much, early stop
            if self.reached_baseline:
                self.wait += 1
                if self.best - current >= 10:
                    self.log('Exiting early as policy {} diverged from best {}'.format(current, self.best))
                    raise KeyboardInterrupt
            # if policy has not improved over last few epochs, early stop
            if self.wait >= self.patience:
                self.stopped_epoch = int(self.total_steps/self.interval)
                self.log('Interval {}: Early Stopping!'.format(self.stopped_epoch + 1))
                raise KeyboardInterrupt

    def readjust_exploration(self, current):
        """ Uses current validation success to adjust the exploration parameter.
        Lower success means explore more, higher means explore less and exploit more.

            Args:
            current: Current validation success
        """
        old = self.model.policy.inner_policy.eps
        if self.total_steps > self.model.policy.nb_steps:
            eps_min_val =  0.01
            eps_min_max_diff = self.model.policy.value_min - eps_min_val
            readjust_range = self.good_policy_threshold - self.random_policy_threshold
            self.model.policy.inner_policy.eps = (1-min(max(current-self.random_policy_threshold,0)/readjust_range,1)) * eps_min_max_diff + eps_min_val
        if old != self.model.policy.inner_policy.eps:
            print("\nEpsilon changed from {} to {}.\n".format(old, self.model.policy.inner_policy.eps))

    def set_seed_and_test_model(self):
        """ Call test_model() after setting the environment seed to validation seeds
        """
        # seed 1 for testing
        current_training_seed = self.env.get_current_seed()
        self.env.seed(1)
        success, termination_reason, _, _ = test_model(self.env, self.model,nb_episodes=self.num_episodes_for_validation,
                                                 visualize=False, verbose=1)
        self.env.seed(current_training_seed)

        return success, termination_reason

    def on_step_end(self, step, logs={}):
        """  What to do after each step end.
        - Check if interval has reached to run validation
        - run validation test and get current success
        - adjust exploration if necessary using current success
        - check early stopping critera using current success
        """
        self.total_steps += 1

        if self.total_steps % self.interval != 0:
            # Nothing to do.
            return

        success, termination_reason = self.set_seed_and_test_model()

        self.last_n.append(success)
        if self.total_steps > self.early_stop_start_step:
            self.readjust_exploration(success)
        self.check_early_stopping(success)

    def on_train_end(self, logs=None):
        """  What to do after training ends.
        - Log best policy
        - Restore the best model
        - Set model training flag to false
        - Remove temporary save file
        """
        self.log("Best:{} at {} steps".format(self.best, self.best_num_steps))
        self.log("------------------------------------------------------------------")
        if not self.reached_baseline:
            print ("Model only achieved {}. Baseline: {}".format(self.best, self.baseline))
        if self.total_steps > self.interval:
            self.restore_best_model()
        self.model.training = False
        if os.path.exists(self.filepath):
            os.remove(self.filepath)

""" Implements a dual experience replay buffer that stores success and failure cases separately
"""
class DualSequentialMemory(SequentialMemory):
    def __init__(self, limit, **kwargs):
        self.success_memory = SequentialMemory(limit, **kwargs)
        self.failure_memory = SequentialMemory(limit, **kwargs)

        self.trajectory = []

        super(SequentialMemory, self).__init__(**kwargs)

    def sample(self, batch_size, batch_idxs=None):
        """Return a randomized batch of experiences.
        Sample uniformly from both success and failure memory

        # Argument
            batch_size (int): Size of the all batch
            batch_idxs (int): Indexes to extract
        # Returns
            A list of experiences randomly selected
        """
        success_experience = self.success_memory.sample(ceil(batch_size/2))
        failure_experience = self.failure_memory.sample(floor(batch_size/2))

        merged = success_experience + failure_experience
        random.shuffle(merged)

        return merged

    def append(self, observation, action, reward, terminal, training=True):
        """Append an observation to the memory. Check if trajectory is success or failure.
        Add to corresponding memory.

        # Argument
            observation (dict): Observation returned by environment
            action (int): Action taken to obtain this observation
            reward (float): Reward obtained by taking this action
            terminal (boolean): Is the state terminal
        """
        self.trajectory.append([observation, action, reward, terminal])

        if terminal:
            if reward > 0:
                for entry in self.trajectory:
                    self.success_memory.append(entry[0], entry[1], entry[2], entry[3], training=training)
            else:
                for entry in self.trajectory:
                    self.failure_memory.append(entry[0], entry[1], entry[2], entry[3], training=training)
            self.trajectory = []

        super(SequentialMemory, self).append(observation, action, reward, terminal, training=training)

    @property
    def nb_entries(self):
        """Return number of observations

        # Returns
            Number of observations
        """
        return len(self.success_memory.observations) + len(self.failure_memory.observations)

    def get_config(self):
        """Return configurations of SequentialMemory

        # Returns
            Dict of config
        """
        config = super(SequentialMemory, self).get_config()
        config['limit'] = self.success_memory.limit
        return config

class RestrictedEpsGreedyQPolicy(EpsGreedyQPolicy):
    """Implement the epsilon greedy policy

    Restricted Eps Greedy policy.
    This policy ensures that it never chooses the action whose value is -inf

    """

    def __init__(self, eps=.1):
        super(RestrictedEpsGreedyQPolicy, self).__init__(eps)

    def select_action(self, q_values):
        """Return the selected action.
        Handles case where q-value can be infinite

        # Arguments
            q_values (np.ndarray): List of the estimations of Q for each action

        # Returns
            Selection action
        """
        assert q_values.ndim == 1
        nb_actions = q_values.shape[0]
        index = list()

        for i in range(0, nb_actions):
            if q_values[i] != -np.inf:
                index.append(i)

        # every q_value is -np.inf (this sometimes inevitably happens within the fit and test functions
        # of kerasrl at the terminal stage as they force to call forward in Kerasrl-learner which calls this function.
        # TODO: exception process or some more process to choose action in this exceptional case.
        if len(index) < 1:
            # every q_value is -np.inf, we choose action = 0
            action = 0
            print("Warning: no action satisfies initiation condition, action = 0 is chosen by default.")

        elif np.random.uniform() <= self.eps:
            action = index[np.random.random_integers(0, len(index) - 1)]
        else:
            action = np.argmax(q_values)

        return action

class ModifiedLinearAnnealedPolicy(LinearAnnealedPolicy):

    def select_action(self, **kwargs):
        """Choose an action to perform.

        # Returns
            Action to take (int)
        """
        if self.agent.step <= self.nb_steps:
            setattr(self.inner_policy, self.attr, self.get_current_value())
        return self.inner_policy.select_action(**kwargs)

class DQNAgentModified(DQNAgent):
    def process_state_batch(self, batch):
        return np.squeeze(np.array(batch), axis=1)

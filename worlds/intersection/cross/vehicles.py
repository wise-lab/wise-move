import numpy as np

from verifier import AtomicPropositions
from vehicle_models import KinematicBicycle
from utilities import normalize_radian

from ..constants import constants as consts


class VehicleState(KinematicBicycle):
    """A class that defines the vehicle's (cont.) state variables and provides
    a mean to calculate the next state.

    * which_route: the route the vehicle follows. It has to be
                   either horizontal ('h') or vertical ('v');
    * which_dir: the direction of the vehicle within the route. It has to
                 be either 'forward' or 'backward'
    * x, y: the x and y position of the vehicle;
    * theta: the heading angle of the vehicle;
    * v: the velocity along the heading angle;
    * steer: the steering angle;
    * (a, steer_rate): the acceleration and the rate of change of the
        steering angle. If u = (u[0], u[1]) in step is in the reasonable
        range, this is equal to the previous input u at the previous step.
    """

    def __init__(self, which_route, lane_i, pos_in_lane, v, steer, mode='fast', max_acc=consts.MAX_ACCELERATION):
        """The constructor which sets the properties of the class.

        Args:
            which_route: the route the vehicle follows. It has to be
                either horizontal ('h') or vertical ('v');
            lane_i: the lane index (either LEFT or RIGHT);
            pos_in_lane: the 1D position in the lane;
            (v, steer): the velocity and the steering angle of the
                vehicle, respectively.
            mode: either 'fast' (use 1D Euler method when steer = 0) or
                'normal'.
            max_acc: max acceleration possible
        """

        assert which_route in ('h', 'v')
        assert lane_i in (0, 1)

        self.mode = mode
        self.which_route = which_route

        if which_route is 'h':
            lane = consts.rd.h.lanes[lane_i]
            x = pos_in_lane
            y = lane.centre
            theta = 0.0

        else:  # the case of "which_route is 'v'"
            lane = consts.rd.v.lanes[lane_i]
            y = pos_in_lane
            x = lane.centre
            theta = - np.sign(consts.rd.v.start_pos) * np.pi / 2.0

        if lane.type is 'backward':
            theta += np.pi
            theta = normalize_radian(theta)

        self.theta_ref = theta
        self.which_dir = lane.type

        super().__init__(x, y, theta, v, steer,
                         consts.VEHICLE_SIZE, consts.VEHICLE_WHEEL_BASE,
                         consts.MAX_STEERING_ANGLE, consts.MAX_STEERING_ANGLE_RATE,
                         a_min=consts.MIN_ACCELERATION, a_max=max_acc)

    def step(self, u, dt=consts.DT):
        """Calculate the next state and update the other properties for the
        given input u.

        Args:
            u: a tuple or an array whose first element corresponds to the
                acceleration and the second element corresponds to the rate
                of change of the steering angle.
        """

        if self.mode is 'fast':
            assert u[1] == 0 and self.steer == 0 and self.theta == self.theta_ref

            self.a = np.clip(u[0], self.a_min, self.a_max)
            self.steer_rate = 0

            if self.which_route == 'h':
                self.x += self.v * dt if self.which_dir == 'forward' else -self.v * dt

            else:  # the case of "which_route is 'v'"
                self.y += self.v * dt if self.which_dir == 'forward' else -self.v * dt

            self.v = np.clip(self.v + self.a * dt, 0, self.v_max)
        else:
            super().step(u, dt)


class Vehicle(VehicleState):
    """A vehicle class that inherits VehicleState (so, contains the
    (continuous) state info.), and also have discrete state variables. (i.e.,
    atomic propositions (APs)).

    Properties are the same as of VehicleState +
        * APs: the class AtomicPropositions that stores all the APs
            of the vehicle as True or False.
        * waited_count: the number of time steps the vehicle stays
            in the stop region; it is -1 when the car is not in the stop region;
    """

    def __init__(self, which_route, lane_i=None, v_upper_lim=consts.rd.speed_limit, np_random=None,
                 max_acc=consts.MAX_ACCELERATION, v_start=None):
        """The constructor which sets the properties of the class, with vehicle
        randomly posed with random speed.

        Args:
            which_route: the direction of the vehicle. It has to be either
                horizontal ('h') or vertical ('v');
            lane_i: the lane number (0 is the right lane, 1 means the left
                lane); if it's equal to None, it chooses 0 or 1
                uniformly randomly.
            v_upper_lim: the upper bound of the random speed of the vehicle
            np_random: RandomState used for scenario generation
            max_acc: max acceleration possible
            v_start: starting velocity. if not specified, it is randomly sampled
        """
        if np_random is None:
            ValueError("np.random.RandomState is not passed. It should be the same one used in simpleintersection")

        self.np_random = np_random
        self.APs = AtomicPropositions(consts.APS_KEYS)
        self.max_acc = max_acc
        self.max_vel = v_upper_lim
        self.v_start = v_start

        # tracks the number of times it has telported when it reached edge of screen
        self.teleport_count = 0

        self.random_reset(which_route, lane_i, v_upper_lim)

    def reset(self, which_route, lane_i, pos_in_lane, v, steer):
        """Reset the properties of the class including vehicle's state
        variables; this reset method updates all the atomic propositions and
        the discrete state (i.e., waited_count)."""

        super().__init__(which_route, lane_i, pos_in_lane, v, steer, max_acc=self.max_acc)
        self.init_local_discrete_var()

    def random_reset(self, which_route, lane_i=None, v_upper_lim=consts.rd.speed_limit):
        """Randomly reset the properties of the class including vehicle's state
        variables; the speed is [and the lane number can be] chosen randomly.

        The parameters of the method are exactly same as those in the
        constructor.
        """
        lane_i = self.np_random.randint(0, 2) if lane_i is None else lane_i

        route = consts.rd.h if which_route is 'h' else consts.rd.v
        pos_in_lane = (route.end_pos - route.start_pos) * self.np_random.rand() + route.start_pos

        if self.v_start is not None:
            v = self.v_start
        else:
            # average vehicle behaviour
            # 0-60 average is 7 seconds (3.83m/s)- 9 (2.98m/s) seconds.
            # That is straight line with fast gear shift. But people don't accelerate that fast
            # so lets say 9-11 seconds
            self.max_acc = self.np_random.uniform(2.4, 3.0)
            # Max vel is a little over speed limit, but mostly under it
            # 2.78m/s = 10km/hr, 1.39 = 5km/hr
            self.max_vel = self.np_random.uniform(self.max_vel-2.78,self.max_vel+1.39)
            v = self.max_vel
            # v2v dist safe: 1.5 - 2.5 seconds
            self.V2V_ref = self.np_random.uniform (self.max_vel*1.5, self.max_vel*2.5)

        self.reset(which_route, lane_i, pos_in_lane, v, 0)

    @property
    def _lane(self):
        return self.y > 0 if self.which_route == 'h' else self.x < 0

    def init_local_discrete_var(self):
        self.update_local_APs()

    def update_local_APs(self):
        """Update local atomic propositions (related to the vehicle, road
        geometry, and road environment); based upon the continuous and the
        previous discrete states, all the atomic propositions within the class
        are updated, except the global ones ('highest_priority',
        'intersection_is_clear', and 'veh_ahead')."""

        APs = self.APs

        APs['lane'] = self._lane

    def step(self, u, dt=consts.DT):
        """Calculate the next state and update the other properties for the
        given input u. NOTE: the global atomic propositions ('highest_priority'
        and 'intersection_is_clear') are not updated.

        Args:
            u: a tuple or an array whose first element corresponds to the
                acceleration and the second element corresponds to the rate
                of change of the steering angle.
        """

        super().step(u, dt)
        self.update_local_APs()

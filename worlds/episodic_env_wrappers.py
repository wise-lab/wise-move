from worlds import EpisodicEnv


# (Jan. 7th, 2020) For now, the wrapper class below was not used anywhere in wise-move
class EpisodicEnvWrapper(EpisodicEnv):

    def __init__(self, env):
        assert isinstance(env, EpisodicEnv)

        super().__init__(time=env.time, terminal_reward=env.terminal_reward, show_info=env.show_info)
        self.env = env

    def step(self, action): return self.env.step(action)

    def reset(self): return self.env.reset()

    def render(self, mode='human'): return self.env.render()

    def get_info(self): return self.env.get_info()

    @property
    def unwrapped(self): return self.env.unwrapped

    @property
    def termination_reasons(self): return self.env.termination_reasons

    def clear_termination_reasons(self): self.env.clear_termination_reasons()

    def update_termination_info(self): return self.env.update_termination_info()

    def termination_condition(self): return self.env.termination_condition()

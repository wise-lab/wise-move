import numpy as np

from utilities import TerminalReward, Time
from .events import Event, TimeOut


class EpisodicEnv(object):
    """
    A base class for any episodic/option environment.

    To derive a new env-class from this object, check the followings.

        0. The 'timeout' Event exists by default, associated with the attribute self.time of class type Time.

        1. If necessary, add more events to the attribute "termination_events" by using the Event classes above.

        2. Implement the step method. An example is:

            def step(self, action):
                .
                .
                .
                observation = <get an observation or a state>
                reward = 1 if <some condition> else 0

                terminal = self.step_termination_events()
                return observation, reward + self.terminal_reward(), terminal, self.get_info()

        3. If necessary override the reset method, e.g.,
            def reset(self):
                terminal = True
                while terminal:
                    ** Do something for reset **
                    terminal = super().reset():

        4. For visualization, implement the render method which has three modes:
            mode='human', mode='rgb_array', or mode='ansi'. For each mode, see the 'render' method in gym.core.Env.

    """

    def __init__(self, **kwargs):
        property_defaults = {
            'time': Time(1, np.inf),  # The Time class controlling the time step and timeout and providing time info.
            'terminal_reward': TerminalReward('min'),  # The TerminalReward class with mode='min' by default
            'show_info': True  # get_info returns an empty dictionary if self.show_info = False
        }

        for (prop, default) in property_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        assert isinstance(self.time, Time)
        assert isinstance(self.terminal_reward, TerminalReward)
        assert isinstance(self.show_info, bool)

        #: The (termination) events. If one needs, add more termination events in the subclass.
        # An example in a subclass __init__ is:
        #   def __init__(self):
        #       super().__init__()
        #
        #       events = {'goal achieved': Event(lambda: ** some condition to check whether the goal is achieved **,
        #                                  lambda: 1)}
        #
        #       self.termination_events.update(events)
        #       self.termination_events[timeout].rewards = lambda: 1
        #
        #       ...
        #
        # The default event for an episode termination at this base class is 'timeout' with zero reward as shown below.
        self.termination_events = {'timeout': TimeOut(self.time)}

        #: a string-set member which informs us of episode termination reasons
        # (will be implemented in subclasses)
        self.__episode_termination_reasons = set()

    def step(self, action):
        """Gym compliant step function which will be implemented in the subclass.
        """
        raise NotImplementedError(self.__class__.__name__ + '.step is not implemented.')

    def step_termination_events(self):
        """ calls 'step' in all termination_events and update the termination info.
        This method is designed to use within the subclass 'step' method --- see step #2 at the class definition.

        Returns: True if the terminal state has been reached, otherwise False.

        """
        for termination_events in self.termination_events.values():
            termination_events.step()

        return self.update_termination_info()

    def reset(self):
        """Gym compliant reset function
        """
        for termination_events in self.termination_events.values():
            termination_events.reset()

        self.terminal_reward.reset()
        self.__episode_termination_reasons.clear()
        terminal = self.update_termination_info()

        if terminal and __debug__:
            import warnings
            warnings.warn('the environment was reset but the state is initially at the terminal...', stacklevel=2)

        return terminal

    def render(self, mode='human'):
        """Gym compliant render function which will be implemented in the subclass."""
        raise NotImplementedError(self.__class__.__name__ + ".render is not implemented.")

    def get_info(self):
        if self.show_info:
            return {'episode_termination_reasons': ','.join(self.__episode_termination_reasons),
                    'timeout': self.termination_events['timeout'].condition()}
        else:
            return {}

    @property
    def unwrapped(self):
        """Completely unwrap this env.

        Returns:
            EpisodicEnv: The completely non-wrapped EpisodicEnv (sub-)class instance
        """
        return self

    @property
    def termination_reasons(self): return self.__episode_termination_reasons.copy()

    def update_termination_info(self):
        """ Checks whether one of the termination event defined in termination_event happens including timeout.
        As a result of any event happening, terminal_reward and the __episode_termination_reasons are updated.

        Returns:
            True if any condition among the "enabled" events defined in termination_event is True, otherwise False.
        """
        terminal = False
        for reason, e in self.termination_events.items():
            if e.condition() and e.enable:
                terminal = True
                self.terminal_reward.update(e.reward())
                self.__episode_termination_reasons |= {reason}

        return terminal

    def termination_condition(self):
        """ Returns True if one of the termination conditions defined by the "enabled" events in termination_events
        is True, otherwise False.
        """
        return any(e.condition() and e.enable for e in self.termination_events.values())

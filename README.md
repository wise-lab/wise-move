<div align=center>

<img src="documentation/markdown/logo.png" height="70" />
<img src="documentation/markdown/motto.png" height="30" /><br>

<img src="documentation/figures/highlevel.gif" width=400/><br><br>

<a href=documentation/markdown/features.md><img src="documentation/markdown/features_button.png" height="30" /></a>
<a href=documentation/markdown/installation.md><img src="documentation/markdown/installation_button.png" height="30" /></a>
<a href=documentation/markdown/repeatability.md><img src="documentation/markdown/repeatability_button.png" height="30" /></a>
<a href=documentation/markdown/contributing.md><img src="documentation/markdown/contributing_button.png" height="30" /></a> 
<a href=documentation/markdown/license.md><img src="documentation/markdown/license_button.png" height="30" /></a>
<a href=documentation/markdown/about.md><img src="documentation/markdown/about_button.png" height="30" /></a>
</div>


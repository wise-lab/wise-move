import numpy as np
from utilities import normalize_radian


class VehicleModel(object):
    """The base class for all vehicle model classes
        * identifier: the name of the model (str)
        * dim: dimension of the model (1 or 2)
        * size: a tuple "(width, length)" representing the size of the vehicle
        * method: a method ('Euler' or 'RK4') calculate the next vehicle state
    """
    def __init__(self, identifier, dim, size, method):
        self.identifier = identifier
        assert isinstance(identifier, str)
        assert dim in {1, 2}
        assert min(size) > 0 and len(size) == 2
        assert method in {'Euler', 'RK4'}

        self.identifier = identifier
        self.dim = dim
        self.size_in_half = (size[0] / 2, size[1] / 2)
        self.method = method

    def step(self, u, dt):
        self._input_channel(u)
        if self.method is 'Euler':
            self._euler(dt)
        else:
            self._rk4(dt)

    def _euler(self, dt):
        raise NotImplementedError('The Euler method of ' +
                                  self.__class__.__name__ +
                                  ' is not implemented.')

    def _rk4(self, dt):
        raise NotImplementedError('The RK4 method of ' +
                                  self.__class__.__name__ +
                                  ' is not implemented.')

    def _input_channel(self, u):
        raise NotImplementedError('The _input_channel method of ' +
                                  self.__class__.__name__ +
                                  ' must be implemented.')

    @property
    def bounding_box(self):
        """Calculate the bounding box of the vehicle.

        Returns: numpy array of the four points of the bounding box
                 depending on the vehicle's pose (x,y,theta)
        """
        raise NotImplementedError('The bounding_box property of ' +
                                  self.__class__.__name__ +
                                  ' is not implemented.')


class PointMass1D(VehicleModel):
    """one dimensional point-mass model.
        * p: the position (scalar)
        * v: velocity (scalar)
        * a: acceleration (scalar)
    """

    def __init__(self, p, v, size, v_max=np.inf, a_min=-np.inf, a_max=np.inf):
        super().__init__('point-mass-1D', 1, size, 'Euler')
        self.p = p
        self.v = v
        self.a = 0

        self.v_max = v_max
        self.a_min = a_min
        self.a_max = a_max

    def _input_channel(self, u):
        self.a = np.clip(u, self.a_min, self.a_max)

    def _euler(self, dt):
        self.p += self.v * dt
        self.v = np.clip(self.v + self.a * dt, 0, self.v_max)


def point_mass_euler_1d(p, v, a, dt, v_max=np.inf):
    p += v * dt
    v = np.clip(v + a * dt, 0, v_max)
    return p, v


class KinematicBicycle(VehicleModel):
    """The kinematic bicycle model.
        * (x, y, theta): the pose of the vehicle;
        * v: velocity (scalar);
        * steer: steering angle (scalar);
        * a: acceleration (scalar);
        * steer_rate: steering angle change rate (scalar).
    """

    def __init__(self, x, y, theta, v, steer, size, wheel_base,
                 steer_max, steer_rate_max, v_max=np.inf,
                 a_min=-np.inf, a_max=np.inf, method='RK4'):
        super().__init__('kinematic-bicycle', 2, size, method)

        #: Initialization of the states of the vehicle.
        self.x = x
        self.y = y
        self.theta = theta
        self.v = v
        self.steer = steer

        #: Vehicle wheel base.
        self.wheel_base = wheel_base

        #: input to the vehicle model
        self.a = 0
        self.steer_rate = 0

        self.v_max = v_max
        self.steer_max = steer_max
        self.steer_rate_max = steer_rate_max
        self.a_min = a_min
        self.a_max = a_max

    def _input_channel(self, u):
        self.a = np.clip(u[0], self.a_min, self.a_max)
        self.steer_rate = np.clip(u[1], -self.steer_rate_max, self.steer_rate_max)

    def _euler(self, dt):
        self.x += dt * self.v * np.cos(self.theta)
        self.y += dt * self.v * np.sin(self.theta)
        self.theta += dt * self.v * np.tan(self.steer) / self.wheel_base

        self.v = np.clip(self.v + self.a * dt, 0, self.v_max)
        self.steer = np.clip(self.steer + dt * self.steer_rate,
                             -self.steer_max, self.steer_max)

        self.theta = normalize_radian(self.theta)

    def _rk4(self, dt):

        dt_over_2 = dt / 2
        dt_over_3 = dt / 3
        dt_over_6 = dt / 6

        K1x = self.v * np.cos(self.theta)
        K1y = self.v * np.sin(self.theta)
        K1th = self.v * np.tan(self.steer) / self.wheel_base

        theta_temp = self.theta + dt_over_2 * K1th
        v_temp = np.clip(self.v + self.a * dt_over_2, 0, self.v_max)
        steer_temp = np.clip(self.steer + self.steer_rate * dt_over_2,
                             -self.steer_max, self.steer_max)

        K23x = np.cos(theta_temp)
        K23y = np.sin(theta_temp)
        K23th = v_temp * np.tan(steer_temp) / self.wheel_base

        theta_temp = self.theta + dt_over_2 * K23th

        K23x += np.cos(theta_temp)
        K23y += np.sin(theta_temp)
        K23x *= v_temp
        K23y *= v_temp

        v_temp = np.clip(self.v + self.a * dt, 0, self.v_max)
        steer_temp = np.clip(self.steer + dt * self.steer_rate,
                             -self.steer_max, self.steer_max)

        K4x = v_temp * np.cos(theta_temp)
        K4y = v_temp * np.sin(theta_temp)
        K4th = v_temp * np.tan(steer_temp) / self.wheel_base

        self.x += dt_over_6 * (K1x + K4x) + dt_over_3 * K23x
        self.y += dt_over_6 * (K1y + K4y) + dt_over_3 * K23y
        self.theta += dt_over_6 * (K1th + K4th) + 2 * dt_over_3 * K23th
        self.v = v_temp
        self.steer = steer_temp

        self.theta = normalize_radian(self.theta)

    @property
    def bounding_box(self):
        """Calculate the bounding box of the vehicle.

        Returns: numpy array of the four points of the bounding box
                 depending on the vehicle's pose (x,y,theta)
        """

        c = np.cos(self.theta)
        s = np.sin(self.theta)

        output = []
        # counter-clockwise order of vertices
        for sign_y in [-1, 1]:
            for sign_x in [1, -1]:
                x = sign_y * sign_x * self.size_in_half[0]
                y = sign_y * self.size_in_half[1]
                output.append([c * x - s * y + self.x, s * x + c * y + self.y])

        return np.array(output)

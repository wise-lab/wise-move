import numpy as np


class Bits(object):
    """A bit-control class that allows us bit-wise manipulation as shown in the
    example::

    bits = Bits()
    bits[0] = False
    bits[2] = bits[0]
    """

    def __init__(self, value=0):
        self._d = value

    def __getitem__(self, index):
        return (self._d >> index) & 1

    def __setitem__(self, index, value):
        # conversion to the int-type is necessary since sometimes value is np.int64 or np.int32 which causes a problem.
        value = int((value & 1) << index)
        mask = 1 << index
        self._d = (self._d & ~mask) | value

    def __getslice__(self, start, end):
        mask = 2**(end - start) - 1
        return (self._d >> start) & mask

    def __setslice__(self, start, end, value):
        mask = 2**(end - start) - 1
        value = (value & mask) << start
        mask = mask << start
        self._d = (self._d & ~mask) | value
        return (self._d >> start) & mask

    def __int__(self):
        return self._d


class BoundingBox(object):
    """A class that provides utility functions related to bounding boxes.

    * Bounding box: ndarray of shape (4,2) containing the vertices of the
        rectangle in order
    """

    @staticmethod
    def does_bounding_box_intersect(first_bb, second_bb):
        """Checks if bounding boxes intersect using separating axis test: Two
        objects don't intersect if you can find a line that separates the two
        objects. Works only for quadrilateral bounding boxes (in 2D).

        Args:
            first_bb:  np.ndarray First bounding box
            second_bb: np.ndarray Second bounding box

        Returns:
            true if there is an intersection
        """

        for i, vertex in enumerate(first_bb):
            # for each vertex, get line joining it and next vertex
            line = np.array([vertex, first_bb[(i + 1) % 4]])

            # check side of other 2 vertices in first_bb
            side_of_first_bb = BoundingBox.localize_point_wrt_line(
                line,
                first_bb[(i + 2) % 4]) + BoundingBox.localize_point_wrt_line(
                    line, first_bb[(i + 3) % 4])

            # Find side of all points in second_bb
            side_of_second_bb = []
            for j in range(second_bb.shape[0]):
                side_of_second_bb.append(
                    BoundingBox.localize_point_wrt_line(line, second_bb[j, :]))

            # Check if all points in second_bb are on same side of line
            # and that other vertices in first_bb are on the opposite side
            if BoundingBox.all_same_sign_in_list(side_of_second_bb) and (
                    side_of_first_bb * sum(side_of_second_bb) < 0):
                return False

        return True

    # TODO: This method was never used in any other places. Remove or keep it?
    @staticmethod
    def does_bounding_box_cross_line(line, bb):
        """Checks if bounding box crosses a line. Line is represented by an
        np.ndarray of size (2,2) representing two points in it. Works only for
        quadrilateral bounding boxes (in 2D).

        Args:
            line:  np.ndarray Two points representing the line
            bb: np.ndarray Bounding box

        Returns:
            true if the bounding box crosses the line
        """

        side_of_vertices = []
        for i, vertex in enumerate(bb):
            side_of_vertices.append(
                BoundingBox.localize_point_wrt_line(line, vertex))

        if BoundingBox.all_same_sign_in_list(side_of_vertices):
            return False
        else:
            return True

    @staticmethod
    def localize_bounding_box_wrt_line(line, bb):
        """Returns the side in which a bounding box is w.r.t a line. Line is
        represented by an np.ndarray of size (2,2) representing two points in
        it. Works only for quadrilateral bounding boxes (in 2D).

        Args:
            line:  np.ndarray Two points representing the line
            bb: np.ndarray Bounding box

        Returns:
            1 or -1 depending upon the side w.r.t the line. returns 0 if it
            intersects the line
        """

        side_of_vertices = []
        for i, vertex in enumerate(bb):
            side_of_vertices.append(
                BoundingBox.localize_point_wrt_line(line, vertex))

        if BoundingBox.all_same_sign_in_list(side_of_vertices):
            for side in side_of_vertices:
                # side_of_vertices can contain 0 if a vertex touches the line
                if side != 0:
                    return side
        else:
            return 0

    @staticmethod
    def localize_point_wrt_line(line, testpoint):
        """Check the side in which a point lies w.r.t a line.

        Args:
            line: np.ndarray Two points representing the line
            testpoint: the point to be checked

        Returns:
            1 or -1 depending upon the side w.r.t the edge;
            returns 0 if it is on the line
        """

        edge = line[0, :] - line[1, :]
        vertex_in_edge = line[0, :]

        # find perpendicular to line
        rotated_edge = np.array([-edge[1], edge[0]])

        # return -1 or 1 depening on the side. 0 if on the line
        return np.sign(rotated_edge[0] * (testpoint[0] - vertex_in_edge[0]) +
                       rotated_edge[1] * (testpoint[1] - vertex_in_edge[1]))

    @staticmethod
    def all_same_sign_in_list(test_list):
        """Check if all elements in a list are of the same sign. Zero does not
        have a sign.

        Args:
            test_list: the python list to be checked

        Returns:
            1 or -1 depending upon the side w.r.t the edge
        """

        return all(k >= 0 for k in test_list) or all(k <= 0 for k in test_list)


class TerminalReward(object):
    def __init__(self, mode='min'):
        self.__terminal_reward = None
        self.mode = mode

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        assert mode in {'min', 'max', 'sum'}
        self.__mode = mode

    def update(self, rewards):
        assert self.__mode in {'min', 'max', 'sum'}

        if rewards is None: return
        if not isinstance(rewards, (list, tuple)): rewards = (rewards,)

        for r in rewards:
            if r is not None:
                self.__terminal_reward = r if self.__terminal_reward is None else \
                                         min(self.__terminal_reward, r) if self.__mode == 'min' else \
                                         max(self.__terminal_reward, r) if self.__mode == 'max' else \
                                         self.__terminal_reward + r  # if self.__mode == 'sum'

        assert self.__terminal_reward is None or isinstance(self.__terminal_reward, (int, float))

    def __call__(self):
        return 0 if self.__terminal_reward is None else self.__terminal_reward

    def reset(self):
        self.__terminal_reward = None


class Time(object):

    def __init__(self, dt, timeout=np.inf):
        self.__num_steps = 0
        self.__dt = dt
        self.set_timeout(timeout)
        self.__timeout_happened = False

    @property
    def t(self):
        return self.__num_steps * self.__dt

    @property
    def num_steps(self):
        return self.__num_steps

    @property
    def dt(self):
        return self.__dt

    @property
    def timeout_happened(self):
        return self.__timeout_happened

    def get_timeout(self):
        return self.__num_max_steps * self.__dt

    def set_timeout(self, t_out):
        dt = self.__dt
        assert t_out >= dt > 0
        self.__num_max_steps = np.inf if t_out == np.inf else round(t_out / dt)

    @property
    def num_max_steps(self):
        return self.__num_max_steps

    def step(self):
        self.__num_steps += 1
        assert self.__num_steps <= self.__num_max_steps
        if self.__num_steps == self.__num_max_steps:
            self.__timeout_happened = True

    def reset(self):
        self.__num_steps = 0
        self.__timeout_happened = False
        return None


def normalize_radian(rad):
    while not (- np.pi < rad <= np.pi):
        rad -= 2 * np.pi * np.sign(rad)

    return rad

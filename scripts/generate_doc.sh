#!/usr/bin/env bash
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# assuming sphinx is installed
function build_doc() {
  curr_dir=$(pwd)
  cd "$SCRIPT_DIR"/../documentation/sphinx
  rm -rf .build
  mkdir .build
  sphinx-apidoc -o ./.doc ../../.
  sphinx-build -b html . ./.build
  rm -rf .doc
  ln -s sphinx/.build/index.html ../index.html
  cd "$curr_dir"  
  echo "Documentation files placed in the .build directory in sphinx folder."

}

function clean_doc() {
  curr_dir=$(pwd)
  cd "$SCRIPT_DIR"/../documentation/sphinx
  rm -rf .build
  rm ../index.html
  cd "$curr_dir"
  echo "Cleaned .build folder."
}

# if arg0 is clean then kill session
if [[ "$#" -eq 1 ]]; then
  if [[ "$1" == "clean" ]]; then
    clean_doc
    exit 0
  elif [[ "$1" == "build" ]]; then
    build_doc
  elif [[ "$1" == "launch" ]]; then
    case "$(uname -s)" in
     Darwin)
       open "$SCRIPT_DIR/../documentation/index.html"
       ;;
     Linux)
       xdg-open "$SCRIPT_DIR/../documentation/index.html"
       ;;
     *)
       echo 'Cannot determine your OS. Open the .build/index.html file in the root folder.' 
       ;;
    esac
  else
    echo "Usage: generate_doc.sh {build|clean|launch}" 
  fi
else
  echo "Usage: generate_doc.sh {build|clean|launch}" 
fi
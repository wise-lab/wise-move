#!/bin/bash
set -eu

docker stop wisemove_box > /dev/null 2>&1
docker container rm wisemove_box > /dev/null 2>&1

echo "Container [wisemove_box] has been stopped and removed"
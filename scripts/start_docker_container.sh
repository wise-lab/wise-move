#!/bin/bash
set -eu

SRC_DIR_HOST="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd .. && pwd )"
XAUTH=/tmp/.docker.xauth
SRC_DIR_CONTAINER="/wise-move"

docker run -it -d \
  --name wisemove_box \
  --volume=$XAUTH:$XAUTH:rw \
  --volume=$SRC_DIR_HOST:$SRC_DIR_CONTAINER \
  --volume=/usr/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:ro \
  --env="LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu" \
  --env="XAUTHORITY=$XAUTH" \
  --env="DISPLAY" \
  --env="QT_X11_NO_MITSHM=1" \
  --gpus all \
  --workdir $SRC_DIR_CONTAINER \
  wisemove_box:latest \
  tail -f /dev/null

echo "Container [wisemove_box] has been started. Use: bash enter_docker_container.sh to enter"
#!/bin/bash
set -eu

DOCKER_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd .. && pwd )"

# Set XAUTH for display
function set_docker_xauth() {
  XAUTH=/tmp/.docker.xauth
  if [ -f "${XAUTH}" ]; then
    echo "/tmp/.docker.xauth already exists. Skipping creation."
  else
    touch $XAUTH
    xauth nlist "$DISPLAY" | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
  fi
}

# Build the docker image
function build_box() {
  echo "Building box..."

  # Dockerfile is in same directory as this script
  docker build \
    --build-arg build_uid=${UID} \
    --build-arg build_username='wisemove-user' \
    "${DOCKER_FILE_DIR}" \
    -t wisemove_box:latest
}

# Do all the setup
set_docker_xauth
build_box
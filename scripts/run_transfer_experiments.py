import sys
import os
import argparse
import subprocess
import signal

# Set the base wise-move directory to the Python working directory.
os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/..")
sys.path.append(os.getcwd())

CONFIG_FILE = "config.json"
TRANSFER_PY_FILE = "worlds/intersection/cross/high_level_main.py"

POLICIES_NOT_TRAINABLE = {'random', 'rule', 'r_rule', 'distill'}

def set_transfer_config():
    with open(CONFIG_FILE, "rt") as fin:
        with open("temp.json", "wt") as fout:
            for line in fin:
                _line = line.replace('simple', 'cross')
                fout.write(_line.replace('Simple', 'Cross'))
    os.remove(CONFIG_FILE)
    os.rename("temp.json", CONFIG_FILE)


def unset_transfer_config():
    with open(CONFIG_FILE, "rt") as fin:
        with open("temp.json", "wt") as fout:
            for line in fin:
                _line = line.replace('cross', 'simple')
                fout.write(_line.replace('Cross', 'Simple'))
    os.remove(CONFIG_FILE)
    os.rename("temp.json", CONFIG_FILE)


def signal_handler(sig, frame):
    unset_transfer_config()
    sys.exit(0)


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--test",
        help="Test a policy. Use corresponding policy argument along with this. Example: --test --lagkf",
        action="store_true")
    parser.add_argument(
        "--train",
        help="Train a policy from scratch. Use corresponding policy argument along with this. Example: --train --lagkf. Note: training process is stochastic so multiple trials may be needed to get a good policy",
        action="store_true")
    parser.add_argument(
        "--saved_policy_in_root",
        help="Use along with --test to test a new policy trained using --train",
        action="store_true")
    parser.add_argument(
        "--perturb",
        help="Perturb feature space for analysis. Use along with --test. Give index of the feature to perturb. 0:x, 1:y, 2:heading, 3:velocity, 4:ttc",
        default=-1,
        type=int)
    parser.add_argument(
        "--visualize",
        help="Use to visualize training/testing.",
        action="store_true")        
    parser.add_argument(
        "--random",
        help="policy: random. Random action selection.",
        action="store_true")
    parser.add_argument(
        "--rule",
        help="policy: rule. TTC-based rule based.",
        action="store_true")
    parser.add_argument(
        "--r_rule",
        help="policy: r-rule. TTC-based rule based using information of target environment.",
        action="store_true")
    parser.add_argument(
        "--distill",
        help="policy: distill. Rule based policy distilled from decision tree.",
        action="store_true")
    parser.add_argument(
        "--wm",
        help="policy: wm. Trained RL policy trained in stack-aligned environment.",
        action="store_true")        
    parser.add_argument(
        "--lag",
        help="policy: lag. Trained RL policy trained in stack-aligned environment with perception lag.",
        action="store_true")
    parser.add_argument(
        "--kf",
        help="policy: kf. Trained RL policy trained in stack-aligned environment with naive velocity error model.",
        action="store_true")
    parser.add_argument(
        "--lagkf",
        help="policy: lagkf. Trained RL policy trained in stack-aligned environment with naive velocity error model and perception lag.",
        action="store_true")
    parser.add_argument(
        "--lag_dr",
        help="policy: lag-dr. Trained RL policy trained in lagkf environment with perception lag DR.",
        action="store_true")
    parser.add_argument(
        "--kf_dr",
        help="policy: kf_dr. Trained RL policy trained in lagkf environment with velocity estimation DR.",
        action="store_true")
    parser.add_argument(
        "--vanish",
        help="policy: vanish. Trained RL policy trained in lagkf environment with vanishing vehicles phenomenon.",
        action="store_true")
    parser.add_argument(
        "--xy_dr",
        help="policy: xy-dr. Trained RL policy trained in lagkf environment with X-Y feature noise.",
        action="store_true")
    parser.add_argument(
        "--percept",
        help="policy: percept. Trained RL policy trained in lagkf environment with all above perception errors.",
        action="store_true")
    parser.add_argument(
        "--best",
        help="policy: best. Trained RL policy trained in lagkf environment with lagdr, kfdr and xy-dr.",
        action="store_true")   

    args = parser.parse_args()
    return args


def select_policy_to_run(args):
    run_arr = []
    train_test_arg = ''
    if args.test:
        train_test_arg = '--test'
    elif args.train:
        train_test_arg = '--train'

        # Check whether the arguments contain any non-trainable ones...
        for policy in POLICIES_NOT_TRAINABLE:
            if getattr(args, policy):
                raise ValueError(f'the policy for --{policy} is not trainable...')

    if args.random:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm_0_1.h5f', '--random_policy']
    elif args.rule:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm_0_1.h5f', '--rule_based']
    elif args.r_rule:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm_0_1.h5f', '--rule_based', '--use_robust']
    elif args.distill:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm_0_1.h5f', '--rule_based', '--use_distilled']
    elif args.wm:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm_0_1.h5f']
    elif args.lag:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-lag-1.h5f', '--lag']
    elif args.kf:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-kf-1.h5f', '--kf']
    elif args.lagkf:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-lagkf-1.h5f', '--lag', '--kf']
    elif args.lag_dr:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-lagdr-1.h5f', '--rand_lag', '--kf']
    elif args.kf_dr:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-kfdr-1.h5f', '--lag', '--rand_kf']
    elif args.vanish:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-vanish-1.h5f', '--lag', '--kf', '--vanish']
    elif args.xy_dr:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-xy-1.h5f', '--lag', '--kf', '--rand_xy']
    elif args.percept:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-percept-1.h5f', '--perception']
    elif args.best:
        run_arr = ['python', TRANSFER_PY_FILE, train_test_arg,
                   '--save_file', 'wm-best_0_1.h5f', '--rand_lag', '--rand_kf', '--rand_xy']
    else:
        print("Please specify a valid policy to train/test.")
        return

    if args.train or args.saved_policy_in_root:
        run_arr.append('--saved_policy_in_root')

    if args.perturb != -1:
        if args.perturb >= 0 and args.perturb <= 4:
            run_arr.append('--perturb_feature')
            run_arr.append(str(args.perturb))
        else:
            print("Perturb feature index is incorrect.")
            return

    if args.visualize:
        run_arr.append('--visualize')

    print("===========")
    print(sys.argv)
    print("===========")
    subprocess.run(run_arr)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    args = get_arguments()
    set_transfer_config()

    select_policy_to_run(args)

    unset_transfer_config()

#!/bin/bash
# Note: use this script if you do not want to use docker
# This will setup anaconda and create a conda environment 
# called wisemove with necessary packages
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd $SCRIPT_DIR

if hash conda 2>/dev/null; then
    echo "conda command is present. Not installing miniconda."
else
    echo "conda command not found. Installing miniconda."
    # install curl if not there
    sudo apt-get update
    sudo apt install -y curl

    # download and install miniconda
    curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh -b
    eval "$(~/miniconda3/bin/conda shell.bash hook)"
    rm Miniconda3-latest-Linux-x86_64.sh
fi

# setup wisemove conda env
conda init bash
conda create -n wisemove python=3.6 -y
conda activate wisemove
conda install tensorflow==1.9 -y
pip install matplotlib keras==2.2.4 keras-rl h5py gym tqdm
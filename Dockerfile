# SET BASE IMAGE:
FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04

# Setup basic commands
RUN apt-get update\
&& apt-get install -y --no-install-recommends \
        x11-apps \
        build-essential \
        curl \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        rsync \
        software-properties-common \
        unzip \
        libcupti-dev

# Setup environment variables
ENV LD_LIBRARY_PATH /usr/local/cuda-9.0/lib64:/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH
ENV CUDA_HOME /usr/local/cuda-9.0
ENV DEBIAN_FRONTEND=noninteractive

# Setup  python 3.6
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update
RUN apt-get install -y python3.6 \
        python3-tk

RUN apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Setup pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py \
 && python3.6 get-pip.py \
 && rm get-pip.py 

# set symlinks to python3.6
RUN rm /usr/bin/python3
RUN ln -s /usr/bin/python3.6 /usr/bin/python
RUN ln -s /usr/bin/python3.6 /usr/bin/python3

# setup pip packages
RUN python -m pip install --no-cache-dir -U ipython pip setuptools
RUN python -m pip install --no-cache-dir tensorflow-gpu==1.9.0
ENV PYTHON_PACKAGES="\
    matplotlib \
    keras==2.2.4 \
    keras-rl \
    h5py \
    gym \
    tqdm \
"
RUN pip install --upgrade pip
RUN pip install --no-cache-dir $PYTHON_PACKAGES

# setup user
ARG build_uid=1000
ARG build_gid=1000
ARG build_username=devuser
RUN groupadd -g ${build_gid} ${build_username} && \
  useradd -m -u ${build_uid} -g ${build_gid} ${build_username}
RUN usermod -a -G sudo ${build_username}
RUN echo "${build_username} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# set user for container login
USER ${build_username}
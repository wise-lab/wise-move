from collections import OrderedDict

from .scanner import Scanner
from .parser import Parser, Errors
from .atomic_propositions import AtomicPropositions


class PropertyObj(object):
    def __init__(self, aps_keys):
        self.aps_keys = aps_keys

    @property
    def done(self): raise NotImplementedError

    def reset(self, state): raise NotImplementedError

    def __call__(self, state): raise NotImplementedError


class LTLProperty(PropertyObj):
    """This is a base class that contains information of an LTL property.

    It encapsulates the model-checking part (see check / check_incremental), and contains additional information.
    """

    def __init__(self, x, aps_keys, mode='violation'):
        """Constructor for LTLPropertyBase. Assumes property does not change,
        but property may be applied to multiple traces.

        Args:
            x: the human readable string representation of the LTL property
            aps_keys: a tuple defining the strings of atomic propositions
            mode: the mode (either 'satisfaction' or 'violation')
        """

        super().__init__(aps_keys)

        assert mode in ('violation', 'satisfaction')
        assert type(x) == str

        #: This property's checker virtual machine
        self.parser = Parser()

        self.x = x
        self.__mode = mode

        #: initialise the Errors class to give meaningful messages when calling parser.SetProperty
        Errors.Init(self.x, "", False, self.parser.getParsingPos, self.parser.errorMessages)

        self.parser.APdict = AtomicPropositions.create_hash_map(aps_keys)
        self.parser.SetProperty(Scanner(self.x))

        #: the result of the model checking (TRUE, UNDECIDED, or FALSE) --
        #  see 'check' method below.
        self.__mc_status = Parser.UNDECIDED
        self.__mc_done_status = {'violation': Parser.FALSE, 'satisfaction': Parser.TRUE}[self.__mode]

    @property
    def mode(self): return self.__mode

    @property
    def mc_status(self): return self.__mc_status

    @property
    def done(self): return self.__mc_status == self.__mc_done_status

    def reset(self, state):
        """Resets existing property so that it can be applied to a new sequence of states.

        Args: state: the new initial state to begin
        """
        self.parser.ResetProperty()
        self.__mc_status = Parser.UNDECIDED
        return self.__call__(state)

    def check(self, trace):
        """Checks the LTL property w.r.t. the given trace.
        Notice that the parser is reset whenever this 'check' is called (see self.parser.Check).

        Args:
            trace: a sequence of states

        Returns:
            result w.r.t. entire trace, in {TRUE, FALSE, UNDECIDED}
        """
        self.__mc_status = self.parser.Check(trace)
        return self.done

    def check_incremental(self, state):
        """Checks an initialised property w.r.t. the next state in a trace.

        Args:
            state: next state (an integer)

        Returns:
            incremental result, in {TRUE, FALSE, UNDECIDED}
        """

        if state is not None:
            if not isinstance(state, int):
                import warnings
                state = int(state)
                warnings.warn('the state is not an integer-type: forced conversion to int.')

            if not self.done:
                self.__mc_status = self.parser.CheckIncremental(state)
        else:
            import warnings
            warnings.warn('the state is None: no operation done within LTLProperty.check_incremental.')

        return self.done

    def __call__(self, state): return self.check_incremental(state)


class LTLProperties(PropertyObj):

    def __init__(self, aps_keys):
        super().__init__(aps_keys)
        self.__ltls = OrderedDict()

    @property
    def aliases(self): return self.__ltls.keys()

    def __len__(self): return len(self.__ltls)

    def add(self, alias, x, mode='violation'):
        """
        adds an LTL property into the class (specifically, its internal dict.self.ltls).
        If the alias exists in the keys, overwrite the value self.ltls[alias], with a warning message.

        Args:
            alias: an alias of the LTL property.
            x: the human readable string representation of the LTL property
            mode: the mode (either 'satisfaction' or 'violation')
        """

        if alias in self.aliases:
            import warnings
            warnings.warn(f'The LTL alias {alias} already exists; the corresponding LTL has been overwritten.')

        self.__ltls[alias] = LTLProperty(x=x, aps_keys=self.aps_keys, mode=mode)

    @property
    def done(self):
        return any(ltl.done for ltl in self.__ltls.values())

    def __getitem__(self, alias):
        assert alias in self.aliases
        return self.__ltls[alias]

    def __setitem__(self, alias, value):
        assert isinstance(value, LTLProperty)
        self.__ltls[alias] = value      # working like a normal dictionary

    def reset(self, state):
        return any(ltl.reset(state) for ltl in self.__ltls.values())

    def __call__(self, state):
        return any(ltl(state) for ltl in self.__ltls.values())

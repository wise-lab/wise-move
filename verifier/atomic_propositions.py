from utilities import Bits


class AtomicPropositions(Bits):
    """An AP-control class for AP-wise manipulation.

    """

    @staticmethod
    def create_hash_map(keys):
        return dict(zip(keys, range(len(keys))))

    def __init__(self, keys):
        Bits.__init__(self)
        assert isinstance(keys, (tuple, list))

        self.nb_keys = len(keys)
        self.hash_map = AtomicPropositions.create_hash_map(keys)

    def __setitem__(self, index, value):
        if index in self.hash_map:
            index = self.hash_map[index]
        assert 0 <= index <= self.nb_keys
        Bits.__setitem__(self, index, value)

    def __getitem__(self, index):
        if index in self.hash_map:
            index = self.hash_map[index]
        assert 0 <= index <= self.nb_keys
        return Bits.__getitem__(self, index)

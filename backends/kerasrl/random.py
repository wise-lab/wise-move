from rl.random import AnnealedGaussianProcess, RandomProcess
import numpy as np


# Based on the third method in
# http://math.stackexchange.com/questions/1287634/implementing-ornstein-uhlenbeck-in-matlab
class StationaryOUProcess(AnnealedGaussianProcess):
    def __init__(self, dt, kappa, sigma, sigma_min=None, n_steps_annealing=1000):
        super().__init__(mu=0, sigma=sigma, sigma_min=sigma_min, n_steps_annealing=n_steps_annealing)
        self.dt = dt
        self.kappa = kappa
        self.A = np.exp(-kappa * dt)
        self.normal_sigma = np.sqrt(np.exp(2 * kappa * dt) - 1)
        self.reset_states()

    def sample(self):
        x = self.A * self.x_prev + self.A * self.current_sigma * self.normal_sigma * np.random.normal()
        self.x_prev = x
        self.n_steps += 1
        return x

    def reset_states(self):
        self.x_prev = self.sigma * np.random.normal()
        self.n_steps = 0


class ZeroMeanMultiOUProcess(RandomProcess):
    def __init__(self, dt=1e-1):
        super().__init__()
        self.dt = dt
        self.size = 0
        self.stationary_ou_processes = list()

    def append(self, kappa, sigma, sigma_min=None, n_steps_annealing=1000):
        self.stationary_ou_processes.append(StationaryOUProcess(self.dt, kappa, sigma, sigma_min, n_steps_annealing))
        self.size += 1

    def sample(self):
        x = list()
        for process in self.stationary_ou_processes:
            x.append(process.sample())
        return np.array(x)

    def reset_states(self):
        for process in self.stationary_ou_processes:
            process.reset_states()

    @property
    def n_steps(self):
        assert self.size > 0
        n_steps = 0
        for process in self.stationary_ou_processes:
            n_steps += process.n_steps
        return n_steps / self.size

    @property
    def x_prev(self):
        x_prev = list()
        for process in self.stationary_ou_processes:
            x_prev.append(process.x_prev)
        return np.array(x_prev)

import numpy as np
from rl.policy import GreedyQPolicy, EpsGreedyQPolicy


class RestrictedEpsGreedyQPolicy(EpsGreedyQPolicy):
    """Implement the epsilon greedy policy

    Restricted Eps Greedy policy.
    This policy ensures that it never chooses the action whose value is -inf

    """

    def __init__(self, eps=.1):
        super(RestrictedEpsGreedyQPolicy, self).__init__(eps)

    def select_action(self, q_values):
        """Return the selected action

        # Arguments
            q_values (np.ndarray): List of the estimations of Q for each action

        # Returns
            Selection action
        """
        assert q_values.ndim == 1
        nb_actions = q_values.shape[0]
        index = list()

        for i in range(0, nb_actions):
            if q_values[i] != -np.inf:
                index.append(i)

        # every q_value is -np.inf (this sometimes inevitably happens within the fit and test functions
        # of kerasrl at the terminal stage as they force to call forward in Kerasrl-learner which calls this function.
        # TODO: exception process or some more process to choose action in this exceptional case.
        if len(index) < 1:
            # every q_value is -np.inf, we choose action = 0
            action = 0
            print("Warning: no action satisfies initiation condition, action = 0 is chosen by default.")

        elif np.random.uniform() <= self.eps:
            action = index[np.random.random_integers(0, len(index) - 1)]

        else:
            action = np.argmax(q_values)

        return action


class RestrictedGreedyQPolicy(GreedyQPolicy):
    """Implement the greedy policy

    Restricted Greedy policy.
    This policy ensures that it never chooses the action whose value is -inf

    """

    def select_action(self, q_values):
        """Return the selected action

        # Arguments
            q_values (np.ndarray): List of the estimations of Q for each action

        # Returns
            Selection action
        """
        assert q_values.ndim == 1

        # TODO: exception process or some more process to choose action in this exceptional case.
        if np.max(q_values) == - np.inf:
            # every q_value is -np.inf, we choose action = 0
            action = 0
            print("Warning: no action satisfies initiation condition, action = 0 is chosen by default.")

        else:
            action = np.argmax(q_values)
        return action

import numpy as np

from rl.agents import DDPGAgent, DQNAgent


class DQNAgentOverOptions(DQNAgent):
    def __init__(self,
                 model,
                 low_level_policies,
                 policy=None,
                 test_policy=None,
                 enable_double_dqn=True,
                 enable_dueling_network=False,
                 dueling_type='avg',
                 *args,
                 **kwargs):
        super(DQNAgentOverOptions, self).__init__(
            model, policy, test_policy, enable_double_dqn,
            enable_dueling_network, dueling_type, *args, **kwargs)

        # TODO: Rename `low_level_policies` just to `policies`.
        self.low_level_policies = low_level_policies
        if low_level_policies is not None:
            self.low_level_policy_aliases = list(
                self.low_level_policies.keys())

    def __get_invalid_node_indices(self):
        """Returns a list of option indices that are invalid according to
        initiation conditions."""

        invalid_node_indices = list()
        for index, option_alias in enumerate(self.low_level_policy_aliases):
            # TODO: Locate reset to another place as this is a "get" function.
            self.low_level_policies[option_alias].reset()
            if not self.low_level_policies[option_alias].initiation_condition:
                invalid_node_indices.append(index)

        return invalid_node_indices

    def forward(self, observation):
        q_values = self.get_modified_q_values(observation)

        if self.training:
            action = self.policy.select_action(q_values=q_values)
        else:
            action = self.test_policy.select_action(q_values=q_values)

        # Book-keeping.
        self.recent_observation = observation
        self.recent_action = action

        # print('forward gives %s from %s' % (action, dict(zip(self.low_level_policy_aliases, q_values))))
        return action

    def get_modified_q_values(self, observation):
        state = self.memory.get_recent_state(observation)
        q_values = self.compute_q_values(state)

        if self.low_level_policies is not None:
            invalid_node_indices = self.__get_invalid_node_indices()

            for node_index in invalid_node_indices:
                q_values[node_index] = -np.inf

        return q_values


import numpy as np

from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Flatten, Input, Concatenate, Lambda
from keras.optimizers import Adam

from .agents import DDPGAgent, DQNAgentOverOptions
from .policy import RestrictedGreedyQPolicy, RestrictedEpsGreedyQPolicy
from .memory import SequentialMemory
from .callbacks import TensorBoard, ModelIntervalCheckpoint, ModelIntervalSavepoint, OptionsDiscounter

from ..learner import Learner


class DDPGLearner(Learner):
    def __init__(self, input_shape, nb_actions, **kwargs):
        """The constructor which sets the properties of the class.

        Args:
            input_shape: Shape of observation space, e.g (10,);
            nb_actions: number of values in action space;
            **kwargs: other optional key-value arguments with defaults defined in property_defaults
        """
        super(DDPGLearner, self).__init__(input_shape, nb_actions, **kwargs)
        property_defaults = {
            "mem_size": 100000,  # size of memory
            "mem_window_length": 1,  # window length of memory
            "nb_steps_warmup_critic": 100,  # steps for critic to warmup
            "nb_steps_warmup_actor": 100,  # steps for actor to warmup
            "critic_lr": 1e-4,  # critic's learning rate
            "actor_lr": 1e-5,   # actor's learning rate
            "target_model_update": 1e-3  # target model update frequency
        }

        for (prop, default) in property_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.agent_model = self._create_agent()

    def _create_agent(self):
        """Creates a KerasRL DDPGAgent with given components.

        Returns: KerasRL DDPGAgent object
        """

        actor = self.actor_model
        critic, critic_action_input = self.critic_model

        agent = DDPGAgent(nb_actions=self.nb_actions, actor=actor, critic=critic,
                          critic_action_input=critic_action_input, memory=self.memory,
                          nb_steps_warmup_critic=self.nb_steps_warmup_critic,
                          nb_steps_warmup_actor=self.nb_steps_warmup_actor,
                          random_process=self.random_process, gamma=self.gamma,
                          target_model_update=self.target_model_update)

        agent.compile([Adam(lr=self.actor_lr, clipnorm=1.), Adam(lr=self.critic_lr, clipnorm=1.)], metrics=['mae'])

        return agent

    @property
    def actor_model(self):
        """ Creates the actor model (Keras Model of actor which takes observation as input and outputs actions).
            This needs to be implemented in the subclass.

        Returns: Keras Model object of actor
        """
        raise NotImplementedError('actor_model in DDPGLearner has to be specified in the subclass')

    @property
    def critic_model(self):
        """Creates the critic model. This needs to be implemented in the subclass.

        Returns: a tuple (critic, critic_action_input), where
            critic: Keras Model of critic that takes concatenation of observation and action and outputs
                    a single value
            critic_action_input: Keras Input which was used in creating action input of the critic model.
        """
        raise NotImplementedError('critic_model in DDPGLearner has to be specified in the subclass')

    @property
    def random_process(self):
        """Creates the random process model to be used for exploration. This needs to be implemented in the subclass.

        Returns: for example, KerasRL OrnsteinUhlenbeckProcess object
        """
        raise NotImplementedError('random_process in DDPGLearner has to be specified in the subclass')

    @property
    def memory(self):
        """Creates the memory model.

        Returns: KerasRL SequentialMemory object
        """
        return SequentialMemory(limit=self.mem_size, window_length=self.mem_window_length)

    def train(self, env, nb_steps, visualize=False, nb_max_episode_steps=np.inf,
              verbose=1, log_interval=10000, tensorboard=False,
              model_savepoints=False, savepoints_label='', savepoints_interval=100000):

        callbacks = []

        if model_savepoints:
            import os
            filepath = os.path.join(os.getcwd(), 'savepoints/')
            if 'savepoints' not in os.listdir(os.getcwd()):
                os.mkdir(filepath)
            filepath = filepath + f'{savepoints_label}' + '_weights_{step}.h5f'
            callbacks += [ModelIntervalSavepoint(filepath, interval=savepoints_interval)]

        if tensorboard:
            callbacks += [TensorBoard(log_dir='./logs')]

        self.agent_model.fit(env, nb_steps=nb_steps, visualize=visualize, nb_max_episode_steps=nb_max_episode_steps,
                             verbose=verbose, log_interval=log_interval, callbacks=callbacks)

    def save_model(self, file_name, overwrite=True):
        self.agent_model.save_weights(file_name, overwrite=overwrite)

    def test_model(self, env, nb_episodes, visualize=True, nb_max_episode_steps=np.infty, callbacks=None):
        self.agent_model.test(env, nb_episodes=nb_episodes, visualize=visualize,
                              nb_max_episode_steps=nb_max_episode_steps, callbacks=callbacks)

    def load_model(self, file_name):
        self.agent_model.load_weights(file_name)

    def predict(self, observation):
        return self.agent_model.forward(observation)


class DQNLearner(Learner):
    def __init__(self,
                 input_shape=(48, ),
                 nb_actions=5,
                 low_level_policies=None,
                 model=None,
                 policy=None,
                 memory=None,
                 test_policy=None,
                 **kwargs):
        """The constructor which sets the properties of the class.

        Args:
            input_shape: Shape of observation space, e.g (10,);
            nb_actions: number of values in action space;
            model: Keras Model of actor which takes observation as input and outputs actions. Uses default if not given
            policy: KerasRL Policy. Uses default RestrictedEpsGreedyQPolicy if not given
            memory: KerasRL Memory. Uses default SequentialMemory if not given
            **kwargs: other optional key-value arguments with defaults defined in property_defaults
        """
        super(DQNLearner, self).__init__(input_shape, nb_actions, **kwargs)
        property_defaults = {
            "mem_size": 100000,  # size of memory
            "mem_window_length": 1,  # window length of memory
            "target_model_update": 1e-3,  # target model update frequency
            "nb_steps_warmup": 100,  # steps for model to warmup
            "lr": 1e-3               # learning rate
        }

        for (prop, default) in property_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        if model is None:
            model = self.get_default_model()
        if policy is None:
            policy = self.get_default_policy()
        if test_policy is None:
            test_policy = self.get_default_test_policy()
        if memory is None:
            memory = self.get_default_memory()

        self.low_level_policies = low_level_policies

        self.agent_model = self.create_agent(model, policy, memory, test_policy)

    def get_default_model(self):
        """Creates the default model.

        Returns:     Keras Model object of actor
        """

        model = Sequential()
        model.add(Flatten(input_shape=(1, ) + self.input_shape))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(64, activation='tanh'))
        model.add(Dense(self.nb_actions))
        print(model.summary())

        return model

    def get_default_policy(self):
        return RestrictedEpsGreedyQPolicy(0.3)

    def get_default_test_policy(self):
        return RestrictedGreedyQPolicy()

    def get_default_memory(self):
        """Creates the default memory model.

        Returns:     KerasRL SequentialMemory object
        """
        memory = SequentialMemory(
            limit=self.mem_size, window_length=self.mem_window_length)
        return memory

    def create_agent(self, model, policy, memory, test_policy):
        """Creates a KerasRL DDPGAgent with given components.

        Args:
            model: Keras Model of model which takes observation as input and outputs discrete actions.
            memory: KerasRL Memory.

        Returns:
            KerasRL DQN object
        """
        agent = DQNAgentOverOptions(
            model=model,
            low_level_policies=self.low_level_policies,
            nb_actions=self.nb_actions,
            memory=memory,
            nb_steps_warmup=self.nb_steps_warmup,
            target_model_update=self.target_model_update,
            policy=policy,
            gamma=self.gamma,
            test_policy=test_policy,
            enable_dueling_network=True)

        agent.compile(Adam(lr=self.lr), metrics=['mae'])

        return agent

    def train(self,
              env,
              nb_steps=1000000,
              visualize=False,
              verbose=1,
              log_interval=10000,
              nb_max_episode_steps=200,
              tensorboard=False,
              model_checkpoints=False,
              checkpoint_interval=10000):

        callbacks = [OptionsDiscounter(self.agent_model, env, self.gamma)]

        if model_checkpoints:
            callbacks += [
                ModelIntervalCheckpoint(
                    './checkpoints/checkpoint_weights.h5f',
                    interval=checkpoint_interval)
            ]
        if tensorboard:
            callbacks += [TensorBoard(log_dir='./logs')]

        self.agent_model.fit(
            env,
            nb_steps=nb_steps,
            visualize=visualize,
            verbose=verbose,
            log_interval=log_interval,
            nb_max_episode_steps=nb_max_episode_steps,
            callbacks=callbacks)

    def save_model(self, file_name="test_weights.h5f", overwrite=True):
        self.agent_model.save_weights(file_name, overwrite=True)

    # TODO: very environment specific. Make it general
    def test_model(self,
                   env,
                   nb_episodes=5,
                   visualize=True,
                   nb_max_episode_steps=400,
                   success_reward_threshold=100):

        print("Testing for {} episodes".format(nb_episodes))
        success_count = 0
        termination_reasons_counter = {}
        atomic_termination_reasons_counter = {}

        def increase_termination_reason_counter(reason, reason_counter):
            if reason in reason_counter:
                reason_counter[reason] += 1
            else:
                reason_counter[reason] = 1

        for n in range(nb_episodes):
            env.reset()
            env.controller.do_transition()
            terminal = False
            step = 0
            episode_reward = 0
            while not terminal and step <= nb_max_episode_steps:
                if visualize:
                    env.render()
                features, R, terminal, info = env.execute_controller_policy()
                step += 1
                episode_reward += R
                if terminal:
                    if 'episode_termination_reasons' in info:
                        reasons = info['episode_termination_reasons']
                        increase_termination_reason_counter(reasons, termination_reasons_counter)

                        for reason in reasons.split(','):
                            increase_termination_reason_counter(reason, atomic_termination_reasons_counter)

                    if env.env.termination_events['goal achieved'].condition():
                        success_count += 1
                    print(f'Episode {n + 1}: steps:{step}, reward:{episode_reward:.2f}')

        print("\nPolicy succeeded {} times!".format(success_count))

        if termination_reasons_counter != atomic_termination_reasons_counter:
            print("\nTermination reason(s)")
            print("\t(episodic): " + str(termination_reasons_counter))
            print("\t(atomic): " + str(atomic_termination_reasons_counter))
        else:
            print("\nTermination reason(s): " + str(atomic_termination_reasons_counter))

        return success_count, termination_reasons_counter, atomic_termination_reasons_counter

    def load_model(self, file_name="test_weights.h5f"):
        self.agent_model.load_weights(file_name)

    def predict(self, observation):
        return self.agent_model.forward(observation)

    def get_q_value(self, observation, action):
        return self.agent_model.get_modified_q_values(observation)[action]

    def get_q_value_using_option_alias(self, observation, option_alias):
        action_num = self.agent_model.low_level_policy_aliases.index(
            option_alias)
        return self.agent_model.get_modified_q_values(observation)[action_num]

    def get_softq_value_using_option_alias(self, observation, option_alias):
        action_num = self.agent_model.low_level_policy_aliases.index(
            option_alias)
        q_values = self.agent_model.get_modified_q_values(observation)
        # print('softq q_values are %s' % dict(zip(self.agent_model.low_level_policy_aliases, q_values)))
        # oq_values = copy.copy(q_values)
        if q_values[action_num] == -np.inf:
            return 0
        max_q_value = np.max(q_values)
        q_values = [np.exp(q_value - max_q_value) for q_value in q_values]
        relevant = q_values[action_num] / np.sum(q_values)
        # print('softq: %s -> %s' % (oq_values, relevant))
        return relevant

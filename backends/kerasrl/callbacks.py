import numpy as np

from keras.callbacks import TensorBoard
from rl.callbacks import Callback, ModelIntervalCheckpoint

from math import log, floor


class OptionsDiscounter(Callback):
    def __init__(self, agent, env, gamma):
        self.agent = agent
        self.env = env
        self.gamma = gamma

    def on_action_end(self, option, logs={}):
        self.agent.gamma = self.gamma ** self.env.current_node.time.num_steps


class ModelIntervalSavepoint(ModelIntervalCheckpoint):

    def __init__(self, filepath, interval, verbose=0):
        super(ModelIntervalSavepoint, self).__init__(filepath, interval, verbose)

    def on_train_begin(self, logs={}):
        self.save_model(logs)

    def on_step_end(self, step, logs={}):
        """ Save weights at interval steps during training """
        self.total_steps += 1
        if self.total_steps % self.interval == 0:
            self.save_model(logs)

    def save_model(self, logs={}):
        filepath = self.filepath.format(step=str(int(self.total_steps / 1000)) + 'k', **logs)
        if self.verbose > 0:
            print('Step {}: saving model to {}'.format(self.total_steps, filepath))
        self.model.save_weights(filepath, overwrite=True)


class EpisodicDataCollector(Callback):

    def __init__(self, env, nb_episodes):
        super().__init__()

        #: the statistics of the test result, of the form (episode, avg, std, min, max)
        self.nb_episodes = nb_episodes
        self.episode_rewards = np.zeros(nb_episodes)
        self.step_rewards = np.zeros(nb_episodes)
        self.termination_rewards = np.zeros(nb_episodes)
        self.nb_steps = np.zeros(nb_episodes)
        self.env = env

    def on_episode_end(self, episode, logs={}):
        self.episode_rewards[episode] = logs['episode_reward']
        self.termination_rewards[episode] = self.env.terminal_reward()
        self.step_rewards[episode] = logs['episode_reward'] - self.termination_rewards[episode]
        self.nb_steps[episode] = logs['nb_steps']

    def on_train_end(self, logs=None):
        print("\nThe statistics:")
        print(f"\n\tEpisodic reward (mean, std, min, max): {self.reward_statistics}")
        print(f"\n\tThe number of Steps (mean, std, min, max): {self.nb_steps_statistics}")

    @staticmethod
    def statistics(data):
        avg = np.average(data)
        std = np.std(data)
        return [avg, std, np.min(data), np.max(data)]

    @property
    def reward_statistics(self): return self.statistics(self.episode_rewards)

    @property
    def nb_steps_statistics(self): return self.statistics(self.nb_steps)

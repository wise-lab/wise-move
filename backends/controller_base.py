
class ControllerBase(object):
    """Abstract class for controllers."""

    def __init__(self, env, low_level_policies, start_node_alias):
        self.env = env
        self.low_level_policies = low_level_policies

        # TODO: Move an intermediate class so that base class can be clean
        self.current_node = None if start_node_alias is None else self.low_level_policies[start_node_alias]
        self.node_terminal_state_reached = False
        self.controller_args_defaults = {}

    def set_controller_args(self, **kwargs):
        for (prop, default) in self.controller_args_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

    def can_transition(self):
        """Returns boolean signifying whether we can transition.

        To be implemented in subclass.
        """

        raise NotImplementedError(self.__class__.__name__ + \
                                  "can_transition is not implemented.")

    def do_transition(self, observation):
        """Do a transition, assuming we can transition. To be implemented in
        subclass.

        Args:
            observation: final observation from episodic step
        """

        raise NotImplementedError(self.__class__.__name__ + \
                                  "do_transition is not implemented.")

    def set_current_node(self, node_alias):
        """Sets the current node which is being executed.

        Args:
            node: node alias of the node to be set
        """
        raise NotImplementedError(self.__class__.__name__ + \
                                  "set_current_node is not implemented.")

    # TODO: Looks generic. Move to an intermediate class/highlevel manager so that base class can be clean
    ''' Executes the current node until node termination condition is reached
    
    Returns state at end of node execution, total reward, epsiode_termination_flag, info
    '''
    # TODO: this is never called when you TEST high-level policy (w/o MCTS) rather than train...
    # (make some integrated interface btw testing and training and b.t.w. the high- and low-level
    # methods with and without MCTS.
    def step_current_node(self, visualize_low_level_steps=False):
        total_reward = 0
        discount_rate = 1

        self.node_terminal_state_reached = False
        self.current_node.reset()
        while not self.node_terminal_state_reached:
            observation, reward, terminal, info = self.low_level_step_current_node()
            if visualize_low_level_steps:
                self.env.render()
            # TODO: make the discount factor as a parameter.
            total_reward += discount_rate * reward
            discount_rate *= 0.9985

        total_reward += self.current_node.high_level_extra_reward

        # import time
        # if (self.env.step_count > self.env.startup_delay):
        #     time.sleep(1)

        # TODO for info
        return observation, total_reward, terminal, info

    # TODO: Looks generic. Move to an intermediate class/highlevel manager so that base class can be clean
    ''' Executes one step of current node. Sets node_terminal_state_reached flag if node termination condition
    has been reached. 

    Returns state after one step, step reward, episode_termination_flag, info
    '''

    def low_level_step_current_node(self):
        u_ego = self.current_node.policy(self.current_node.get_reduced_features_tuple())
        feature, R, terminal, info = self.current_node.step(u_ego)
        self.node_terminal_state_reached = terminal
        return self.env.get_features_tuple(), R, self.env.termination_condition(), info

    def execute_and_get_terminal_reward(self, visualize_low_level_steps=False):
        self.node_terminal_state_reached = False
        terminal = False
        while not terminal:
            observation, reward, terminal, info = self.step_current_node(
                visualize_low_level_steps=visualize_low_level_steps)

        # TODO for info
        return observation, reward, terminal, info

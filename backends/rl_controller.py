from .controller_base import ControllerBase


class RLController(ControllerBase):
    """RL controller using a trained policy."""

    def __init__(self, env, low_level_policies, start_node_alias):
        """Constructor for manual policy execution.

        Args:
            env: env instance
            low_level_policies: low level policies dictionary
        """
        super(RLController, self).__init__(env, low_level_policies,
                                           start_node_alias)
        self.low_level_policy_aliases = list(self.low_level_policies.keys())
        self.trained_policy = None
        self.node_terminal_state_reached = False

    # TODO: move this to controller_base?
    def set_current_node(self, node_alias):
        if node_alias is None:
            self.current_node = None
        else:
            self.current_node = self.low_level_policies[node_alias]
            self.env.set_ego_info_text(node_alias)

    def set_trained_policy(self, policy):
        self.trained_policy = policy

    def can_transition(self):
        return self.node_terminal_state_reached

    def do_transition(self):
        if self.trained_policy is None:
            raise Exception(self.__class__.__name__ + \
                                 "trained_policy is not set. Use set_trained_policy().")
        node_index_after_transition = self.trained_policy(
            self.env.get_features_tuple())
        self.set_current_node(
            self.low_level_policy_aliases[node_index_after_transition])
        self.node_terminal_state_reached = False

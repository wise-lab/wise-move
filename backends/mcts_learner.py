from .controller_base import ControllerBase
import numpy as np
import pickle

# TODO: Clean up debug comments

class Node:
    """Represents a node in a tree."""

    def __init__(self, node_num):
        """Initialize a node.

        Args:
            node_num: the unique number of a node in the tree
        """

        self.num = node_num
        self.state = ''
        self.cstate = None
        self.edges = {}
        self.parent_num = None
        self.N = 0
        self.Q = 0
        self.depth = None

    def is_terminal(self):
        """Check whether this node is a leaf node or not."""

        return self.edges == {}

class Tree:
    """Tree representation used for MCTS."""

    def __init__(self, max_depth):
        """Constructor for Tree.

        Args:
            max_depth: max possible distance between the root node
                and any leaf node.
        """

        self.nodes = {}
        self.new_node_num = 0
        self.root = self._create_node()
        self.root_node_num = 0
        self.root.depth = 0
        self.curr_node_num = 0
        self.max_depth = max_depth
        self.latest_obs = None
        self.latest_dis_obs = None

    def _create_node(self):
        """Internal function to create a node, without edges."""

        created_node = Node(self.new_node_num)
        self.nodes[self.new_node_num] = created_node
        self.new_node_num += 1
        return created_node

    def new_node(self, option_alias):
        """Creates a new node that is a child of the current node.
        The new node can be reached by the option_alias edge from
        the current node.

        Args:
            option_alias: the edge between current node and new node
        """

        created_node = self._create_node()
        self.nodes[self.curr_node_num].edges[option_alias] = created_node.num
        created_node.parent_num = self.curr_node_num
        created_node.depth = self.nodes[self.curr_node_num].depth+1

    def move(self, option_alias):
        """Use the edge option_alias and move from current node to
        a next node.

        Args:
            option_alias: edge to move along
        """

        possible_edges = self.nodes[self.curr_node_num].edges
        assert(option_alias in possible_edges.keys())
        self.curr_node_num = possible_edges[option_alias]

    def add_state(self, obs, dis_obs):
        """Associates observation and discrete observation to the current
        node. Useful to keep track of the last known observation(s).

        Args:
            obs: observation to save to current node's cstate
            dis_obs: observation to save to current node's state
        """

        if self.nodes[self.curr_node_num].state == '':
            self.nodes[self.curr_node_num].state = dis_obs
            self.nodes[self.curr_node_num].cstate = obs
        # assert(self.nodes[self.curr_node_num].state == dis_obs)
        # assert on the continuous obs?
        self.latest_obs = obs
        self.latest_dis_obs = dis_obs

    def reconstruct(self, option_alias):
        """Use the option_alias from the root node and reposition the tree
        such that the new root node is the node reached by following option_alias
        from the current root node.

        Args:
            option_alias: edge to follow from the root node
        """
        new_root_num = self.nodes[self.root_node_num].edges[option_alias]
        self.root_node_num = new_root_num
        self.root = self.nodes[self.root_node_num]
        self.curr_node_num = self.root_node_num

class MCTSLearner(ControllerBase):
    """MCTS Logic."""

    def __init__(self, env, low_level_policies, max_depth=10, debug=False,
        rollout_timeout=500):
        """Constructor for MCTSLearner.

        Args:
            env: environment
            low_level_policies: given low level maneuvers
            max_depth: the tree's max depth
            debug: whether or not to print debug statements
            rollout_timeout: timeout for the rollout
        """

        self.env = env # super?
        self.low_level_policies = low_level_policies # super?
        self.controller_args_defaults = {"predictor": None}
        self.tree = Tree(max_depth=max_depth)
        self.debug = debug
        self.rollout_timeout = rollout_timeout

    def reset(self):
        """Resets maneuvers and sets current node to root."""

        all_options = set(self.low_level_policies.keys())
        for option_alias in all_options:
            self.low_level_policies[option_alias].reset()
        self.tree.curr_node_num = self.tree.root_node_num

    def _get_possible_options(self):
        """Return all option_alias whose init condition is satisfied from
        the current node."""

        all_options = set(self.low_level_policies.keys())
        filtered_options = set()
        for option_alias in all_options:
            self.low_level_policies[option_alias].reset()
            if self.low_level_policies[option_alias].initiation_condition:
                filtered_options.add(option_alias)
        return filtered_options

    def _get_sorted_possible_options(self, node):
        """Return all option_alias whose init condition is satisfied from
        the given node. The options are returned in decreasing order of
        preference, which is determined from the q values given by the
        predictor.

        Args:
            node: the node from which the possible options are to be found
        """

        possible_options = self._get_possible_options()
        q_values = {}
        for option_alias in possible_options:
            q_values[option_alias] = self.predictor(node.cstate, option_alias)
        # print('to sort %s' % q_values)
        sorted_list = sorted(possible_options, key=lambda z: q_values[z])
        # print('sorted: %s' % sorted_list)
        return sorted_list[::-1]

    def _to_discrete(self, observation):
        """Converts a given observation to discrete form. Since the
        discrete features don't capture all the information in the
        environment, two additional features are added.

        Args:
            observation: observation to discretize

        Returns a string of 0s and 1s
        """

        dis_observation = ''
        for item in observation[12:20]:
            if type(item) == bool:
                dis_observation += '1' if item is True else '0'
            if type(item) == int and item in [0, 1]:
                dis_observation += str(item)
        assert(len(dis_observation) == 8)

        # Are we following a vehicle?
        target_veh_i, V2V_dist = self.env.get_v2v_distance()
        if target_veh_i is not None and V2V_dist <= 30:
            dis_observation += '1'
        else:
            dis_observation += '0'

        # Is there a vehicle in the opposite lane in approximately the
        # same x position?
        delta_x = 15
        ego = self.env.vehs[0]
        possible_collision = '0'
        for veh in self.env.vehs[1:]:
            abs_delta_x = abs(ego.x - veh.x)
            opp_lane = ego.APs['lane'] != veh.APs['lane']
            is_horizontal = veh.APs['on_route']
            if abs_delta_x <= delta_x and opp_lane and is_horizontal:
                possible_collision = '1'
                break
        dis_observation += possible_collision

        return dis_observation

    def move(self, option_alias):
        """Move in the MCTS tree. This means moving in the tree, updating
        state information and stepping through option_alias.

        Args:
            option_alias: edge or option to execute to reach a next node
        """

        # take the edge option_alias and move to a new node
        self.set_current_node(option_alias)
        pre_x = self.env.vehs[0].x
        next_obs, eps_R = self.option_step()
        post_x = self.env.vehs[0].x
        dis_obs = self._to_discrete(next_obs)
        self.tree.move(option_alias)
        self.tree.add_state(next_obs, dis_obs)
        # print('moved %s: %f-> %f' % (option_alias, pre_x, post_x))
        if eps_R == None:
            return 0
        return eps_R

    def search(self, obs):
        """Perform a traversal from the root node.

        Args:
            obs: current observation
        """

        success = 0
        self.reset() # reset tree and get to root
        dis_obs = self._to_discrete(obs)
        self.tree.add_state(obs, dis_obs)
        reached_leaf = self.tree_policy() # until we reach a leaf
        # print('Reached depth %d' % self.tree.nodes[self.tree.curr_node_num].depth, end=' ')
        # print('at node: %d, reached leaf: %s, terminated: %s' % (self.tree.curr_node_num, reached_leaf, self.env.termination_condition()))
        if reached_leaf:
            rollout_reward, timed_out = self.def_policy() # from leaf node
            if rollout_reward > 0:
                self.backup(1.0) # from leaf node
                success = 1
            elif rollout_reward < -150 or timed_out:
                # TODO: -150 is arbitrary. It should be set from outside or
                # provided through a variable, based on the env. Same for timeout,
                # it is specific to the env. Also, for smaller timeouts it
                # may be a good idea to propagate 0 instead of -1.
                self.backup(-1.0)
            else:
                self.backup(0)
        else:
            # violation happened or we terminated before reaching a leaf (weird)
            if self.current_node.env.goal_achieved:
                self.backup(1.0)
            else:
                self.backup(-1.0)
        # p = {'overall': self.tree.root.Q * 1.0 / self.tree.root.N}
        # for edge in self.tree.root.edges.keys():
        #     next_node = self.tree.nodes[self.tree.root.edges[edge]]
        #     p[edge] = next_node.Q * 1.0 / next_node.N
        # return self.best_action(self.tree.root, 0), success, p
        return success

    def tree_policy(self):
        """Policy that determines how to move through the MCTS tree.
        Terminates either when environment reaches a terminal state, or we
        reach a leaf node."""

        while not self.env.termination_condition():

            node = self.tree.nodes[self.tree.curr_node_num]
            # Implementation IDEA: Choose the option which has the highest q first
            possible_options = self._get_sorted_possible_options(node)
            # possible_options = self._get_possible_options()
            # print('at node: %d' % node.num)
            already_visited = set(node.edges.keys())
            not_visited = []
            for item in possible_options:
                if item not in already_visited:
                    not_visited.append(item)
            # print('not visited %s' % not_visited)
            if len(not_visited) > 0 and node.depth+1-self.tree.root.depth < self.tree.max_depth and np.power(node.N, 0.5) >= len(already_visited):
                self.expand(node, not_visited)
                return True
            elif node.depth+1-self.tree.root.depth >= self.tree.max_depth:
                return True # cant go beyond, just do a rollout
            else:
                option_alias = self.best_action(node, 1)
                if self.debug:
                    print("%s" % option_alias[:2], end=',')
                eps_R = self.move(option_alias)
                if eps_R < -150: # violation
                    return False

        return False

    def expand(self, node, not_visited):
        """Create a new node from the given node. Chooses an option from the
        not_visited list. Also moves to the newly created node.

        Args:
            node: node to expand from
            not_visited: new possible edges or options
        """

        # Implementation IDEA: Choose the option which has the highest q first
        random_option_alias = list(not_visited)[0] # np.random.choice(list(not_visited))
        if self.debug:
            print("expand(%s)"%random_option_alias[:2], end=',')
        self.tree.new_node(random_option_alias)
        self.move(random_option_alias)

    def best_action(self, node, C):
        """Find the best option to execute from a given node. The constant
        C determines the coefficient of the uncertainty estimate.

        Args:
            node: node from which the best action needs to be found
            C: coefficient of uncertainty term

        Returns the best possible option alias from given node.
        """

        next_options = list(node.edges.keys())
        Q_UCB = {}
        for option_alias in next_options:
            next_node_num = node.edges[option_alias]
            next_node = self.tree.nodes[next_node_num]
            Q_UCB[option_alias] = 0
            Q_UCB[option_alias] += next_node.Q / (next_node.N + 1)
            obs = self.tree.latest_obs
            pred = self.predictor(obs, option_alias)
            if not np.isnan(pred):
                Q_UCB[option_alias] += C * pred / (next_node.N + 1)
            # print('%s => Q:%f, N+1:%f, pr: %f' % (option_alias, next_node.Q, next_node.N+1, self.predictor(obs, option_alias)))
        k, v = list(Q_UCB.keys()), list(Q_UCB.values())
        # print('Q_UCB: %s' % str(Q_UCB))
        # print('')
        return k[np.argmax(v)]

    def def_policy(self):
        """Default policy, used for rollouts."""

        rollout_reward = 0
        obs = self.tree.latest_obs
        it = 0
        while not self.env.termination_condition() and it < self.rollout_timeout:
            it += 1
            possible_options = self._get_possible_options()
            # print('possible is %s' % possible_options)
            Q_HL = {}
            for option_alias in possible_options:
                Q_HL[option_alias] = self.predictor(obs, option_alias)
            # print(Q_HL)
            k, v = list(Q_HL.keys()), list(Q_HL.values())
            chosen = k[np.argmax(v)]
            # print('chosen is %s from %s' % (chosen, Q_HL))
            # hlk = list(self.low_level_policies.keys())
            # iii = self.debug_agent.predict(obs)
            # assert(chosen == hlk[iii])
            if self.debug:
                print("D(%s)" % (chosen[:2]), end=",")
            # print()
            self.set_current_node(chosen)
            next_obs, eps_R = self.option_step()
            obs = next_obs
            if eps_R != None:
                rollout_reward += eps_R
        # print('Rollout steps = %d' % it)
        timed_out = (it < self.rollout_timeout)
        if self.debug:
            print(' <<%g>>' % rollout_reward, end='  ')
        return rollout_reward, timed_out

    def backup(self, rollout_reward):
        """Reward backup strategy.

        Args:
            rollout_reward: reward to back up
        """

        # print('Backing up %d' % rollout_reward)
        if self.debug:
            print('  === %s' % rollout_reward)
        curr_node = self.tree.nodes[self.tree.curr_node_num]
        while (curr_node is not None) and (curr_node.num != self.tree.root.parent_num):
            curr_node.N += 1
            curr_node.Q += rollout_reward
            if curr_node.parent_num is not None:
                curr_node = self.tree.nodes[curr_node.parent_num]
            else:
                curr_node = None

    def set_current_node(self, node_alias):
        """Set current node so that option_step can execute it later.

        Args:
            node_alias: option alias to execute next
        """
        self.current_node = self.low_level_policies[node_alias]
        self.current_node.reset()
        self.env.set_ego_info_text(node_alias)
        # print('set to %s' % node_alias)

    def option_step(self):
        """Step through the current option_alias."""

        total_reward = 0
        node_terminal_state_reached = False
        while not node_terminal_state_reached:

            u_ego = self.current_node.policy(
                self.current_node.get_reduced_features_tuple())
            feature, R, terminal, info = self.current_node.step(u_ego)
            node_terminal_state_reached = terminal            
            observation = self.env.get_features_tuple()
            total_reward += R

        total_reward += self.current_node.high_level_extra_reward

        # TODO for info
        return observation, total_reward

